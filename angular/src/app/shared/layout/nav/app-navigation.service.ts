import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { FeatureCheckerService } from '@abp/features/feature-checker.service';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {

    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _featureCheckerService: FeatureCheckerService,
        private _appSessionService: AppSessionService
    ) {
    }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem('Dashboard', 'Pages.Administration.Host.Dashboard', 'flaticon-line-graph', '/app/admin/hostDashboard'),
            new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'flaticon-line-graph', '/app/main/dashboard'),
            new AppMenuItem('Tenants', 'Pages.Tenants', 'flaticon-list-3', '/app/admin/tenants'),
            new AppMenuItem('Editions', 'Pages.Editions', 'flaticon-app', '/app/admin/editions'),

            new AppMenuItem('Request', '', 'flaticon-interface-8', '', [
                new AppMenuItem('Requests', 'Pages.Requests', 'fa fa-cash-register', '/app/main/requests/requests'),
                new AppMenuItem('ApproveRequests', 'Pages.ApproveRequests', 'fa fa-check', '/app/main/approveRequests/approveRequests'),
                new AppMenuItem('TreatRequests', 'Pages.TreatRequests', 'fa fa-chromecast', '/app/main/treatRequests/treatRequests'),
                //new AppMenuItem('ViewApprovedRequests', 'Pages.ViewApprovedRequests', 'fa flaticonmore', '/app/main/viewApprovedRequests/viewApprovedRequests'),
            ]),

            new AppMenuItem('Configurations', '', 'flaticon-interface-8', '', [
                new AppMenuItem('AssetTypes', 'Pages.AssetTypes', 'fa fa-circle-notch', '/app/main/assetTypes/assetTypes'),
                new AppMenuItem('Assets', 'Pages.Assets', 'fa fa-circle', '/app/main/assets/assets'),
                new AppMenuItem('RequestTypes', 'Pages.RequestTypes', 'fa fa-clipboard-list', '/app/main/requestTypes/requestTypes'),
            ]),

            new AppMenuItem('User Priviledges', '', 'flaticon-interface-8', '', [
                new AppMenuItem('UserPriviledges', 'Pages.UserPriviledges', 'fa fa-user-lock', '/app/main/userPriviledges/userPriviledges'),
            ]),

            new AppMenuItem('Responsibilities', '', 'flaticon-interface-8', '', [
                new AppMenuItem('Departments', 'Pages.Departments', 'fa fa-suitcase', '/app/main/departments/departments'),
                new AppMenuItem('Branches', 'Pages.Branches', 'fa fa-users', '/app/main/branches/branches'),
                new AppMenuItem('Applications', 'Pages.Applications', 'fa fa-stroopwafel', '/app/main/applications/applications'),
                new AppMenuItem('UserSupervisors', 'Pages.UserSupervisors', 'fa fa-users-cog', '/app/main/userSupervisors/userSupervisors'),
            ]),

            new AppMenuItem('Job Functions', '', 'flaticon-interface-8', '', [
                new AppMenuItem('JobFunctions', 'Pages.JobFunctions', 'fa fa-user-tag', '/app/main/jobFunctions/jobFunctions'),
                //new AppMenuItem('JobFunctionAssets', 'Pages.JobFunctionAssets', 'fa flaticonmore', '/app/main/jobFunctionAssets/jobFunctionAssets'),
            ]),

            new AppMenuItem('Conflict', '', 'flaticon-interface-8', '', [
                new AppMenuItem('Conflicts', 'Pages.Conflicts', 'fa fa-pied-piper-hat', '/app/main/conflicts/conflicts'),
            ]),

            new AppMenuItem('Administration', '', 'flaticon-interface-8', '', [
                new AppMenuItem('OrganizationUnits', 'Pages.Administration.OrganizationUnits', 'flaticon-map', '/app/admin/organization-units'),
                new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-suitcase', '/app/admin/roles'),
                new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-users', '/app/admin/users'),
                new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages'),
                new AppMenuItem('AuditLogs', 'Pages.Administration.AuditLogs', 'flaticon-folder-1', '/app/admin/auditLogs'),
                new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-lock', '/app/admin/maintenance'),
                new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
                new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
                new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-settings', '/app/admin/hostSettings'),
                new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-settings', '/app/admin/tenantSettings')
            ]),
            new AppMenuItem('DemoUiComponents', 'Pages.DemoUiComponents', 'flaticon-shapes', '/app/admin/demo-ui-components')
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {

        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName && this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                return this.checkChildMenuItemPermission(subMenuItem);
            } else if (!subMenuItem.permissionName) {
                return true;
            }
        }

        return false;
    }

    countApprovalRequest() {
        let num = 5;
        return num.toString();
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
            hideMenuItem = true;
        }

        if (hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }
}
