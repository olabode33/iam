import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { JobFunctionAssetBranchesServiceProxy, JobFunctionAssetBrancheDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditJobFunctionAssetBrancheModalComponent } from './create-or-edit-jobFunctionAssetBranche-modal.component';
import { ViewJobFunctionAssetBrancheModalComponent } from './view-jobFunctionAssetBranche-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './jobFunctionAssetBranches.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobFunctionAssetBranchesComponent extends AppComponentBase {

    @ViewChild('createOrEditJobFunctionAssetBrancheModal') createOrEditJobFunctionAssetBrancheModal: CreateOrEditJobFunctionAssetBrancheModalComponent;
    @ViewChild('viewJobFunctionAssetBrancheModalComponent') viewJobFunctionAssetBrancheModal: ViewJobFunctionAssetBrancheModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;
	
    advancedFiltersAreShown = false;
	filterText = '';
		jobFunctionAssetJobFunctionIdFilter = '';
		branchBranchNameFilter = '';

	
	

    constructor(
        injector: Injector,
        private _jobFunctionAssetBranchesServiceProxy: JobFunctionAssetBranchesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }
	
    getJobFunctionAssetBranches(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._jobFunctionAssetBranchesServiceProxy.getAll(
			this.filterText,
			this.jobFunctionAssetJobFunctionIdFilter,
			this.branchBranchNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobFunctionAssetBranche(): void {
        this.createOrEditJobFunctionAssetBrancheModal.show();
    }
	
    deleteJobFunctionAssetBranche(jobFunctionAssetBranche: JobFunctionAssetBrancheDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobFunctionAssetBranchesServiceProxy.delete(jobFunctionAssetBranche.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

	exportToExcel(): void {
        this._jobFunctionAssetBranchesServiceProxy.getJobFunctionAssetBranchesToExcel(
		this.filterText,
			this.jobFunctionAssetJobFunctionIdFilter,
			this.branchBranchNameFilter,
		)
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}