import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { JobFunctionAssetBranchesServiceProxy, CreateOrEditJobFunctionAssetBrancheDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobFunctionAssetLookupTableModalComponent } from '../jobFunctionAssetBranchs/jobFunctionAsset-lookup-table-modal.component';
import { BranchLookupTableModalComponent } from '../../jobFunctionAssetBranches/jobFunctionAssetBranchs/branch-lookup-table-modal.component';


@Component({
    selector: 'createOrEditJobFunctionAssetBrancheModal',
    templateUrl: './create-or-edit-jobFunctionAssetBranche-modal.component.html'
})
export class CreateOrEditJobFunctionAssetBrancheModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('jobFunctionAssetLookupTableModal') jobFunctionAssetLookupTableModal: JobFunctionAssetLookupTableModalComponent;
    @ViewChild('branchLookupTableModal') branchLookupTableModal: BranchLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobFunctionAssetBranche: CreateOrEditJobFunctionAssetBrancheDto = new CreateOrEditJobFunctionAssetBrancheDto();
    jobFunctionAssetJobFunctionId = '';
    branchBranchName = '';


    constructor(
        injector: Injector,
        private _jobFunctionAssetBranchesServiceProxy: JobFunctionAssetBranchesServiceProxy
    ) {
        super(injector);
    }

    show(jobFunctionAssetBrancheId?: number): void {
        if (!jobFunctionAssetBrancheId) {
            this.jobFunctionAssetBranche = new CreateOrEditJobFunctionAssetBrancheDto();
            this.jobFunctionAssetBranche.id = jobFunctionAssetBrancheId;
            this.jobFunctionAssetJobFunctionId = '';
            this.branchBranchName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._jobFunctionAssetBranchesServiceProxy.getJobFunctionAssetBrancheForEdit(jobFunctionAssetBrancheId).subscribe(result => {
                this.jobFunctionAssetBranche = result.jobFunctionAssetBranche;
                this.jobFunctionAssetJobFunctionId = result.jobFunctionAssetJobFunctionId;
                this.branchBranchName = result.branchBranchName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._jobFunctionAssetBranchesServiceProxy.createOrEdit(this.jobFunctionAssetBranche)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectJobFunctionAssetModal() {
        this.jobFunctionAssetLookupTableModal.id = this.jobFunctionAssetBranche.jobFunctionAssetId;
        this.jobFunctionAssetLookupTableModal.displayName = this.jobFunctionAssetJobFunctionId;
        this.jobFunctionAssetLookupTableModal.show();
    }
    openSelectBranchModal() {
        this.branchLookupTableModal.id = this.jobFunctionAssetBranche.branchId;
        this.branchLookupTableModal.displayName = this.branchBranchName;
        this.branchLookupTableModal.show();
    }


    setJobFunctionAssetIdNull() {
        this.jobFunctionAssetBranche.jobFunctionAssetId = null;
        this.jobFunctionAssetJobFunctionId = '';
    }
    setBranchIdNull() {
        this.jobFunctionAssetBranche.branchId = null;
        this.branchBranchName = '';
    }


    getNewJobFunctionAssetId() {
        this.jobFunctionAssetBranche.jobFunctionAssetId = this.jobFunctionAssetLookupTableModal.id;
        this.jobFunctionAssetJobFunctionId = this.jobFunctionAssetLookupTableModal.displayName;
    }
    getNewBranchId() {
        this.jobFunctionAssetBranche.branchId = this.branchLookupTableModal.id;
        this.branchBranchName = this.branchLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
