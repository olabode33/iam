import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetJobFunctionAssetBrancheForView, JobFunctionAssetBrancheDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewJobFunctionAssetBrancheModal',
    templateUrl: './view-jobFunctionAssetBranche-modal.component.html'
})
export class ViewJobFunctionAssetBrancheModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetJobFunctionAssetBrancheForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetJobFunctionAssetBrancheForView();
        this.item.jobFunctionAssetBranche = new JobFunctionAssetBrancheDto();
    }

    show(item: GetJobFunctionAssetBrancheForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
