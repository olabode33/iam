import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { JobFunctionAssetBranchsServiceProxy, CreateOrEditJobFunctionAssetBranchDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { JobFunctionAssetLookupTableModalComponent } from './jobFunctionAsset-lookup-table-modal.component';
import { BranchLookupTableModalComponent } from './branch-lookup-table-modal.component';


@Component({
    selector: 'createOrEditJobFunctionAssetBranchModal',
    templateUrl: './create-or-edit-jobFunctionAssetBranch-modal.component.html'
})
export class CreateOrEditJobFunctionAssetBranchModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('jobFunctionAssetLookupTableModal') jobFunctionAssetLookupTableModal: JobFunctionAssetLookupTableModalComponent;
    @ViewChild('branchLookupTableModal') branchLookupTableModal: BranchLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobFunctionAssetBranch: CreateOrEditJobFunctionAssetBranchDto = new CreateOrEditJobFunctionAssetBranchDto();
    jobFunctionAssetJobFunctionId = '';
    branchBranchName = '';


    constructor(
        injector: Injector,
        private _jobFunctionAssetBranchsServiceProxy: JobFunctionAssetBranchsServiceProxy
    ) {
        super(injector);
    }

    show(jobFunctionAssetBranchId?: number): void {
        if (!jobFunctionAssetBranchId) {
            this.jobFunctionAssetBranch = new CreateOrEditJobFunctionAssetBranchDto();
            this.jobFunctionAssetBranch.id = jobFunctionAssetBranchId;
            this.jobFunctionAssetJobFunctionId = '';
            this.branchBranchName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._jobFunctionAssetBranchsServiceProxy.getJobFunctionAssetBranchForEdit(jobFunctionAssetBranchId).subscribe(result => {
                this.jobFunctionAssetBranch = result.jobFunctionAssetBranch;
                this.jobFunctionAssetJobFunctionId = result.jobFunctionAssetJobFunctionId;
                this.branchBranchName = result.branchBranchName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._jobFunctionAssetBranchsServiceProxy.createOrEdit(this.jobFunctionAssetBranch)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectJobFunctionAssetModal() {
        this.jobFunctionAssetLookupTableModal.id = this.jobFunctionAssetBranch.jobFunctionAssetId;
        this.jobFunctionAssetLookupTableModal.displayName = this.jobFunctionAssetJobFunctionId;
        this.jobFunctionAssetLookupTableModal.show();
    }
    openSelectBranchModal() {
        this.branchLookupTableModal.id = this.jobFunctionAssetBranch.branchId;
        this.branchLookupTableModal.displayName = this.branchBranchName;
        this.branchLookupTableModal.show();
    }


    setJobFunctionAssetIdNull() {
        this.jobFunctionAssetBranch.jobFunctionAssetId = null;
        this.jobFunctionAssetJobFunctionId = '';
    }
    setBranchIdNull() {
        this.jobFunctionAssetBranch.branchId = null;
        this.branchBranchName = '';
    }


    getNewJobFunctionAssetId() {
        this.jobFunctionAssetBranch.jobFunctionAssetId = this.jobFunctionAssetLookupTableModal.id;
        this.jobFunctionAssetJobFunctionId = this.jobFunctionAssetLookupTableModal.displayName;
    }
    getNewBranchId() {
        this.jobFunctionAssetBranch.branchId = this.branchLookupTableModal.id;
        this.branchBranchName = this.branchLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
