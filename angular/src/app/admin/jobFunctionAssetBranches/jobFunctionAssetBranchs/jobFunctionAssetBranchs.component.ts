import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { JobFunctionAssetBranchsServiceProxy, JobFunctionAssetBranchDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditJobFunctionAssetBranchModalComponent } from './create-or-edit-jobFunctionAssetBranch-modal.component';
import { ViewJobFunctionAssetBranchModalComponent } from './view-jobFunctionAssetBranch-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './jobFunctionAssetBranchs.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobFunctionAssetBranchsComponent extends AppComponentBase {

    @ViewChild('createOrEditJobFunctionAssetBranchModal') createOrEditJobFunctionAssetBranchModal: CreateOrEditJobFunctionAssetBranchModalComponent;
    @ViewChild('viewJobFunctionAssetBranchModalComponent') viewJobFunctionAssetBranchModal: ViewJobFunctionAssetBranchModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    jobFunctionAssetJobFunctionIdFilter = '';
    branchBranchNameFilter = '';




    constructor(
        injector: Injector,
        private _jobFunctionAssetBranchsServiceProxy: JobFunctionAssetBranchsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getJobFunctionAssetBranchs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._jobFunctionAssetBranchsServiceProxy.getAll(
            this.filterText,
            this.jobFunctionAssetJobFunctionIdFilter,
            this.branchBranchNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobFunctionAssetBranch(): void {
        this.createOrEditJobFunctionAssetBranchModal.show();
    }

    deleteJobFunctionAssetBranch(jobFunctionAssetBranch: JobFunctionAssetBranchDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobFunctionAssetBranchsServiceProxy.delete(jobFunctionAssetBranch.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._jobFunctionAssetBranchsServiceProxy.getJobFunctionAssetBranchsToExcel(
            this.filterText,
            this.jobFunctionAssetJobFunctionIdFilter,
            this.branchBranchNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
