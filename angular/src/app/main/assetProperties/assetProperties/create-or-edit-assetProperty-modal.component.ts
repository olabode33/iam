import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { AssetPropertiesServiceProxy, CreateOrEditAssetPropertyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';


@Component({
    selector: 'createOrEditAssetPropertyModal',
    templateUrl: './create-or-edit-assetProperty-modal.component.html'
})
export class CreateOrEditAssetPropertyModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
	 @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
	

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    assetProperty: CreateOrEditAssetPropertyDto = new CreateOrEditAssetPropertyDto();
	assetAssetName = '';
		 

    constructor(
        injector: Injector,
        private _assetPropertiesServiceProxy: AssetPropertiesServiceProxy
    ) {
        super(injector);
    }

    show(assetPropertyId?: number): void {
        if (!assetPropertyId) { 
			this.assetProperty = new CreateOrEditAssetPropertyDto();
			this.assetProperty.id = assetPropertyId;
			this.assetAssetName = '';
		 
			this.active = true;
			this.modal.show();
        }
		else{
			this._assetPropertiesServiceProxy.getAssetPropertyForEdit(assetPropertyId).subscribe(result => {
				this.assetProperty = result.assetProperty;
				this.assetAssetName = result.assetAssetName;
		 
				this.active = true;
				this.modal.show();
			});
		}  
    }

    save(): void {
			this.saving = true;
			this._assetPropertiesServiceProxy.createOrEdit(this.assetProperty)
			 .pipe(finalize(() => { this.saving = false; }))
			 .subscribe(() => {
			    this.notify.info(this.l('SavedSuccessfully'));
				this.close();
				this.modalSave.emit(null);
             });
    }

	    openSelectAssetModal() {
        this.assetLookupTableModal.id = this.assetProperty.assetId;
        this.assetLookupTableModal.displayName = this.assetAssetName;
        this.assetLookupTableModal.show(0);
    }
	

	    setAssetIdNull() {
        this.assetProperty.assetId = null;
        this.assetAssetName = '';
    }
	

	    getNewAssetId() {
        this.assetProperty.assetId = this.assetLookupTableModal.id;
        this.assetAssetName = this.assetLookupTableModal.displayName;
    }
	

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
