import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetAssetPropertyForView, AssetPropertyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewAssetPropertyModal',
    templateUrl: './view-assetProperty-modal.component.html'
})
export class ViewAssetPropertyModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetAssetPropertyForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetAssetPropertyForView();
        this.item.assetProperty = new AssetPropertyDto();
    }

    show(item: GetAssetPropertyForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
