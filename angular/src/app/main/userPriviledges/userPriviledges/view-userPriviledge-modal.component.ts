import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetUserPriviledgeForView, UserPriviledgeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewUserPriviledgeModal',
    templateUrl: './view-userPriviledge-modal.component.html'
})
export class ViewUserPriviledgeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetUserPriviledgeForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetUserPriviledgeForView();
        this.item.userPriviledge = new UserPriviledgeDto();
    }

    show(item: GetUserPriviledgeForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
