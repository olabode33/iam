import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { UserPriviledgesServiceProxy, CreateOrEditUserPriviledgeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RequestLookupTableModalComponent } from '../../requestEvents/requestEvents/request-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';


@Component({
    selector: 'createOrEditUserPriviledgeModal',
    templateUrl: './create-or-edit-userPriviledge-modal.component.html'
})
export class CreateOrEditUserPriviledgeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('requestLookupTableModal') requestLookupTableModal: RequestLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    userPriviledge: CreateOrEditUserPriviledgeDto = new CreateOrEditUserPriviledgeDto();
    requestRequestCode = '';
    userName = '';
    assetAssetName = '';


    constructor(
        injector: Injector,
        private _userPriviledgesServiceProxy: UserPriviledgesServiceProxy
    ) {
        super(injector);
    }

    show(userPriviledgeId?: number): void {
        if (!userPriviledgeId) {
            this.userPriviledge = new CreateOrEditUserPriviledgeDto();
            this.userPriviledge.id = userPriviledgeId;
            this.requestRequestCode = '';
            this.userName = '';
            this.assetAssetName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._userPriviledgesServiceProxy.getUserPriviledgeForEdit(userPriviledgeId).subscribe(result => {
                this.userPriviledge = result.userPriviledge;
                this.requestRequestCode = result.requestRequestCode;
                this.userName = result.userName;
                this.assetAssetName = result.assetAssetName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._userPriviledgesServiceProxy.createOrEdit(this.userPriviledge)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectRequestModal() {
        this.requestLookupTableModal.id = this.userPriviledge.requestId;
        this.requestLookupTableModal.displayName = this.requestRequestCode;
        this.requestLookupTableModal.show();
    }
    openSelectUserModal() {
        this.userLookupTableModal.id = this.userPriviledge.userId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }
    openSelectAssetModal() {
        this.assetLookupTableModal.id = this.userPriviledge.assetId;
        this.assetLookupTableModal.displayName = this.assetAssetName;
        this.assetLookupTableModal.show(0);
    }


    setRequestIdNull() {
        this.userPriviledge.requestId = null;
        this.requestRequestCode = '';
    }
    setUserIdNull() {
        this.userPriviledge.userId = null;
        this.userName = '';
    }
    setAssetIdNull() {
        this.userPriviledge.assetId = null;
        this.assetAssetName = '';
    }


    getNewRequestId() {
        this.userPriviledge.requestId = this.requestLookupTableModal.id;
        this.requestRequestCode = this.requestLookupTableModal.displayName;
    }
    getNewUserId() {
        this.userPriviledge.userId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }
    getNewAssetId() {
        this.userPriviledge.assetId = this.assetLookupTableModal.id;
        this.assetAssetName = this.assetLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
