import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { UserPriviledgesServiceProxy, UserPriviledgeDto, AssetLookupTableDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditUserPriviledgeModalComponent } from './create-or-edit-userPriviledge-modal.component';
import { ViewUserPriviledgeModalComponent } from './view-userPriviledge-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';
import { RequestTypeLookupTableModalComponent } from '../../requestEvents/requestEvents/requestType-lookup-table-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './userPriviledges.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserPriviledgesComponent extends AppComponentBase {

    @ViewChild('createOrEditUserPriviledgeModal') createOrEditUserPriviledgeModal: CreateOrEditUserPriviledgeModalComponent;
    @ViewChild('viewUserPriviledgeModalComponent') viewUserPriviledgeModal: ViewUserPriviledgeModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;
    @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('requestTypeLookupTableModal') requestTypeLookupTableModal: RequestTypeLookupTableModalComponent;

    advancedFiltersAreShown = false;
    filterText = '';
    statusFilter = -1;
    maxStartDateFilter: moment.Moment;
    minStartDateFilter: moment.Moment;
    applicationUsernameFilter = '';
    maxEndDateFilter: moment.Moment;
    minEndDateFilter: moment.Moment;
    lastExpiryDate: Date = new Date(2000, 1, 1);
    requestRequestCodeFilter = '';
    userNameFilter = '';
    assetAssetNameFilter = '';
    ShowPriviledgesGrid = false;
    AssetListing = [];
    AssetIDArray: number[];

    UserListing = [];
    UserIDArray: number[];

    RequestListing = [];
    RequestIDArray: number[];


    constructor(
        injector: Injector,
        private _userPriviledgesServiceProxy: UserPriviledgesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.AssetIDArray = [];
        this.RequestIDArray = [];
        this.UserIDArray = [];
    }

    getUserPriviledges(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._userPriviledgesServiceProxy.getAll(
            this.filterText,
            this.statusFilter,
            this.maxStartDateFilter,
            this.minStartDateFilter,
            this.applicationUsernameFilter,
            this.maxEndDateFilter,
            this.minEndDateFilter,
            this.requestRequestCodeFilter,
            this.userNameFilter,
            this.assetAssetNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            //this.primengTableHelper.totalRecordsCount = result.totalCount;
            //this.primengTableHelper.records = result.items;
            //this.primengTableHelper.hideLoadingIndicator();
        });

        this.primengTableHelper.hideLoadingIndicator();
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createUserPriviledge(): void {
        this.createOrEditUserPriviledgeModal.show();
    }

    deleteUserPriviledge(userPriviledge: UserPriviledgeDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._userPriviledgesServiceProxy.delete(userPriviledge.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._userPriviledgesServiceProxy.getUserPriviledgesToExcel(
            this.filterText,
            this.statusFilter,
            this.maxStartDateFilter,
            this.minStartDateFilter,
            this.applicationUsernameFilter,
            this.maxEndDateFilter,
            this.minEndDateFilter,
            this.requestRequestCodeFilter,
            this.userNameFilter,
            this.assetAssetNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    openSelectAssetModal() {
        this.assetLookupTableModal.show(0);
    }

    openSelectUserModal() {
        this.userLookupTableModal.show();
    }

    //Asset

    getNewAssetId(asset?: AssetLookupTableDto) {
        if (this.assetLookupTableModal.id > 0) {
            for (let x = 0; x < this.AssetListing.length; x++) {
                if (this.AssetListing[x].id === this.assetLookupTableModal.id) {
                    return;
                }
            }
            let item = {

                id: asset.id,
                displayName: asset.displayName,
                assetDescription: asset.assetDescription
            };
            this.AssetListing.push(item);
        }
    }

    removeNewAssetId(AppID: number): void {
        for (let x = 0; x < this.AssetListing.length; x++) {
            if (this.AssetListing[x].id === AppID) {
                this.AssetListing.splice(x, 1);
                return;
            }
        }
    }

    //User
    getNewUserId() {
        if (this.userLookupTableModal.id > 0) {
            for (let x = 0; x < this.UserListing.length; x++) {
                if (this.UserListing[x].id === this.userLookupTableModal.id) {
                    return;
                }
            }
            let item = {
                id: this.userLookupTableModal.id,
                displayName: this.userLookupTableModal.displayName
            };
            this.UserListing.push(item);
        }
    }

    removeNewUserId(AppID: number): void {
        for (let x = 0; x < this.UserListing.length; x++) {
            if (this.UserListing[x].id === AppID) {
                this.UserListing.splice(x, 1);
                return;
            }
        }
    }


    ClearPriviledgesGrid(): void {
        this.primengTableHelper.showLoadingIndicator();
        this.primengTableHelper.records = [];
        this.AssetListing = [];
        this.RequestListing = [];
        this.UserListing = [];
        this.AssetIDArray = [];
        this.RequestIDArray = [];
        this.UserIDArray = [];
        this.ShowPriviledgesGrid = false;
        this.primengTableHelper.hideLoadingIndicator();
    }

    openSelectRequestTypeModal() {
        this.requestTypeLookupTableModal.show();
    }

    getNewRequestTypeId() {
        if (this.requestTypeLookupTableModal.id > 0) {
            for (let x = 0; x < this.RequestListing.length; x++) {
                if (this.RequestListing[x].id === this.requestTypeLookupTableModal.id) {
                    return;
                }
            }
            let item = {
                id: this.requestTypeLookupTableModal.id,
                displayName: this.requestTypeLookupTableModal.displayName
            };
            this.RequestListing.push(item);
        }
    }

    removeNewRequestTypeId(AppID: number): void {
        for (let x = 0; x < this.RequestListing.length; x++) {
            if (this.RequestListing[x].id === AppID) {
                this.RequestListing.splice(x, 1);
                return;
            }
        }
    }

    loadPriviledges(): void {
        this.primengTableHelper.showLoadingIndicator();
        this.primengTableHelper.records = [];
        this.primengTableHelper.totalRecordsCount = 0;
        for (let x = 0; x < this.RequestListing.length; x++) { this.RequestIDArray.push(this.RequestListing[x].id); }
        for (let x = 0; x < this.UserListing.length; x++) { this.UserIDArray.push(this.UserListing[x].id); }
        for (let x = 0; x < this.AssetListing.length; x++) { this.AssetIDArray.push(this.AssetListing[x].id); }

        this._userPriviledgesServiceProxy.getSelectedAll(
            this.UserIDArray,
            this.RequestIDArray,
            this.AssetIDArray,
            this.minStartDateFilter,
            this.maxStartDateFilter
        ).subscribe(result => {
            console.log(result.items);
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.ShowPriviledgesGrid = true;
        });
        this.primengTableHelper.hideLoadingIndicator();
    }
}
