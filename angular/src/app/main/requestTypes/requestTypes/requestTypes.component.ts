import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { RequestTypesServiceProxy, RequestTypeDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditRequestTypeModalComponent } from './create-or-edit-requestType-modal.component';
import { ViewRequestTypeModalComponent } from './view-requestType-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './requestTypes.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RequestTypesComponent extends AppComponentBase {

    @ViewChild('createOrEditRequestTypeModal') createOrEditRequestTypeModal: CreateOrEditRequestTypeModalComponent;
    @ViewChild('viewRequestTypeModalComponent') viewRequestTypeModal: ViewRequestTypeModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    requestCodeFilter = '';
    requestDescriptionFilter = '';
    requestOtherDetailsFilter = '';
    maxApprovalNoFilter: number;
    maxApprovalNoFilterEmpty: number;
    minApprovalNoFilter: number;
    minApprovalNoFilterEmpty: number;
    assetTypeAssetTypeNameFilter = '';




    constructor(
        injector: Injector,
        private _requestTypesServiceProxy: RequestTypesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getRequestTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._requestTypesServiceProxy.getAll(
            this.filterText,
            this.requestCodeFilter,
            this.requestDescriptionFilter,
            this.requestOtherDetailsFilter,
            this.maxApprovalNoFilter == null ? this.maxApprovalNoFilterEmpty : this.maxApprovalNoFilter,
            this.minApprovalNoFilter == null ? this.minApprovalNoFilterEmpty : this.minApprovalNoFilter,
            this.assetTypeAssetTypeNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createRequestType(): void {
        this.createOrEditRequestTypeModal.show();
    }

    deleteRequestType(requestType: RequestTypeDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._requestTypesServiceProxy.delete(requestType.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._requestTypesServiceProxy.getRequestTypesToExcel(
            this.filterText,
            this.requestCodeFilter,
            this.requestDescriptionFilter,
            this.requestOtherDetailsFilter,
            this.maxApprovalNoFilter == null ? this.maxApprovalNoFilterEmpty : this.maxApprovalNoFilter,
            this.minApprovalNoFilter == null ? this.minApprovalNoFilterEmpty : this.minApprovalNoFilter,
            this.assetTypeAssetTypeNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
