import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {RequestTypesServiceProxy, AssetTypeLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'assetTypeLookupTableModal',
    styleUrls: ['./assetType-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './assetType-lookup-table-modal.component.html'
})
export class AssetTypeLookupTableModalComponent extends AppComponentBase{

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id :number;
    displayName :string;
    assetTypeDesc: string;

    @Output() modalSave: EventEmitter<AssetTypeLookupTableDto> = new EventEmitter<AssetTypeLookupTableDto>();

    active = false;
    saving = false;
    

    constructor(
        injector: Injector,
        private _requestTypesServiceProxy: RequestTypesServiceProxy
    ) {
        super(injector);
    }

    show(): void {
        this.active = true;
        this.paginator.rows = 5;
        this.getAll();
        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

		this._requestTypesServiceProxy.getAllAssetTypeForLookupTable(
			this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(assetType: AssetTypeLookupTableDto) {
        this.id = assetType.id;
        this.displayName = assetType.displayName;
        this.assetTypeDesc = assetType.assetTypeDescription;
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(assetType);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
