import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetRequestTypeForView, RequestTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewRequestTypeModal',
    templateUrl: './view-requestType-modal.component.html'
})
export class ViewRequestTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetRequestTypeForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetRequestTypeForView();
        this.item.requestType = new RequestTypeDto();
    }

    show(item: GetRequestTypeForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
