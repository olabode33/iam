import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { RequestTypesServiceProxy, CreateOrEditRequestTypeDto, AssetTypeLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AssetTypeLookupTableModalComponent } from './assetType-lookup-table-modal.component';
import { RequestTypeLookupTableModalComponent } from '../../requestEvents/requestEvents/requestType-lookup-table-modal.component';


@Component({
    selector: 'createOrEditRequestTypeModal',
    templateUrl: './create-or-edit-requestType-modal.component.html'
})
export class CreateOrEditRequestTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('assetTypeLookupTableModal') assetTypeLookupTableModal: AssetTypeLookupTableModalComponent;
    @ViewChild('requestTypeLookupTableModal') requestTypeLookupTableModal: RequestTypeLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    requestType: CreateOrEditRequestTypeDto = new CreateOrEditRequestTypeDto();
    assetTypeAssetTypeName = '';
    ApplicationApplicationName: AssetTypeLookupTableDto[] = new Array<AssetTypeLookupTableDto>();
    requestTypeRequestCode = '';

    constructor(
        injector: Injector,
        private _requestTypesServiceProxy: RequestTypesServiceProxy
    ) {
        super(injector);
    }

    show(requestTypeId?: number): void {
        if (!requestTypeId) {
            this.requestType = new CreateOrEditRequestTypeDto();
            this.requestType.id = requestTypeId;
            this.assetTypeAssetTypeName = '';
            this.ApplicationApplicationName = [];
            this.requestTypeRequestCode = '';
            this.active = true;
            this.modal.show();
        } else {
            this.ApplicationApplicationName = [];
            this._requestTypesServiceProxy.getRequestTypeForEdit(requestTypeId).subscribe(result => {
                this.ApplicationApplicationName = result.assetTypeID;
                this.requestType = result.requestType;
                this.assetTypeAssetTypeName = result.assetTypeAssetTypeName;
                this.requestTypeRequestCode = result.reverseRequestTypeName;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this.requestType.assetTypeID = this.ApplicationApplicationName;
        this._requestTypesServiceProxy.createOrEdit(this.requestType)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectAssetTypeModal() {
        this.assetTypeLookupTableModal.show();
    }


    setAssetTypeIdNull() {
        this.requestType.assetTypeId = null;
        this.assetTypeAssetTypeName = '';
    }

    getNewAssetTypeId(asset?: AssetTypeLookupTableDto) {
        if (this.assetTypeLookupTableModal.id > 0) {
            for (let x = 0; x < this.ApplicationApplicationName.length; x++) {
                if (this.ApplicationApplicationName[x].id === this.assetTypeLookupTableModal.id) {
                    return;
                }
            }
            let item: AssetTypeLookupTableDto = new AssetTypeLookupTableDto();
            item.id = asset.id;
            item.displayName = asset.displayName;
            item.assetTypeDescription = asset.assetTypeDescription;
            this.ApplicationApplicationName.push(item);
        }
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }

    removeApplicationNameId(AppID: number) {
        for (let x = 0; x < this.ApplicationApplicationName.length; x++) {
            if (this.ApplicationApplicationName[x].id === AppID) {
                this.ApplicationApplicationName.splice(x, 1);
                return;
            }
        }
    }

    openSelectRequestTypeModal() {
        this.requestTypeLookupTableModal.id = this.requestType.reverseRequestType;
        this.requestTypeLookupTableModal.displayName = this.requestTypeRequestCode;
        this.requestTypeLookupTableModal.show();
    }

    setRequestTypeIdNull() {
        this.requestType.reverseRequestType = null;
        this.requestTypeRequestCode = '';
    }

    getNewRequestTypeId() {
        this.ApplicationApplicationName = [];
        this.requestType.reverseRequestType = this.requestTypeLookupTableModal.id;
        this.requestTypeRequestCode = this.requestTypeLookupTableModal.displayName;
        //console.log(this.requestType.reverseRequestType);
        this._requestTypesServiceProxy.getSingleRequestTypeAssets(this.requestTypeLookupTableModal.id)
        .subscribe(result => {
            let comp: boolean = (Object.keys(result)).length > 0;
            if (comp) {
                for (let x in result) {
                    if (comp) {
                        let item: AssetTypeLookupTableDto = new AssetTypeLookupTableDto();
                        item.id = result[x].id;
                        item.displayName = result[x].displayName;
                        item.assetTypeDescription = result[x].assetTypeDescription;
                        this.ApplicationApplicationName.push(item);
                    }
                }
            }
        });

    }
}
