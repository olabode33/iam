import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { UserSupervisorsServiceProxy, CreateOrEditUserSupervisorDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';


@Component({
    selector: 'createOrEditUserSupervisorModal',
    templateUrl: './create-or-edit-userSupervisor-modal.component.html'
})
export class CreateOrEditUserSupervisorModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('userLookupTableModal2') userLookupTableModal2: UserLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    userSupervisor: CreateOrEditUserSupervisorDto = new CreateOrEditUserSupervisorDto();
    userName = '';
    userName2 = '';


    constructor(
        injector: Injector,
        private _userSupervisorsServiceProxy: UserSupervisorsServiceProxy
    ) {
        super(injector);
    }

    show(userSupervisorId?: number): void {
        if (!userSupervisorId) {
            this.userSupervisor = new CreateOrEditUserSupervisorDto();
            this.userSupervisor.id = userSupervisorId;
            this.userName = '';
            this.userName2 = '';

            this.active = true;
            this.modal.show();
        } else {
            this._userSupervisorsServiceProxy.getUserSupervisorForEdit(userSupervisorId).subscribe(result => {
                this.userSupervisor = result.userSupervisor;
                this.userName = result.userName;
                this.userName2 = result.userName2;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._userSupervisorsServiceProxy.createOrEdit(this.userSupervisor)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectUserModal() {
        this.userLookupTableModal.id = this.userSupervisor.userId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }
    openSelectUserModal2() {
        this.userLookupTableModal2.id = this.userSupervisor.supervisorUserId;
        this.userLookupTableModal2.displayName = this.userName;
        this.userLookupTableModal2.show();
    }


    setUserIdNull() {
        this.userSupervisor.userId = null;
        this.userName = '';
    }
    setSupervisorUserIdNull() {
        this.userSupervisor.supervisorUserId = null;
        this.userName2 = '';
    }


    getNewUserId() {
        this.userSupervisor.userId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }
    getNewSupervisorUserId() {
        this.userSupervisor.supervisorUserId = this.userLookupTableModal2.id;
        this.userName2 = this.userLookupTableModal2.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
