import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetRequestForView, RequestDto, RequestsServiceProxy, AssetLookupTableDto, GetRequestForEditOutput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewRequestModal',
    templateUrl: './view-request-modal.component.html'
})
export class ViewRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: any;
    ApplicationApplicationName = [];
    AssetList = [];
    AssetListSelected: GetRequestForEditOutput[] = [];

    constructor(
        injector: Injector,
        private _requestsServiceProxy: RequestsServiceProxy
    ) {
        super(injector);
        this.item = new GetRequestForView();
        this.item.request = new RequestDto();
    }

    show(id: number): void    {
        this.ApplicationApplicationName = [];
        this._requestsServiceProxy.getSingleRequest(id).subscribe(result => {
            for (let x in result.assets) {
                if (result.assets[x].id > 0) {
                    this.ApplicationApplicationName.push({
                        id: result.assets[x].id,
                        displayName: result.assets[x].assetName,
                        assetDescription: result.assets[x].assetDescription,
                        RejectedBy: result.assets[x].rejectedBy,
                        RequestStatus: result.assets[x].requestAssetStatus
                    });
                }
            }
            this.item = result;
        });
        this.primengTableHelper.records = this.ApplicationApplicationName;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
