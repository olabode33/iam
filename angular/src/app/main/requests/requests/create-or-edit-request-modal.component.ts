import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import {
    RequestsServiceProxy,
    CreateOrEditRequestDto,
    AssetLookupTableDto,
    AssetTypeLookupTableDto,
    RequestTypesServiceProxy,
    CreateOrEditAssetDto,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RequestTypeLookupTableModalComponent } from '../../requestEvents/requestEvents/requestType-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';
import { moment } from '../../../../../node_modules/ngx-bootstrap/chronos/test/chain';
import { BranchLookupTableModalComponent } from '../../assets/assets/branch-lookup-table-modal.component';

@Component({
    selector: 'createOrEditRequestModal',
    templateUrl: './create-or-edit-request-modal.component.html'
})
export class CreateOrEditRequestModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('requestTypeLookupTableModal') requestTypeLookupTableModal: RequestTypeLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
    @ViewChild('branchLookupTableModal') branchLookupTableModal: BranchLookupTableModalComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    request: CreateOrEditRequestDto = new CreateOrEditRequestDto();
    requestTypeRequestCode = '';
    userName = '';
    RiskLevel = '';
    assetAssetName = '';
    assetList = [];
    createRequestForAnotherUser = false;
    filterByBranch = false;
    otherUserId: number;
    AssetArray = [];
    showExpiryField = false;

    // asset: CreateOrEditAssetDto = new CreateOrEditAssetDto();
    branchId: number;
    branchBranchName = '';

    constructor(
        injector: Injector,
        private _requestsServiceProxy: RequestsServiceProxy,
        private _requestTypesServiceProxy: RequestTypesServiceProxy
    ) {
        super(injector);
    }

    show(requestId?: number): void {
        this.assetList = []; //@redundant emptying array should happen once
        if (!requestId) {
            this.request = new CreateOrEditRequestDto();
            this.request.id = requestId;
            this.requestTypeRequestCode = '';
            this.userName = '';
            this.assetAssetName = '';
            this.request.userId = null;
            this.active = true;
            this.modal.show();
            this.assetList = []; //@redundant emptying array should happen once
        } else {
            this._requestsServiceProxy
                .getRequestForEdit(requestId)
                .subscribe(result => {
                    this.assetList = []; //@redundant emptying array should happen once
                    for (let x in result.assets) {
                        if (result.assets[x].id > 0) {
                            this.assetList.push({
                                id: result.assets[x].id,
                                displayName: result.assets[x].assetName,
                                assetDescription:
                                    result.assets[x].assetDescription
                            });
                        }
                    }
                    this.request = result.request;
                    this.requestTypeRequestCode = result.requestTypeRequestCode;
                    this.userName = result.userName;
                    this.assetAssetName = result.assetAssetName;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.AssetArray = [];
        // Setting request owner
        if (this.createRequestForAnotherUser === false) {
            // Current session user is request owner
            // The requestOwner field will be handled in the back end
            this.request.userId = this.appSession.userId;
        }
        this.saving = true;

        let ProcessRequest = false;
        if (this.showExpiryField) {
            if (this.request.expiryDate) {
                ProcessRequest = true;
            } else {
                this.notify.error('Expiry Date is Mandatory');
                this.saving = false;
            }
        } else {
            ProcessRequest = true;
        }


        if (this.assetList.length === 0) {
            this.notify.error('Please select at least one asset');
            this.saving = false;
        }

        if (ProcessRequest && this.assetList.length > 0) {
            for (let i = 0; i < this.assetList.length; i++) {
                this.AssetArray.push(this.assetList[i].id);
            }
            this.request.assetId = this.AssetArray;
            this._requestsServiceProxy
                .createOrEdit(this.request)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                }, error => {
                    // alert(error);
                    this.close();
                    this.notify.error('"' + this.appSession.user.userName + '" does not have a supervisor');
                });
        }
    }

    openSelectRequestTypeModal() {
        this.requestTypeLookupTableModal.id = this.request.requestTypeId;
        this.requestTypeLookupTableModal.displayName = this.requestTypeRequestCode;

        this.assetList = [];
        this.requestTypeLookupTableModal.show();
    }
    openSelectUserModal() {
        this.userLookupTableModal.id = this.request.userId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }

    openSelectAssetModal() {
        this.assetLookupTableModal.show(this.request.requestTypeId, this.branchId);
    }

    setRequestTypeIdNull() {
        this.request.requestTypeId = null;
        this.requestTypeRequestCode = '';
    }
    setUserIdNull() {
        this.request.userId = null;
        this.userName = '';
    }
    setAssetIdNull() {
        this.request.assetId = null;
        this.assetAssetName = '';
    }

    setExpryDate(id) {
        this._requestTypesServiceProxy
            .getRequestTypeForEdit(id)
            .subscribe(result => {
                this.showExpiryField = result.requestType.requestExpiry;
            });
    }

    getNewRequestTypeId() {
        this.request.requestTypeId = this.requestTypeLookupTableModal.id;
        this.requestTypeRequestCode = this.requestTypeLookupTableModal.displayName;
        this.setExpryDate(this.requestTypeLookupTableModal.id);
    }

    openSelectBranchModal() {
        this.branchLookupTableModal.id = this.branchId;
        this.branchLookupTableModal.displayName = this.branchBranchName;
        this.branchLookupTableModal.show();
    }

    setBranchIdNull() {
        this.branchId = null;
        this.branchBranchName = '';
    }

    getNewBranchId() {
        this.branchId = this.branchLookupTableModal.id;
        this.branchBranchName = this.branchLookupTableModal.displayName;
    }

    getNewUserId() {
        this.request.userId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }

    getNewAssetId(asset?: AssetLookupTableDto) {
        if (this.assetLookupTableModal.id > 0) {
            for (let x = 0; x < this.assetList.length; x++) {
                if (
                    this.assetList[x].id ===
                    this.assetLookupTableModal.id
                ) {
                    return;
                }
            }
            let item = {
                id: asset.id,
                displayName: asset.displayName,
                assetDescription: asset.assetDescription
            };
            this.assetList.push(item);
        }
    }

    removeApplicationNameId(AppID: number) {
        for (let x = 0; x < this.assetList.length; x++) {
            if (this.assetList[x].id === AppID) {
                this.assetList.splice(x, 1);
                return;
            }
        }
    }

    close(): void {
        this.active = false;
        this.createRequestForAnotherUser = false;
        this.setAssetIdNull();
        this.setBranchIdNull();
        this.setRequestTypeIdNull();
        this.setUserIdNull();
        this.modal.hide();
    }
}
