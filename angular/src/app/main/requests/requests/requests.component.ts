import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { RequestsServiceProxy, RequestDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditRequestModalComponent } from './create-or-edit-request-modal.component';
import { ViewRequestModalComponent } from './view-request-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './requests.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RequestsComponent extends AppComponentBase {

    @ViewChild('createOrEditRequestModal') createOrEditRequestModal: CreateOrEditRequestModalComponent;
    @ViewChild('viewRequestModalComponent') viewRequestModal: ViewRequestModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    requestDescriptionFilter = '';
    maxExpiryDateFilter: moment.Moment;
    minExpiryDateFilter: moment.Moment;
    lastExpiryDate: Date = new Date(2000, 1, 1);
    requestTypeRequestCodeFilter = '';
    userNameFilter = '';
    assetAssetNameFilter = '';

    constructor(
        injector: Injector,
        private _requestsServiceProxy: RequestsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getRequests(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._requestsServiceProxy.getAll(
            this.filterText,
            this.requestDescriptionFilter,
            this.maxExpiryDateFilter,
            this.minExpiryDateFilter,
            this.requestTypeRequestCodeFilter,
            this.userNameFilter,
            this.assetAssetNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            // console.log(result.items);
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createRequest(): void {
        this.createOrEditRequestModal.show();
    }

    deleteRequest(request: RequestDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._requestsServiceProxy.delete(request.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._requestsServiceProxy.getRequestsToExcel(
            this.filterText,
            this.requestDescriptionFilter,
            this.maxExpiryDateFilter,
            this.minExpiryDateFilter,
            this.requestTypeRequestCodeFilter,
            this.userNameFilter,
            this.assetAssetNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
