import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { RequestsServiceProxy, AssetLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'assetLookupTableModal',
    styleUrls: ['./asset-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './asset-lookup-table-modal.component.html'
})
export class AssetLookupTableModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id: number;
    displayName: string;
    assetDescription: string;

    @Output() modalSave: EventEmitter<AssetLookupTableDto> = new EventEmitter<AssetLookupTableDto>();

    active = false;
    saving = false;


    constructor(
        injector: Injector,
        private _requestsServiceProxy: RequestsServiceProxy
    ) {
        super(injector);
    }

    show(requestTypeId: number, branchId?): void {
        this.active = true;
        this.paginator.rows = 5;

        if (requestTypeId >= 1) {
            this.getAllAssetType(requestTypeId, branchId);
        } else {
            this.getAll();
        }

        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._requestsServiceProxy.getAllAssetForLookupTable(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    getAllAssetType(requestTypeId: number, branchId?, event?: LazyLoadEvent) {
        //if (!this.active) {
        //    return;
        //}

        //if (this.primengTableHelper.shouldResetPaging(event)) {
        //    this.paginator.changePage(0);
        //    return;
        //}

        //this.primengTableHelper.showLoadingIndicator();

        this._requestsServiceProxy.getSingleAssetForLookupTable(requestTypeId, branchId).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(asset: AssetLookupTableDto) {

        this.id = asset.id;
        this.displayName = asset.displayName;
        this.assetDescription = asset.assetDescription;

        this.active = false;
        this.modal.hide();

        this.modalSave.emit(asset);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
