import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { JobFunctionAssetsServiceProxy, JobFunctionAssetDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditJobFunctionAssetModalComponent } from './create-or-edit-jobFunctionAsset-modal.component';
import { ViewJobFunctionAssetModalComponent } from './view-jobFunctionAsset-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './jobFunctionAssets.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobFunctionAssetsComponent extends AppComponentBase {

    @ViewChild('createOrEditJobFunctionAssetModal') createOrEditJobFunctionAssetModal: CreateOrEditJobFunctionAssetModalComponent;
    @ViewChild('viewJobFunctionAssetModalComponent') viewJobFunctionAssetModal: ViewJobFunctionAssetModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    assetAssetNameFilter = '';
    jobFunctionJobNameFilter = '';
    branchBranchNameFilter = '';

    constructor(
        injector: Injector,
        private _jobFunctionAssetsServiceProxy: JobFunctionAssetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getJobFunctionAssets(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._jobFunctionAssetsServiceProxy.getSelected(0).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobFunctionAsset(): void {
        this.createOrEditJobFunctionAssetModal.show();
    }

    deleteJobFunctionAsset(jobFunctionAsset: JobFunctionAssetDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobFunctionAssetsServiceProxy.delete(jobFunctionAsset.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._jobFunctionAssetsServiceProxy.getJobFunctionAssetsToExcel(
            this.filterText,
        this.assetAssetNameFilter,
        this.jobFunctionJobNameFilter,
        this.branchBranchNameFilter,
    )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
