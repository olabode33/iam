import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { JobFunctionAssetsServiceProxy, CreateOrEditJobFunctionAssetDto, AssetLookupTableDto, BranchLookupTableDto, JobFunctionsServiceProxy,
    GetJobFunctionAssetForViewEvery, JobFunctionAssetAssetsList, JobFunctionAssetBranchesList } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';
import { JobFunctionLookupTableModalComponent } from './jobFunction-lookup-table-modal.component';
import { BranchLookupTableModalComponent } from '../../assets/assets/branch-lookup-table-modal.component';


@Component({
    selector: 'createOrEditJobFunctionAssetModal',
    templateUrl: './create-or-edit-jobFunctionAsset-modal.component.html'
})
export class CreateOrEditJobFunctionAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
    @ViewChild('jobFunctionLookupTableModal') jobFunctionLookupTableModal: JobFunctionLookupTableModalComponent;
    @ViewChild('branchLookupTableModal') branchLookupTableModal: BranchLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobFunctionAsset: CreateOrEditJobFunctionAssetDto = new CreateOrEditJobFunctionAssetDto();
    assetAssetName = '';
    jobFunctionJobName = '';
    branchBranchName = '';
    assetTypeAssetTypeName = '';
    AssetData: AssetLookupTableDto[] = new Array<AssetLookupTableDto>();
    BranchData: BranchLookupTableDto[] = new Array<BranchLookupTableDto>();


    Branches = '';

    constructor(
        injector: Injector,
        private _jobFunctionAssetsServiceProxy: JobFunctionAssetsServiceProxy,
        private _jobFunctionsServiceProxy: JobFunctionsServiceProxy
    ) {
        super(injector);

    }

    AddNewAsset(jobFunctionID?: number): void {
        this.Branches = '';
        this.AssetData = [];
        this.BranchData = [];
        this.jobFunctionAsset.jobFunctionId = 0;
        this.jobFunctionJobName = '';
        if (!jobFunctionID) {
            this.notify.error('No job function selected');
        } else {
           this._jobFunctionAssetsServiceProxy.checkIfJobCreated(jobFunctionID).subscribe(x => {
                if (x > 0) {
                    this.show(x);
                } else {
                    this._jobFunctionsServiceProxy.getJobFunctionForEdit(jobFunctionID).subscribe(result => {
                        this.jobFunctionAsset.jobFunctionId = result.jobFunction.id;
                        this.jobFunctionJobName = result.jobFunction.jobName;
                    });
                }
                this.active = true;
                this.modal.show();
           });
        }
    }

    show(jobFunctionAssetId?: number): void {
        console.log(jobFunctionAssetId);
        this.Branches = '';
        this.AssetData = [];
        this.BranchData = [];
        if (!jobFunctionAssetId) {
            this.jobFunctionAsset = new CreateOrEditJobFunctionAssetDto();
            this.jobFunctionAsset.id = jobFunctionAssetId;
            this.assetAssetName = '';
            this.jobFunctionJobName = '';
            this.branchBranchName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._jobFunctionAssetsServiceProxy.getSingleSelected(jobFunctionAssetId).subscribe(result => {


                this.Branches = result.selectedBranchType.toString();

                for (let x in result.jbAsset) {
                    if (result.jbAsset[x].assetID > 0) {
                        let Assetitem: AssetLookupTableDto = new AssetLookupTableDto();
                        Assetitem.id = result.jbAsset[x].assetID;
                        Assetitem.displayName = result.jbAsset[x].assetName;
                        Assetitem.assetDescription = result.jbAsset[x].assetDescription;
                        this.AssetData.push(Assetitem);
                    }
                }

                for (let x in result.jbAssetBranches) {
                    if (result.jbAssetBranches[x].branchID > 0) {
                        let Branchitem: BranchLookupTableDto = new BranchLookupTableDto();
                        Branchitem.id = result.jbAssetBranches[x].branchID;
                        Branchitem.displayName = result.jbAssetBranches[x].branchName;
                        Branchitem.branchCode = result.jbAssetBranches[x].branchCode;
                        this.BranchData.push(Branchitem);
                    }
                }
                this.jobFunctionAsset.jobFunctionId = result.jobFunctionID;
                this.jobFunctionJobName = result.jobFunction;
                this.jobFunctionAsset.id = jobFunctionAssetId;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.jobFunctionAsset.branchId = [];
        this.jobFunctionAsset.assetId = [];
        this.saving = true;
        this.jobFunctionAsset.assetId = this.AssetData;
        this.jobFunctionAsset.branchId = this.BranchData;
        this.jobFunctionAsset.jbAssetBranches = this.Branches;
        this._jobFunctionAssetsServiceProxy.createOrEdit(this.jobFunctionAsset)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    //JobFunctionModal
    openSelectJobFunctionModal() {
        this.jobFunctionLookupTableModal.id = this.jobFunctionAsset.jobFunctionId;
        this.jobFunctionLookupTableModal.displayName = this.jobFunctionJobName;
        this.jobFunctionLookupTableModal.show();
    }

    getNewJobFunctionId() {
        this.jobFunctionAsset.jobFunctionId = this.jobFunctionLookupTableModal.id;
        this.jobFunctionJobName = this.jobFunctionLookupTableModal.displayName;
    }

    openSelectAssetModal() {
        this.assetLookupTableModal.show(0);
    }

    openSelectBranchModal() {
        this.branchLookupTableModal.show();
    }

    setAssetIdNull() {
        this.AssetData = [];
    }

    setJobFunctionIdNull() {
        this.jobFunctionAsset.jobFunctionId = null;
        this.jobFunctionJobName = '';
    }

    setBranchIdNull() {
        this.BranchData = [];
    }

    getNewAssetId(asset: AssetLookupTableDto) {
        if (this.assetLookupTableModal.id > 0) {
            for (let x = 0; x < this.AssetData.length; x++) {
                if (this.AssetData[x].id === this.assetLookupTableModal.id) {
                    return;
                }
            }
            let item: AssetLookupTableDto = new AssetLookupTableDto();
            item.id = asset.id;
            item.displayName = asset.displayName;
            item.assetDescription = asset.assetDescription;
            this.AssetData.push(item);
        }
    }

    removeAssetId(AppID: number) {
        for (let x = 0; x < this.AssetData.length; x++) {
            if (this.AssetData[x].id === AppID) {
                this.AssetData.splice(x, 1);
                return;
            }
        }
    }

    getNewBranchId(brancho: BranchLookupTableDto) {
        //console.log(brancho);
        if (this.branchLookupTableModal.id > 0) {
            for (let x = 0; x < this.BranchData.length; x++) {
                if (this.BranchData[x].id === this.branchLookupTableModal.id) {
                    return;
                }
            }
            let item: BranchLookupTableDto = new BranchLookupTableDto();
            item.id = brancho.id;
            item.displayName = brancho.displayName;
            item.branchCode = brancho.branchCode;
            this.BranchData.push(item);
        }
    }

    removeBranchId(AppID: number) {
        for (let x = 0; x < this.BranchData.length; x++) {
            if (this.BranchData[x].id === AppID) {
                this.BranchData.splice(x, 1);
                return;
            }
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
