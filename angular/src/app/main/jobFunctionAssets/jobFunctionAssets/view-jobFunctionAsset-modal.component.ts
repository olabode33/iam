import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    GetJobFunctionAssetForView, JobFunctionAssetDto, AssetLookupTableDto, BranchLookupTableDto, JobFunctionAssetsServiceProxy, JobFunctionAssetAssetsList,
    JobFunctionAssetBranchesList, GetJobFunctionAssetForViewEvery } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RegisterTenantResultComponent } from 'account/register/register-tenant-result.component';

@Component({
    selector: 'viewJobFunctionAssetModal',
    templateUrl: './view-jobFunctionAsset-modal.component.html'
})
export class ViewJobFunctionAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    BData: number;
    item: GetJobFunctionAssetForViewEvery;
    JobName: string;
    SelectedBranches: string;
    AssetLists: JobFunctionAssetAssetsList[] = [];
    BranchList: JobFunctionAssetBranchesList[] = [];
    AssetData: AssetLookupTableDto[] = new Array<AssetLookupTableDto>();
    BranchData: BranchLookupTableDto[] = new Array<BranchLookupTableDto>();

    constructor(
        injector: Injector,
        private _jobFunctionAssetsServiceProxy: JobFunctionAssetsServiceProxy
    ) {
        super(injector);
        this.item = new GetJobFunctionAssetForViewEvery();
    }

    show(id: number): void {
        this.AssetData = [];
        this.BranchData = [];
        this._jobFunctionAssetsServiceProxy.getSingleSelected(id).subscribe(result => {
            this.JobName = result.jobFunction;
            for (let x in result.jbAsset) {
                if (result.jbAsset[x].assetID > 0) {
                    let Assetitem: AssetLookupTableDto = new AssetLookupTableDto();
                    Assetitem.id = result.jbAsset[x].assetID;
                    Assetitem.displayName = result.jbAsset[x].assetName;
                    Assetitem.assetDescription = result.jbAsset[x].assetDescription;
                    this.AssetData.push(Assetitem);
                }
            }
            if (result.selectedBranchType === 'S') {
                this.SelectedBranches = 'Specific Branches Attached';
                for (let x in result.jbAssetBranches) {
                    if (result.jbAssetBranches[x].branchID > 0) {
                        let Branchitem: BranchLookupTableDto = new BranchLookupTableDto();
                        Branchitem.id = result.jbAssetBranches[x].branchID;
                        Branchitem.displayName = result.jbAssetBranches[x].branchName;
                        Branchitem.branchCode = result.jbAssetBranches[x].branchCode;
                        this.BranchData.push(Branchitem);
                    }
                }
                this.BData = 1;
            } else if (result.selectedBranchType === 'A') {
                this.BData = 0;
                this.SelectedBranches = 'All Branches';
            } else if (result.selectedBranchType = 'N') {
                this.BData = 0;
                this.SelectedBranches = 'No Branch attached';
            }
        });
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
