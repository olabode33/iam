import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {JobFunctionAssetsServiceProxy, BranchLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'branchLookupTableModal2',
    styleUrls: ['./branch-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './branch-lookup-table-modal.component.html'
})
export class BranchLookupTableModalComponent extends AppComponentBase{

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id :number;
    displayName :string;
    branchCode: string;

    @Output() modalSave: EventEmitter<BranchLookupTableDto> = new EventEmitter<BranchLookupTableDto>();

    active = false;
    saving = false;
    

    constructor(
        injector: Injector,
        private _jobFunctionAssetsServiceProxy: JobFunctionAssetsServiceProxy
    ) {
        super(injector);
    }

    show(): void {
        this.active = true;
        this.paginator.rows = 10;
        this.getAll();
        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

		this._jobFunctionAssetsServiceProxy.getAllBranchForLookupTable(
			this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(branchM: BranchLookupTableDto) {
        console.log(branchM);
        this.id = branchM.id;
        this.displayName = branchM.displayName;
        this.branchCode = branchM.branchCode;

        this.active = false;
        this.modal.hide();
        this.modalSave.emit(branchM);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
