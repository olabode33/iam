import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { RequestEventsServiceProxy, CreateOrEditRequestEventDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RequestTypeLookupTableModalComponent } from '../../requestEvents/requestEvents/requestType-lookup-table-modal.component';
import { RequestLookupTableModalComponent } from '../../requestEvents/requestEvents/request-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';


@Component({
    selector: 'createOrEditRequestEventModal',
    templateUrl: './create-or-edit-requestEvent-modal.component.html'
})
export class CreateOrEditRequestEventModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('requestTypeLookupTableModal') requestTypeLookupTableModal: RequestTypeLookupTableModalComponent;
    @ViewChild('requestLookupTableModal') requestLookupTableModal: RequestLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    requestEvent: CreateOrEditRequestEventDto = new CreateOrEditRequestEventDto();
    requestTypeRequestCode = '';
    requestTenantId = '';
    userName = '';


    constructor(
        injector: Injector,
        private _requestEventsServiceProxy: RequestEventsServiceProxy
    ) {
        super(injector);
    }

    show(requestEventId?: number): void {
        if (!requestEventId) {
            this.requestEvent = new CreateOrEditRequestEventDto();
            this.requestEvent.id = requestEventId;
            this.requestTypeRequestCode = '';
            this.requestTenantId = '';
            this.userName = '';

            this.active = true;
            this.modal.show();
        }
        else {
            this._requestEventsServiceProxy.getRequestEventForEdit(requestEventId).subscribe(result => {
                this.requestEvent = result.requestEvent;
                this.requestTypeRequestCode = result.requestTypeRequestCode;
                this.requestTenantId = result.requestTenantId;
                this.userName = result.userName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this._requestEventsServiceProxy.createOrEdit(this.requestEvent)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectRequestTypeModal() {
        this.requestTypeLookupTableModal.id = this.requestEvent.requestTypeId;
        this.requestTypeLookupTableModal.displayName = this.requestTypeRequestCode;
        this.requestTypeLookupTableModal.show();
    }
    openSelectRequestModal() {
        this.requestLookupTableModal.id = this.requestEvent.requestId;
        this.requestLookupTableModal.displayName = this.requestTenantId;
        this.requestLookupTableModal.show();
    }
    openSelectUserModal() {
        this.userLookupTableModal.id = this.requestEvent.userId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }


    setRequestTypeIdNull() {
        this.requestEvent.requestTypeId = null;
        this.requestTypeRequestCode = '';
    }
    setRequestIdNull() {
        this.requestEvent.requestId = null;
        this.requestTenantId = '';
    }
    setUserIdNull() {
        this.requestEvent.userId = null;
        this.userName = '';
    }


    getNewRequestTypeId() {
        this.requestEvent.requestTypeId = this.requestTypeLookupTableModal.id;
        this.requestTypeRequestCode = this.requestTypeLookupTableModal.displayName;
    }
    getNewRequestId() {
        this.requestEvent.requestId = this.requestLookupTableModal.id;
        this.requestTenantId = this.requestLookupTableModal.displayName;
    }
    getNewUserId() {
        this.requestEvent.userId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
