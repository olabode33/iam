import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetRequestEventForView, RequestEventDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewRequestEventModal',
    templateUrl: './view-requestEvent-modal.component.html'
})
export class ViewRequestEventModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetRequestEventForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetRequestEventForView();
        this.item.requestEvent = new RequestEventDto();
    }

    show(item: GetRequestEventForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
