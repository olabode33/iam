import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { AssetTypesServiceProxy, CreateOrEditAssetTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditAssetTypeModal',
    templateUrl: './create-or-edit-assetType-modal.component.html'
})
export class CreateOrEditAssetTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    assetType: CreateOrEditAssetTypeDto = new CreateOrEditAssetTypeDto();
    asset_Properties: any[];

    constructor(
        injector: Injector,
        private _assetTypesServiceProxy: AssetTypesServiceProxy
    ) {
        super(injector);
    }

    show(assetTypeId?: number): void {
        if (!assetTypeId) {
            this.assetType = new CreateOrEditAssetTypeDto();
            this.assetType.id = assetTypeId;
            this.asset_Properties = [];
            this.active = true;
            this.modal.show();
        } else {
            this._assetTypesServiceProxy.getAssetTypeForEdit(assetTypeId).subscribe(result => {
                this.asset_Properties = [];
                for (let x in result.assetProperties) {
                    if (true) {
                        this.asset_Properties.push(result.assetProperties[x].propertyName);
                    }
                }
                this.assetType = result.assetType;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this.assetType.assetProperties = this.asset_Properties;
        this._assetTypesServiceProxy.createOrEdit(this.assetType)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }


    removeAssetProperty(item: string) {
        for (let x = 0; x < this.asset_Properties.length; x++) {
            if (this.asset_Properties[x] === item) {
                this.asset_Properties.splice(x, 1);
                return;
            }
        }
    }

    AddAssetProperty() {
        let data: string;
        data = this.assetType.assetTypeOtherDetails;
        for (let x = 0; x < this.asset_Properties.length; x++) {
            if (this.asset_Properties[x] === data) {
                this.assetType.assetTypeOtherDetails = '';
                return;
            }
        }
        if (data) { this.asset_Properties.push(data); }
        this.assetType.assetTypeOtherDetails = '';
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
