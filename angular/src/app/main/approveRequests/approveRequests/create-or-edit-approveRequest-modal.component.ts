import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ApproveRequestsServiceProxy, CreateOrEditApproveRequestDto, RejectRequestDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { RequestLookupTableModalComponent } from '../../requestEvents/requestEvents/request-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '../../requestEvents/requestEvents/user-lookup-table-modal.component';


@Component({
    selector: 'createOrEditApproveRequestModal',
    templateUrl: './create-or-edit-approveRequest-modal.component.html'
})
export class CreateOrEditApproveRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('requestLookupTableModal') requestLookupTableModal: RequestLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('userLookupTableModal2') userLookupTableModal2: UserLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    RejectRequest_: RejectRequestDto = new RejectRequestDto();
    requestTenantId = '';
    userName = '';
    userName2 = '';
    RejectionMessage = '';
    approveRequestID: number;


    constructor(
        injector: Injector,
        private _approveRequestsServiceProxy: ApproveRequestsServiceProxy
    ) {
        super(injector);
    }

    show(approveRequestId: number): void {
        this.approveRequestID = approveRequestId;
        this.active = true;
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this.RejectRequest_.requestId = this.approveRequestID;
        this.RejectRequest_.rejectionMessage = this.RejectionMessage;
        console.log(this.RejectionMessage);
        this._approveRequestsServiceProxy.rejectCompleteRequest(this.RejectRequest_)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info('Request Rejected Successfully.');
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
