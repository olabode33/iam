import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { ApproveRequestsServiceProxy, ApproveRequestDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditApproveRequestModalComponent } from './create-or-edit-approveRequest-modal.component';
import { ViewApproveRequestModalComponent } from './view-approveRequest-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './approveRequests.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ApproveRequestsComponent extends AppComponentBase {

    @ViewChild('createOrEditApproveRequestModal') createOrEditApproveRequestModal: CreateOrEditApproveRequestModalComponent;
    @ViewChild('viewApproveRequestModalComponent') viewApproveRequestModal: ViewApproveRequestModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    requestTenantIdFilter = '';
    userNameFilter = '';
    userName2Filter = '';
    lastExpiryDate: Date = new Date(2000, 1, 1);




    constructor(
        injector: Injector,
        private _approveRequestsServiceProxy: ApproveRequestsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getApproveRequests(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._approveRequestsServiceProxy.getAll(
            this.filterText,
            this.requestTenantIdFilter,
            this.userNameFilter,
            this.userName2Filter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            // console.log(result);
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createApproveRequest(approveRequestId): void {
        this.createOrEditApproveRequestModal.show(approveRequestId);
    }

    deleteApproveRequest(approveRequest: ApproveRequestDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._approveRequestsServiceProxy.delete(approveRequest.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._approveRequestsServiceProxy.getApproveRequestsToExcel(
            this.filterText,
            this.requestTenantIdFilter,
            this.userNameFilter,
            this.userName2Filter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    ApproveRequest(approveRequest) {
        //this._approveRequestsServiceProxy.approveRequest(approveRequest.approveRequest.id)
        //    .subscribe(() => {
        //        this.reloadPage();
        //        this.notify.success("Request Approved Successfully!");
        //    });
    }

    RejectRequest(rejectRequest) {
        this._approveRequestsServiceProxy.rejectRequest(rejectRequest.approveRequest.id)
            .subscribe(() => {
                this.reloadPage();
                this.notify.success('Request Rejected Successfully!');
            });
    }

}
