import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    GetApproveRequestForView,
    ApproveRequestDto,
    ApproveRequestsServiceProxy,
    GetRequestForEditOutput,
    AssetDto,
    AssetLookupTableDto,
    ConflictDto,
    RequestDto,
    CreateOrEditRequestDto,
    RequestAssetDto,
    RequestTypeDto,
    RejectRequestDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { ViewConflictModalComponent } from '../../conflicts/conflicts/view-conflict-modal.component';
import { ApproveRequestsComponent } from './approveRequests.component';

@Component({
    selector: 'viewApproveRequestModal',
    templateUrl: './view-approveRequest-modal.component.html'
})
export class ViewApproveRequestModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('viewConflictModalComponent') viewConflictModal: ViewConflictModalComponent;

    approveRequests: ApproveRequestsComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    //ApproveRequestDto
    AssetArray = AssetDto;
    item: any;
    ApplicationApplicationName = [];
    AssetList = [];
    AssetListSelected: AssetLookupTableDto[] = [];
    ApprovalRequest: ApproveRequestDto;
    ConflictItems = [];
    showConflicts: boolean;
    requestDetails: RequestDto;
    ConflictAssets: number[];
    requestAssets: RequestAssetDto;
    ConflictAssetsNames: string[];
    conflictItems: string;
    requestType: RequestTypeDto;
    SelectedID: number;
    RejectRequest: RejectRequestDto;

    constructor(
        injector: Injector,
        private _approveRequestsServiceProxy: ApproveRequestsServiceProxy
    ) {
        super(injector);
        this.showConflicts = false;
        this.ApprovalRequest = new ApproveRequestDto();
        this.requestDetails = new RequestDto();
        this.requestAssets = new RequestAssetDto();
        this.requestType = new RequestTypeDto();
        this.AssetList = [];
        this.RejectRequest = new RejectRequestDto();
    }

    show(id: number): void {
        this.showConflicts = false;
        this.ApplicationApplicationName = [];
        this.AssetListSelected = [];
        this.ConflictItems = [];
        this.SelectedID = id;
        this._approveRequestsServiceProxy
            .getSingleRequest(id)
            .subscribe(result => {
                this.requestDetails = result.requestDto;
                console.log(this.requestDetails);
                let numb: boolean = Object.keys(result.assets).length > 0;
                let conflictsnumb: boolean =
                    Object.keys(result.conflicts).length > 0;
                if (numb) {
                    for (let x in result.assets) {
                        if (numb) {
                            this.ConflictAssetsNames =
                                result.assets[x].assetConflictNames;
                            this.conflictItems = '';
                            if (conflictsnumb) {
                                for (let y in this.ConflictAssetsNames) {
                                    if (this.ConflictAssetsNames.length > 0) {
                                        this.conflictItems =
                                            this.conflictItems +
                                            this.ConflictAssetsNames[y] +
                                            ', ';
                                    }
                                }
                                this.conflictItems.slice(0, -2);
                            } else {
                                this.conflictItems = 'No Conflict';
                            }
                            this.ApplicationApplicationName.push({
                                id: result.assets[x].id,
                                displayName: result.assets[x].assetName,
                                assetDescription:
                                    result.assets[x].assetDescription,
                                conflictList: this.conflictItems
                            });
                        }
                    }
                    this.primengTableHelper.records = this.ApplicationApplicationName;
                    this.active = true;
                    this.modal.show();
                }
                if (conflictsnumb) {
                    for (let x in result.conflicts) {
                        if (conflictsnumb) {
                            this.ConflictItems.push({
                                conflictname: result.conflicts[x].conflictName,
                                conflictdescription:
                                    result.conflicts[x].conflictDescription,
                                id: result.conflicts[x].id,
                                conflictrisk: result.conflicts[x].conflictRisk
                            });
                        }
                    }
                    this.showConflicts = true;
                }
                this.requestType = result.requestType;
            });
    }

    reject(): void {
        this.AssetList = [];
        let SelectedNumber: boolean =
            Object.keys(this.AssetListSelected).length > 0;
        if (SelectedNumber) {
            for (let x in this.AssetListSelected) {
                if (SelectedNumber) {
                    this.AssetList.push(this.AssetListSelected[x].id);
                }
            }
            this.RejectRequest.assetId = this.AssetList;
            this.RejectRequest.requestId = this.SelectedID;
            this.RejectRequest.id = this.SelectedID;
            this._approveRequestsServiceProxy
                .rejectRequest(this.RejectRequest)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(result => {
                    this.notify.success('Request Rejected Successfully!');
                });
            this.approveRequests.reloadPage();
            this.active = false;
            this.modalSave.emit(null);
            this.close();
        } else {
            this.notify.error('Kindly select the assets you want to reject.');
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    approve(): void {
        this.AssetList = [];
        let SelectedNumber: boolean =
            Object.keys(this.AssetListSelected).length > 0;
        if (SelectedNumber) {
            for (let x in this.AssetListSelected) {
                if (SelectedNumber) {
                    this.AssetList.push(this.AssetListSelected[x].id);
                }
            }
            this.ApprovalRequest.assets = this.AssetList;
            this.ApprovalRequest.requestId = this.SelectedID;

            this._approveRequestsServiceProxy
                .approveRequestByAssets(this.ApprovalRequest)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.success('Request Approved Successfully');
                });
            this.approveRequests.reloadPage();
            this.active = false;
            this.modalSave.emit(null);
            this.close();
        } else {
            this.notify.error('Kindly select Assests to Approve.');
        }
    }
}
