import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ConflictsServiceProxy, CreateOrEditConflictDto, ConflictDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewConflictModal',
    templateUrl: './view-conflict-modal.component.html'
})
export class ViewConflictModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    ApplicationApplicationName = [];
    conflict: CreateOrEditConflictDto = new CreateOrEditConflictDto();


    constructor(
        injector: Injector,
        private _conflictsServiceProxy: ConflictsServiceProxy
    ) {
        super(injector);
        this.conflict = new CreateOrEditConflictDto();
    }

    show(id: number): void {
        this._conflictsServiceProxy.getSingleConflict(id).subscribe(result => {
            this.ApplicationApplicationName = result.assets;
            this.conflict = result.conflict;
        });
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
