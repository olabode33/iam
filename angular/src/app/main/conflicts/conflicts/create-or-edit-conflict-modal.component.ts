import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ConflictsServiceProxy, CreateOrEditConflictDto, AssetLookupTableDto, GetConflictForView } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';


@Component({
    selector: 'createOrEditConflictModal',
    templateUrl: './create-or-edit-conflict-modal.component.html'
})
export class CreateOrEditConflictModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    conflict: CreateOrEditConflictDto = new CreateOrEditConflictDto();
    assetAssetName = '';
    ApplicationApplicationName = [];
    AssetArray = [];
    RiskLevel = '';

    constructor(
        injector: Injector,
        private _conflictsServiceProxy: ConflictsServiceProxy
    ) {
        super(injector);
    }

    show(conflictId?: number): void {
        console.log(conflictId);
        this.ApplicationApplicationName = [];
        if (!conflictId) {
            this.conflict = new CreateOrEditConflictDto();
            this.conflict.id = conflictId;
            this.assetAssetName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._conflictsServiceProxy.getSingleConflict(conflictId).subscribe(result => {
                this.conflict = result.conflict;
                this.RiskLevel = result.conflict.conflictRisk;
                this.ApplicationApplicationName = result.assets;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this.AssetArray = [];
        for (let i = 0; i < this.ApplicationApplicationName.length; i++) {
            this.AssetArray.push(this.ApplicationApplicationName[i].id);
        }
        this.conflict.assets = this.AssetArray;
        this.conflict.conflictRisk = this.RiskLevel;
        this._conflictsServiceProxy.createOrEdit(this.conflict)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectAssetModal() {
        this.assetLookupTableModal.show(0);
    }

    openSelectAssetModalPersonal() {
        this.assetLookupTableModal.show(0);
    }

    setAssetIdNull() {

    }

    getNewAssetId(asset?: AssetLookupTableDto) {
        if (this.assetLookupTableModal.id > 0) {
            for (let x = 0; x < this.ApplicationApplicationName.length; x++) {
                if (this.ApplicationApplicationName[x].id === this.assetLookupTableModal.id) {
                    return;
                }
            }
            let item = {
                id: asset.id,
                displayName: asset.displayName,
                assetDescription: asset.assetDescription
            };
            this.ApplicationApplicationName.push(item);
        }
    }

    removeApplicationNameId(AppID: number) {
        for (let x = 0; x < this.ApplicationApplicationName.length; x++) {
            if (this.ApplicationApplicationName[x].id === AppID) {
                this.ApplicationApplicationName.splice(x, 1);
                return;
            }
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
