import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ViewApprovedRequestsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditViewApprovedRequestModal',
    templateUrl: './create-or-edit-viewApprovedRequest-modal.component.html'
})
export class CreateOrEditViewApprovedRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
	

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    	
    constructor(
        injector: Injector,
        private _viewApprovedRequestsServiceProxy: ViewApprovedRequestsServiceProxy
    ) {
        super(injector);
    }

    show(viewApprovedRequestId?: number): void {
        
    }

    save(): void {
			
    }

	

	

	

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
