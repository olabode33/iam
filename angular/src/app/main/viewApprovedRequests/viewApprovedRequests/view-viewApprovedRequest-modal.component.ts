import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewViewApprovedRequestModal',
    templateUrl: './view-viewApprovedRequest-modal.component.html'
})
export class ViewViewApprovedRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

	

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    show(): void {

    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
