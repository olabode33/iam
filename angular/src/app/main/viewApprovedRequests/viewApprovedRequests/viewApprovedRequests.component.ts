import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { RequestsServiceProxy, GetRequestForView  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditViewApprovedRequestModalComponent } from './create-or-edit-viewApprovedRequest-modal.component';
import { ViewViewApprovedRequestModalComponent } from './view-viewApprovedRequest-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './viewApprovedRequests.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ViewApprovedRequestsComponent extends AppComponentBase {

    @ViewChild('viewViewApprovedRequestModalComponent') viewViewApprovedRequestModal: ViewViewApprovedRequestModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;
	
    advancedFiltersAreShown = false;
    filterText = '';
    requestDescriptionFilter = '';
    maxExpiryDateFilter: moment.Moment;
    minExpiryDateFilter: moment.Moment;
    requestTypeRequestCodeFilter = '';
    userNameFilter = '';
    assetAssetNameFilter = '';
    lastExpiryDate: Date = new Date(2000, 1, 1);	
	

    constructor(
        injector: Injector,
        private _requestsServiceProxy: RequestsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }
	
    getViewApprovedRequests(event?: LazyLoadEvent) {
        //if (this.primengTableHelper.shouldResetPaging(event)) {
        //    this.paginator.changePage(0);
        //    return;
        //}

        //this.primengTableHelper.showLoadingIndicator();

        //this._requestsServiceProxy.getAll(
        //    this.filterText,
        //    this.requestDescriptionFilter,
        //    this.maxExpiryDateFilter,
        //    this.minExpiryDateFilter,
        //    this.requestTypeRequestCodeFilter,
        //    this.userNameFilter,
        //    this.assetAssetNameFilter,
        //    this.primengTableHelper.getSorting(this.dataTable),
        //    this.primengTableHelper.getSkipCount(this.paginator, event),
        //    this.primengTableHelper.getMaxResultCount(this.paginator, event)
        //).subscribe(result => {
        //    this.primengTableHelper.totalRecordsCount = result.totalCount;
        //    this.primengTableHelper.records = result.items;
        //    this.primengTableHelper.hideLoadingIndicator();
        //});
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createViewApprovedRequest(): void {
        
    }
	
    
	
}
