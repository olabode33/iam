import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { TreatRequestsServiceProxy, TreatRequestAssetDto, PagedResultDtoOfTreatRequestAssetDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { ViewTreatRequestModalComponent } from './view-treatRequest-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './treatRequests.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
    styleUrls: ['./treatRequest.css']
})
export class TreatRequestsComponent extends AppComponentBase {

    @ViewChild('viewTreatRequestModalComponent') viewTreatRequestModal: ViewTreatRequestModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    requestNameFilter = '';
    maxExpiryDateFilter: moment.Moment;
    minExpiryDateFilter: moment.Moment;
    lastExpiryDate: Date = new Date(2000, 1, 1);
    RejectedRequests = true;
    ApprovedRequests = true;
    pendingRequests = true;
    data: TreatRequestAssetDto[];
    AllData: PagedResultDtoOfTreatRequestAssetDto;

    constructor(
        injector: Injector,
        private _treatRequestsServiceProxy: TreatRequestsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.AllData = new PagedResultDtoOfTreatRequestAssetDto();
    }

    getTreatRequests(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._treatRequestsServiceProxy.getAll().subscribe(result => {
            this.AllData = result;
            this.ShowItems();
        });
    }

    ShowItems(): void {
        this.data = [];
        this.primengTableHelper.records = [];
        this.primengTableHelper.totalRecordsCount = 0;
        for (let x = 0; x < this.AllData.items.length; x++) {
            if (this.AllData.items[x].requestAssetTreated === 'A' && this.ApprovedRequests === true) {
                this.data.push(this.AllData.items[x]);
            } else if (this.AllData.items[x].requestAssetTreated === 'P' && this.pendingRequests === true) {
                this.data.push(this.AllData.items[x]);
            } else if (this.AllData.items[x].requestAssetTreated === 'R' && this.RejectedRequests === true) {
                this.data.push(this.AllData.items[x]);
            }
        }
        this.primengTableHelper.totalRecordsCount = this.data.length;
        this.primengTableHelper.records = this.data;
        this.primengTableHelper.hideLoadingIndicator();
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTreatRequest(): void {

    }

    exportToExcel(): void {
        this._treatRequestsServiceProxy.getTreatRequestsToExcel(
            this.filterText,
            this.requestNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    FilterData1(): void {
        if (this.pendingRequests) { this.pendingRequests = false; } else { this.pendingRequests = true; }
        this.ShowItems();
    }


    FilterData2(): void {
        if (this.ApprovedRequests) { this.ApprovedRequests = false; } else { this.ApprovedRequests = true; }
        this.ShowItems();
    }


    FilterData3(): void {
        if (this.RejectedRequests) { this.RejectedRequests = false; } else { this.RejectedRequests = true; }
        this.ShowItems();
    }
}
