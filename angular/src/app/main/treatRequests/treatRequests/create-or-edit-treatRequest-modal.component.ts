import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { TreatRequestsServiceProxy, CreateOrEditTreatRequestDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditTreatRequestModal',
    templateUrl: './create-or-edit-treatRequest-modal.component.html'
})
export class CreateOrEditTreatRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    treatRequest: CreateOrEditTreatRequestDto = new CreateOrEditTreatRequestDto();

    constructor(
        injector: Injector,
        private _treatRequestsServiceProxy: TreatRequestsServiceProxy
    ) {
        super(injector);
    }

    show(treatRequestId?: number): void {
        if (!treatRequestId) {
            this.treatRequest = new CreateOrEditTreatRequestDto();
            this.treatRequest.id = treatRequestId;

            this.active = true;
            this.modal.show();
        } else {
            this._treatRequestsServiceProxy.getTreatRequestForEdit(treatRequestId).subscribe(result => {
                this.treatRequest = result.treatRequest;

                this.active = true;
                this.modal.show();
            });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
