import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { TreatRequestsServiceProxy, GetRequestForView, TreatRequestAssetDto, RequestTypeDto, RequestDto, AssetTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewTreatRequestModal',
    templateUrl: './view-treatRequest-modal.component.html'
})
export class ViewTreatRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    item: TreatRequestAssetDto;
    request: RequestDto;
    requestType: RequestTypeDto;
    assetType: AssetTypeDto;

    constructor(
        injector: Injector,
        private _treatRequestsServiceProxy: TreatRequestsServiceProxy
    ) {
        super(injector);
        this.item = new TreatRequestAssetDto();
        this.request = new RequestDto();
        this.requestType = new RequestTypeDto();
        this.assetType = new AssetTypeDto();
    }

    show(item: TreatRequestAssetDto): void {
        this.item = item;
        this.request = item.requests;
        this.requestType = item.rType;
        this.assetType = item.assetType;
        this.active = true;
        this.modal.show();
    }

    treat(id: number): void {
        this._treatRequestsServiceProxy.requestTreated(this.item.id).subscribe(() => {
            this.active = false;
            this.modalSave.emit(null);
            this.modal.hide();
        });
    }

    reject(id: number): void {
        this._treatRequestsServiceProxy.requestRejected(this.item.id).subscribe(() => {
            this.active = false;
            this.modalSave.emit(null);
            this.modal.hide();
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
