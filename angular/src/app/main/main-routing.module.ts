import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TreatRequestsComponent } from './treatRequests/treatRequests/treatRequests.component';
import { ViewApprovedRequestsComponent } from './viewApprovedRequests/viewApprovedRequests/viewApprovedRequests.component';
import { ConflictsComponent } from './conflicts/conflicts/conflicts.component';
import { ApproveRequestsComponent } from './approveRequests/approveRequests/approveRequests.component';
import { UserSupervisorsComponent } from './userSupervisors/userSupervisors/userSupervisors.component';
import { UserPriviledgesComponent } from './userPriviledges/userPriviledges/userPriviledges.component';
import { RequestAssetsComponent } from './requestAssets/requestAssets/requestAssets.component';
import { RequestEventsComponent } from './requestEvents/requestEvents/requestEvents.component';
import { RequestsComponent } from './requests/requests/requests.component';
import { AssetPropertiesComponent } from './assetProperties/assetProperties/assetProperties.component';
import { JobFunctionAssetsComponent } from './jobFunctionAssets/jobFunctionAssets/jobFunctionAssets.component';
import { JobFunctionsComponent } from './jobFunctions/jobFunctions/jobFunctions.component';
import { AssetsComponent } from './assets/assets/assets.component';
import { RequestTypesComponent } from './requestTypes/requestTypes/requestTypes.component';
import { AssetTypesComponent } from './assetTypes/assetTypes/assetTypes.component';
import { ApplicationsComponent } from './applications/applications/applications.component';
import { BranchesComponent } from './branches/branches/branches.component';
import { DepartmentsComponent } from './departments/departments/departments.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'treatRequests/treatRequests', component: TreatRequestsComponent, data: { permission: 'Pages.TreatRequests' }  },
                    { path: 'viewApprovedRequests/viewApprovedRequests', component: ViewApprovedRequestsComponent, data: { permission: 'Pages.ViewApprovedRequests' }  },
                    { path: 'conflicts/conflicts', component: ConflictsComponent, data: { permission: 'Pages.Conflicts' }  },
                    { path: 'approveRequests/approveRequests', component: ApproveRequestsComponent, data: { permission: 'Pages.ApproveRequests' }  },
                    { path: 'userSupervisors/userSupervisors', component: UserSupervisorsComponent, data: { permission: 'Pages.UserSupervisors' }  },
                    { path: 'userPriviledges/userPriviledges', component: UserPriviledgesComponent, data: { permission: 'Pages.UserPriviledges' }  },
                    { path: 'requestAssets/requestAssets', component: RequestAssetsComponent, data: { permission: 'Pages.RequestAssets' }  },
                    { path: 'requestEvents/requestEvents', component: RequestEventsComponent, data: { permission: 'Pages.RequestEvents' }  },
                    { path: 'requests/requests', component: RequestsComponent, data: { permission: 'Pages.Requests' }  },
                    { path: 'assetProperties/assetProperties', component: AssetPropertiesComponent, data: { permission: 'Pages.AssetProperties' }  },
                    { path: 'jobFunctionAssets/jobFunctionAssets', component: JobFunctionAssetsComponent, data: { permission: 'Pages.JobFunctionAssets' }  },
                    { path: 'jobFunctions/jobFunctions', component: JobFunctionsComponent, data: { permission: 'Pages.JobFunctions' }  },
                    { path: 'assets/assets', component: AssetsComponent, data: { permission: 'Pages.Assets' }  },
                    { path: 'requestTypes/requestTypes', component: RequestTypesComponent, data: { permission: 'Pages.RequestTypes' }  },
                    { path: 'assetTypes/assetTypes', component: AssetTypesComponent, data: { permission: 'Pages.AssetTypes' }  },
                    { path: 'applications/applications', component: ApplicationsComponent, data: { permission: 'Pages.Applications' }  },
                    { path: 'branches/branches', component: BranchesComponent, data: { permission: 'Pages.Branches' }  },
                    { path: 'departments/departments', component: DepartmentsComponent, data: { permission: 'Pages.Departments' }  },
                    { path: 'dashboard', component: DashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' } }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
