import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { JobFunctionsServiceProxy, JobFunctionDto, JobFunctionAssetsServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditJobFunctionModalComponent } from './create-or-edit-jobFunction-modal.component';
import { CreateOrEditJobFunctionAssetModalComponent } from '../../jobFunctionAssets/jobFunctionAssets/create-or-edit-jobFunctionAsset-modal.component';
import { ViewJobFunctionModalComponent } from './view-jobFunction-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './jobFunctions.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class JobFunctionsComponent extends AppComponentBase {

    @ViewChild('createOrEditJobFunctionModal') createOrEditJobFunctionModal: CreateOrEditJobFunctionModalComponent;
    @ViewChild('createOrEditJobFunctionAssetModal') createOrEditJobFunctionAssetModal: CreateOrEditJobFunctionAssetModalComponent;
    @ViewChild('viewJobFunctionModalComponent') viewJobFunctionModal: ViewJobFunctionModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    jobCodeFilter = '';
    jobNameFilter = '';
    departmentDeptNameFilter = '';

    constructor(
        injector: Injector,
        private _jobFunctionsServiceProxy: JobFunctionsServiceProxy,
        private _jobFunctionAssetsServiceProxy: JobFunctionAssetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getJobFunctions(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._jobFunctionsServiceProxy.getAll(
            this.filterText,
            this.jobCodeFilter,
            this.jobNameFilter,
            this.departmentDeptNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createJobFunction(): void {
        this.createOrEditJobFunctionModal.show();
    }

    deleteJobFunction(jobFunction: JobFunctionDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._jobFunctionsServiceProxy.delete(jobFunction.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._jobFunctionsServiceProxy.getJobFunctionsToExcel(
            this.filterText,
            this.jobCodeFilter,
            this.jobNameFilter,
            this.departmentDeptNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    createJobFunctionAsset(id: number): void {
        this.createOrEditJobFunctionAssetModal.AddNewAsset(id);
    }

    getJobFunctionAssets(): void {

    }
}
