import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetJobFunctionForView, JobFunctionDto, JobFunctionsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewJobFunctionModal',
    templateUrl: './view-jobFunction-modal.component.html'
})
export class ViewJobFunctionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetJobFunctionForView;


    constructor(
        injector: Injector,
        private _jobFunctionsServiceProxy: JobFunctionsServiceProxy
    ) {
        super(injector);
        this.item = new GetJobFunctionForView();
        this.item.jobFunction = new JobFunctionDto();
    }

    show(jobFunctionId: number): void {
        this._jobFunctionsServiceProxy.getJobFuntionAndRelatedAssets(jobFunctionId).subscribe(
            result => {
                this.item = result;
            }
        );

        this.active = true;
        this.modal.show();
        // console.log(this.item);
    }

    close(): void {
        this.active = false;
        this.item = new GetJobFunctionForView();
        this.item.jobFunction = new JobFunctionDto();
        this.item.assetForView = null;
        this.modal.hide();
    }
}
