import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { JobFunctionsServiceProxy, CreateOrEditJobFunctionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DepartmentLookupTableModalComponent } from './department-lookup-table-modal.component';
import { CreateOrEditJobFunctionAssetModalComponent } from '@app/main/jobFunctionAssets/jobFunctionAssets/create-or-edit-jobFunctionAsset-modal.component';


@Component({
    selector: 'createOrEditJobFunctionModal',
    templateUrl: './create-or-edit-jobFunction-modal.component.html'
})
export class CreateOrEditJobFunctionModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('departmentLookupTableModal') departmentLookupTableModal: DepartmentLookupTableModalComponent;
    @ViewChild('createOrEditJobFunctionAssetModal') createOrEditJobFunctionAssetModal: CreateOrEditJobFunctionAssetModalComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobFunctionRelatedAssets: {};
    jobFunction: CreateOrEditJobFunctionDto = new CreateOrEditJobFunctionDto();
    departmentDeptName = '';


    constructor(
        injector: Injector,
        private _jobFunctionsServiceProxy: JobFunctionsServiceProxy
    ) {
        super(injector);
    }

    show(jobFunctionId?: number): void {
        if (!jobFunctionId) {
            this.jobFunction = new CreateOrEditJobFunctionDto();
            this.jobFunction.id = jobFunctionId;
            this.departmentDeptName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._jobFunctionsServiceProxy.getJobFunctionForEdit(jobFunctionId).subscribe(result => {
                this.jobFunction = result.jobFunction;
                this.departmentDeptName = result.departmentDeptName;


                this.active = true;
                this.modal.show();
            });

        }
    }

    save(): void {
        this.saving = true;
        this._jobFunctionsServiceProxy.createOrEdit(this.jobFunction)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectDepartmentModal() {
        this.departmentLookupTableModal.id = this.jobFunction.departmentId;
        this.departmentLookupTableModal.displayName = this.departmentDeptName;
        this.departmentLookupTableModal.show();
    }


    setDepartmentIdNull() {
        this.jobFunction.departmentId = null;
        this.departmentDeptName = '';
    }


    getNewDepartmentId() {
        this.jobFunction.departmentId = this.departmentLookupTableModal.id;
        this.departmentDeptName = this.departmentLookupTableModal.displayName;
    }

    createJobFunctionAsset(id: number): void {
        this.createOrEditJobFunctionAssetModal.AddNewAsset(id);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
