import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { RequestAssetsServiceProxy, RequestAssetDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditRequestAssetModalComponent } from './create-or-edit-requestAsset-modal.component';
import { ViewRequestAssetModalComponent } from './view-requestAsset-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './requestAssets.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RequestAssetsComponent extends AppComponentBase {

    @ViewChild('createOrEditRequestAssetModal') createOrEditRequestAssetModal: CreateOrEditRequestAssetModalComponent;
    @ViewChild('viewRequestAssetModalComponent') viewRequestAssetModal: ViewRequestAssetModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;
	
    advancedFiltersAreShown = false;
	filterText = '';
		assetRequestFilter = '';
		assetAssetNameFilter = '';
		requestTenantIdFilter = '';

	
	

    constructor(
        injector: Injector,
        private _requestAssetsServiceProxy: RequestAssetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }
	
    getRequestAssets(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._requestAssetsServiceProxy.getAll(
			this.filterText,
			this.assetRequestFilter,
			this.assetAssetNameFilter,
			this.requestTenantIdFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createRequestAsset(): void {
        this.createOrEditRequestAssetModal.show();
    }
	
    deleteRequestAsset(requestAsset: RequestAssetDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._requestAssetsServiceProxy.delete(requestAsset.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

	exportToExcel(): void {
        this._requestAssetsServiceProxy.getRequestAssetsToExcel(
		this.filterText,
			this.assetRequestFilter,
			this.assetAssetNameFilter,
			this.requestTenantIdFilter,
		)
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
