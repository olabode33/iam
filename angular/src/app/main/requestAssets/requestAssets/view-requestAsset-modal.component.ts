import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetRequestAssetForView, RequestAssetDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewRequestAssetModal',
    templateUrl: './view-requestAsset-modal.component.html'
})
export class ViewRequestAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetRequestAssetForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetRequestAssetForView();
        this.item.requestAsset = new RequestAssetDto();
    }

    show(item: GetRequestAssetForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
