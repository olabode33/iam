import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { RequestAssetsServiceProxy, CreateOrEditRequestAssetDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AssetLookupTableModalComponent } from '../../requests/requests/asset-lookup-table-modal.component';
import { RequestLookupTableModalComponent } from '../../requestEvents/requestEvents/request-lookup-table-modal.component';


@Component({
    selector: 'createOrEditRequestAssetModal',
    templateUrl: './create-or-edit-requestAsset-modal.component.html'
})
export class CreateOrEditRequestAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
	 @ViewChild('assetLookupTableModal') assetLookupTableModal: AssetLookupTableModalComponent;
	 @ViewChild('requestLookupTableModal') requestLookupTableModal: RequestLookupTableModalComponent;
	

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    requestAsset: CreateOrEditRequestAssetDto = new CreateOrEditRequestAssetDto();
	assetAssetName = '';
		 requestTenantId = '';
		 

    constructor(
        injector: Injector,
        private _requestAssetsServiceProxy: RequestAssetsServiceProxy
    ) {
        super(injector);
    }

    show(requestAssetId?: number): void {
        if (!requestAssetId) { 
			this.requestAsset = new CreateOrEditRequestAssetDto();
			this.requestAsset.id = requestAssetId;
			this.assetAssetName = '';
		 this.requestTenantId = '';
		 
			this.active = true;
			this.modal.show();
        }
		else{
			this._requestAssetsServiceProxy.getRequestAssetForEdit(requestAssetId).subscribe(result => {
				this.requestAsset = result.requestAsset;
				this.assetAssetName = result.assetAssetName;
		 this.requestTenantId = result.requestTenantId;
		 
				this.active = true;
				this.modal.show();
			});
		}  
    }

    save(): void {
			this.saving = true;
			this._requestAssetsServiceProxy.createOrEdit(this.requestAsset)
			 .pipe(finalize(() => { this.saving = false; }))
			 .subscribe(() => {
			    this.notify.info(this.l('SavedSuccessfully'));
				this.close();
				this.modalSave.emit(null);
             });
    }

	    openSelectAssetModal() {
        this.assetLookupTableModal.id = this.requestAsset.assetId;
        this.assetLookupTableModal.displayName = this.assetAssetName;
        this.assetLookupTableModal.show(0);
    }
	    openSelectRequestModal() {
        this.requestLookupTableModal.id = this.requestAsset.requestId;
        this.requestLookupTableModal.displayName = this.requestTenantId;
        this.requestLookupTableModal.show();
    }
	

	    setAssetIdNull() {
        this.requestAsset.assetId = null;
        this.assetAssetName = '';
    }
	    setRequestIdNull() {
        this.requestAsset.requestId = null;
        this.requestTenantId = '';
    }
	

	    getNewAssetId() {
        this.requestAsset.assetId = this.assetLookupTableModal.id;
        this.assetAssetName = this.assetLookupTableModal.displayName;
    }
	    getNewRequestId() {
        this.requestAsset.requestId = this.requestLookupTableModal.id;
        this.requestTenantId = this.requestLookupTableModal.displayName;
    }
	

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
