import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BranchesServiceProxy, CreateOrEditBranchDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';


@Component({
    selector: 'createOrEditBranchModal',
    templateUrl: './create-or-edit-branch-modal.component.html'
})
export class CreateOrEditBranchModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
	

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    branch: CreateOrEditBranchDto = new CreateOrEditBranchDto();
	

    constructor(
        injector: Injector,
        private _branchesServiceProxy: BranchesServiceProxy
    ) {
        super(injector);
    }

    show(branchId?: number): void {
        if (!branchId) { 
			this.branch = new CreateOrEditBranchDto();
			this.branch.id = branchId;
			
			this.active = true;
			this.modal.show();
        }
		else{
			this._branchesServiceProxy.getBranchForEdit(branchId).subscribe(result => {
				this.branch = result.branch;
				
				this.active = true;
				this.modal.show();
			});
		}  
    }

    save(): void {
			this.saving = true;
			this._branchesServiceProxy.createOrEdit(this.branch)
			 .pipe(finalize(() => { this.saving = false; }))
			 .subscribe(() => {
			    this.notify.info(this.l('SavedSuccessfully'));
				this.close();
				this.modalSave.emit(null);
             });
    }

	

	

	

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}