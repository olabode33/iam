import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetBranchForView, BranchDto, BranchesServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewBranchModal',
    templateUrl: './view-branch-modal.component.html'
})
export class ViewBranchModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetBranchForView;
    branch: BranchDto = new BranchDto();

    constructor(
        injector: Injector,
      private  _branchesServiceProxy : BranchesServiceProxy
    ) {
        super(injector);
        this.item = new GetBranchForView();
        this.item.branch = new BranchDto();
    }

    show(item: GetBranchForView): void {
        this._branchesServiceProxy.getBranch(item.branch.id).subscribe(result => {
            this.branch = result.branch;
            this.item = result;
            this.active = true;
            this.modal.show();
        });

        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
