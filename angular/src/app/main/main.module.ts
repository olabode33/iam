import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { TreatRequestsComponent } from './treatRequests/treatRequests/treatRequests.component';
import { ViewTreatRequestModalComponent } from './treatRequests/treatRequests/view-treatRequest-modal.component';
import { CreateOrEditTreatRequestModalComponent } from './treatRequests/treatRequests/create-or-edit-treatRequest-modal.component';

import { ViewApprovedRequestsComponent } from './viewApprovedRequests/viewApprovedRequests/viewApprovedRequests.component';
import { ViewViewApprovedRequestModalComponent } from './viewApprovedRequests/viewApprovedRequests/view-viewApprovedRequest-modal.component';
import { CreateOrEditViewApprovedRequestModalComponent } from './viewApprovedRequests/viewApprovedRequests/create-or-edit-viewApprovedRequest-modal.component';

import { ConflictsComponent } from './conflicts/conflicts/conflicts.component';
import { ViewConflictModalComponent } from './conflicts/conflicts/view-conflict-modal.component';
import { CreateOrEditConflictModalComponent } from './conflicts/conflicts/create-or-edit-conflict-modal.component';

import { ApproveRequestsComponent } from './approveRequests/approveRequests/approveRequests.component';
import { ViewApproveRequestModalComponent } from './approveRequests/approveRequests/view-approveRequest-modal.component';
import { CreateOrEditApproveRequestModalComponent } from './approveRequests/approveRequests/create-or-edit-approveRequest-modal.component';

import { RequestTypesComponent } from './requestTypes/requestTypes/requestTypes.component';
import { ViewRequestTypeModalComponent } from './requestTypes/requestTypes/view-requestType-modal.component';
import { CreateOrEditRequestTypeModalComponent } from './requestTypes/requestTypes/create-or-edit-requestType-modal.component';
import { AssetTypeLookupTableModalComponent } from './requestTypes/requestTypes/assetType-lookup-table-modal.component';

import { UserSupervisorsComponent } from './userSupervisors/userSupervisors/userSupervisors.component';
import { ViewUserSupervisorModalComponent } from './userSupervisors/userSupervisors/view-userSupervisor-modal.component';
import { CreateOrEditUserSupervisorModalComponent } from './userSupervisors/userSupervisors/create-or-edit-userSupervisor-modal.component';

import { UserPriviledgesComponent } from './userPriviledges/userPriviledges/userPriviledges.component';
import { ViewUserPriviledgeModalComponent } from './userPriviledges/userPriviledges/view-userPriviledge-modal.component';
import { CreateOrEditUserPriviledgeModalComponent } from './userPriviledges/userPriviledges/create-or-edit-userPriviledge-modal.component';

import { RequestAssetsComponent } from './requestAssets/requestAssets/requestAssets.component';
import { ViewRequestAssetModalComponent } from './requestAssets/requestAssets/view-requestAsset-modal.component';
import { CreateOrEditRequestAssetModalComponent } from './requestAssets/requestAssets/create-or-edit-requestAsset-modal.component';

import { RequestEventsComponent } from './requestEvents/requestEvents/requestEvents.component';
import { ViewRequestEventModalComponent } from './requestEvents/requestEvents/view-requestEvent-modal.component';
import { CreateOrEditRequestEventModalComponent } from './requestEvents/requestEvents/create-or-edit-requestEvent-modal.component';
import { RequestTypeLookupTableModalComponent } from './requestEvents/requestEvents/requestType-lookup-table-modal.component';
import { RequestLookupTableModalComponent } from './requestEvents/requestEvents/request-lookup-table-modal.component';
import { UserLookupTableModalComponent } from './requestEvents/requestEvents/user-lookup-table-modal.component';

import { RequestsComponent } from './requests/requests/requests.component';
import { ViewRequestModalComponent } from './requests/requests/view-request-modal.component';
import { CreateOrEditRequestModalComponent } from './requests/requests/create-or-edit-request-modal.component';

import { AssetPropertiesComponent } from './assetProperties/assetProperties/assetProperties.component';
import { ViewAssetPropertyModalComponent } from './assetProperties/assetProperties/view-assetProperty-modal.component';
import { CreateOrEditAssetPropertyModalComponent } from './assetProperties/assetProperties/create-or-edit-assetProperty-modal.component';

import { JobFunctionAssetsComponent } from './jobFunctionAssets/jobFunctionAssets/jobFunctionAssets.component';
import { ViewJobFunctionAssetModalComponent } from './jobFunctionAssets/jobFunctionAssets/view-jobFunctionAsset-modal.component';
import { CreateOrEditJobFunctionAssetModalComponent } from './jobFunctionAssets/jobFunctionAssets/create-or-edit-jobFunctionAsset-modal.component';
import { AssetLookupTableModalComponent } from './requests/requests/asset-lookup-table-modal.component';
//import { JobFunctionLookupTableModalComponent } from './jobFunctionAssets/jobFunctionAssets/jobFunction-lookup-table-modal.component';

import { JobFunctionsComponent } from './jobFunctions/jobFunctions/jobFunctions.component';
import { ViewJobFunctionModalComponent } from './jobFunctions/jobFunctions/view-jobFunction-modal.component';
import { CreateOrEditJobFunctionModalComponent } from './jobFunctions/jobFunctions/create-or-edit-jobFunction-modal.component';
//import { DepartmentLookupTableModalComponent } from './jobFunctions/jobFunctions/department-lookup-table-modal.component';

import { AssetsComponent } from './assets/assets/assets.component';
import { ViewAssetModalComponent } from './assets/assets/view-asset-modal.component';
import { CreateOrEditAssetModalComponent } from './assets/assets/create-or-edit-asset-modal.component';
import { ApplicationLookupTableModalComponent } from './assets/assets/application-lookup-table-modal.component';
import { BranchLookupTableModalComponent } from './assets/assets/branch-lookup-table-modal.component';
import { BranchLookupTableModalComponent as BranchLookupTableModalComponent2 } from './jobFunctionAssets/jobFunctionAssets/branch-lookup-table-modal.component';

//import { RequestTypesComponent } from './requestTypes/requestTypes/requestTypes.component';
//import { ViewRequestTypeModalComponent } from './requestTypes/requestTypes/view-requestType-modal.component';
//import { CreateOrEditRequestTypeModalComponent } from './requestTypes/requestTypes/create-or-edit-requestType-modal.component';

import { AssetTypesComponent } from './assetTypes/assetTypes/assetTypes.component';
import { ViewAssetTypeModalComponent } from './assetTypes/assetTypes/view-assetType-modal.component';
import { CreateOrEditAssetTypeModalComponent } from './assetTypes/assetTypes/create-or-edit-assetType-modal.component';

import { ApplicationsComponent } from './applications/applications/applications.component';
import { ViewApplicationModalComponent } from './applications/applications/view-application-modal.component';
import { CreateOrEditApplicationModalComponent } from './applications/applications/create-or-edit-application-modal.component';

import { BranchesComponent } from './branches/branches/branches.component';
import { ViewBranchModalComponent } from './branches/branches/view-branch-modal.component';
import { CreateOrEditBranchModalComponent } from './branches/branches/create-or-edit-branch-modal.component';

import { DepartmentsComponent } from './departments/departments/departments.component';
import { ViewDepartmentModalComponent } from './departments/departments/view-department-modal.component';
import { CreateOrEditDepartmentModalComponent } from './departments/departments/create-or-edit-department-modal.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng'; import { FileUploadModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';

import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule, TabsModule, TooltipModule, BsDropdownModule } from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';

import { MultiSelectModule } from 'primeng/multiselect';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
imports: [
	FileUploadModule,
		AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule, TableModule,

		CommonModule,
		FormsModule,
		ModalModule,
		TabsModule,
		TooltipModule,
		AppCommonModule,
		UtilsModule,
		MainRoutingModule,
		CountoModule,
		NgxChartsModule,
		BsDatepickerModule.forRoot(),
		BsDropdownModule.forRoot(),
		MultiSelectModule
	],
	declarations: [
		TreatRequestsComponent,
		ViewTreatRequestModalComponent, CreateOrEditTreatRequestModalComponent,
		ViewApprovedRequestsComponent,
		ViewViewApprovedRequestModalComponent, CreateOrEditViewApprovedRequestModalComponent,
		ConflictsComponent,
		ViewConflictModalComponent, CreateOrEditConflictModalComponent,
		BranchLookupTableModalComponent2,
		ApproveRequestsComponent,
		ViewApproveRequestModalComponent, CreateOrEditApproveRequestModalComponent,
		//RequestTypesComponent,
		ViewRequestTypeModalComponent, CreateOrEditRequestTypeModalComponent,
		AssetTypeLookupTableModalComponent,
		UserSupervisorsComponent,
		ViewUserSupervisorModalComponent, CreateOrEditUserSupervisorModalComponent,
		UserPriviledgesComponent,
		ViewUserPriviledgeModalComponent, CreateOrEditUserPriviledgeModalComponent,
		RequestAssetsComponent,
		ViewRequestAssetModalComponent, CreateOrEditRequestAssetModalComponent,
		RequestEventsComponent,
		ViewRequestEventModalComponent, CreateOrEditRequestEventModalComponent,
		RequestTypeLookupTableModalComponent,
		RequestLookupTableModalComponent,
		UserLookupTableModalComponent,
		RequestsComponent,
		ViewRequestModalComponent, CreateOrEditRequestModalComponent,
		AssetPropertiesComponent,
		ViewAssetPropertyModalComponent, CreateOrEditAssetPropertyModalComponent,
		JobFunctionAssetsComponent,
		ViewJobFunctionAssetModalComponent, CreateOrEditJobFunctionAssetModalComponent,
		AssetLookupTableModalComponent,
		//JobFunctionLookupTableModalComponent,
		JobFunctionsComponent,
		ViewJobFunctionModalComponent, CreateOrEditJobFunctionModalComponent,
		//DepartmentLookupTableModalComponent,
		AssetsComponent,
		ViewAssetModalComponent, CreateOrEditAssetModalComponent,
		ApplicationLookupTableModalComponent,
		BranchLookupTableModalComponent,
		RequestTypesComponent,
		ViewRequestTypeModalComponent, CreateOrEditRequestTypeModalComponent,
		AssetTypesComponent,
		ViewAssetTypeModalComponent, CreateOrEditAssetTypeModalComponent,
		ApplicationsComponent,
		ViewApplicationModalComponent, CreateOrEditApplicationModalComponent,
		BranchesComponent,
		ViewBranchModalComponent, CreateOrEditBranchModalComponent,
		DepartmentsComponent,
		ViewDepartmentModalComponent, CreateOrEditDepartmentModalComponent,
		DashboardComponent
	],
	providers: [
		{ provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
		{ provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
		{ provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale }
	]
})
export class MainModule { }
