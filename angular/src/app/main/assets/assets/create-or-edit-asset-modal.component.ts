import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import {
    AssetsServiceProxy,
    CreateOrEditAssetDto, GetAssetTypeOtherPropertyForView,
    AssetTypeOtherPropertiesServiceProxy, AssetTypesServiceProxy, AssetProps
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ApplicationLookupTableModalComponent } from './application-lookup-table-modal.component';
import { BranchLookupTableModalComponent } from './branch-lookup-table-modal.component';
import { AssetTypeLookupTableModalComponent } from '../../requestTypes/requestTypes/assetType-lookup-table-modal.component';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
    selector: 'createOrEditAssetModal',
    templateUrl: './create-or-edit-asset-modal.component.html'
})
export class CreateOrEditAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('applicationLookupTableModal') applicationLookupTableModal: ApplicationLookupTableModalComponent;
    @ViewChild('branchLookupTableModal') branchLookupTableModal: BranchLookupTableModalComponent;
    @ViewChild('assetTypeLookupTableModal') assetTypeLookupTableModal: AssetTypeLookupTableModalComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    showAssetProperties = false;

    showBranchAssetType: boolean;
    showApplicationAssetType: boolean;

    asset: CreateOrEditAssetDto = new CreateOrEditAssetDto();
    applicationApplicationName = '';
    branchBranchName = '';
    assetTypeAssetTypeName = '';
    assetTypeProperties: any[] = [];


    constructor(
        injector: Injector,
        private _assetsServiceProxy: AssetsServiceProxy,
        private _assetTypeServiceProxy: AssetTypesServiceProxy
    ) {
        super(injector);
        this.showBranchAssetType = false;
        this.showApplicationAssetType = false;
    }

    show(assetId?: number): void {
        if (!assetId) {
            this.asset = new CreateOrEditAssetDto();
            this.asset.id = assetId;
            this.applicationApplicationName = '';
            this.branchBranchName = '';
            this.assetTypeAssetTypeName = '';
            this.assetTypeProperties = [];
            this.active = true;
            this.modal.show();
        } else {
            this._assetsServiceProxy.getAssetForEdit(assetId).subscribe(result => {
                this.assetTypeProperties = [];
                this.SetAssetProperties(result.asset.assetTypeId);
                this.asset = result.asset;
                this.applicationApplicationName = result.applicationApplicationName;
                this.branchBranchName = result.branchBranchName;
                this.assetTypeAssetTypeName = result.assetTypeAssetTypeName;
                this.assetTypeProperties = result.assetProps;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;
        this.asset.assetProps = new Array<AssetProps>();
        for (let i = 0; i < this.assetTypeProperties.length; i++) {
            let item = new AssetProps();
            item.id = this.assetTypeProperties[i].id;
            item.value = this.assetTypeProperties[i].value;
            item.name = this.assetTypeProperties[i].name;
            console.log(item);
            this.asset.assetProps.push(item);
        }

        console.log(this.asset);
        this._assetsServiceProxy.createOrEdit(this.asset)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectApplicationModal() {
        this.applicationLookupTableModal.id = this.asset.applicationId;
        this.applicationLookupTableModal.displayName = this.applicationApplicationName;
        this.applicationLookupTableModal.show();
    }

    openSelectBranchModal() {
        this.branchLookupTableModal.id = this.asset.branchId;
        this.branchLookupTableModal.displayName = this.branchBranchName;
        this.branchLookupTableModal.show();
    }
    openSelectAssetTypeModal() {
        this.assetTypeLookupTableModal.id = this.asset.assetTypeId;
        this.assetTypeLookupTableModal.displayName = this.assetTypeAssetTypeName;
        this.assetTypeLookupTableModal.show();
    }


    setApplicationIdNull() {
        this.asset.applicationId = null;
        this.applicationApplicationName = '';
    }
    setBranchIdNull() {
        this.asset.branchId = null;
        this.branchBranchName = '';
    }
    setAssetTypeIdNull() {
        this.asset.assetTypeId = null;
        this.assetTypeAssetTypeName = '';
    }


    getNewApplicationId() {
        this.asset.applicationId = this.applicationLookupTableModal.id;
        this.applicationApplicationName = this.applicationLookupTableModal.displayName;
    }

    getNewBranchId() {
        this.asset.branchId = this.branchLookupTableModal.id;
        this.branchBranchName = this.branchLookupTableModal.displayName;
    }

    getAssetProperties(id) {
        this.assetTypeProperties = [];
        this._assetsServiceProxy.getAllProps('', '', '', id, '', 0, 100)
            .subscribe(result => {
                let assetTypeProp = [];
                for (let i = 0; i < result.items.length; i++) {
                    let item = {
                        id: result.items[i].assetTypeOtherProperty.id,
                        name: result.items[i].assetTypeOtherProperty.propertyName,
                        value: ''
                    };
                    this.assetTypeProperties.push(item);
                }
            });
    }



    SetAssetProperties(id) {
        this._assetTypeServiceProxy.getAssetTypeForEdit(id)
            .subscribe(result => {
                this.showApplicationAssetType = false;
                this.showBranchAssetType = false;
                this.showBranchAssetType = result.assetType.attachBranch;
                this.showApplicationAssetType = result.assetType.attachApplication;
            });
    }

    getNewAssetTypeId() {
        this.asset.assetTypeId = this.assetTypeLookupTableModal.id;
        this.assetTypeAssetTypeName = this.assetTypeLookupTableModal.displayName;

        this.SetAssetProperties(this.asset.assetTypeId);
        this.getAssetProperties(this.asset.assetTypeId);
    }

    close(): void {
        this.active = false;
        this.showBranchAssetType = false;
        this.showApplicationAssetType = false;
        this.modal.hide();
    }
}
