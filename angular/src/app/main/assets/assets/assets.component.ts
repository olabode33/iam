import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { AssetsServiceProxy, AssetDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditAssetModalComponent } from './create-or-edit-asset-modal.component';
import { ViewAssetModalComponent } from './view-asset-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './assets.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AssetsComponent extends AppComponentBase {

    @ViewChild('createOrEditAssetModal') createOrEditAssetModal: CreateOrEditAssetModalComponent;
    @ViewChild('viewAssetModalComponent') viewAssetModal: ViewAssetModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    assetNameFilter = '';
    assetCodeFilter = '';
    assetDescriptionFilter = '';
    applicationApplicationNameFilter = '';
    branchBranchNameFilter = '';
    assetTypeAssetTypeNameFilter = '';

    constructor(
        injector: Injector,
        private _assetsServiceProxy: AssetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAssets(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._assetsServiceProxy.getAll(
            this.filterText,
            this.assetNameFilter,
            this.assetCodeFilter,
            this.assetDescriptionFilter,
            this.applicationApplicationNameFilter,
            this.branchBranchNameFilter,
            this.assetTypeAssetTypeNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createAsset(): void {
        this.createOrEditAssetModal.show();
    }

    deleteAsset(asset: AssetDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._assetsServiceProxy.delete(asset.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._assetsServiceProxy.getAssetsToExcel(
            this.filterText,
            this.assetNameFilter,
            this.assetCodeFilter,
            this.assetDescriptionFilter,
            this.applicationApplicationNameFilter,
            this.branchBranchNameFilter,
            this.assetTypeAssetTypeNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
