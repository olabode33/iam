import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetAssetForView, AssetDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewAssetModal',
    templateUrl: './view-asset-modal.component.html'
})
export class ViewAssetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item : GetAssetForView;
	

    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetAssetForView();
        this.item.asset = new AssetDto();
    }

    show(item: GetAssetForView): void {
        console.log(item);
        this.item = item;
        this.active = true;
        this.modal.show();
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
