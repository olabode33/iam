using Test.Assets;
using Test.Requests;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestAssets
{
	[Table("RequestAssets")]
    public class RequestAsset : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string AssetRequest { get; set; }
		public virtual int? AssetId { get; set; }
		public Asset Asset { get; set; }
		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		public virtual string RequestAssetApproved { get; set; }
        public virtual string LastApprovedBy { get; set; }
        public virtual DateTime LastApprovedDate { get; set; }
        public virtual string RejectedBy { get; set; }
        public virtual DateTime RejectedDate { get; set; }
        public virtual string RequestAssetTreated { get; set; }
        public virtual DateTime RequestAssetTreatedDate { get; set; }
        public virtual string RequestAssetTreatedBy { get; set; }
    }
}