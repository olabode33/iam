using Test.Conflicts;
using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.ConflictAssets
{
	[Table("ConflictAssets")]
    public class ConflictAsset : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual int? ConflictId { get; set; }
		public Conflict Conflict { get; set; }
		
		public virtual int? AssetId { get; set; }
		public Asset Asset { get; set; }
		
    }
}