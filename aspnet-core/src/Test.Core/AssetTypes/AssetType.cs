using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.AssetTypes
{
	[Table("AssetTypes")]
    public class AssetType : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }		
		public virtual string AssetTypeName { get; set; }		
		public virtual string AssetTypeDesc { get; set; }		
		public virtual string AssetTypeOtherDetails { get; set; }		
		public virtual bool Permission { get; set; }		
		public virtual bool AttachBranch { get; set; }		
		public virtual bool AttachApplication { get; set; }
    }
}