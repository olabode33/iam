using Test.Authorization.Users;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.UserSupervisors
{
	[Table("UserSupervisors")]
    public class UserSupervisor : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual long? UserId { get; set; }
		public User User { get; set; }
		
		public virtual long SupervisorUserId { get; set; }
		public User SupervisorUser { get; set; }
		
    }
}