namespace Test.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_ConflictAssets = "Pages.ConflictAssets";
        public const string Pages_ConflictAssets_Create = "Pages.ConflictAssets.Create";
        public const string Pages_ConflictAssets_Edit = "Pages.ConflictAssets.Edit";
        public const string Pages_ConflictAssets_Delete = "Pages.ConflictAssets.Delete";

        public const string Pages_TreatRequests = "Pages.TreatRequests";
        public const string Pages_TreatRequests_Create = "Pages.TreatRequests.Create";
        public const string Pages_TreatRequests_Edit = "Pages.TreatRequests.Edit";
        public const string Pages_TreatRequests_Delete = "Pages.TreatRequests.Delete";

        public const string Pages_ViewApprovedRequests = "Pages.ViewApprovedRequests";
        public const string Pages_ViewApprovedRequests_Create = "Pages.ViewApprovedRequests.Create";
        public const string Pages_ViewApprovedRequests_Edit = "Pages.ViewApprovedRequests.Edit";
        public const string Pages_ViewApprovedRequests_Delete = "Pages.ViewApprovedRequests.Delete";

        public const string Pages_Conflicts = "Pages.Conflicts";
        public const string Pages_Conflicts_Create = "Pages.Conflicts.Create";
        public const string Pages_Conflicts_Edit = "Pages.Conflicts.Edit";
        public const string Pages_Conflicts_Delete = "Pages.Conflicts.Delete";

        public const string Pages_JobFunctionsAssetAssets = "Pages.JobFunctionsAssetAssets";
        public const string Pages_JobFunctionsAssetAssets_Create = "Pages.JobFunctionsAssetAssets.Create";
        public const string Pages_JobFunctionsAssetAssets_Edit = "Pages.JobFunctionsAssetAssets.Edit";
        public const string Pages_JobFunctionsAssetAssets_Delete = "Pages.JobFunctionsAssetAssets.Delete";

        public const string Pages_RequestRejections = "Pages.RequestRejections";
        public const string Pages_RequestRejections_Create = "Pages.RequestRejections.Create";
        public const string Pages_RequestRejections_Edit = "Pages.RequestRejections.Edit";
        public const string Pages_RequestRejections_Delete = "Pages.RequestRejections.Delete";

        public const string Pages_ApproveRequests = "Pages.ApproveRequests";
        public const string Pages_ApproveRequests_Create = "Pages.ApproveRequests.Create";
        public const string Pages_ApproveRequests_Edit = "Pages.ApproveRequests.Edit";
        public const string Pages_ApproveRequests_Delete = "Pages.ApproveRequests.Delete";

        public const string Pages_RequestTypesAssetTypes = "Pages.RequestTypesAssetTypes";
        public const string Pages_RequestTypesAssetTypes_Create = "Pages.RequestTypesAssetTypes.Create";
        public const string Pages_RequestTypesAssetTypes_Edit = "Pages.RequestTypesAssetTypes.Edit";
        public const string Pages_RequestTypesAssetTypes_Delete = "Pages.RequestTypesAssetTypes.Delete";

        public const string Pages_AssetTypeOtherProperties = "Pages.AssetTypeOtherProperties";
        public const string Pages_AssetTypeOtherProperties_Create = "Pages.AssetTypeOtherProperties.Create";
        public const string Pages_AssetTypeOtherProperties_Edit = "Pages.AssetTypeOtherProperties.Edit";
        public const string Pages_AssetTypeOtherProperties_Delete = "Pages.AssetTypeOtherProperties.Delete";

        public const string Pages_Administration_JobFunctionAssetBranches = "Pages.Administration.JobFunctionAssetBranches";
        public const string Pages_Administration_JobFunctionAssetBranches_Create = "Pages.Administration.JobFunctionAssetBranches.Create";
        public const string Pages_Administration_JobFunctionAssetBranches_Edit = "Pages.Administration.JobFunctionAssetBranches.Edit";
        public const string Pages_Administration_JobFunctionAssetBranches_Delete = "Pages.Administration.JobFunctionAssetBranches.Delete";

        public const string Pages_Administration_JobFunctionAssetBranchs = "Pages.Administration.JobFunctionAssetBranchs";
        public const string Pages_Administration_JobFunctionAssetBranchs_Create = "Pages.Administration.JobFunctionAssetBranchs.Create";
        public const string Pages_Administration_JobFunctionAssetBranchs_Edit = "Pages.Administration.JobFunctionAssetBranchs.Edit";
        public const string Pages_Administration_JobFunctionAssetBranchs_Delete = "Pages.Administration.JobFunctionAssetBranchs.Delete";

        public const string Pages_JobFunctionAssetBranchs = "Pages.JobFunctionAssetBranchs";
        public const string Pages_JobFunctionAssetBranchs_Create = "Pages.JobFunctionAssetBranchs.Create";
        public const string Pages_JobFunctionAssetBranchs_Edit = "Pages.JobFunctionAssetBranchs.Edit";
        public const string Pages_JobFunctionAssetBranchs_Delete = "Pages.JobFunctionAssetBranchs.Delete";

        public const string Pages_UserSupervisors = "Pages.UserSupervisors";
        public const string Pages_UserSupervisors_Create = "Pages.UserSupervisors.Create";
        public const string Pages_UserSupervisors_Edit = "Pages.UserSupervisors.Edit";
        public const string Pages_UserSupervisors_Delete = "Pages.UserSupervisors.Delete";

        public const string Pages_UserPriviledges = "Pages.UserPriviledges";
        public const string Pages_UserPriviledges_Create = "Pages.UserPriviledges.Create";
        public const string Pages_UserPriviledges_Edit = "Pages.UserPriviledges.Edit";
        public const string Pages_UserPriviledges_Delete = "Pages.UserPriviledges.Delete";

        public const string Pages_RequestAssets = "Pages.RequestAssets";
        public const string Pages_RequestAssets_Create = "Pages.RequestAssets.Create";
        public const string Pages_RequestAssets_Edit = "Pages.RequestAssets.Edit";
        public const string Pages_RequestAssets_Delete = "Pages.RequestAssets.Delete";

        public const string Pages_RequestEvents = "Pages.RequestEvents";
        public const string Pages_RequestEvents_Create = "Pages.RequestEvents.Create";
        public const string Pages_RequestEvents_Edit = "Pages.RequestEvents.Edit";
        public const string Pages_RequestEvents_Delete = "Pages.RequestEvents.Delete";

        public const string Pages_RequestApprovals = "Pages.RequestApprovals";
        public const string Pages_RequestApprovals_Create = "Pages.RequestApprovals.Create";
        public const string Pages_RequestApprovals_Edit = "Pages.RequestApprovals.Edit";
        public const string Pages_RequestApprovals_Delete = "Pages.RequestApprovals.Delete";

        public const string Pages_Requests = "Pages.Requests";
        public const string Pages_Requests_Create = "Pages.Requests.Create";
        public const string Pages_Requests_Edit = "Pages.Requests.Edit";
        public const string Pages_Requests_Delete = "Pages.Requests.Delete";

        public const string Pages_AssetProperties = "Pages.AssetProperties";
        public const string Pages_AssetProperties_Create = "Pages.AssetProperties.Create";
        public const string Pages_AssetProperties_Edit = "Pages.AssetProperties.Edit";
        public const string Pages_AssetProperties_Delete = "Pages.AssetProperties.Delete";

        public const string Pages_JobFunctionAssets = "Pages.JobFunctionAssets";
        public const string Pages_JobFunctionAssets_Create = "Pages.JobFunctionAssets.Create";
        public const string Pages_JobFunctionAssets_Edit = "Pages.JobFunctionAssets.Edit";
        public const string Pages_JobFunctionAssets_Delete = "Pages.JobFunctionAssets.Delete";

        public const string Pages_JobFunctions = "Pages.JobFunctions";
        public const string Pages_JobFunctions_Create = "Pages.JobFunctions.Create";
        public const string Pages_JobFunctions_Edit = "Pages.JobFunctions.Edit";
        public const string Pages_JobFunctions_Delete = "Pages.JobFunctions.Delete";

        public const string Pages_Assets = "Pages.Assets";
        public const string Pages_Assets_Create = "Pages.Assets.Create";
        public const string Pages_Assets_Edit = "Pages.Assets.Edit";
        public const string Pages_Assets_Delete = "Pages.Assets.Delete";

        public const string Pages_RequestTypes = "Pages.RequestTypes";
        public const string Pages_RequestTypes_Create = "Pages.RequestTypes.Create";
        public const string Pages_RequestTypes_Edit = "Pages.RequestTypes.Edit";
        public const string Pages_RequestTypes_Delete = "Pages.RequestTypes.Delete";

        public const string Pages_AssetTypes = "Pages.AssetTypes";
        public const string Pages_AssetTypes_Create = "Pages.AssetTypes.Create";
        public const string Pages_AssetTypes_Edit = "Pages.AssetTypes.Edit";
        public const string Pages_AssetTypes_Delete = "Pages.AssetTypes.Delete";

        public const string Pages_Applications = "Pages.Applications";
        public const string Pages_Applications_Create = "Pages.Applications.Create";
        public const string Pages_Applications_Edit = "Pages.Applications.Edit";
        public const string Pages_Applications_Delete = "Pages.Applications.Delete";

        public const string Pages_Branches = "Pages.Branches";
        public const string Pages_Branches_Create = "Pages.Branches.Create";
        public const string Pages_Branches_Edit = "Pages.Branches.Edit";
        public const string Pages_Branches_Delete = "Pages.Branches.Delete";

        public const string Pages_Departments = "Pages.Departments";
        public const string Pages_Departments_Create = "Pages.Departments.Create";
        public const string Pages_Departments_Edit = "Pages.Departments.Edit";
        public const string Pages_Departments_Delete = "Pages.Departments.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}