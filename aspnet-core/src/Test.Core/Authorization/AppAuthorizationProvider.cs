using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Test.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var conflictAssets = pages.CreateChildPermission(AppPermissions.Pages_ConflictAssets, L("ConflictAssets"));
            conflictAssets.CreateChildPermission(AppPermissions.Pages_ConflictAssets_Create, L("CreateNewConflictAsset"));
            conflictAssets.CreateChildPermission(AppPermissions.Pages_ConflictAssets_Edit, L("EditConflictAsset"));
            conflictAssets.CreateChildPermission(AppPermissions.Pages_ConflictAssets_Delete, L("DeleteConflictAsset"));



            var treatRequests = pages.CreateChildPermission(AppPermissions.Pages_TreatRequests, L("TreatRequests"));
            treatRequests.CreateChildPermission(AppPermissions.Pages_TreatRequests_Create, L("CreateNewTreatRequest"));
            treatRequests.CreateChildPermission(AppPermissions.Pages_TreatRequests_Edit, L("EditTreatRequest"));
            treatRequests.CreateChildPermission(AppPermissions.Pages_TreatRequests_Delete, L("DeleteTreatRequest"));



            var viewApprovedRequests = pages.CreateChildPermission(AppPermissions.Pages_ViewApprovedRequests, L("ViewApprovedRequests"));
            viewApprovedRequests.CreateChildPermission(AppPermissions.Pages_ViewApprovedRequests_Create, L("CreateNewViewApprovedRequest"));
            viewApprovedRequests.CreateChildPermission(AppPermissions.Pages_ViewApprovedRequests_Edit, L("EditViewApprovedRequest"));
            viewApprovedRequests.CreateChildPermission(AppPermissions.Pages_ViewApprovedRequests_Delete, L("DeleteViewApprovedRequest"));



            var conflicts = pages.CreateChildPermission(AppPermissions.Pages_Conflicts, L("Conflicts"));
            conflicts.CreateChildPermission(AppPermissions.Pages_Conflicts_Create, L("CreateNewConflict"));
            conflicts.CreateChildPermission(AppPermissions.Pages_Conflicts_Edit, L("EditConflict"));
            conflicts.CreateChildPermission(AppPermissions.Pages_Conflicts_Delete, L("DeleteConflict"));



            var jobFunctionsAssetAssets = pages.CreateChildPermission(AppPermissions.Pages_JobFunctionsAssetAssets, L("JobFunctionsAssetAssets"));
            jobFunctionsAssetAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionsAssetAssets_Create, L("CreateNewJobFunctionsAssetAsset"));
            jobFunctionsAssetAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionsAssetAssets_Edit, L("EditJobFunctionsAssetAsset"));
            jobFunctionsAssetAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionsAssetAssets_Delete, L("DeleteJobFunctionsAssetAsset"));



            var requestRejections = pages.CreateChildPermission(AppPermissions.Pages_RequestRejections, L("RequestRejections"));
            requestRejections.CreateChildPermission(AppPermissions.Pages_RequestRejections_Create, L("CreateNewRequestRejection"));
            requestRejections.CreateChildPermission(AppPermissions.Pages_RequestRejections_Edit, L("EditRequestRejection"));
            requestRejections.CreateChildPermission(AppPermissions.Pages_RequestRejections_Delete, L("DeleteRequestRejection"));



            var approveRequests = pages.CreateChildPermission(AppPermissions.Pages_ApproveRequests, L("ApproveRequests"));
            approveRequests.CreateChildPermission(AppPermissions.Pages_ApproveRequests_Create, L("CreateNewApproveRequest"));
            approveRequests.CreateChildPermission(AppPermissions.Pages_ApproveRequests_Edit, L("EditApproveRequest"));
            approveRequests.CreateChildPermission(AppPermissions.Pages_ApproveRequests_Delete, L("DeleteApproveRequest"));



            var requestTypesAssetTypes = pages.CreateChildPermission(AppPermissions.Pages_RequestTypesAssetTypes, L("RequestTypesAssetTypes"));
            requestTypesAssetTypes.CreateChildPermission(AppPermissions.Pages_RequestTypesAssetTypes_Create, L("CreateNewRequestTypesAssetType"));
            requestTypesAssetTypes.CreateChildPermission(AppPermissions.Pages_RequestTypesAssetTypes_Edit, L("EditRequestTypesAssetType"));
            requestTypesAssetTypes.CreateChildPermission(AppPermissions.Pages_RequestTypesAssetTypes_Delete, L("DeleteRequestTypesAssetType"));



            var assetTypeOtherProperties = pages.CreateChildPermission(AppPermissions.Pages_AssetTypeOtherProperties, L("AssetTypeOtherProperties"));
            assetTypeOtherProperties.CreateChildPermission(AppPermissions.Pages_AssetTypeOtherProperties_Create, L("CreateNewAssetTypeOtherProperty"));
            assetTypeOtherProperties.CreateChildPermission(AppPermissions.Pages_AssetTypeOtherProperties_Edit, L("EditAssetTypeOtherProperty"));
            assetTypeOtherProperties.CreateChildPermission(AppPermissions.Pages_AssetTypeOtherProperties_Delete, L("DeleteAssetTypeOtherProperty"));



            var jobFunctionAssetBranchs = pages.CreateChildPermission(AppPermissions.Pages_JobFunctionAssetBranchs, L("JobFunctionAssetBranchs"));
            jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_JobFunctionAssetBranchs_Create, L("CreateNewJobFunctionAssetBranch"));
            jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_JobFunctionAssetBranchs_Edit, L("EditJobFunctionAssetBranch"));
            jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_JobFunctionAssetBranchs_Delete, L("DeleteJobFunctionAssetBranch"));



            var userSupervisors = pages.CreateChildPermission(AppPermissions.Pages_UserSupervisors, L("UserSupervisors"));
            userSupervisors.CreateChildPermission(AppPermissions.Pages_UserSupervisors_Create, L("CreateNewUserSupervisor"));
            userSupervisors.CreateChildPermission(AppPermissions.Pages_UserSupervisors_Edit, L("EditUserSupervisor"));
            userSupervisors.CreateChildPermission(AppPermissions.Pages_UserSupervisors_Delete, L("DeleteUserSupervisor"));



            var userPriviledges = pages.CreateChildPermission(AppPermissions.Pages_UserPriviledges, L("UserPriviledges"));
            userPriviledges.CreateChildPermission(AppPermissions.Pages_UserPriviledges_Create, L("CreateNewUserPriviledge"));
            userPriviledges.CreateChildPermission(AppPermissions.Pages_UserPriviledges_Edit, L("EditUserPriviledge"));
            userPriviledges.CreateChildPermission(AppPermissions.Pages_UserPriviledges_Delete, L("DeleteUserPriviledge"));



            var requestAssets = pages.CreateChildPermission(AppPermissions.Pages_RequestAssets, L("RequestAssets"));
            requestAssets.CreateChildPermission(AppPermissions.Pages_RequestAssets_Create, L("CreateNewRequestAsset"));
            requestAssets.CreateChildPermission(AppPermissions.Pages_RequestAssets_Edit, L("EditRequestAsset"));
            requestAssets.CreateChildPermission(AppPermissions.Pages_RequestAssets_Delete, L("DeleteRequestAsset"));



            var requestEvents = pages.CreateChildPermission(AppPermissions.Pages_RequestEvents, L("RequestEvents"));
            requestEvents.CreateChildPermission(AppPermissions.Pages_RequestEvents_Create, L("CreateNewRequestEvent"));
            requestEvents.CreateChildPermission(AppPermissions.Pages_RequestEvents_Edit, L("EditRequestEvent"));
            requestEvents.CreateChildPermission(AppPermissions.Pages_RequestEvents_Delete, L("DeleteRequestEvent"));



            var requestApprovals = pages.CreateChildPermission(AppPermissions.Pages_RequestApprovals, L("RequestApprovals"));
            requestApprovals.CreateChildPermission(AppPermissions.Pages_RequestApprovals_Create, L("CreateNewRequestApproval"));
            requestApprovals.CreateChildPermission(AppPermissions.Pages_RequestApprovals_Edit, L("EditRequestApproval"));
            requestApprovals.CreateChildPermission(AppPermissions.Pages_RequestApprovals_Delete, L("DeleteRequestApproval"));



            var requests = pages.CreateChildPermission(AppPermissions.Pages_Requests, L("Requests"));
            requests.CreateChildPermission(AppPermissions.Pages_Requests_Create, L("CreateNewRequest"));
            requests.CreateChildPermission(AppPermissions.Pages_Requests_Edit, L("EditRequest"));
            requests.CreateChildPermission(AppPermissions.Pages_Requests_Delete, L("DeleteRequest"));



            var assetProperties = pages.CreateChildPermission(AppPermissions.Pages_AssetProperties, L("AssetProperties"));
            assetProperties.CreateChildPermission(AppPermissions.Pages_AssetProperties_Create, L("CreateNewAssetProperty"));
            assetProperties.CreateChildPermission(AppPermissions.Pages_AssetProperties_Edit, L("EditAssetProperty"));
            assetProperties.CreateChildPermission(AppPermissions.Pages_AssetProperties_Delete, L("DeleteAssetProperty"));



            var jobFunctionAssets = pages.CreateChildPermission(AppPermissions.Pages_JobFunctionAssets, L("JobFunctionAssets"));
            jobFunctionAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionAssets_Create, L("CreateNewJobFunctionAsset"));
            jobFunctionAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionAssets_Edit, L("EditJobFunctionAsset"));
            jobFunctionAssets.CreateChildPermission(AppPermissions.Pages_JobFunctionAssets_Delete, L("DeleteJobFunctionAsset"));



            var jobFunctions = pages.CreateChildPermission(AppPermissions.Pages_JobFunctions, L("JobFunctions"));
            jobFunctions.CreateChildPermission(AppPermissions.Pages_JobFunctions_Create, L("CreateNewJobFunction"));
            jobFunctions.CreateChildPermission(AppPermissions.Pages_JobFunctions_Edit, L("EditJobFunction"));
            jobFunctions.CreateChildPermission(AppPermissions.Pages_JobFunctions_Delete, L("DeleteJobFunction"));



            var assets = pages.CreateChildPermission(AppPermissions.Pages_Assets, L("Assets"));
            assets.CreateChildPermission(AppPermissions.Pages_Assets_Create, L("CreateNewAsset"));
            assets.CreateChildPermission(AppPermissions.Pages_Assets_Edit, L("EditAsset"));
            assets.CreateChildPermission(AppPermissions.Pages_Assets_Delete, L("DeleteAsset"));



            var requestTypes = pages.CreateChildPermission(AppPermissions.Pages_RequestTypes, L("RequestTypes"));
            requestTypes.CreateChildPermission(AppPermissions.Pages_RequestTypes_Create, L("CreateNewRequestType"));
            requestTypes.CreateChildPermission(AppPermissions.Pages_RequestTypes_Edit, L("EditRequestType"));
            requestTypes.CreateChildPermission(AppPermissions.Pages_RequestTypes_Delete, L("DeleteRequestType"));



            var assetTypes = pages.CreateChildPermission(AppPermissions.Pages_AssetTypes, L("AssetTypes"));
            assetTypes.CreateChildPermission(AppPermissions.Pages_AssetTypes_Create, L("CreateNewAssetType"));
            assetTypes.CreateChildPermission(AppPermissions.Pages_AssetTypes_Edit, L("EditAssetType"));
            assetTypes.CreateChildPermission(AppPermissions.Pages_AssetTypes_Delete, L("DeleteAssetType"));



            var applications = pages.CreateChildPermission(AppPermissions.Pages_Applications, L("Applications"));
            applications.CreateChildPermission(AppPermissions.Pages_Applications_Create, L("CreateNewApplication"));
            applications.CreateChildPermission(AppPermissions.Pages_Applications_Edit, L("EditApplication"));
            applications.CreateChildPermission(AppPermissions.Pages_Applications_Delete, L("DeleteApplication"));



            var branches = pages.CreateChildPermission(AppPermissions.Pages_Branches, L("Branches"));
            branches.CreateChildPermission(AppPermissions.Pages_Branches_Create, L("CreateNewBranch"));
            branches.CreateChildPermission(AppPermissions.Pages_Branches_Edit, L("EditBranch"));
            branches.CreateChildPermission(AppPermissions.Pages_Branches_Delete, L("DeleteBranch"));



            var departments = pages.CreateChildPermission(AppPermissions.Pages_Departments, L("Departments"));
            departments.CreateChildPermission(AppPermissions.Pages_Departments_Create, L("CreateNewDepartment"));
            departments.CreateChildPermission(AppPermissions.Pages_Departments_Edit, L("EditDepartment"));
            departments.CreateChildPermission(AppPermissions.Pages_Departments_Delete, L("DeleteDepartment"));


            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var jobFunctionAssetBranches = administration.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranches, L("JobFunctionAssetBranches"));
            jobFunctionAssetBranches.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Create, L("CreateNewJobFunctionAssetBranche"));
            jobFunctionAssetBranches.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Edit, L("EditJobFunctionAssetBranche"));
            jobFunctionAssetBranches.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Delete, L("DeleteJobFunctionAssetBranche"));



            //var jobFunctionAssetBranchs = administration.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranchs, L("JobFunctionAssetBranchs"));
            //jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Create, L("CreateNewJobFunctionAssetBranch"));
            //jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Edit, L("EditJobFunctionAssetBranch"));
            //jobFunctionAssetBranchs.CreateChildPermission(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Delete, L("DeleteJobFunctionAssetBranch"));



            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TestConsts.LocalizationSourceName);
        }
    }
}
