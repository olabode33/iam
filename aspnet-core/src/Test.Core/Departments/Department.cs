using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Departments
{
	[Table("Departments")]
    public class Department : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string DeptCode { get; set; }
		
		public virtual string DeptName { get; set; }
		

    }
}