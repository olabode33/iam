using Test.AssetTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.AssetTypeOtherProperties
{
	[Table("AssetTypeOtherProperties")]
    public class AssetTypeOtherProperty : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string PropertyName { get; set; }
		public virtual int? AssetTypeId { get; set; }
		public AssetType AssetType { get; set; }
		
    }
}