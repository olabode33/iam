using Test.JobFunctions;
using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Test.JobFunctionAssets;

namespace Test.JobFunctionsAssetAssets
{
	[Table("JobFunctionsAssetAssets")]
    public class JobFunctionsAssetAsset : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual int JobFunctionAssetId { get; set; }
		public JobFunctionAsset JobFunctionAsset { get; set; }
		
		public virtual int AssetId { get; set; }
		public Asset Asset { get; set; }

        public string SelectedBranches { get; set; }
		
    }
}