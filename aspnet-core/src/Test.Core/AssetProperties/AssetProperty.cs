using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.AssetProperties
{
	[Table("AssetProperties")]
    public class AssetProperty : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual int? PropertyOfAsset { get; set; }
		
		public virtual string PropertyValue { get; set; }
		
		public virtual int? AssetId { get; set; }
		public Asset Asset { get; set; }
		
    }
}