using Test.AssetTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestTypes
{
	[Table("RequestTypes")]
    public class RequestType : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string RequestCode { get; set; }		
		public virtual string RequestDescription { get; set; }		
		public virtual string RequestOtherDetails { get; set; }		
		public virtual int ApprovalNo { get; set; }		
		public virtual bool RequestExpiry { get; set; }
		public virtual int? AssetTypeId { get; set; }
		public AssetType AssetType { get; set; }		
        public virtual int? ReverseRequestType { get; set; }
    }
}