using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.ViewApprovedRequests
{
	[Table("ViewApprovedRequests")]
    public class ViewApprovedRequest : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string RequestName { get; set; }
		

    }
}