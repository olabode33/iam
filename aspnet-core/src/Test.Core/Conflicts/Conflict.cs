using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Conflicts
{
	[Table("Conflicts")]
    public class Conflict : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string ConflictName { get; set; }		
		public virtual string ConflictDescription { get; set; }		
        public virtual string ConflictRisk { get; set; }
    }
}