using Test.RequestTypes;
using Test.Requests;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestEvents
{
	[Table("RequestEvents")]
    public class RequestEvent : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string Event { get; set; }
		
		public virtual string EventDescription { get; set; }
		
		public virtual DateTime EventDate { get; set; }
		

		public virtual int? RequestTypeId { get; set; }
		public RequestType RequestType { get; set; }
		
		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		
		public virtual long? UserId { get; set; }
		public User User { get; set; }
		
    }
}