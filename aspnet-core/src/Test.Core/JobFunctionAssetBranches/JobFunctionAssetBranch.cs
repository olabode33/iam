using Test.JobFunctionAssets;
using Test.Branches;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.JobFunctionAssetBranches
{
	[Table("JobFunctionAssetBranchs")]
    public class JobFunctionAssetBranch : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual int JobFunctionAssetId { get; set; }
		public JobFunctionAsset JobFunctionAsset { get; set; }
		
		public virtual int? BranchId { get; set; }
		public Branch Branch { get; set; }
		
    }
}