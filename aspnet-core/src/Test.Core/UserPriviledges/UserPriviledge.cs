using Test.Requests;
using Test.Authorization.Users;
using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.UserPriviledges
{
	[Table("UserPriviledges")]
    public class UserPriviledge : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual bool Status { get; set; }
		
		public virtual DateTime StartDate { get; set; }
		
		public virtual string ApplicationUsername { get; set; }
		
		public virtual DateTime EndDate { get; set; }
		

		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		
		public virtual long? UserId { get; set; }
		public User User { get; set; }
		
		public virtual int? AssetId { get; set; }
		public Asset Asset { get; set; }
		
    }
}