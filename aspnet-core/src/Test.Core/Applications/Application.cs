using Test;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Applications
{
	[Table("Applications")]
    public class Application : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string ApplicationName { get; set; }
		
		public virtual string ApplicationDesc { get; set; }
		
		public virtual string URL { get; set; }
		
		public virtual string ApplicationUsername { get; set; }
		
		public virtual string ApplicationPassword { get; set; }
		
		public virtual ApplicationImportance Importance { get; set; }
		

    }
}