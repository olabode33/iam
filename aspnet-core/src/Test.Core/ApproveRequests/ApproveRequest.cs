using Test.Requests;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.ApproveRequests
{
	[Table("ApproveRequests")]
    public class ApproveRequest : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual bool ApprovalStatus { get; set; }
		public virtual bool PriorApprovalStatus { get; set; }
		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		public virtual long ApproverID { get; set; }
		public User Approver { get; set; }
		public virtual long PriorApprover { get; set; }
		public User PriorApprov { get; set; }
        public virtual DateTime? PriorApprovalDate { get; set; }
        public virtual DateTime? ApproveDate { get; set; }
		public virtual int? AssetID { get; set; }
    }
}