using Test.Assets;
using Test.JobFunctions;
using Test.Branches;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.JobFunctionAssets
{
	[Table("JobFunctionAssets")]
    public class JobFunctionAsset : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		
		public virtual int? JobFunctionId { get; set; }
		public JobFunction JobFunction { get; set; }
		public string SelectedbranchType { get; set; }
		
    }
}