using Test.Departments;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.JobFunctions
{
	[Table("JobFunctions")]
    public class JobFunction : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		
		public virtual string JobCode { get; set; }
		
		public virtual string JobName { get; set; }
		
		public virtual int? DepartmentId { get; set; }
		public Department Department { get; set; }
		
        //public virtual JobFunctionAssetBranch JFAssetBranch { get; set; }
    }
}