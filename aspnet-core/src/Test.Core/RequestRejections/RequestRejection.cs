using Test.Requests;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestRejections
{
	[Table("RequestRejections")]
    public class RequestRejection : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			
		[StringLength(RequestRejectionConsts.MaxRejectionMessageLength, MinimumLength = RequestRejectionConsts.MinRejectionMessageLength)]
		public virtual string RejectionMessage { get; set; }
		
		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		
		public virtual long? UserId { get; set; }
		public User User { get; set; }
		
    }
}