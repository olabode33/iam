using Test.RequestTypes;
using Test.Authorization.Users;
using Test.Assets;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Requests
{
	[Table("Requests")]
    public class Request : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string RequestDescription { get; set; }
		public virtual DateTime ExpiryDate { get; set; }
		public virtual int? RequestTypeId { get; set; }
		public RequestType RequestType { get; set; }
		public virtual long? UserId { get; set; }
		public User User { get; set; }
        public virtual bool RequestStats { get; set; }
        public virtual DateTime RequestApprovetime { get; set; }	
        public virtual bool RequestApprovalStatus { get; set; }	
        public virtual DateTime InitiationTime { get; set; }
        public virtual string RequestStatus { get; set; }
        public virtual long? RequestOwnerId { get; set; }
        public virtual long InitiatingRequestID { get; set; }
    }
}