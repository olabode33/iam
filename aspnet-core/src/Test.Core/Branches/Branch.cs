using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Branches
{
	[Table("Branches")]
    public class Branch : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string BranchName { get; set; }
		
		public virtual string BranchCode { get; set; }
		

    }
}