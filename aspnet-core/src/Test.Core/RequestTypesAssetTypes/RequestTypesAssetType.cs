using Test.AssetTypes;
using Test.RequestTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestTypesAssetTypes
{
	[Table("RequestTypesAssetTypes")]
    public class RequestTypesAssetType : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual int? AssetTypeId { get; set; }
		public AssetType AssetType { get; set; }
		
		public virtual int? RequestTypeId { get; set; }
		public RequestType RequestType { get; set; }
		
    }
}