using Test.Authorization.Users;
using Test.Requests;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.RequestApprovals
{
	[Table("RequestApprovals")]
    public class RequestApproval : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual bool Status { get; set; }
		

		public virtual long? UserId { get; set; }
		public User User { get; set; }
		
		public virtual int? RequestId { get; set; }
		public Request Request { get; set; }
		
    }
}