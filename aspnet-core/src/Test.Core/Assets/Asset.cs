using Test.Applications;
using Test.Branches;
using Test.AssetTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Assets
{
	[Table("Assets")]
    public class Asset : FullAuditedEntity , IMayHaveTenant
    {
		public int? TenantId { get; set; }
		public virtual string AssetName { get; set; }		
		public virtual string AssetCode { get; set; }		
		public virtual string AssetDescription { get; set; }
		public virtual int? ApplicationId { get; set; }
		public Application Application { get; set; }		
		public virtual int? BranchId { get; set; }
		public Branch Branch { get; set; }		
		public virtual int? AssetTypeId { get; set; }
		public AssetType AssetType { get; set; }
    }
}