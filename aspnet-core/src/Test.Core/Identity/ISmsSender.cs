using System.Threading.Tasks;

namespace Test.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}