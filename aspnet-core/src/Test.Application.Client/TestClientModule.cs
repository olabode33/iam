﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Test
{
    public class TestClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestClientModule).GetAssembly());
        }
    }
}
