﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class RemovedJFAssetBranch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JFAssetBranch",
                table: "JobFunctions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JFAssetBranch",
                table: "JobFunctions",
                nullable: false,
                defaultValue: 0);
        }
    }
}
