﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_Request1062 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Applications_ApplicationId",
                table: "Requests");

            migrationBuilder.RenameColumn(
                name: "ApplicationId",
                table: "Requests",
                newName: "AssetId");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_ApplicationId",
                table: "Requests",
                newName: "IX_Requests_AssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Assets_AssetId",
                table: "Requests",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Assets_AssetId",
                table: "Requests");

            migrationBuilder.RenameColumn(
                name: "AssetId",
                table: "Requests",
                newName: "ApplicationId");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_AssetId",
                table: "Requests",
                newName: "IX_Requests_ApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Applications_ApplicationId",
                table: "Requests",
                column: "ApplicationId",
                principalTable: "Applications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
