﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_Request5401 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ApplicationId",
                table: "Requests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Requests_ApplicationId",
                table: "Requests",
                column: "ApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Applications_ApplicationId",
                table: "Requests",
                column: "ApplicationId",
                principalTable: "Applications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Applications_ApplicationId",
                table: "Requests");

            migrationBuilder.DropIndex(
                name: "IX_Requests_ApplicationId",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "ApplicationId",
                table: "Requests");
        }
    }
}
