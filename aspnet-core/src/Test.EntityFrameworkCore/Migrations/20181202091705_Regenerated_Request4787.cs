﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_Request4787 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestApprovals_Requests_RequestId",
                table: "RequestApprovals");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestAssets_Requests_RequestId",
                table: "RequestAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestEvents_Requests_RequestId",
                table: "RequestEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_UserPriviledges_Requests_RequestId",
                table: "UserPriviledges");

            migrationBuilder.DropIndex(
                name: "IX_UserPriviledges_RequestId",
                table: "UserPriviledges");

            migrationBuilder.DropIndex(
                name: "IX_RequestEvents_RequestId",
                table: "RequestEvents");

            migrationBuilder.DropIndex(
                name: "IX_RequestAssets_RequestId",
                table: "RequestAssets");

            migrationBuilder.DropIndex(
                name: "IX_RequestApprovals_RequestId",
                table: "RequestApprovals");

            migrationBuilder.DropColumn(
                name: "RequestDate",
                table: "Requests");

            migrationBuilder.AddColumn<int>(
                name: "RequestId1",
                table: "UserPriviledges",
                nullable: true);

            //migrationBuilder.AlterColumn<long>(
            //    name: "Id",
            //    table: "Requests",
            //    nullable: false,
            //    oldClrType: typeof(int))
            //    .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
            //    .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "RequestId1",
                table: "RequestEvents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RequestId1",
                table: "RequestAssets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RequestId1",
                table: "RequestApprovals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserPriviledges_RequestId1",
                table: "UserPriviledges",
                column: "RequestId1");

            migrationBuilder.CreateIndex(
                name: "IX_RequestEvents_RequestId1",
                table: "RequestEvents",
                column: "RequestId1");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAssets_RequestId1",
                table: "RequestAssets",
                column: "RequestId1");

            migrationBuilder.CreateIndex(
                name: "IX_RequestApprovals_RequestId1",
                table: "RequestApprovals",
                column: "RequestId1");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_RequestApprovals_Requests_RequestId1",
            //    table: "RequestApprovals",
            //    column: "RequestId1",
            //    principalTable: "Requests",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_RequestAssets_Requests_RequestId1",
            //    table: "RequestAssets",
            //    column: "RequestId1",
            //    principalTable: "Requests",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_RequestEvents_Requests_RequestId1",
            //    table: "RequestEvents",
            //    column: "RequestId1",
            //    principalTable: "Requests",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_UserPriviledges_Requests_RequestId1",
            //    table: "UserPriviledges",
            //    column: "RequestId1",
            //    principalTable: "Requests",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestApprovals_Requests_RequestId1",
                table: "RequestApprovals");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestAssets_Requests_RequestId1",
                table: "RequestAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_RequestEvents_Requests_RequestId1",
                table: "RequestEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_UserPriviledges_Requests_RequestId1",
                table: "UserPriviledges");

            migrationBuilder.DropIndex(
                name: "IX_UserPriviledges_RequestId1",
                table: "UserPriviledges");

            migrationBuilder.DropIndex(
                name: "IX_RequestEvents_RequestId1",
                table: "RequestEvents");

            migrationBuilder.DropIndex(
                name: "IX_RequestAssets_RequestId1",
                table: "RequestAssets");

            migrationBuilder.DropIndex(
                name: "IX_RequestApprovals_RequestId1",
                table: "RequestApprovals");

            migrationBuilder.DropColumn(
                name: "RequestId1",
                table: "UserPriviledges");

            migrationBuilder.DropColumn(
                name: "RequestId1",
                table: "RequestEvents");

            migrationBuilder.DropColumn(
                name: "RequestId1",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RequestId1",
                table: "RequestApprovals");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Requests",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestDate",
                table: "Requests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_UserPriviledges_RequestId",
                table: "UserPriviledges",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestEvents_RequestId",
                table: "RequestEvents",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAssets_RequestId",
                table: "RequestAssets",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestApprovals_RequestId",
                table: "RequestApprovals",
                column: "RequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestApprovals_Requests_RequestId",
                table: "RequestApprovals",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestAssets_Requests_RequestId",
                table: "RequestAssets",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RequestEvents_Requests_RequestId",
                table: "RequestEvents",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserPriviledges_Requests_RequestId",
                table: "UserPriviledges",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
