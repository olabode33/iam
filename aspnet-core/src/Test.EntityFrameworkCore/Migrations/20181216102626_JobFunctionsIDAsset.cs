﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class JobFunctionsIDAsset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctions_JobFunctionId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.RenameColumn(
                name: "JobFunctionId",
                table: "JobFunctionsAssetAssets",
                newName: "JobFunctionAssetId");

            migrationBuilder.RenameIndex(
                name: "IX_JobFunctionsAssetAssets_JobFunctionId",
                table: "JobFunctionsAssetAssets",
                newName: "IX_JobFunctionsAssetAssets_JobFunctionAssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                column: "JobFunctionAssetId",
                principalTable: "JobFunctionAssets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.RenameColumn(
                name: "JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                newName: "JobFunctionId");

            migrationBuilder.RenameIndex(
                name: "IX_JobFunctionsAssetAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                newName: "IX_JobFunctionsAssetAssets_JobFunctionId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctions_JobFunctionId",
                table: "JobFunctionsAssetAssets",
                column: "JobFunctionId",
                principalTable: "JobFunctions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
