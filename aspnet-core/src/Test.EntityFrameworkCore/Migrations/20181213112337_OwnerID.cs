﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class OwnerID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RequestOwnerId",
                table: "Requests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequestOwnerId",
                table: "Requests");
        }
    }
}
