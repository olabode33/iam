﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class ReverseRequestType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReverseRequestType",
                table: "RequestTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReverseRequestType",
                table: "RequestTypes");
        }
    }
}
