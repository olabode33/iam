﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_AssetTypeOtherProperty6523 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssetTypeId",
                table: "AssetTypeOtherProperties",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssetTypeOtherProperties_AssetTypeId",
                table: "AssetTypeOtherProperties",
                column: "AssetTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetTypeOtherProperties_AssetTypes_AssetTypeId",
                table: "AssetTypeOtherProperties",
                column: "AssetTypeId",
                principalTable: "AssetTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetTypeOtherProperties_AssetTypes_AssetTypeId",
                table: "AssetTypeOtherProperties");

            migrationBuilder.DropIndex(
                name: "IX_AssetTypeOtherProperties_AssetTypeId",
                table: "AssetTypeOtherProperties");

            migrationBuilder.DropColumn(
                name: "AssetTypeId",
                table: "AssetTypeOtherProperties");
        }
    }
}
