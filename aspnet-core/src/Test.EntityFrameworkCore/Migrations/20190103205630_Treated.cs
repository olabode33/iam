﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Treated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RequestAssetTreated",
                table: "RequestAssets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RequestAssetTreatedBy",
                table: "RequestAssets",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestAssetTreatedDate",
                table: "RequestAssets",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequestAssetTreated",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RequestAssetTreatedBy",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RequestAssetTreatedDate",
                table: "RequestAssets");
        }
    }
}
