﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_ApproveRequest3009 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovalTime",
                table: "ApproveRequests");

            migrationBuilder.DropColumn(
                name: "PriorApprovalTime",
                table: "ApproveRequests");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovalTime",
                table: "ApproveRequests",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PriorApprovalTime",
                table: "ApproveRequests",
                nullable: true);
        }
    }
}
