﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_AssetType6122 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AttachApplication",
                table: "AssetTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AttachBranch",
                table: "AssetTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AssetTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AssetTypes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AssetTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AssetTypes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AssetTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AssetTypes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AssetTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachApplication",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "AttachBranch",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AssetTypes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AssetTypes");
        }
    }
}
