﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Added_ApproveRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApproveRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: true),
                    ApprovalStatus = table.Column<bool>(nullable: false),
                    PriorApprovalStatus = table.Column<bool>(nullable: false),
                    RequestId = table.Column<int>(nullable: true),
                    ApproverID = table.Column<long>(nullable: false),
                    PriorApprover = table.Column<long>(nullable: false),
                    PriorApprovId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApproveRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApproveRequests_AbpUsers_ApproverID",
                        column: x => x.ApproverID,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApproveRequests_AbpUsers_PriorApprovId",
                        column: x => x.PriorApprovId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApproveRequests_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApproveRequests_ApproverID",
                table: "ApproveRequests",
                column: "ApproverID");

            migrationBuilder.CreateIndex(
                name: "IX_ApproveRequests_PriorApprovId",
                table: "ApproveRequests",
                column: "PriorApprovId");

            migrationBuilder.CreateIndex(
                name: "IX_ApproveRequests_RequestId",
                table: "ApproveRequests",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_ApproveRequests_TenantId",
                table: "ApproveRequests",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApproveRequests");
        }
    }
}
