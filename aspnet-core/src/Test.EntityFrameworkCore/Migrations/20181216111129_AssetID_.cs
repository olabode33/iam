﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class AssetID_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_Assets_AssetId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.AlterColumn<int>(
                name: "JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssetId",
                table: "JobFunctionsAssetAssets",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AssetID",
                table: "ApproveRequests",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_Assets_AssetId",
                table: "JobFunctionsAssetAssets",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                column: "JobFunctionAssetId",
                principalTable: "JobFunctionAssets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_Assets_AssetId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets");

            migrationBuilder.DropColumn(
                name: "AssetID",
                table: "ApproveRequests");

            migrationBuilder.AlterColumn<int>(
                name: "JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AssetId",
                table: "JobFunctionsAssetAssets",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_Assets_AssetId",
                table: "JobFunctionsAssetAssets",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionsAssetAssets_JobFunctionAssets_JobFunctionAssetId",
                table: "JobFunctionsAssetAssets",
                column: "JobFunctionAssetId",
                principalTable: "JobFunctionAssets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
