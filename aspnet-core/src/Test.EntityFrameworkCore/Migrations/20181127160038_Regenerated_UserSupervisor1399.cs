﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_UserSupervisor1399 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SupervisorUserId",
                table: "UserSupervisors",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_UserSupervisors_SupervisorUserId",
                table: "UserSupervisors",
                column: "SupervisorUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSupervisors_AbpUsers_SupervisorUserId",
                table: "UserSupervisors",
                column: "SupervisorUserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSupervisors_AbpUsers_SupervisorUserId",
                table: "UserSupervisors");

            migrationBuilder.DropIndex(
                name: "IX_UserSupervisors_SupervisorUserId",
                table: "UserSupervisors");

            migrationBuilder.DropColumn(
                name: "SupervisorUserId",
                table: "UserSupervisors");
        }
    }
}
