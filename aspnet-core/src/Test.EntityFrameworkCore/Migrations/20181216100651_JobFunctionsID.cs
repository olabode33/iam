﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class JobFunctionsID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionAssets_Assets_AssetId",
                table: "JobFunctionAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionAssets_Branches_BranchId",
                table: "JobFunctionAssets");

            migrationBuilder.DropIndex(
                name: "IX_JobFunctionAssets_AssetId",
                table: "JobFunctionAssets");

            migrationBuilder.DropIndex(
                name: "IX_JobFunctionAssets_BranchId",
                table: "JobFunctionAssets");

            migrationBuilder.DropColumn(
                name: "AssetId",
                table: "JobFunctionAssets");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "JobFunctionAssets");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssetId",
                table: "JobFunctionAssets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "JobFunctionAssets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctionAssets_AssetId",
                table: "JobFunctionAssets",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctionAssets_BranchId",
                table: "JobFunctionAssets",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionAssets_Assets_AssetId",
                table: "JobFunctionAssets",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionAssets_Branches_BranchId",
                table: "JobFunctionAssets",
                column: "BranchId",
                principalTable: "Branches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
