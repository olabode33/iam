﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_Request9992 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_JobFunctions_JobFunctionId",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "RequestCode",
                table: "Requests");

            migrationBuilder.RenameColumn(
                name: "JobFunctionId",
                table: "Requests",
                newName: "RequestTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_JobFunctionId",
                table: "Requests",
                newName: "IX_Requests_RequestTypeId");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Requests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Requests_UserId",
                table: "Requests",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_RequestTypes_RequestTypeId",
                table: "Requests",
                column: "RequestTypeId",
                principalTable: "RequestTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_AbpUsers_UserId",
                table: "Requests",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_RequestTypes_RequestTypeId",
                table: "Requests");

            migrationBuilder.DropForeignKey(
                name: "FK_Requests_AbpUsers_UserId",
                table: "Requests");

            migrationBuilder.DropIndex(
                name: "IX_Requests_UserId",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Requests");

            migrationBuilder.RenameColumn(
                name: "RequestTypeId",
                table: "Requests",
                newName: "JobFunctionId");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_RequestTypeId",
                table: "Requests",
                newName: "IX_Requests_JobFunctionId");

            migrationBuilder.AddColumn<string>(
                name: "RequestCode",
                table: "Requests",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_JobFunctions_JobFunctionId",
                table: "Requests",
                column: "JobFunctionId",
                principalTable: "JobFunctions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
