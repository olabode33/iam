﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class RequestApprova : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LastApprovedBy",
                table: "RequestAssets",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastApprovedDate",
                table: "RequestAssets",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "RejectedBy",
                table: "RequestAssets",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RejectedDate",
                table: "RequestAssets",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "RequestAssetApproved",
                table: "RequestAssets",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastApprovedBy",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "LastApprovedDate",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RejectedBy",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RejectedDate",
                table: "RequestAssets");

            migrationBuilder.DropColumn(
                name: "RequestAssetApproved",
                table: "RequestAssets");
        }
    }
}
