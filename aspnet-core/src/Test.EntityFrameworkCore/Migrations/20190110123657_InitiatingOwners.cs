﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class InitiatingOwners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "InitiatingRequestID",
                table: "Requests",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InitiatingRequestID",
                table: "Requests");
        }
    }
}
