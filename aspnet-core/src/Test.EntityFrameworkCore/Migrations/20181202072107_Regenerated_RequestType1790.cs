﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_RequestType1790 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssetTypeId",
                table: "RequestTypes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RequestTypes_AssetTypeId",
                table: "RequestTypes",
                column: "AssetTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestTypes_AssetTypes_AssetTypeId",
                table: "RequestTypes",
                column: "AssetTypeId",
                principalTable: "AssetTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestTypes_AssetTypes_AssetTypeId",
                table: "RequestTypes");

            migrationBuilder.DropIndex(
                name: "IX_RequestTypes_AssetTypeId",
                table: "RequestTypes");

            migrationBuilder.DropColumn(
                name: "AssetTypeId",
                table: "RequestTypes");
        }
    }
}
