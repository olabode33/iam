﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Added_PriorDateChage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ApproveDate",
                table: "ApproveRequests",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PriorApprovalDate",
                table: "ApproveRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApproveDate",
                table: "ApproveRequests");

            migrationBuilder.DropColumn(
                name: "PriorApprovalDate",
                table: "ApproveRequests");
        }
    }
}
