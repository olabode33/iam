﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_JobFunctionAsset7093 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BranchCode",
                table: "JobFunctionAssets");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BranchCode",
                table: "JobFunctionAssets",
                nullable: true);
        }
    }
}
