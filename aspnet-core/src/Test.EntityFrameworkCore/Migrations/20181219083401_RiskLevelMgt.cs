﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class RiskLevelMgt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Conflicts_Assets_AssetId",
                table: "Conflicts");

            migrationBuilder.DropIndex(
                name: "IX_Conflicts_AssetId",
                table: "Conflicts");

            migrationBuilder.DropColumn(
                name: "AssetId",
                table: "Conflicts");

            migrationBuilder.AddColumn<string>(
                name: "ConflictRisk",
                table: "Conflicts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConflictRisk",
                table: "Conflicts");

            migrationBuilder.AddColumn<int>(
                name: "AssetId",
                table: "Conflicts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Conflicts_AssetId",
                table: "Conflicts",
                column: "AssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Conflicts_Assets_AssetId",
                table: "Conflicts",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
