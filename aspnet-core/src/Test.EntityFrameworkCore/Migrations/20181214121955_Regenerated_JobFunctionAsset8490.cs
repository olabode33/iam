﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Regenerated_JobFunctionAsset8490 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Branch",
                table: "JobFunctionAssets");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "JobFunctionAssets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctionAssets_BranchId",
                table: "JobFunctionAssets",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobFunctionAssets_Branches_BranchId",
                table: "JobFunctionAssets",
                column: "BranchId",
                principalTable: "Branches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobFunctionAssets_Branches_BranchId",
                table: "JobFunctionAssets");

            migrationBuilder.DropIndex(
                name: "IX_JobFunctionAssets_BranchId",
                table: "JobFunctionAssets");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "JobFunctionAssets");

            migrationBuilder.AddColumn<int>(
                name: "Branch",
                table: "JobFunctionAssets",
                nullable: false,
                defaultValue: 0);
        }
    }
}
