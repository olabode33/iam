using Test.ConflictAssets;
using Test.TreatRequests;
using Test.ViewApprovedRequests;
using Test.Conflicts;
using Test.JobFunctionsAssetAssets;
using Test.RequestRejections;
using Test.ApproveRequests;
using Test.RequestTypesAssetTypes;
using Test.AssetTypeOtherProperties;
using Test.JobFunctionAssetBranches;
using Test.UserSupervisors;
using Test.UserPriviledges;
using Test.RequestAssets;
using Test.RequestEvents;
using Test.RequestApprovals;
using Test.Requests;
using Test.AssetProperties;
using Test.JobFunctionAssets;
using Test.JobFunctions;
using Test.Assets;
using Test.RequestTypes;
using Test.AssetTypes;
using Test.Applications;
using Test.Branches;
using Test.Departments;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Test.Authorization.Roles;
using Test.Authorization.Users;
using Test.Chat;
using Test.Editions;
using Test.Friendships;
using Test.MultiTenancy;
using Test.MultiTenancy.Accounting;
using Test.MultiTenancy.Payments;
using Test.Storage;

namespace Test.EntityFrameworkCore
{
    public class TestDbContext : AbpZeroDbContext<Tenant, Role, User, TestDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<ConflictAsset> ConflictAssets { get; set; }

        public virtual DbSet<TreatRequest> TreatRequests { get; set; }

        public virtual DbSet<ViewApprovedRequest> ViewApprovedRequests { get; set; }

        public virtual DbSet<Conflict> Conflicts { get; set; }

        public virtual DbSet<JobFunctionsAssetAsset> JobFunctionsAssetAssets { get; set; }

        public virtual DbSet<RequestRejection> RequestRejections { get; set; }

        public virtual DbSet<ApproveRequest> ApproveRequests { get; set; }

        public virtual DbSet<RequestTypesAssetType> RequestTypesAssetTypes { get; set; }

        public virtual DbSet<AssetTypeOtherProperty> AssetTypeOtherProperties { get; set; }

        public virtual DbSet<JobFunctionAssetBranche> JobFunctionAssetBranches { get; set; }

        //public virtual DbSet<JobFunctionAssetBranch> JobFunctionAssetBranchs { get; set; }

        public virtual DbSet<UserSupervisor> UserSupervisors { get; set; }

        public virtual DbSet<UserPriviledge> UserPriviledges { get; set; }

        public virtual DbSet<RequestAsset> RequestAssets { get; set; }

        public virtual DbSet<RequestEvent> RequestEvents { get; set; }

        public virtual DbSet<RequestApproval> RequestApprovals { get; set; }

        public virtual DbSet<Request> Requests { get; set; }

        public virtual DbSet<AssetProperty> AssetProperties { get; set; }

        public virtual DbSet<JobFunctionAsset> JobFunctionAssets { get; set; }

        public virtual DbSet<JobFunction> JobFunctions { get; set; }

        public virtual DbSet<Asset> Assets { get; set; }

        public virtual DbSet<RequestType> RequestTypes { get; set; }

        public virtual DbSet<AssetType> AssetTypes { get; set; }

        public virtual DbSet<Application> Applications { get; set; }

        public virtual DbSet<Branch> Branches { get; set; }

        public virtual DbSet<Department> Departments { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


           
           
           
           
           
           
           
           
           
           
           
           
           
           
            modelBuilder.Entity<ConflictAsset>(C =>
            {
                C.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<TreatRequest>(T =>
            {
                T.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<ViewApprovedRequest>(V =>
            {
                V.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<Conflict>(C =>
            {
                C.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<JobFunctionsAssetAsset>(J =>
            {
                J.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<JobFunctionAsset>(J =>
            {
                J.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<RequestRejection>(R =>
            {
                R.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<ApproveRequest>(A =>
            {
                A.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<RequestTypesAssetType>(R =>
            {
                R.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<AssetTypeOtherProperty>(A =>
            {
                A.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<Application>(A =>
            {
                A.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<Request>(R =>
            {
                R.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<Asset>(A =>
            {
                A.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<RequestType>(R =>
            {
                R.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<JobFunctionAssetBranche>(J =>
            {
                J.HasIndex(e => new { e.TenantId });
            });
            //modelBuilder.Entity<JobFunctionAssetBranch>(J =>
            //           {
            //               J.HasIndex(e => new { e.TenantId });
            //           });
            modelBuilder.Entity<UserSupervisor>(U =>
                       {
                           U.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<UserPriviledge>(U =>
                       {
                           U.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<RequestAsset>(R =>
                       {
                           R.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<RequestEvent>(R =>
                       {
                           R.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<RequestApproval>(R =>
                       {
                           R.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Request>(R =>
                       {
                           R.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<AssetProperty>(A =>
                       {
                           A.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<JobFunctionAsset>(J =>
                       {
                           J.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<JobFunction>(J =>
                       {
                           J.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Asset>(A =>
                       {
                           A.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<RequestType>(R =>
                       {
                           R.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<AssetType>(A =>
                       {
                           A.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Application>(A =>
                       {
                           A.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Branch>(B =>
                       {
                           B.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Department>(D =>
                       {
                           D.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<BinaryObject>(b =>
                       {
                           b.HasIndex(e => new { e.TenantId });
                       });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { e.PaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
