
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Applications.Exporting;
using Test.Applications.Dtos;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Dto;

namespace Test.Applications
{
	[AbpAuthorize(AppPermissions.Pages_Applications)]
    public class ApplicationsAppService : TestAppServiceBase, IApplicationsAppService
    {
		 private readonly IRepository<Application> _applicationRepository;
		 private readonly IApplicationsExcelExporter _applicationsExcelExporter;
		 

		  public ApplicationsAppService(IRepository<Application> applicationRepository, IApplicationsExcelExporter applicationsExcelExporter ) 
		  {
			_applicationRepository = applicationRepository;
			_applicationsExcelExporter = applicationsExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetApplicationForView>> GetAll(GetAllApplicationsInput input)
         {
			var importanceFilter = (ApplicationImportance) input.ImportanceFilter;
			
			var filteredApplications = _applicationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.ApplicationName.Contains(input.Filter) || e.ApplicationDesc.Contains(input.Filter) || e.URL.Contains(input.Filter) || e.ApplicationUsername.Contains(input.Filter) || e.ApplicationPassword.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationNameFilter),  e => e.ApplicationName.ToLower() == input.ApplicationNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationDescFilter),  e => e.ApplicationDesc.ToLower() == input.ApplicationDescFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.URLFilter),  e => e.URL.ToLower() == input.URLFilter.ToLower().Trim())
						.WhereIf(input.ImportanceFilter > -1, e => e.Importance == importanceFilter);


			var query = (from o in filteredApplications
                         select new GetApplicationForView() {
							Application = ObjectMapper.Map<ApplicationDto>(o)
						});

            var totalCount = await query.CountAsync();
            var applications = await query
                .OrderBy(input.Sorting ?? "application.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetApplicationForView>(
                totalCount,
                applications
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Applications_Edit)]
		 public async Task<GetApplicationForEditOutput> GetApplicationForEdit(EntityDto input)
         {
            var application = await _applicationRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetApplicationForEditOutput {Application = ObjectMapper.Map<CreateOrEditApplicationDto>(application)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditApplicationDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Applications_Create)]
		 private async Task Create(CreateOrEditApplicationDto input)
         {
            var application = ObjectMapper.Map<Application>(input);

			
			if (AbpSession.TenantId != null)
			{
				application.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _applicationRepository.InsertAsync(application);
         }

		 [AbpAuthorize(AppPermissions.Pages_Applications_Edit)]
		 private async Task Update(CreateOrEditApplicationDto input)
         {
            var application = await _applicationRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, application);
         }

		 [AbpAuthorize(AppPermissions.Pages_Applications_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _applicationRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetApplicationsToExcel(GetAllApplicationsForExcelInput input)
         {
			var importanceFilter = (ApplicationImportance) input.ImportanceFilter;
			
			var filteredApplications = _applicationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.ApplicationName.Contains(input.Filter) || e.ApplicationDesc.Contains(input.Filter) || e.URL.Contains(input.Filter) || e.ApplicationUsername.Contains(input.Filter) || e.ApplicationPassword.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationNameFilter),  e => e.ApplicationName.ToLower() == input.ApplicationNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationDescFilter),  e => e.ApplicationDesc.ToLower() == input.ApplicationDescFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.URLFilter),  e => e.URL.ToLower() == input.URLFilter.ToLower().Trim())
						.WhereIf(input.ImportanceFilter > -1, e => e.Importance == importanceFilter);


			var query = (from o in filteredApplications
                         select new GetApplicationForView() { 
							Application = ObjectMapper.Map<ApplicationDto>(o)
						 });


            var applicationListDtos = await query.ToListAsync();

            return _applicationsExcelExporter.ExportToFile(applicationListDtos);
         }


    }
}