using System.Collections.Generic;
using Test.Applications.Dtos;
using Test.Dto;

namespace Test.Applications.Exporting
{
    public interface IApplicationsExcelExporter
    {
        FileDto ExportToFile(List<GetApplicationForView> applications);
    }
}