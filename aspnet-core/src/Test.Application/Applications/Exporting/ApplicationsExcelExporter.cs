using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Applications.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Applications.Exporting
{
    public class ApplicationsExcelExporter : EpPlusExcelExporterBase, IApplicationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ApplicationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetApplicationForView> applications)
        {
            return CreateExcelPackage(
                "Applications.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Applications"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ApplicationName"),
                        L("ApplicationDesc"),
                        L("URL"),
                        L("Importance")
                        );

                    AddObjects(
                        sheet, 2, applications,
                        _ => _.Application.ApplicationName,
                        _ => _.Application.ApplicationDesc,
                        _ => _.Application.URL,
                        _ => _.Application.Importance
                        );

					

                });
        }
    }
}
