using Test.Requests;
using Test.Authorization.Users;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.ApproveRequests.Exporting;
using Test.ApproveRequests.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.Requests.Dtos;
using Test.RequestTypes;
using Test.RequestTypes.Dtos;
using Test.RequestRejections;
using Test.RequestAssets;
using Test.Assets;
using Test.Assets.Dtos;
using Abp.UI;
using Test.UserSupervisors;
using Test.UserPriviledges;
using Test.Conflicts;
using Test.ConflictAssets;
using Test.Conflicts.Dtos;
using Test.ConflictAssets.Dtos;

namespace Test.ApproveRequests
{
	[AbpAuthorize(AppPermissions.Pages_ApproveRequests)]
    public class ApproveRequestsAppService : TestAppServiceBase, IApproveRequestsAppService
    {
        private readonly IRepository<ApproveRequest> _approveRequestRepository;
        private readonly IApproveRequestsExcelExporter _approveRequestsExcelExporter;
        private readonly IRepository<Request, int> _requestRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<RequestType, int> _requestTypeRepository;
        private readonly IRepository<RequestRejection> _requestRejectionRepository;
        private readonly IRepository<RequestAsset> _requestAssetsRepository;
        private readonly IRepository<UserSupervisor> _userSupervisorRepository;
        private readonly IRepository<UserPriviledge> _userPriviledgeRepository;
        private readonly IRepository<Conflict> _conflictRepository;
        private readonly IRepository<ConflictAsset> _conflictAssetRepository;


        public ApproveRequestsAppService(IRepository<ApproveRequest> approveRequestRepository, IApproveRequestsExcelExporter approveRequestsExcelExporter , IRepository<Request, int> requestRepository, 
            IRepository<User, long> userRepository, IRepository<RequestType, int> requestTypeRepository, IRepository<RequestRejection> requestRejectionRepository,
            IRepository<RequestAsset> requestAssetsRepository, IRepository<UserSupervisor> userSupervisorRepository, IRepository<UserPriviledge> userPriviledgeRepository,
            IRepository<Conflict> conflictRepository, IRepository<ConflictAsset> conflictAssetRepository) 
		{
		    _approveRequestRepository = approveRequestRepository;
		    _approveRequestsExcelExporter = approveRequestsExcelExporter;
		    _requestRepository = requestRepository;
            _userRepository = userRepository;
            _requestTypeRepository = requestTypeRepository;
            _requestRejectionRepository = requestRejectionRepository;
            _requestAssetsRepository = requestAssetsRepository;
            _userSupervisorRepository = userSupervisorRepository;
            _userPriviledgeRepository = userPriviledgeRepository;
            _conflictRepository = conflictRepository;
            _conflictAssetRepository = conflictAssetRepository;
        }

        public async Task RejectRequestByAssets(RejectRequestDto input)
        {
            var rej = new RequestRejection()
            {
                RejectionMessage = input.RejectionMessage,
                RequestId = input.RequestId,
                UserId = AbpSession.UserId,
                TenantId = AbpSession.TenantId
            };

            var RequestRecord = await _requestRepository.FirstOrDefaultAsync((int)input.RequestId);
            RequestRecord.RequestApprovetime = DateTime.Now;
            RequestRecord.RequestApprovalStatus = true;
            RequestRecord.RequestStatus = "R";
            RequestRecord.RequestStats = true;

            List<RequestAsset> ReqAsset = new List<RequestAsset>();
            ReqAsset = _requestAssetsRepository.GetAllList().Where(x => x.RequestId == input.Id).ToList();
            if (ReqAsset.Count > 0)
            {
                for (int x = 0; x < ReqAsset.Count; x++)
                {
                    var RequestAssets = _requestAssetsRepository.FirstOrDefault((int)ReqAsset[x].Id);
                    RequestAssets.RejectedBy = Convert.ToString(AbpSession.UserId);
                    RequestAssets.RejectedDate = DateTime.Now;
                    RequestAssets.RequestAssetApproved = "R";
                    _requestAssetsRepository.Update(RequestAssets);
                }
            }

            await _requestRejectionRepository.InsertAsync(rej);
            await _requestRepository.UpdateAsync(RequestRecord);
        }

        public async Task RejectCompleteRequest(RejectRequestDto input)
        {
            List<ApproveRequest> Approve_Request = new List<ApproveRequest>();
            Approve_Request = await _approveRequestRepository.GetAll().Where(x=> x.RequestId == (int)input.RequestId).ToListAsync();

            if (Approve_Request.Count > 0)
            {
                foreach(var x in Approve_Request)
                {
                    x.ApprovalStatus = true;
                    x.ApproveDate = DateTime.Now;
                    await _approveRequestRepository.UpdateAsync(x);
                }
            }           
        }

        public async Task<PagedResultDto<GetApproveRequestForView>> GetAll(GetAllApproveRequestsInput input)
         {
            var filteredApproveRequests = _approveRequestRepository.GetAll().Where(x => x.ApprovalStatus == false)
                .Where(y => y.PriorApprovalStatus == true).Where(e=>e.ApproverID == AbpSession.UserId);

            var query = (from o in filteredApproveRequests
                         join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.ApproverID equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _userRepository.GetAll() on o.CreatorUserId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.PriorApprover equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _requestTypeRepository.GetAll() on s1.RequestTypeId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetApproveRequestForView()
                         {
                             RequesterName = s5 == null ? "" : s5.FullName.ToString(),
                             reqType = ObjectMapper.Map<RequestTypeDto>(s4),
                             Req = ObjectMapper.Map<RequestDto>(s1),
                             ApproveRequest = ObjectMapper.Map<ApproveRequestDto>(o),
                             RequestTenantId = s1 == null ? "" : s1.TenantId.ToString(),
                             ApproverIDName = s2 == null ? "" : s2.FullName.ToString(),
                             PriorApprover = s3 == null ? "" : s3.FullName.ToString()
                         });

            var totalCount = await query.CountAsync();
            var approveRequests = await query.OrderBy(input.Sorting ?? "approveRequest.id desc").PageBy(input).ToListAsync();

            return new PagedResultDto<GetApproveRequestForView>(
                totalCount,
                approveRequests
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_ApproveRequests_Edit)]
		 public async Task<GetApproveRequestForEditOutput> GetApproveRequestForEdit(EntityDto input)
         {
            var approveRequest = await _approveRequestRepository.FirstOrDefaultAsync(input.Id);           
		    var output = new GetApproveRequestForEditOutput {ApproveRequest = ObjectMapper.Map<CreateOrEditApproveRequestDto>(approveRequest)};            			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditApproveRequestDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_ApproveRequests_Create)]
		 private async Task Create(CreateOrEditApproveRequestDto input)
         {
            var approveRequest = ObjectMapper.Map<ApproveRequest>(input);
			if (AbpSession.TenantId != null)
			{
				approveRequest.TenantId = (int?) AbpSession.TenantId;
			}
            await _approveRequestRepository.InsertAsync(approveRequest);
         }

		 [AbpAuthorize(AppPermissions.Pages_ApproveRequests_Edit)]
		 private async Task Update(CreateOrEditApproveRequestDto input)
         {
            var approveRequest = await _approveRequestRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, approveRequest);
         }

		 [AbpAuthorize(AppPermissions.Pages_ApproveRequests_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _approveRequestRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetApproveRequestsToExcel(GetAllApproveRequestsForExcelInput input)
         {

            var filteredApproveRequests = _approveRequestRepository.GetAll();
            var query = (from o in filteredApproveRequests
                join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                from s1 in j1.DefaultIfEmpty()

                join o2 in _userRepository.GetAll() on o.ApproverID equals o2.Id into j2
                from s2 in j2.DefaultIfEmpty()

                join o3 in _userRepository.GetAll() on o.PriorApprover equals o3.Id into j3
                from s3 in j3.DefaultIfEmpty()

                join o4 in _requestTypeRepository.GetAll() on s1.RequestTypeId equals o4.Id into j4
                from s4 in j4.DefaultIfEmpty()

                         select new GetApproveRequestForView()
                         {
                             ApproveRequest = ObjectMapper.Map<ApproveRequestDto>(o),
                             RequestTenantId = s1 == null ? "" : s1.TenantId.ToString(),
                             ApproverIDName = s2 == null ? "" : s2.Name.ToString(),
                             PriorApprover = s3 == null ? "" : s3.Name.ToString(),
                             RequestTypeName = s4 == null ? "" : s4.RequestCode.ToString()
                         });

            var approveRequestListDtos = await query.ToListAsync();

            return _approveRequestsExcelExporter.ExportToFile(approveRequestListDtos);
         }

		[AbpAuthorize(AppPermissions.Pages_ApproveRequests)]
         public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestLookupTableDto>();
			foreach(var request in requestList){
				lookupTableDtoList.Add(new RequestLookupTableDto
				{
					Id = request.Id,
					DisplayName = request.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_ApproveRequests)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
        
        public async Task<CompleteRequestDto> getThatRequest(int RequestID)
        {
            var output = new CompleteRequestDto();
            var assetsList = await _requestAssetsRepository.GetAllIncluding(x => x.Asset).Where(x => x.RequestId == RequestID).ToListAsync();
            List<Asset> assetList = new List<Asset>(); foreach (var asset in assetsList) { assetList.Add(asset.Asset); }

            var requestList = await _requestRepository.FirstOrDefaultAsync(RequestID);
            List<RequestDto> ReqDto = new List<RequestDto>();
            ReqDto.Add(new RequestDto
            {
                RequestDescription = requestList.RequestDescription
            });
            output.Requests = ReqDto.ToArray();

            var requestTypeList = await _requestTypeRepository.FirstOrDefaultAsync(Convert.ToInt32(requestList.RequestTypeId));
            output.RType = ObjectMapper.Map<List<RequestTypeDto>>(requestTypeList).ToArray();

            var user = await _userRepository.FirstOrDefaultAsync((long)requestList.RequestOwnerId);
            output.RequestOwner = user.FullName.ToString();

            output.assets = ObjectMapper.Map<List<AssetDto>>(assetList).ToArray();
            return output;
        }

        public async Task ApproveRequestByAssets(ApproveRequestDto input)
        {
            RejectAll((int)input.RequestId);
            foreach (var AssetID in input.Assets)
            {
                AcceptList((int)input.RequestId, (int)AssetID, "A");
            }
            await UpdateApprovalStatus((int)input.RequestId);
        }

        public async Task RejectRequest(RejectRequestDto input)
        {
            foreach (var AssetID in input.AssetId) { AcceptList((int)input.RequestId, (int)AssetID, "R"); }
            List<RequestAsset> reqAssets = new List<RequestAsset>();
            reqAssets = _requestAssetsRepository.GetAll().Where(x => x.RequestId == (int)input.RequestId).Where(f => f.RequestAssetTreated == "P" || f.RequestAssetTreated == "A").ToList();
            if (reqAssets.Count <= 0)
            {
                var RequestRecord = await _requestRepository.FirstOrDefaultAsync((int)input.RequestId);
                if (RequestRecord != null)
                {
                    RequestRecord.RequestApprovetime = DateTime.Now;
                    RequestRecord.RequestApprovalStatus = true;
                    RequestRecord.RequestStatus = "R";
                    RequestRecord.RequestStats = true;
                    await _requestRepository.UpdateAsync(RequestRecord);
                }
                var RequestRejectRecord = await _requestRepository.FirstOrDefaultAsync(x => x.InitiatingRequestID == (int)input.RequestId);
                if (RequestRejectRecord != null)
                {
                    RequestRejectRecord.RequestApprovetime = DateTime.Now;
                    RequestRejectRecord.RequestApprovalStatus = true;
                    RequestRejectRecord.RequestStatus = "R";
                    RequestRejectRecord.RequestStats = true;
                    await _requestRepository.UpdateAsync(RequestRejectRecord);
                }
            }
        }

        private void RejectAll(int id)
        {
            List<RequestAsset> ReqAsset = new List<RequestAsset>();
            ReqAsset = _requestAssetsRepository.GetAllList().Where(x => x.RequestId == id).ToList();
            if (ReqAsset.Count > 0)
            {
                for (int x = 0; x < ReqAsset.Count; x++)
                {
                    if (String.IsNullOrEmpty(ReqAsset[x].RejectedBy))
                    {
                        var RequestAssets = _requestAssetsRepository.FirstOrDefault((int)ReqAsset[x].Id);
                        RequestAssets.RejectedBy = Convert.ToString(AbpSession.UserId);
                        RequestAssets.RejectedDate = DateTime.Now;
                        RequestAssets.RequestAssetApproved = "R";
                        _requestAssetsRepository.Update(RequestAssets);
                    }
                }
            }
        }
        private void AcceptList(int RequestId, int AssetID, string RType)
        {
            List<RequestAsset> ReqAsset = new List<RequestAsset>();
            ReqAsset = _requestAssetsRepository.GetAllList().Where(x => x.RequestId == RequestId).Where(y => y.AssetId == (int)AssetID).ToList();
            for (int x = 0; x < ReqAsset.Count; x++)
            {
                var RequestAssets = _requestAssetsRepository.FirstOrDefault((int)ReqAsset[x].Id);
                if (RType.Equals("A")) {
                    RequestAssets.RejectedBy = String.Empty;
                    RequestAssets.RejectedDate = Convert.ToDateTime("0001-01-01");
                    RequestAssets.RequestAssetApproved = RType;
                }
                else
                {
                    RequestAssets.RejectedBy = AbpSession.UserId.ToString();
                    RequestAssets.RejectedDate = DateTime.Now;
                    RequestAssets.RequestAssetApproved = RType;
                }
                RequestAssets.LastApprovedBy = AbpSession.UserId.ToString();
                RequestAssets.LastApprovedDate = DateTime.Now;
                _requestAssetsRepository.Update(RequestAssets);
            }
        }
        private long getMySupervisor()
        {
            long supervisorID = 0;
            List<UserSupervisor> UserSup = new List<UserSupervisor>();
            UserSup = _userSupervisorRepository.GetAllList().Where(x => x.UserId == (int)AbpSession.UserId).ToList();
            supervisorID = (long)UserSup[0].SupervisorUserId;
            return supervisorID;
        }

        private async Task UpdateApprovalStatus(int RequestId)
        {
            //Update Prior Approval
            List<ApproveRequest> PriorRequest = new List<ApproveRequest>();
            PriorRequest = _approveRequestRepository.GetAllList().Where(x => x.RequestId == RequestId).Where(x => x.PriorApprover == AbpSession.UserId).ToList();
            if (PriorRequest.Count > 0)
            {
                foreach (var x in PriorRequest)
                {
                    x.PriorApprovalStatus = true;
                    x.PriorApprovalDate = DateTime.Now;
                    _approveRequestRepository.Update(x);
                }
            }

            //Update Approval
            List<ApproveRequest>  MyApproval = _approveRequestRepository.GetAllList().Where(x => x.RequestId == RequestId).Where(x => x.ApproverID == AbpSession.UserId).ToList();
            if (MyApproval.Count > 0)
            {
                foreach (var x in MyApproval)
                {
                    var ApproveRequest = await _approveRequestRepository.FirstOrDefaultAsync(x.Id);
                    ApproveRequest.ApprovalStatus = true;
                    ApproveRequest.ApproveDate = DateTime.Now;
                    await _approveRequestRepository.UpdateAsync(ApproveRequest);
                }
            }

            //UpdateRequestCompleted.
            List<ApproveRequest> AllRequest = _approveRequestRepository.GetAllList().Where(x => x.RequestId == RequestId).Where(y => y.ApprovalStatus == false).ToList();
            if (AllRequest.Count == 0)
            {
                var RequestRecord = _requestRepository.FirstOrDefault(RequestId);
                RequestRecord.RequestApprovetime = DateTime.Now;
                RequestRecord.RequestApprovalStatus = true;
                RequestRecord.RequestStatus = "A";
                RequestRecord.RequestStats = true;
                await _requestRepository.UpdateAsync(RequestRecord);
            }
        }

        public async Task<List<ConflictDto>> ConflictsChecker(int RequestID)
        {
            ConflictDto c = new ConflictDto();
            List<ConflictDto> conflictDto = new List<ConflictDto>();
            List<RequestAsset> ReqAssets = await _requestAssetsRepository.GetAllListAsync(x=>x.RequestId == RequestID);
            var RequestDetails = await _requestRepository.FirstOrDefaultAsync(Convert.ToInt32(ReqAssets[0].RequestId));
            int RequestUserID = Convert.ToInt32(RequestDetails.RequestOwnerId);
            List<UserPriviledge> Priviledges = await _userPriviledgeRepository.GetAllListAsync(x => x.UserId == RequestUserID);

            List<int> CurrentAssetsID = new List<int>();
            for (int x = 0; x < ReqAssets.Count; x++) {  CurrentAssetsID.Add(Convert.ToInt32(ReqAssets[x].AssetId)); }
            for (int x = 0; x < Priviledges.Count; x++) { CurrentAssetsID.Add(Convert.ToInt32(Priviledges[x].AssetId)); }

            List<Conflict> ConlictsLog = await _conflictRepository.GetAllListAsync();
            for (int x = 0; x < ConlictsLog.Count; x++)
            {
                List<int> ConflictsAssetID = new List<int>();
                List<ConflictAsset> Conflict_Assets = await _conflictAssetRepository.GetAllListAsync(xe => xe.ConflictId == ConlictsLog[x].Id);
                for (int o = 0; o < Conflict_Assets.Count; o++) { ConflictsAssetID.Add(Convert.ToInt32(Conflict_Assets[o].AssetId)); }
                bool DoesConflictExist = true;
                for (int w = 0; w < ConflictsAssetID.Count; w++)
                {
                    if (DoesConflictExist) {
                        DoesConflictExist = CurrentAssetsID.IndexOf(ConflictsAssetID[w]) != -1;
                    } else { break; }
                }

                if (DoesConflictExist)
                {
                    var d = _conflictRepository.FirstOrDefault(ConlictsLog[x].Id);
                    var AssetsConflicts = new List<int>();
                    List<ConflictAsset> conflictassets = await _conflictAssetRepository.GetAllListAsync(y => y.ConflictId == ConlictsLog[x].Id);
                    for (int z = 0; z < conflictassets.Count; z++) { AssetsConflicts.Add((int)conflictassets[z].AssetId); }
                    conflictDto.Add(new ConflictDto {
                        ConflictDescription = d.ConflictDescription,
                        ConflictName = d.ConflictName,
                        ConflictRisk = d.ConflictRisk,
                        Id = d.Id,
                        ConflictsAssetsID = AssetsConflicts.ToArray()
                    });
                }
            }
            return conflictDto;
        }


        public async Task<GetRequestForApprovalDto> GetSingleRequest(EntityDto<int> input)
        {
            var request = await _requestRepository.FirstOrDefaultAsync(input.Id);
            GetRequestForApprovalDto output = new GetRequestForApprovalDto
            {
                RequestDto = ObjectMapper.Map<RequestDto>(request),
                Conflicts = await ConflictsChecker(input.Id)
            };

            var requestOwner = await _userRepository.FirstOrDefaultAsync((long)request.RequestOwnerId);
            output.RequestDto.RequestOwnerName = requestOwner.UserName;
            
            //List<ConflictDto> ConflictsDetails = await ConflictsChecker(input.Id);

            //output.conflicts = ConflictsDetails;
            
            // add assets to list, the assets are fetched from RequestAsset through the foreign key relationship

            var requestAssets = await _requestAssetsRepository.GetAllIncluding(x => x.Asset)
                                    .Where(x => x.RejectedBy == null)
                                    .Where(x => x.RequestId == input.Id)
                                    .ToListAsync();

            List<Asset> assetList = new List<Asset>();

            //foreach (var t in requestAssets)
            //{
            //    // @Todo review this condition, if you need to add requestassets that have not been approved
            //    // Filter only requestassets that have not been rejected
            //    if (String.IsNullOrEmpty(t.RejectedBy))
            //    {
            //        assetList.Add(t.Asset);
            //    }
            //}

            List<RequestAssetsDto> data = new List<RequestAssetsDto>();

            foreach (var r in requestAssets)
            {
                var reqq = new RequestAssetsDto
                {
                    Id = r.Asset.Id,
                    AssetCode = r.Asset.AssetCode,
                    AssetName = r.Asset.AssetName,
                    AssetDescription = r.Asset.AssetDescription,
                    AssetConflictChecker = false,
                    AssetConflictNames = new List<string>()
                };

                foreach (var c in output.Conflicts)
                {
                    if (c.ConflictsAssetsID.Contains(reqq.Id))
                    {
                        reqq.AssetConflictChecker = true;
                        reqq.AssetConflictNames.Add(c.ConflictName);
                    }
                }

                data.Add(reqq);
            };

            //List<RequestAssetsDto> data = new List<RequestAssetsDto>();
            //for (int x =0; x < assetList.Count; x++)
            //{
            //    int id = assetList[x].Id;
            //    var requestDto = new RequestAssetsDto
            //    {
            //        AssetCode = assetList[x].AssetCode,
            //        AssetName = assetList[x].AssetName,
            //        AssetDescription = assetList[x].AssetDescription,
            //        Id = assetList[x].Id,

            //        AssetConflictChecker = false,
            //        AssetConflictNames = new List<string>()
            //    };

            //    foreach (var c in output.Conflicts)
            //    {
            //        if(c.ConflictsAssetsID.Contains(requestDto.Id))
            //        {
            //            requestDto.AssetConflictChecker = true;
            //            requestDto.AssetConflictNames.Add(c.ConflictName);
            //        }
            //    }

            //    data.Add(requestDto);
            //}

            var rType = await _requestTypeRepository.FirstOrDefaultAsync(x=>x.Id == request.RequestTypeId);
            output.RequestType = ObjectMapper.Map<RequestTypeDto>(rType);
            output.Assets = data;
            
            return output;
        }


    }
}