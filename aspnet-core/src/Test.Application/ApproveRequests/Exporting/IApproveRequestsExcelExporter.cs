using System.Collections.Generic;
using Test.ApproveRequests.Dtos;
using Test.Dto;

namespace Test.ApproveRequests.Exporting
{
    public interface IApproveRequestsExcelExporter
    {
        FileDto ExportToFile(List<GetApproveRequestForView> approveRequests);
    }
}