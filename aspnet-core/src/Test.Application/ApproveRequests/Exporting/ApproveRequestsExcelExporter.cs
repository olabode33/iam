using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.ApproveRequests.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.ApproveRequests.Exporting
{
    public class ApproveRequestsExcelExporter : EpPlusExcelExporterBase, IApproveRequestsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ApproveRequestsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetApproveRequestForView> approveRequests)
        {
            return CreateExcelPackage(
                "ApproveRequests.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ApproveRequests"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("Request")) + L("TenantId"),
                        (L("User")) + L("Name"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, approveRequests,
                        _ => _.RequestTenantId,
                        _ => _.ApproverIDName,
                        _ => _.PriorApprover
                        );

					

                });
        }
    }
}
