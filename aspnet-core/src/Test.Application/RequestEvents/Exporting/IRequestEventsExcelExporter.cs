using System.Collections.Generic;
using Test.RequestEvents.Dtos;
using Test.Dto;

namespace Test.RequestEvents.Exporting
{
    public interface IRequestEventsExcelExporter
    {
        FileDto ExportToFile(List<GetRequestEventForView> requestEvents);
    }
}