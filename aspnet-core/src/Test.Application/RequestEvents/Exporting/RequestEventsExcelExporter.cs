using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RequestEvents.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RequestEvents.Exporting
{
    public class RequestEventsExcelExporter : EpPlusExcelExporterBase, IRequestEventsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RequestEventsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRequestEventForView> requestEvents)
        {
            return CreateExcelPackage(
                "RequestEvents.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestEvents"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Event"),
                        L("EventDescription"),
                        L("EventDate"),
                        (L("RequestType")) + L("RequestCode"),
                        (L("Request")) + L("TenantId"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, requestEvents,
                        _ => _.RequestEvent.Event,
                        _ => _.RequestEvent.EventDescription,
                        _ => _timeZoneConverter.Convert(_.RequestEvent.EventDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.RequestTypeRequestCode,
                        _ => _.RequestTenantId,
                        _ => _.UserName
                        );

					var eventDateColumn = sheet.Column(3);
                    eventDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					eventDateColumn.AutoFit();
					

                });
        }
    }
}
