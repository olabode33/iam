using Test.RequestTypes;
using Test.Requests;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestEvents.Exporting;
using Test.RequestEvents.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.RequestEvents
{
	[AbpAuthorize(AppPermissions.Pages_RequestEvents)]
    public class RequestEventsAppService : TestAppServiceBase, IRequestEventsAppService
    {
		 private readonly IRepository<RequestEvent> _requestEventRepository;
		 private readonly IRequestEventsExcelExporter _requestEventsExcelExporter;
		 private readonly IRepository<RequestType,int> _requestTypeRepository;
		 private readonly IRepository<Request,int> _requestRepository;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public RequestEventsAppService(IRepository<RequestEvent> requestEventRepository, IRequestEventsExcelExporter requestEventsExcelExporter , IRepository<RequestType, int> requestTypeRepository, IRepository<Request, int> requestRepository, IRepository<User, long> userRepository) 
		  {
			_requestEventRepository = requestEventRepository;
			_requestEventsExcelExporter = requestEventsExcelExporter;
			_requestTypeRepository = requestTypeRepository;
		_requestRepository = requestRepository;
		_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetRequestEventForView>> GetAll(GetAllRequestEventsInput input)
         {
			
			var filteredRequestEvents = _requestEventRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Event.Contains(input.Filter) || e.EventDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.EventFilter),  e => e.Event.ToLower() == input.EventFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.EventDescriptionFilter),  e => e.EventDescription.ToLower() == input.EventDescriptionFilter.ToLower().Trim())
						.WhereIf(input.MinEventDateFilter != null, e => e.EventDate >= input.MinEventDateFilter)
						.WhereIf(input.MaxEventDateFilter != null, e => e.EventDate <= input.MaxEventDateFilter);


			var query = (from o in filteredRequestEvents
                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         join o3 in _userRepository.GetAll() on o.UserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         
                         select new GetRequestEventForView() {
							RequestEvent = ObjectMapper.Map<RequestEventDto>(o),
                         	RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString(),
                         	UserName = s3 == null ? "" : s3.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTypeRequestCodeFilter), e => e.RequestTypeRequestCode.ToLower() == input.RequestTypeRequestCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestEvents = await query
                .OrderBy(input.Sorting ?? "requestEvent.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestEventForView>(
                totalCount,
                requestEvents
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestEvents_Edit)]
		 public async Task<GetRequestEventForEditOutput> GetRequestEventForEdit(EntityDto input)
         {
            var requestEvent = await _requestEventRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestEventForEditOutput {RequestEvent = ObjectMapper.Map<CreateOrEditRequestEventDto>(requestEvent)};

		    if (output.RequestEvent.RequestTypeId != null)
            {
                var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)output.RequestEvent.RequestTypeId);
                output.RequestTypeRequestCode = requestType.RequestCode.ToString();
            }

		    if (output.RequestEvent.RequestId != null)
            {
                var request = await _requestRepository.FirstOrDefaultAsync((int)output.RequestEvent.RequestId);
                output.RequestTenantId = request.TenantId.ToString();
            }

		    if (output.RequestEvent.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.RequestEvent.UserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestEventDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestEvents_Create)]
		 private async Task Create(CreateOrEditRequestEventDto input)
         {
            var requestEvent = ObjectMapper.Map<RequestEvent>(input);

			
			if (AbpSession.TenantId != null)
			{
				requestEvent.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _requestEventRepository.InsertAsync(requestEvent);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestEvents_Edit)]
		 private async Task Update(CreateOrEditRequestEventDto input)
         {
            var requestEvent = await _requestEventRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestEvent);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestEvents_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _requestEventRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRequestEventsToExcel(GetAllRequestEventsForExcelInput input)
         {
			
			var filteredRequestEvents = _requestEventRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Event.Contains(input.Filter) || e.EventDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.EventFilter),  e => e.Event.ToLower() == input.EventFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.EventDescriptionFilter),  e => e.EventDescription.ToLower() == input.EventDescriptionFilter.ToLower().Trim())
						.WhereIf(input.MinEventDateFilter != null, e => e.EventDate >= input.MinEventDateFilter)
						.WhereIf(input.MaxEventDateFilter != null, e => e.EventDate <= input.MaxEventDateFilter);


			var query = (from o in filteredRequestEvents
                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         join o3 in _userRepository.GetAll() on o.UserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         
                         select new GetRequestEventForView() { 
							RequestEvent = ObjectMapper.Map<RequestEventDto>(o),
                         	RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString(),
                         	UserName = s3 == null ? "" : s3.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTypeRequestCodeFilter), e => e.RequestTypeRequestCode.ToLower() == input.RequestTypeRequestCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());


            var requestEventListDtos = await query.ToListAsync();

            return _requestEventsExcelExporter.ExportToFile(requestEventListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_RequestEvents)]
         public async Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.RequestCode.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestTypeLookupTableDto>();
			foreach(var requestType in requestTypeList){
				lookupTableDtoList.Add(new RequestTypeLookupTableDto
				{
					Id = requestType.Id,
					DisplayName = requestType.RequestCode.ToString()
				});
			}

            return new PagedResultDto<RequestTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestEvents)]
         public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestLookupTableDto>();
			foreach(var request in requestList){
				lookupTableDtoList.Add(new RequestLookupTableDto
				{
					Id = request.Id,
					DisplayName = request.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestEvents)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}