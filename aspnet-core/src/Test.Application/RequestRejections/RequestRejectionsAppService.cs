using Test.Requests;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestRejections.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.RequestRejections
{
	[AbpAuthorize(AppPermissions.Pages_RequestRejections)]
    public class RequestRejectionsAppService : TestAppServiceBase, IRequestRejectionsAppService
    {
		 private readonly IRepository<RequestRejection> _requestRejectionRepository;
		 private readonly IRepository<Request,int> _requestRepository;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public RequestRejectionsAppService(IRepository<RequestRejection> requestRejectionRepository , IRepository<Request, int> requestRepository, IRepository<User, long> userRepository) 
		  {
			_requestRejectionRepository = requestRejectionRepository;
			_requestRepository = requestRepository;
		_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetRequestRejectionForView>> GetAll(GetAllRequestRejectionsInput input)
         {
			
			var filteredRequestRejections = _requestRejectionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RejectionMessage.Contains(input.Filter));


			var query = (from o in filteredRequestRejections
                         join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestRejectionForView() {
							RequestRejection = ObjectMapper.Map<RequestRejectionDto>(o),
                         	RequestTenantId = s1 == null ? "" : s1.TenantId.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestRejections = await query
                .OrderBy(input.Sorting ?? "requestRejection.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestRejectionForView>(
                totalCount,
                requestRejections
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestRejections_Edit)]
		 public async Task<GetRequestRejectionForEditOutput> GetRequestRejectionForEdit(EntityDto input)
         {
            var requestRejection = await _requestRejectionRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestRejectionForEditOutput {RequestRejection = ObjectMapper.Map<CreateOrEditRequestRejectionDto>(requestRejection)};

		    if (output.RequestRejection.RequestId != null)
            {
                var request = await _requestRepository.FirstOrDefaultAsync((int)output.RequestRejection.RequestId);
                output.RequestTenantId = request.TenantId.ToString();
            }

		    if (output.RequestRejection.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.RequestRejection.UserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestRejectionDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestRejections_Create)]
		 private async Task Create(CreateOrEditRequestRejectionDto input)
         {
            var requestRejection = ObjectMapper.Map<RequestRejection>(input);
			if (AbpSession.TenantId != null)
			{
				requestRejection.TenantId = (int?) AbpSession.TenantId;
			}
            await _requestRejectionRepository.InsertAsync(requestRejection);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestRejections_Edit)]
		 private async Task Update(CreateOrEditRequestRejectionDto input)
         {
            var requestRejection = await _requestRejectionRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestRejection);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestRejections_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _requestRejectionRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_RequestRejections)]
         public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestLookupTableDto>();
			foreach(var request in requestList){
				lookupTableDtoList.Add(new RequestLookupTableDto
				{
					Id = request.Id,
					DisplayName = request.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestRejections)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}