
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.AssetTypes.Exporting;
using Test.AssetTypes.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.AssetTypeOtherProperties;
using Test.AssetTypeOtherProperties.Dtos;

namespace Test.AssetTypes
{
	[AbpAuthorize(AppPermissions.Pages_AssetTypes)]
    public class AssetTypesAppService : TestAppServiceBase, IAssetTypesAppService
    {
		 private readonly IRepository<AssetType> _assetTypeRepository;
		 private readonly IAssetTypesExcelExporter _assetTypesExcelExporter;
        private readonly IRepository<AssetTypeOtherProperty> _assetTypeOtherPropertyRepository;

        public AssetTypesAppService(IRepository<AssetType> assetTypeRepository, IAssetTypesExcelExporter assetTypesExcelExporter, IRepository<AssetTypeOtherProperty> assetTypeOtherPropertyRepository) 
		  {
			_assetTypeRepository = assetTypeRepository;
			_assetTypesExcelExporter = assetTypesExcelExporter;
            _assetTypeOtherPropertyRepository = assetTypeOtherPropertyRepository;

          }

		 public async Task<PagedResultDto<GetAssetTypeForView>> GetAll(GetAllAssetTypesInput input)
         {
			
			var filteredAssetTypes = _assetTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetTypeName.Contains(input.Filter) || e.AssetTypeDesc.Contains(input.Filter) || e.AssetTypeOtherDetails.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeNameFilter),  e => e.AssetTypeName.ToLower() == input.AssetTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeDescFilter),  e => e.AssetTypeDesc.ToLower() == input.AssetTypeDescFilter.ToLower().Trim())
						.WhereIf(input.PermissionFilter > -1,  e => Convert.ToInt32(e.Permission) == input.PermissionFilter );


			var query = (from o in filteredAssetTypes
                         select new GetAssetTypeForView() {
							AssetType = ObjectMapper.Map<AssetTypeDto>(o)
						});

            var totalCount = await query.CountAsync();

            var assetTypes = await query
                .OrderBy(input.Sorting ?? "assetType.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetAssetTypeForView>(
                totalCount,
                assetTypes
            );
         }

        

        [AbpAuthorize(AppPermissions.Pages_AssetTypes_Edit)]
		 public async Task<GetAssetTypeForEditOutput> GetAssetTypeForEdit(EntityDto input)
         {
            var assetType = await _assetTypeRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetAssetTypeForEditOutput {AssetType = ObjectMapper.Map<CreateOrEditAssetTypeDto>(assetType)};

            //CreateOrEditAssetTypeDto
            var assets = await _assetTypeOtherPropertyRepository.GetAllIncluding(x => x.AssetType).Where(x => x.AssetTypeId == input.Id).ToListAsync();
            output.AssetProperties = ObjectMapper.Map<List<AssetTypeOtherPropertyDto>>(assets);

            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditAssetTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else
            {
                var RequestAssetDel = await _assetTypeOtherPropertyRepository.GetAllListAsync(x => x.AssetTypeId == input.Id);
                if (input.Id > 0 && RequestAssetDel.Count > 0)
                {
                    foreach (var r in RequestAssetDel)
                    {
                        await _assetTypeOtherPropertyRepository.DeleteAsync(r);
                    }
                }
                await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetTypes_Create)]
		 private async Task Create(CreateOrEditAssetTypeDto input)
         {
            var assetType = ObjectMapper.Map<AssetType>(input);
			if (AbpSession.TenantId != null)
			{
				assetType.TenantId = (int?) AbpSession.TenantId;
			}
            var AssetTypeID = await _assetTypeRepository.InsertAndGetIdAsync(assetType);
            CurrentUnitOfWork.SaveChanges();
            
            foreach (var AssetProp in input.AssetProperties)
            {
                AssetTypeOtherProperty req = new AssetTypeOtherProperty();
                req.CreatorUserId = AbpSession.UserId;
                req.TenantId = AbpSession.TenantId;
                req.PropertyName = AssetProp;
                req.AssetTypeId = Convert.ToInt32(AssetTypeID);
                await _assetTypeOtherPropertyRepository.InsertAsync(req);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_AssetTypes_Edit)]
		 private async Task Update(CreateOrEditAssetTypeDto input)
         {
            var assetType = await _assetTypeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, assetType);
            foreach (var AssetProp in input.AssetProperties)
            {
                AssetTypeOtherProperty req = new AssetTypeOtherProperty();
                req.CreatorUserId = AbpSession.UserId;
                req.TenantId = AbpSession.TenantId;
                req.PropertyName = AssetProp;
                req.AssetTypeId = Convert.ToInt32(input.Id);
                await _assetTypeOtherPropertyRepository.InsertAsync(req);
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_AssetTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            var RequestAssetDel = await _assetTypeOtherPropertyRepository.GetAllListAsync(x => x.AssetTypeId == input.Id);
            if (input.Id > 0 && RequestAssetDel.Count > 0)
            {
                foreach (var r in RequestAssetDel)
                {
                    await _assetTypeOtherPropertyRepository.DeleteAsync(r);
                }
            }
            await _assetTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetAssetTypesToExcel(GetAllAssetTypesForExcelInput input)
         {
			
			var filteredAssetTypes = _assetTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetTypeName.Contains(input.Filter) || e.AssetTypeDesc.Contains(input.Filter) || e.AssetTypeOtherDetails.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeNameFilter),  e => e.AssetTypeName.ToLower() == input.AssetTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeDescFilter),  e => e.AssetTypeDesc.ToLower() == input.AssetTypeDescFilter.ToLower().Trim())
						.WhereIf(input.PermissionFilter > -1,  e => Convert.ToInt32(e.Permission) == input.PermissionFilter )
	;


			var query = (from o in filteredAssetTypes
                         select new GetAssetTypeForView() { 
							AssetType = ObjectMapper.Map<AssetTypeDto>(o)
						 });


            var assetTypeListDtos = await query.ToListAsync();

            return _assetTypesExcelExporter.ExportToFile(assetTypeListDtos);
         }


    }
}