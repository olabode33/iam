using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.AssetTypes.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.AssetTypes.Exporting
{
    public class AssetTypesExcelExporter : EpPlusExcelExporterBase, IAssetTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public AssetTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAssetTypeForView> assetTypes)
        {
            return CreateExcelPackage(
                "AssetTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("AssetTypes"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AssetTypeName"),
                        L("AssetTypeDesc"),
                        L("Permission"),
                        L("AttachBranch"),
                        L("AttachApplication")
                        );

                    AddObjects(
                        sheet, 2, assetTypes,
                        _ => _.AssetType.AssetTypeName,
                        _ => _.AssetType.AssetTypeDesc,
                        _ => _.AssetType.Permission,
                        _ => _.AssetType.AttachBranch,
                        _ => _.AssetType.AttachApplication
                        );

					

                });
        }
    }
}
