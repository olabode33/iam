using System.Collections.Generic;
using Test.AssetTypes.Dtos;
using Test.Dto;

namespace Test.AssetTypes.Exporting
{
    public interface IAssetTypesExcelExporter
    {
        FileDto ExportToFile(List<GetAssetTypeForView> assetTypes);
    }
}