using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.UserSupervisors.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.UserSupervisors.Exporting
{
    public class UserSupervisorsExcelExporter : EpPlusExcelExporterBase, IUserSupervisorsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public UserSupervisorsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetUserSupervisorForView> userSupervisors)
        {
            return CreateExcelPackage(
                "UserSupervisors.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("UserSupervisors"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("User")) + L("Name"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, userSupervisors,
                        _ => _.UserName,
                        _ => _.UserName2
                        );

					

                });
        }
    }
}
