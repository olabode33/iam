using System.Collections.Generic;
using Test.UserSupervisors.Dtos;
using Test.Dto;

namespace Test.UserSupervisors.Exporting
{
    public interface IUserSupervisorsExcelExporter
    {
        FileDto ExportToFile(List<GetUserSupervisorForView> userSupervisors);
    }
}