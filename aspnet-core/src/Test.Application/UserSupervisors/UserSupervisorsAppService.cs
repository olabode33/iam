using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.UserSupervisors.Exporting;
using Test.UserSupervisors.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.UserSupervisors
{
	[AbpAuthorize(AppPermissions.Pages_UserSupervisors)]
    public class UserSupervisorsAppService : TestAppServiceBase, IUserSupervisorsAppService
    {
		 private readonly IRepository<UserSupervisor> _userSupervisorRepository;
		 private readonly IUserSupervisorsExcelExporter _userSupervisorsExcelExporter;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public UserSupervisorsAppService(IRepository<UserSupervisor> userSupervisorRepository, IUserSupervisorsExcelExporter userSupervisorsExcelExporter , IRepository<User, long> userRepository) 
		  {
			_userSupervisorRepository = userSupervisorRepository;
			_userSupervisorsExcelExporter = userSupervisorsExcelExporter;
			_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetUserSupervisorForView>> GetAll(GetAllUserSupervisorsInput input)
         {
			
			var filteredUserSupervisors = _userSupervisorRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredUserSupervisors
                         join o1 in _userRepository.GetAll() on o.UserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.SupervisorUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetUserSupervisorForView() {
							UserSupervisor = ObjectMapper.Map<UserSupervisorDto>(o),
                         	UserName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName2 = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserName2Filter), e => e.UserName2.ToLower() == input.UserName2Filter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var userSupervisors = await query
                .OrderBy(input.Sorting ?? "userSupervisor.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetUserSupervisorForView>(
                totalCount,
                userSupervisors
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_UserSupervisors_Edit)]
		 public async Task<GetUserSupervisorForEditOutput> GetUserSupervisorForEdit(EntityDto input)
         {
            var userSupervisor = await _userSupervisorRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetUserSupervisorForEditOutput {UserSupervisor = ObjectMapper.Map<CreateOrEditUserSupervisorDto>(userSupervisor)};

		    if (output.UserSupervisor.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.UserSupervisor.UserId);
                output.UserName = user.Name.ToString();
            }

		    if (output.UserSupervisor.SupervisorUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.UserSupervisor.SupervisorUserId);
                output.UserName2 = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditUserSupervisorDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_UserSupervisors_Create)]
		 private async Task Create(CreateOrEditUserSupervisorDto input)
         {
            var userSupervisor = ObjectMapper.Map<UserSupervisor>(input);

			
			if (AbpSession.TenantId != null)
			{
				userSupervisor.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _userSupervisorRepository.InsertAsync(userSupervisor);
         }

		 [AbpAuthorize(AppPermissions.Pages_UserSupervisors_Edit)]
		 private async Task Update(CreateOrEditUserSupervisorDto input)
         {
            var userSupervisor = await _userSupervisorRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, userSupervisor);
         }

		 [AbpAuthorize(AppPermissions.Pages_UserSupervisors_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _userSupervisorRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetUserSupervisorsToExcel(GetAllUserSupervisorsForExcelInput input)
         {
			
			var filteredUserSupervisors = _userSupervisorRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredUserSupervisors
                         join o1 in _userRepository.GetAll() on o.UserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.SupervisorUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetUserSupervisorForView() { 
							UserSupervisor = ObjectMapper.Map<UserSupervisorDto>(o),
                         	UserName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName2 = s2 == null ? "" : s2.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserName2Filter), e => e.UserName2.ToLower() == input.UserName2Filter.ToLower().Trim());


            var userSupervisorListDtos = await query.ToListAsync();

            return _userSupervisorsExcelExporter.ExportToFile(userSupervisorListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_UserSupervisors)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
            try
            {
                var query = _userRepository.GetAll().WhereIf(
                       !string.IsNullOrWhiteSpace(input.Filter),
                      e => e.Name.ToString().Contains(input.Filter)
                   );

                var totalCount = await query.CountAsync();

                var userList = await query
                    .PageBy(input)
                    .ToListAsync();

                var lookupTableDtoList = new List<UserLookupTableDto>();
                foreach (var user in userList)
                {
                    lookupTableDtoList.Add(new UserLookupTableDto
                    {
                        Id = user.Id,
                        DisplayName = user.Name.ToString()
                    });
                }

                return new PagedResultDto<UserLookupTableDto>(
                    totalCount,
                    lookupTableDtoList
                );
            }
            catch(Exception ex)
            {
                return null;
            }
         }
    }
}