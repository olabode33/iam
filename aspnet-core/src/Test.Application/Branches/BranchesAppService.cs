
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Branches.Exporting;
using Test.Branches.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Authorization.Users;

namespace Test.Branches
{
	[AbpAuthorize(AppPermissions.Pages_Branches)]
    public class BranchesAppService : TestAppServiceBase, IBranchesAppService
    {
		 private readonly IRepository<Branch> _branchRepository;
		 private readonly IBranchesExcelExporter _branchesExcelExporter;
		 

		  public BranchesAppService(IRepository<Branch> branchRepository, IBranchesExcelExporter branchesExcelExporter ) 
		  {
			_branchRepository = branchRepository;
			_branchesExcelExporter = branchesExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetBranchForView>> GetAll(GetAllBranchesInput input)
         {
			
			var filteredBranches = _branchRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.BranchName.Contains(input.Filter) || e.BranchCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchNameFilter),  e => e.BranchName.ToLower() == input.BranchNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchCodeFilter),  e => e.BranchCode.ToLower() == input.BranchCodeFilter.ToLower().Trim());


			var query = (from o in filteredBranches

                         select new GetBranchForView() {
							Branch = ObjectMapper.Map<BranchDto>(o)
						});

            var totalCount = await query.CountAsync();

            var branches = await query
                .OrderBy(input.Sorting ?? "branch.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetBranchForView>(
                totalCount,
                branches
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Branches_Edit)]
		 public async Task<GetBranchForEditOutput> GetBranchForEdit(EntityDto input)
         {
            var branch = await _branchRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetBranchForEditOutput {Branch = ObjectMapper.Map<CreateOrEditBranchDto>(branch)};
			
            return output;
         }

        
        public async Task<GetBranchForView> GetBranch(EntityDto input)
        {
            var branch = await _branchRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBranchForView { Branch = ObjectMapper.Map<BranchDto>(branch) };

            if (branch.CreatorUserId > 0)
            {
                User user = await UserManager.Users.FirstOrDefaultAsync(x => x.Id == branch.CreatorUserId);
                output.Creator = user.FullName;
            }


            if (branch.LastModifierUserId != null)
            {
                User user = await UserManager.Users.FirstOrDefaultAsync(x => x.Id == branch.LastModifierUserId);
                output.Modifier = user.FullName;
            }


            return output;
        }

        public async Task CreateOrEdit(CreateOrEditBranchDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Branches_Create)]
		 private async Task Create(CreateOrEditBranchDto input)
         {
            var branch = ObjectMapper.Map<Branch>(input);

			
			if (AbpSession.TenantId != null)
			{
				branch.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _branchRepository.InsertAsync(branch);
         }

		 [AbpAuthorize(AppPermissions.Pages_Branches_Edit)]
		 private async Task Update(CreateOrEditBranchDto input)
         {
            var branch = await _branchRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, branch);
         }

		 [AbpAuthorize(AppPermissions.Pages_Branches_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _branchRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetBranchesToExcel(GetAllBranchesForExcelInput input)
         {
			
			var filteredBranches = _branchRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.BranchName.Contains(input.Filter) || e.BranchCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchNameFilter),  e => e.BranchName.ToLower() == input.BranchNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchCodeFilter),  e => e.BranchCode.ToLower() == input.BranchCodeFilter.ToLower().Trim());


			var query = (from o in filteredBranches
                         select new GetBranchForView() { 
							Branch = ObjectMapper.Map<BranchDto>(o)
						 });


            var branchListDtos = await query.ToListAsync();

            return _branchesExcelExporter.ExportToFile(branchListDtos);
         }


    }
}