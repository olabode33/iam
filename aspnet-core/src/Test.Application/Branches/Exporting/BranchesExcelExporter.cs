using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Branches.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Branches.Exporting
{
    public class BranchesExcelExporter : EpPlusExcelExporterBase, IBranchesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BranchesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBranchForView> branches)
        {
            return CreateExcelPackage(
                "Branches.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Branches"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("BranchName"),
                        L("BranchCode")
                        );

                    AddObjects(
                        sheet, 2, branches,
                        _ => _.Branch.BranchName,
                        _ => _.Branch.BranchCode
                        );

					

                });
        }
    }
}
