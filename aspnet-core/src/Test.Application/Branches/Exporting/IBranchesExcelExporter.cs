using System.Collections.Generic;
using Test.Branches.Dtos;
using Test.Dto;

namespace Test.Branches.Exporting
{
    public interface IBranchesExcelExporter
    {
        FileDto ExportToFile(List<GetBranchForView> branches);
    }
}