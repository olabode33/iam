using Test.JobFunctions;
using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.JobFunctionsAssetAssets.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.JobFunctionAssets;
using Test.Applications;

namespace Test.JobFunctionsAssetAssets
{
	[AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets)]
    public class JobFunctionsAssetAssetsAppService : TestAppServiceBase, IJobFunctionsAssetAssetsAppService
    {
		 private readonly IRepository<JobFunctionsAssetAsset> _jobFunctionsAssetAssetRepository;
		 private readonly IRepository<JobFunction,int> _jobFunctionRepository;
		 private readonly IRepository<Asset,int> _assetRepository;
        private readonly IRepository<JobFunctionAsset, int> _jobFunctionAssetRepository;
        private readonly IRepository<Application, int> _applicationRepository;
		 

		  public JobFunctionsAssetAssetsAppService(
              IRepository<JobFunctionsAssetAsset> jobFunctionsAssetAssetRepository,
              IRepository<JobFunction, int> jobFunctionRepository, 
              IRepository<Asset, int> assetRepository,
              IRepository<JobFunctionAsset, int> jobFunctionAssetRepository,
              IRepository<Application, int> applicationRepository) 
		  {
			_jobFunctionsAssetAssetRepository = jobFunctionsAssetAssetRepository;
			_jobFunctionRepository = jobFunctionRepository;
		    _assetRepository = assetRepository;
            _jobFunctionAssetRepository = jobFunctionAssetRepository;
            _applicationRepository = applicationRepository;
		  }

		 public async Task<PagedResultDto<GetJobFunctionsAssetAssetForView>> GetAll(GetAllJobFunctionsAssetAssetsInput input)
         {
			
			var filteredJobFunctionsAssetAssets = _jobFunctionsAssetAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionsAssetAssets
                         join o1 in _jobFunctionRepository.GetAll() on o.JobFunctionAssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _assetRepository.GetAll() on o.AssetId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionsAssetAssetForView() {
							JobFunctionsAssetAsset = ObjectMapper.Map<JobFunctionsAssetAssetDto>(o),
                         	JobFunctionJobName = s1 == null ? "" : s1.JobName.ToString(),
                         	AssetAssetName = s2 == null ? "" : s2.AssetName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionJobNameFilter), e => e.JobFunctionJobName.ToLower() == input.JobFunctionJobNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var jobFunctionsAssetAssets = await query
                .OrderBy(input.Sorting ?? "jobFunctionsAssetAsset.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetJobFunctionsAssetAssetForView>(
                totalCount,
                jobFunctionsAssetAssets
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets_Edit)]
		 public async Task<GetJobFunctionsAssetAssetForEditOutput> GetJobFunctionsAssetAssetForEdit(EntityDto input)
         {
            var jobFunctionsAssetAsset = await _jobFunctionsAssetAssetRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetJobFunctionsAssetAssetForEditOutput {JobFunctionsAssetAsset = ObjectMapper.Map<CreateOrEditJobFunctionsAssetAssetDto>(jobFunctionsAssetAsset)};

		    if (output.JobFunctionsAssetAsset.JobFunctionId != null)
            {
                var jobFunction = await _jobFunctionRepository.FirstOrDefaultAsync((int)output.JobFunctionsAssetAsset.JobFunctionId);
                output.JobFunctionJobName = jobFunction.JobName.ToString();
            }

		    if (output.JobFunctionsAssetAsset.AssetId != null)
            {
                var asset = await _assetRepository.FirstOrDefaultAsync((int)output.JobFunctionsAssetAsset.AssetId);
                output.AssetAssetName = asset.AssetName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditJobFunctionsAssetAssetDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets_Create)]
		 private async Task Create(CreateOrEditJobFunctionsAssetAssetDto input)
         {
            var jobFunctionsAssetAsset = ObjectMapper.Map<JobFunctionsAssetAsset>(input);

			
			if (AbpSession.TenantId != null)
			{
				jobFunctionsAssetAsset.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _jobFunctionsAssetAssetRepository.InsertAsync(jobFunctionsAssetAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets_Edit)]
		 private async Task Update(CreateOrEditJobFunctionsAssetAssetDto input)
         {
            var jobFunctionsAssetAsset = await _jobFunctionsAssetAssetRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, jobFunctionsAssetAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _jobFunctionsAssetAssetRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets)]
         public async Task<PagedResultDto<JobFunctionLookupTableDto>> GetAllJobFunctionForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _jobFunctionRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.JobName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var jobFunctionList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<JobFunctionLookupTableDto>();
			foreach(var jobFunction in jobFunctionList){
				lookupTableDtoList.Add(new JobFunctionLookupTableDto
				{
					Id = jobFunction.Id,
					DisplayName = jobFunction.JobName.ToString()
				});
			}

            return new PagedResultDto<JobFunctionLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_JobFunctionsAssetAssets)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString()
				});
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        
    }
}