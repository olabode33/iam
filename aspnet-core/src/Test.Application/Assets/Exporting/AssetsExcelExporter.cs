using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Assets.Dtos;
using Test.Dto;
using Test.Storage;
using Test.IDMShared.Dtos;

namespace Test.Assets.Exporting
{
    public class AssetsExcelExporter : EpPlusExcelExporterBase, IAssetsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public AssetsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAssetForView> assets)
        {
            return CreateExcelPackage(
                "Assets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Assets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AssetName"),
                        L("AssetCode"),
                        L("AssetDescription"),
                        (L("Application")) + L("ApplicationName"),
                        (L("Branch")) + L("BranchName"),
                        (L("AssetType")) + L("AssetTypeName")
                        );

                    AddObjects(
                        sheet, 2, assets,
                        _ => _.Asset.AssetName,
                        _ => _.Asset.AssetCode,
                        _ => _.Asset.AssetDescription,
                        _ => _.ApplicationApplicationName,
                        _ => _.BranchBranchName,
                        _ => _.AssetTypeAssetTypeName
                        );

					

                });
        }
    }
}
