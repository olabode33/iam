using System.Collections.Generic;
using Test.Assets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.Assets.Exporting
{
    public interface IAssetsExcelExporter
    {
        FileDto ExportToFile(List<GetAssetForView> assets);
    }
}