using Test.Applications;
using Test.Branches;
using Test.AssetTypes;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Assets.Exporting;
using Test.Assets.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.AssetTypeOtherProperties.Dtos;
using Test.AssetTypeOtherProperties;
using Test.AssetProperties;

namespace Test.Assets
{
	[AbpAuthorize(AppPermissions.Pages_Assets)]
    public class AssetsAppService : TestAppServiceBase, IAssetsAppService
    {
		 private readonly IRepository<Asset> _assetRepository;
		 private readonly IAssetsExcelExporter _assetsExcelExporter;
		 private readonly IRepository<Application,int> _applicationRepository;
		 private readonly IRepository<Branch,int> _branchRepository;
		 private readonly IRepository<AssetType,int> _assetTypeRepository;
        private readonly IRepository<AssetTypeOtherProperty> _assetTypeOtherPropertyRepository;
        private readonly IRepository<AssetProperty> _assetPropertyRepository;

        public AssetsAppService(IRepository<Asset> assetRepository, IAssetsExcelExporter assetsExcelExporter , IRepository<Application, int> applicationRepository, IRepository<Branch, int> branchRepository, IRepository<AssetType, int> assetTypeRepository, IRepository<AssetTypeOtherProperty> assetTypeOtherPropertyRepository, IRepository<AssetProperty> assetPropertyRepository) 
		  {
			_assetRepository = assetRepository;
			_assetsExcelExporter = assetsExcelExporter;
			_applicationRepository = applicationRepository;
		_branchRepository = branchRepository;
		_assetTypeRepository = assetTypeRepository;
            _assetTypeOtherPropertyRepository = assetTypeOtherPropertyRepository;
            _assetPropertyRepository = assetPropertyRepository;


        }

		 public async Task<PagedResultDto<GetAssetForView>> GetAll(GetAllAssetsInput input)
         {
			
			var filteredAssets = _assetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetName.Contains(input.Filter) || e.AssetCode.Contains(input.Filter) || e.AssetDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetNameFilter),  e => e.AssetName.ToLower() == input.AssetNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetCodeFilter),  e => e.AssetCode.ToLower() == input.AssetCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetDescriptionFilter),  e => e.AssetDescription.ToLower() == input.AssetDescriptionFilter.ToLower().Trim());


			var query = (from o in filteredAssets
                         join o1 in _applicationRepository.GetAll() on o.ApplicationId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         join o3 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         
                         select new GetAssetForView() {
							Asset = ObjectMapper.Map<AssetDto>(o),
                         	ApplicationApplicationName = s1 == null ? "" : s1.ApplicationName.ToString(),
                         	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString(),
                         	AssetTypeAssetTypeName = s3 == null ? "" : s3.AssetTypeName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationApplicationNameFilter), e => e.ApplicationApplicationName.ToLower() == input.ApplicationApplicationNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var assets = await query
                .OrderBy(input.Sorting ?? "asset.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetAssetForView>(
                totalCount,
                assets
            );
         }

        public async Task<PagedResultDto<GetAssetTypeOtherPropertyForView>> GetAllProps(GetAllAssetTypeOtherPropertiesInput input)
        {         
            var filteredAssetTypeOtherProperties = _assetTypeOtherPropertyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PropertyName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PropertyNameFilter), e => e.PropertyName.ToLower() == input.PropertyNameFilter.ToLower().Trim())
                        .WhereIf(!(input.AssetTypeId == 0), e => e.AssetTypeId == input.AssetTypeId);


            var query = (from o in filteredAssetTypeOtherProperties
                         join o1 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetAssetTypeOtherPropertyForView()
                         {
                             AssetTypeOtherProperty = ObjectMapper.Map<AssetTypeOtherPropertyDto>(o),
                             AssetTypeAssetTypeName = s1 == null ? "" : s1.AssetTypeName.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var assetTypeOtherProperties = await query
                .OrderBy(input.Sorting ?? "assetTypeOtherProperty.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetAssetTypeOtherPropertyForView>(
                totalCount,
                assetTypeOtherProperties
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Assets_Edit)]
		 public async Task<GetAssetForEditOutput> GetAssetForEdit(EntityDto input)
         {
            var asset = await _assetRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetAssetForEditOutput {
                Asset = ObjectMapper.Map<CreateOrEditAssetDto>(asset)
            };

            var filteredAsset = _assetPropertyRepository.GetAll().Where(x=>x.AssetId == input.Id);
            var query = (from o in filteredAsset
                         join o1 in _assetTypeOtherPropertyRepository.GetAll() on o.PropertyOfAsset equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         select new AssetProps()
                         {
                             id = (int)o.PropertyOfAsset,
                             name=s1.PropertyName,
                             value=o.PropertyValue
                         });

            var assetProperties = await query.ToArrayAsync();
            output.AssetProps = assetProperties;

            if (output.Asset.ApplicationId != null)
            {
                var application = await _applicationRepository.FirstOrDefaultAsync((int)output.Asset.ApplicationId);
                output.ApplicationApplicationName = application.ApplicationName.ToString();
            }

		    if (output.Asset.BranchId != null)
            {
                var branch = await _branchRepository.FirstOrDefaultAsync((int)output.Asset.BranchId);
                output.BranchBranchName = branch.BranchName.ToString();
            }

		    if (output.Asset.AssetTypeId != null)
            {
                var assetType = await _assetTypeRepository.FirstOrDefaultAsync((int)output.Asset.AssetTypeId);
                output.AssetTypeAssetTypeName = assetType.AssetTypeName.ToString();
            }
            
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditAssetDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
                var RequestAssetDel = await _assetPropertyRepository.GetAllListAsync(x => x.AssetId == input.Id);
                if (input.Id > 0 && RequestAssetDel.Count > 0)
                {
                    foreach (var r in RequestAssetDel)
                    {
                        await _assetPropertyRepository.DeleteAsync(r);
                    }
                }
                await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Assets_Create)]
		 private async Task Create(CreateOrEditAssetDto input)
         {
            var asset = ObjectMapper.Map<Asset>(input);			
			if (AbpSession.TenantId != null)
			{
				asset.TenantId = (int?) AbpSession.TenantId;
			}	

            var AssetID = await _assetRepository.InsertAndGetIdAsync(asset);
            CurrentUnitOfWork.SaveChanges();
            if (AssetID > 0)
            {
                foreach (var AssetProp in input.AssetProps)
                {
                    AssetProperty req = new AssetProperty();
                    req.CreatorUserId = AbpSession.UserId;
                    req.AssetId = Convert.ToInt32(AssetID);
                    req.TenantId = AbpSession.TenantId;
                    req.PropertyOfAsset = (AssetProp.id);
                    req.PropertyValue = AssetProp.value;
                    req.AssetId = AssetID;
                    await _assetPropertyRepository.InsertAsync(req);
                }
            }
         }

		 [AbpAuthorize(AppPermissions.Pages_Assets_Edit)]
		 private async Task Update(CreateOrEditAssetDto input)
         {
            var asset = await _assetRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, asset);
            if (true)
            {
                foreach (var AssetProp in input.AssetProps)
                {
                    AssetProperty req = new AssetProperty();
                    req.CreatorUserId = AbpSession.UserId;
                    req.AssetId = Convert.ToInt32(input.Id);
                    req.TenantId = AbpSession.TenantId;
                    req.PropertyOfAsset = (AssetProp.id);
                    req.PropertyValue = AssetProp.value;
                    await _assetPropertyRepository.InsertAsync(req);
                }
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_Assets_Delete)]
         public async Task Delete(EntityDto input)
        {
            var RequestAssetDel = await _assetPropertyRepository.GetAllListAsync(x => x.AssetId == input.Id);
            if (input.Id > 0 && RequestAssetDel.Count > 0)
            {
                foreach (var r in RequestAssetDel)
                {
                    await _assetPropertyRepository.DeleteAsync(r);
                }
            }
            await _assetRepository.DeleteAsync(input.Id);
         } 

		//public async Task<FileDto> GetAssetsToExcel(GetAllAssetsForExcelInput input)
  //       {
			
		//	var filteredAssets = _assetRepository.GetAll()
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetName.Contains(input.Filter) || e.AssetCode.Contains(input.Filter) || e.AssetDescription.Contains(input.Filter))
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.AssetNameFilter),  e => e.AssetName.ToLower() == input.AssetNameFilter.ToLower().Trim())
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.AssetCodeFilter),  e => e.AssetCode.ToLower() == input.AssetCodeFilter.ToLower().Trim())
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.AssetDescriptionFilter),  e => e.AssetDescription.ToLower() == input.AssetDescriptionFilter.ToLower().Trim());


		//	var query = (from o in filteredAssets
  //                       join o1 in _applicationRepository.GetAll() on o.ApplicationId equals o1.Id into j1
  //                       from s1 in j1.DefaultIfEmpty()
                         
  //                       join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
  //                       from s2 in j2.DefaultIfEmpty()
                         
  //                       join o3 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o3.Id into j3
  //                       from s3 in j3.DefaultIfEmpty()
                         
  //                       select new GetAssetForView() { 
		//					Asset = ObjectMapper.Map<AssetDto>(o),
  //                       	ApplicationApplicationName = s1 == null ? "" : s1.ApplicationName.ToString(),
  //                       	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString(),
  //                       	AssetTypeAssetTypeName = s3 == null ? "" : s3.AssetTypeName.ToString()
		//				 })
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationApplicationNameFilter), e => e.ApplicationApplicationName.ToLower() == input.ApplicationApplicationNameFilter.ToLower().Trim())
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim())
		//				.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());


  //          var assetListDtos = await query.ToListAsync();

  //          return _assetsExcelExporter.ExportToFile(assetListDtos);
  //       }



		[AbpAuthorize(AppPermissions.Pages_Assets)]
         public async Task<PagedResultDto<ApplicationLookupTableDto>> GetAllApplicationForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _applicationRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.ApplicationName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var applicationList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<ApplicationLookupTableDto>();
			foreach(var application in applicationList){
				lookupTableDtoList.Add(new ApplicationLookupTableDto
				{
					Id = application.Id,
					DisplayName = application.ApplicationName.ToString()
				});
			}

            return new PagedResultDto<ApplicationLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Assets)]
         public async Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _branchRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.BranchName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var branchList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<BranchLookupTableDto>();
			foreach(var branch in branchList){
				lookupTableDtoList.Add(new BranchLookupTableDto
				{
					Id = branch.Id,
					DisplayName = branch.BranchName.ToString()
				});
			}

            return new PagedResultDto<BranchLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Assets)]
         public async Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetTypeName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetTypeLookupTableDto>();
			foreach(var assetType in assetTypeList){
				lookupTableDtoList.Add(new AssetTypeLookupTableDto
				{
					Id = assetType.Id,
					DisplayName = assetType.AssetTypeName.ToString()
				});
			}

            return new PagedResultDto<AssetTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        public Task<FileDto> GetAssetsToExcel(GetAllAssetsForExcelInput input)
        {
            throw new NotImplementedException();
        }
    }
}