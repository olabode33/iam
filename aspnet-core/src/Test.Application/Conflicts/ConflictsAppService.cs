using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Conflicts.Exporting;
using Test.Conflicts.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.ConflictAssets;

namespace Test.Conflicts
{
	[AbpAuthorize(AppPermissions.Pages_Conflicts)]
    public class ConflictsAppService : TestAppServiceBase, IConflictsAppService
    {
		 private readonly IRepository<Conflict> _conflictRepository;
        private readonly IRepository<ConflictAsset> _conflictAssetRepository;
        private readonly IConflictsExcelExporter _conflictsExcelExporter;
		 private readonly IRepository<Asset,int> _assetRepository;
		 

		  public ConflictsAppService(IRepository<Conflict> conflictRepository, IConflictsExcelExporter conflictsExcelExporter , IRepository<Asset, int> assetRepository,
              IRepository<ConflictAsset> conflictAssetRepository) 
		  {
			_conflictRepository = conflictRepository;
            _conflictAssetRepository = conflictAssetRepository;
            _conflictsExcelExporter = conflictsExcelExporter;
			_assetRepository = assetRepository;
		
		  }

		 public async Task<PagedResultDto<GetConflictForView>> GetAll(GetAllConflictsInput input)
         {
			
			var filteredConflicts = _conflictRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.ConflictName.Contains(input.Filter) || e.ConflictDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.ConflictNameFilter),  e => e.ConflictName.ToLower() == input.ConflictNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ConflictDescriptionFilter),  e => e.ConflictDescription.ToLower() == input.ConflictDescriptionFilter.ToLower().Trim());

            var query = (from o in filteredConflicts

                         select new GetConflictForView()
                         {
                             Conflict = ObjectMapper.Map<ConflictDto>(o)
                         });

            var totalCount = await query.CountAsync();

            var conflicts = await query
                .OrderBy(input.Sorting ?? "conflict.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetConflictForView>(
                totalCount,
                conflicts
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Conflicts_Edit)]
		 public async Task<GetConflictForEditOutput> GetConflictForEdit(EntityDto input)
         {
            var conflict = await _conflictRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetConflictForEditOutput {Conflict = ObjectMapper.Map<CreateOrEditConflictDto>(conflict)};
            			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditConflictDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
                var RequestAssetDel = await _conflictAssetRepository.GetAllListAsync(x => x.ConflictId == input.Id);
                if (input.Id > 0 && RequestAssetDel.Count > 0)
                {
                    foreach (var r in RequestAssetDel)
                    {
                        await _conflictAssetRepository.DeleteAsync(r);
                    }
                }
                await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Conflicts_Create)]
		 private async Task Create(CreateOrEditConflictDto input)
         {
            var conflict = ObjectMapper.Map<Conflict>(input);
			if (AbpSession.TenantId != null)
			{
				conflict.TenantId = (int?) AbpSession.TenantId;
			}
            conflict.ConflictRisk = input.ConflictRisk;
            var x = await _conflictRepository.InsertAndGetIdAsync(conflict);
            CurrentUnitOfWork.SaveChanges();

            foreach (var a in input.Assets)
            {
                var e = new ConflictAsset
                {
                    AssetId = a,
                    ConflictId = x,
                    TenantId = AbpSession.TenantId,
                    CreatorUserId = AbpSession.UserId
                };
                await _conflictAssetRepository.InsertAsync(e);
            }
         }

		 [AbpAuthorize(AppPermissions.Pages_Conflicts_Edit)]
		 private async Task Update(CreateOrEditConflictDto input)
         {
            var conflict = await _conflictRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, conflict);
            foreach (var a in input.Assets)
            {
                var e = new ConflictAsset
                {
                    AssetId = a,
                    ConflictId = (int)input.Id,
                    TenantId = AbpSession.TenantId,
                    CreatorUserId = AbpSession.UserId
                };
                await _conflictAssetRepository.InsertAsync(e);
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_Conflicts_Delete)]
         public async Task Delete(EntityDto input)
         {
            var RequestAssetDel = await _conflictAssetRepository.GetAllListAsync(x => x.ConflictId == input.Id);
            if (input.Id > 0 && RequestAssetDel.Count > 0)
            {
                foreach (var r in RequestAssetDel)
                {
                    await _conflictAssetRepository.DeleteAsync(r);
                }
            }
            await _conflictRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetConflictsToExcel(GetAllConflictsForExcelInput input)
         {
			
			var filteredConflicts = _conflictRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.ConflictName.Contains(input.Filter) || e.ConflictDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.ConflictNameFilter),  e => e.ConflictName.ToLower() == input.ConflictNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.ConflictDescriptionFilter),  e => e.ConflictDescription.ToLower() == input.ConflictDescriptionFilter.ToLower().Trim());


            var query = (from o in filteredConflicts

                         select new GetConflictForView()
                         {
                             Conflict = ObjectMapper.Map<ConflictDto>(o)
                         });


            var conflictListDtos = await query.ToListAsync();

            return _conflictsExcelExporter.ExportToFile(conflictListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Conflicts)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString(),
                    AssetDescription = asset.AssetDescription.ToString()
                });
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        public async Task<GetConflictForEditOutput> GetSingleConflict(int id)
        {
            List<AssetLookupTableDto> ConflictAsset = new List<AssetLookupTableDto>();
            List<ConflictAsset> ConflictAssetList = new List<ConflictAsset>();
            var ConflictView = new GetConflictForEditOutput();

            var ConflictDetails = await _conflictRepository.FirstOrDefaultAsync(id);
            ConflictView.Conflict = ObjectMapper.Map<CreateOrEditConflictDto>(ConflictDetails);
            ConflictAssetList = _conflictAssetRepository.GetAllList(x => x.ConflictId == ConflictDetails.Id).ToList();
            for (int x = 0; x < ConflictAssetList.Count; x++) {
                var AssetList = await _assetRepository.FirstOrDefaultAsync(Convert.ToInt32(ConflictAssetList[x].AssetId));
                if (AssetList != null) {
                    ConflictAsset.Add(new AssetLookupTableDto {
                        AssetDescription = AssetList.AssetDescription,
                        DisplayName = AssetList.AssetCode,
                        Id = AssetList.Id
                    });
                }
            }
            ConflictView.Assets = ConflictAsset.ToArray();
            return ConflictView;
        }
    }
}