using System.Collections.Generic;
using Test.Conflicts.Dtos;
using Test.Dto;

namespace Test.Conflicts.Exporting
{
    public interface IConflictsExcelExporter
    {
        FileDto ExportToFile(List<GetConflictForView> conflicts);
    }
}