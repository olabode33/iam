using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Conflicts.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Conflicts.Exporting
{
    public class ConflictsExcelExporter : EpPlusExcelExporterBase, IConflictsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ConflictsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetConflictForView> conflicts)
        {
            return CreateExcelPackage(
                "Conflicts.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Conflicts"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ConflictName"),
                        L("ConflictDescription"),
                        (L("Asset")) + L("AssetName")
                        );

                    AddObjects(
                        sheet, 2, conflicts,
                        _ => _.Conflict.ConflictName,
                        _ => _.Conflict.ConflictDescription
                        );

					

                });
        }
    }
}
