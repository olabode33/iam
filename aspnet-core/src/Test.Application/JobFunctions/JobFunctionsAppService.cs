using Test.Departments;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.JobFunctions.Exporting;
using Test.JobFunctions.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.JobFunctionsAssetAssets;
using Test.JobFunctionAssetBranches;
using Test.IDMShared.Dtos;
using Test.Assets;
using Test.JobFunctionAssets;
using Test.Applications;

namespace Test.JobFunctions
{
	[AbpAuthorize(AppPermissions.Pages_JobFunctions)]
    public class JobFunctionsAppService : TestAppServiceBase, IJobFunctionsAppService
    {
		private readonly IRepository<JobFunction> _jobFunctionRepository;
		private readonly IJobFunctionsExcelExporter _jobFunctionsExcelExporter;
		private readonly IRepository<Department,int> _departmentRepository;
        private readonly IRepository<JobFunctionsAssetAsset> _jobFunctionsAssetAssetRepository;
        private readonly IRepository<JobFunctionAssetBranche> _jobFunctionAssetBrancheRepository;
        private readonly IRepository<Asset, int> _assetRepository;
        private readonly IRepository<JobFunctionAsset, int> _jobFunctionAssetRepository;
        private readonly IRepository<Application, int> _applicationRepository;


        public JobFunctionsAppService(
            IRepository<JobFunction> jobFunctionRepository, 
            IJobFunctionsExcelExporter jobFunctionsExcelExporter,
            IRepository<Department, int> departmentRepository, 
            IRepository<JobFunctionsAssetAsset> jobFunctionsAssetAssetRepository, 
            IRepository<JobFunctionAssetBranche> jobFunctionAssetBrancheRepository,
            IRepository<JobFunctionAsset, int> jobFunctionAssetRepository,
            IRepository<Application, int> applicationRepository,
            IRepository<Asset, int> assetRepository
            ) 
		  {
			_jobFunctionRepository = jobFunctionRepository;
			_jobFunctionsExcelExporter = jobFunctionsExcelExporter;
			_departmentRepository = departmentRepository;
            _jobFunctionAssetBrancheRepository = jobFunctionAssetBrancheRepository;
            _jobFunctionsAssetAssetRepository = jobFunctionsAssetAssetRepository;
            _jobFunctionAssetRepository = jobFunctionAssetRepository;
            _applicationRepository = applicationRepository;
            _assetRepository = assetRepository;
        }

		 public async Task<PagedResultDto<GetJobFunctionForView>> GetAll(GetAllJobFunctionsInput input)
         {
			
			var filteredJobFunctions = _jobFunctionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.JobCode.Contains(input.Filter) || e.JobName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobCodeFilter),  e => e.JobCode.ToLower() == input.JobCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobNameFilter),  e => e.JobName.ToLower() == input.JobNameFilter.ToLower().Trim());


            var query = (from o in filteredJobFunctions
                         join o1 in _departmentRepository.GetAll() on o.DepartmentId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetJobFunctionForView() {
                             JobFunction = ObjectMapper.Map<JobFunctionDto>(o),
                             DepartmentDeptName = s1 == null ? "" : s1.DeptName.ToString(),
                             JFAssetCount = "",
                             JFBranchCount = ""
                         })
						.WhereIf(!string.IsNullOrWhiteSpace(input.DepartmentDeptNameFilter), e => e.DepartmentDeptName.ToLower() == input.DepartmentDeptNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var jobFunctions = await query
                .OrderBy(input.Sorting ?? "jobFunction.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetJobFunctionForView>(
                totalCount,
                jobFunctions
            );
         }

        private async Task<int> CountAssetID(int assetID)
        {
            List<JobFunctionsAssetAsset> JFAsset = new List<JobFunctionsAssetAsset>();
            JFAsset = await _jobFunctionsAssetAssetRepository.GetAll().Where(x => x.JobFunctionAssetId == assetID).ToListAsync();
            return (JFAsset.Count);
        }

        private async Task<int> CountBranchID(int assetID)
        {
            List<JobFunctionAssetBranche> JFAsset = new List<JobFunctionAssetBranche>();
            JFAsset = await _jobFunctionAssetBrancheRepository.GetAll().Where(x => x.JobFunctionAssetId == assetID).ToListAsync();
            return (JFAsset.Count);
        }

        [AbpAuthorize(AppPermissions.Pages_JobFunctions_Edit)]
		 public async Task<GetJobFunctionForEditOutput> GetJobFunctionForEdit(EntityDto input)
         {
            var jobFunction = await _jobFunctionRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetJobFunctionForEditOutput {JobFunction = ObjectMapper.Map<CreateOrEditJobFunctionDto>(jobFunction)};

		    if (output.JobFunction.DepartmentId != null)
            {
                var department = await _departmentRepository.FirstOrDefaultAsync((int)output.JobFunction.DepartmentId);
                output.DepartmentDeptName = department.DeptName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditJobFunctionDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctions_Create)]
		 private async Task Create(CreateOrEditJobFunctionDto input)
         {
            var jobFunction = ObjectMapper.Map<JobFunction>(input);

			
			if (AbpSession.TenantId != null)
			{
				jobFunction.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _jobFunctionRepository.InsertAsync(jobFunction);
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctions_Edit)]
		 private async Task Update(CreateOrEditJobFunctionDto input)
         {
            var jobFunction = await _jobFunctionRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, jobFunction);
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctions_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _jobFunctionRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetJobFunctionsToExcel(GetAllJobFunctionsForExcelInput input)
         {
			
			var filteredJobFunctions = _jobFunctionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.JobCode.Contains(input.Filter) || e.JobName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobCodeFilter),  e => e.JobCode.ToLower() == input.JobCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobNameFilter),  e => e.JobName.ToLower() == input.JobNameFilter.ToLower().Trim());


			var query = (from o in filteredJobFunctions
                         join o1 in _departmentRepository.GetAll() on o.DepartmentId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetJobFunctionForView() { 
							JobFunction = ObjectMapper.Map<JobFunctionDto>(o),
                         	DepartmentDeptName = s1 == null ? "" : s1.DeptName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.DepartmentDeptNameFilter), e => e.DepartmentDeptName.ToLower() == input.DepartmentDeptNameFilter.ToLower().Trim());


            var jobFunctionListDtos = await query.ToListAsync();

            return _jobFunctionsExcelExporter.ExportToFile(jobFunctionListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_JobFunctions)]
         public async Task<PagedResultDto<DepartmentLookupTableDto>> GetAllDepartmentForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _departmentRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.DeptName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var departmentList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<DepartmentLookupTableDto>();
			foreach(var department in departmentList){
				lookupTableDtoList.Add(new DepartmentLookupTableDto
				{
					Id = department.Id,
					DisplayName = department.DeptName.ToString()
				});
			}

            return new PagedResultDto<DepartmentLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        public async Task<GetJobFunctionForView> GetJobFuntionAndRelatedAssets(int jobFunctionId)
        {
            var jobFunction = _jobFunctionRepository.FirstOrDefault(jobFunctionId);

            var result = new GetJobFunctionForView { JobFunction = ObjectMapper.Map<JobFunctionDto>(jobFunction) };

            if (result.JobFunction == null)
            {
                throw new Exception();
            }

            var department = await _departmentRepository.FirstOrDefaultAsync(x => x.Id == result.JobFunction.DepartmentId);
            result.DepartmentDeptName = department.DeptName;

            var jobFunctionAsset = await _jobFunctionAssetRepository.FirstOrDefaultAsync(x => x.JobFunctionId == result.JobFunction.Id);

            if (jobFunctionAsset == null) // job function does not have any asset attached
            {
                return result;
            }

            var jobFunctionAssetAsset = await _jobFunctionsAssetAssetRepository
                                            .FirstOrDefaultAsync(x => x.JobFunctionAssetId == jobFunctionAsset.Id);

            if (jobFunctionAssetAsset == null)
            {
                throw new Exception();
            }

            // get list of assets
            var assetList = _assetRepository.GetAll().Where(x => x.Id == jobFunctionAssetAsset.AssetId).ToList();

            if (assetList.Count() > 0)
            {
                var query = (from o in assetList
                             join o1 in _applicationRepository.GetAll() on o.ApplicationId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             select new GetAssetForView()
                             {
                                 Asset = ObjectMapper.Map<AssetDto>(o),
                                 ApplicationApplicationName = s1 == null ? "" : s1.ApplicationName.ToString(),
                             });

                var assets = query.ToList();

                result.AssetForView = assets;
            }
            else
            {
                result.AssetForView = null;
            }
            
            return result;
        }
    }
}