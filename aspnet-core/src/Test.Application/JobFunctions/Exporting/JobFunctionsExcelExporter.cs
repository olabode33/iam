using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.JobFunctions.Dtos;
using Test.Dto;
using Test.Storage;
using Test.IDMShared.Dtos;

namespace Test.JobFunctions.Exporting
{
    public class JobFunctionsExcelExporter : EpPlusExcelExporterBase, IJobFunctionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobFunctionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobFunctionForView> jobFunctions)
        {
            return CreateExcelPackage(
                "JobFunctions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JobFunctions"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("JobCode"),
                        L("JobName"),
                        (L("Department")) + L("DeptName")
                        );

                    AddObjects(
                        sheet, 2, jobFunctions,
                        _ => _.JobFunction.JobCode,
                        _ => _.JobFunction.JobName,
                        _ => _.DepartmentDeptName
                        );

					

                });
        }
    }
}
