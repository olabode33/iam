using System.Collections.Generic;
using Test.JobFunctions.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctions.Exporting
{
    public interface IJobFunctionsExcelExporter
    {
        FileDto ExportToFile(List<GetJobFunctionForView> jobFunctions);
    }
}