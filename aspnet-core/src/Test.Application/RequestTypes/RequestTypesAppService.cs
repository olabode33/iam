using Test.AssetTypes;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestTypes.Exporting;
using Test.RequestTypes.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.RequestTypesAssetTypes;

namespace Test.RequestTypes
{
	[AbpAuthorize(AppPermissions.Pages_RequestTypes)]
    public class RequestTypesAppService : TestAppServiceBase, IRequestTypesAppService
    {
		 private readonly IRepository<RequestType> _requestTypeRepository;
		 private readonly IRequestTypesExcelExporter _requestTypesExcelExporter;
		 private readonly IRepository<AssetType,int> _assetTypeRepository;
        private readonly IRepository<RequestTypesAssetType> _requestTypesAssetTypeRepository;


        public RequestTypesAppService(IRepository<RequestType> requestTypeRepository, IRequestTypesExcelExporter requestTypesExcelExporter , IRepository<AssetType, int> assetTypeRepository, IRepository<RequestTypesAssetType> requestTypesAssetTypeRepository) 
		  {
			_requestTypeRepository = requestTypeRepository;
			_requestTypesExcelExporter = requestTypesExcelExporter;
			_assetTypeRepository = assetTypeRepository;
            _requestTypesAssetTypeRepository = requestTypesAssetTypeRepository;

          }

		 public async Task<PagedResultDto<GetRequestTypeForView>> GetAll(GetAllRequestTypesInput input)
         {
			
			var filteredRequestTypes = _requestTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RequestCode.Contains(input.Filter) || e.RequestDescription.Contains(input.Filter) || e.RequestOtherDetails.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestCodeFilter),  e => e.RequestCode.ToLower() == input.RequestCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestDescriptionFilter),  e => e.RequestDescription.ToLower() == input.RequestDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestOtherDetailsFilter),  e => e.RequestOtherDetails.ToLower() == input.RequestOtherDetailsFilter.ToLower().Trim())
						.WhereIf(input.MinApprovalNoFilter != null, e => e.ApprovalNo >= input.MinApprovalNoFilter)
						.WhereIf(input.MaxApprovalNoFilter != null, e => e.ApprovalNo <= input.MaxApprovalNoFilter);


			var query = (from o in filteredRequestTypes
                         join o1 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetRequestTypeForView() {
							RequestType = ObjectMapper.Map<RequestTypeDto>(o),
                         	AssetTypeAssetTypeName = s1 == null ? "" : s1.AssetTypeName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestTypes = await query
                .OrderBy(input.Sorting ?? "requestType.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestTypeForView>(
                totalCount,
                requestTypes
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestTypes_Edit)]
		 public async Task<GetRequestTypeForEditOutput> GetRequestTypeForEdit(EntityDto input)
         {
            var requestType = await _requestTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestTypeForEditOutput {RequestType = ObjectMapper.Map<CreateOrEditRequestTypeDto>(requestType)};

            var filteredAsset = _requestTypesAssetTypeRepository.GetAll().Where(x => x.RequestTypeId == input.Id);
            var query = (from o in filteredAsset
                         join o1 in _assetTypeRepository.GetAllIncluding() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         select new AssetTypeLookupTableDto()
                         {
                             Id = (int)o.AssetTypeId,
                             DisplayName = s1.AssetTypeName,
                             AssetTypeDescription = s1.AssetTypeDesc
                         });
            var assetTypesProperties = await query.ToArrayAsync();
            output.AssetTypeID = assetTypesProperties;
            if (requestType.RequestExpiry) {
                var requestOtherType = await _requestTypeRepository.FirstOrDefaultAsync(Convert.ToInt32(requestType.ReverseRequestType));
                output.ReverseRequestTypeName = requestOtherType.RequestCode;
            }
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
                var RequestAssetDel = await _requestTypesAssetTypeRepository.GetAllListAsync(x => x.RequestTypeId == input.Id);
                if (input.Id > 0 && RequestAssetDel.Count > 0)
                {
                    foreach (var r in RequestAssetDel)
                    {
                        await _requestTypesAssetTypeRepository.DeleteAsync(r);
                    }
                }
                await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypes_Create)]
		 private async Task Create(CreateOrEditRequestTypeDto input)
         {
            var requestType = ObjectMapper.Map<RequestType>(input);
			if (AbpSession.TenantId != null) { requestType.TenantId = (int?) AbpSession.TenantId; }
            var RequestReturnID = await _requestTypeRepository.InsertAndGetIdAsync(requestType);
            CurrentUnitOfWork.SaveChanges();
            if (true)
            {
                foreach (var d in input.AssetTypeID)
                {
                    if (!String.IsNullOrEmpty(d.Id.ToString()))
                    {
                        RequestTypesAssetType req = new RequestTypesAssetType();
                        req.CreatorUserId = AbpSession.UserId;
                        req.TenantId = AbpSession.TenantId;
                        req.AssetTypeId = Convert.ToInt32(d.Id);
                        req.RequestTypeId = Convert.ToInt32(RequestReturnID);
                        await _requestTypesAssetTypeRepository.InsertAsync(req);
                    }
                }
            }
            //Update the Reverse RequestType
            if (!String.IsNullOrEmpty(input.ReverseRequestType.ToString()))
            {
                RequestType rType = new RequestType();
                rType = await _requestTypeRepository.FirstOrDefaultAsync((int)input.ReverseRequestType);
                rType.ReverseRequestType = RequestReturnID;
                rType.RequestExpiry = true;
                await _requestTypeRepository.UpdateAsync(rType);
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypes_Edit)]
		 private async Task Update(CreateOrEditRequestTypeDto input)
         {
            var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestType);
            foreach (var d in input.AssetTypeID)
            {
                if (!String.IsNullOrEmpty(d.Id.ToString()))
                {
                    RequestTypesAssetType req = new RequestTypesAssetType();
                    req.CreatorUserId = AbpSession.UserId;
                    req.TenantId = AbpSession.TenantId;
                    req.AssetTypeId = Convert.ToInt32(d.Id);
                    req.RequestTypeId = Convert.ToInt32(input.Id);
                    await _requestTypesAssetTypeRepository.InsertAsync(req);
                }
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            var RequestAssetDel = await _requestTypesAssetTypeRepository.GetAllListAsync(x => x.RequestTypeId == input.Id);
            if (input.Id > 0 && RequestAssetDel.Count > 0)
            {
                foreach (var r in RequestAssetDel)
                {
                    await _requestTypesAssetTypeRepository.DeleteAsync(r);
                }
            }
            await _requestTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRequestTypesToExcel(GetAllRequestTypesForExcelInput input)
         {
			
			var filteredRequestTypes = _requestTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RequestCode.Contains(input.Filter) || e.RequestDescription.Contains(input.Filter) || e.RequestOtherDetails.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestCodeFilter),  e => e.RequestCode.ToLower() == input.RequestCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestDescriptionFilter),  e => e.RequestDescription.ToLower() == input.RequestDescriptionFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestOtherDetailsFilter),  e => e.RequestOtherDetails.ToLower() == input.RequestOtherDetailsFilter.ToLower().Trim())
						.WhereIf(input.MinApprovalNoFilter != null, e => e.ApprovalNo >= input.MinApprovalNoFilter)
						.WhereIf(input.MaxApprovalNoFilter != null, e => e.ApprovalNo <= input.MaxApprovalNoFilter);


			var query = (from o in filteredRequestTypes
                         join o1 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetRequestTypeForView() { 
							RequestType = ObjectMapper.Map<RequestTypeDto>(o),
                         	AssetTypeAssetTypeName = s1 == null ? "" : s1.AssetTypeName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());


            var requestTypeListDtos = await query.ToListAsync();

            return _requestTypesExcelExporter.ExportToFile(requestTypeListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_RequestTypes)]
         public async Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetTypeName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetTypeLookupTableDto>();
			foreach(var assetType in assetTypeList){
				lookupTableDtoList.Add(new AssetTypeLookupTableDto
				{
					Id = assetType.Id,
					DisplayName = assetType.AssetTypeName.ToString(),
                    AssetTypeDescription = assetType.AssetTypeDesc.ToString()
                    
				});
			}

            return new PagedResultDto<AssetTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        public async Task<List<AssetTypeLookupTableDto>> GetSingleRequestTypeAssets(int id)
        {
            List<RequestTypesAssetType> assetTypeList = await _requestTypesAssetTypeRepository.GetAllListAsync(x=>x.RequestTypeId == id);

            var lookupTableDtoList = new List<AssetTypeLookupTableDto>();
            foreach (var assetType in assetTypeList)
            {
                var assetTypeDetails = await _assetTypeRepository.FirstOrDefaultAsync((int)assetType.AssetTypeId);
                lookupTableDtoList.Add(new AssetTypeLookupTableDto
                {
                    Id = (int)assetType.AssetTypeId,
                    DisplayName = assetTypeDetails.AssetTypeName.ToString(),
                    AssetTypeDescription = assetTypeDetails.AssetTypeDesc.ToString()
                });
            }

            return lookupTableDtoList;
        }
    }
}