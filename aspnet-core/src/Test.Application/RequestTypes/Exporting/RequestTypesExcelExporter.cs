using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RequestTypes.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RequestTypes.Exporting
{
    public class RequestTypesExcelExporter : EpPlusExcelExporterBase, IRequestTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RequestTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRequestTypeForView> requestTypes)
        {
            return CreateExcelPackage(
                "RequestTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestTypes"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("RequestCode"),
                        L("RequestDescription"),
                        L("RequestOtherDetails"),
                        L("ApprovalNo"),
                        L("RequestExpiry"),
                        (L("AssetType")) + L("AssetTypeName")
                        );

                    AddObjects(
                        sheet, 2, requestTypes,
                        _ => _.RequestType.RequestCode,
                        _ => _.RequestType.RequestDescription,
                        _ => _.RequestType.RequestOtherDetails,
                        _ => _.RequestType.ApprovalNo,
                        _ => _.RequestType.RequestExpiry,
                        _ => _.AssetTypeAssetTypeName
                        );

					

                });
        }
    }
}
