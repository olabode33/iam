using System.Collections.Generic;
using Test.RequestTypes.Dtos;
using Test.Dto;

namespace Test.RequestTypes.Exporting
{
    public interface IRequestTypesExcelExporter
    {
        FileDto ExportToFile(List<GetRequestTypeForView> requestTypes);
    }
}