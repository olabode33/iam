
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.ViewApprovedRequests.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Requests;
using Test.RequestTypes;
using Test.Authorization.Users;
using Test.Assets;
using Test.RequestAssets;
using Test.UserSupervisors;
using Test.ApproveRequests;
using Test.Requests.Dtos;
using Test.Assets.Dtos;
using Test.IDMShared.Dtos;

namespace Test.ViewApprovedRequests
{
	[AbpAuthorize(AppPermissions.Pages_ViewApprovedRequests)]
    public class ViewApprovedRequestsAppService : TestAppServiceBase, IViewApprovedRequestsAppService
    {
		 private readonly IRepository<ViewApprovedRequest> _viewApprovedRequestRepository;
        private readonly IRepository<Request> _requestRepository;
        private readonly IRepository<RequestType, int> _requestTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Asset, int> _assetRepository;
        private readonly IRepository<RequestAsset> _requestAssetsRepository;
        private readonly IRepository<UserSupervisor> _userSupervisorRepository;
        private readonly IRepository<ApproveRequest> _approveRequestRepository;


        public ViewApprovedRequestsAppService(IRepository<ViewApprovedRequest> viewApprovedRequestRepository, IRepository<Request> requestRepository,
            IRepository<RequestType, int> requestTypeRepository, IRepository<User, long> userRepository, IRepository<Asset, int> assetRepository,
            IRepository<RequestAsset> requestAssetsRepository, IRepository<UserSupervisor> userSupervisorRepository, IRepository<ApproveRequest> approveRequestRepository) 
		  {
			_viewApprovedRequestRepository = viewApprovedRequestRepository;
            _requestRepository = requestRepository;
            _requestTypeRepository = requestTypeRepository;
            _approveRequestRepository = approveRequestRepository;
            _userSupervisorRepository = userSupervisorRepository;
            _requestAssetsRepository = requestAssetsRepository;
            _userRepository = userRepository;
            _assetRepository = assetRepository;
          }

		 public async Task<PagedResultDto<GetRequestForView>> GetAll(GetAllViewApprovedRequestsInput input)
         {

            var filteredRequests = _requestRepository.GetAll().Where(x => x.RequestOwnerId == AbpSession.UserId);

            var query = (from o in filteredRequests
                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetRequestForView()
                         {
                             Request = ObjectMapper.Map<RequestDto>(o),
                             RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                             UserName = s2 == null ? "" : s2.Name.ToString()
                         });
                        //.Where(x => x.Request.InitiationTime < DateTime.Now);

            var totalCount = await query.CountAsync();

            var requests = await query
                .OrderBy(input.Sorting ?? "request.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestForView>(
                totalCount,
                requests
            );
        }
		 
		 [AbpAuthorize(AppPermissions.Pages_ViewApprovedRequests_Edit)]
		 public async Task<GetRequestForEditOutput> GetViewApprovedRequestForEdit(EntityDto input)
        {
            var request = await _requestRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetRequestForEditOutput { Request = ObjectMapper.Map<CreateOrEditRequestDto>(request) };
            if (output.Request.RequestTypeId != null)
            {
                var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)output.Request.RequestTypeId);
                output.RequestTypeRequestCode = requestType.RequestCode.ToString();
            }
            if (output.Request.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Request.UserId);
                output.UserName = user.Name.ToString();
            }

            var assets = await _requestAssetsRepository.GetAllIncluding(x => x.Asset).Where(x => x.RequestId == input.Id).ToListAsync();
            List<Asset> assetList = new List<Asset>();
            foreach (var asset in assets)
            {
                assetList.Add(asset.Asset);
            }
            output.Assets = ObjectMapper.Map<List<AssetDto>>(assetList).ToArray();

            return output;
        }



    }
}