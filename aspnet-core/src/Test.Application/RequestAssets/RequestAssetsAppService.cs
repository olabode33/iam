using Test.Assets;
using Test.Requests;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestAssets.Exporting;
using Test.RequestAssets.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.Assets.Dtos;

namespace Test.RequestAssets
{
	[AbpAuthorize(AppPermissions.Pages_RequestAssets)]
    public class RequestAssetsAppService : TestAppServiceBase, IRequestAssetsAppService
    {
		 private readonly IRepository<RequestAsset> _requestAssetRepository;
		 private readonly IRequestAssetsExcelExporter _requestAssetsExcelExporter;
		 private readonly IRepository<Asset,int> _assetRepository;
		 private readonly IRepository<Request,int> _requestRepository;
		 

		  public RequestAssetsAppService(IRepository<RequestAsset> requestAssetRepository, IRequestAssetsExcelExporter requestAssetsExcelExporter , IRepository<Asset, int> assetRepository, IRepository<Request, int> requestRepository) 
		  {
			_requestAssetRepository = requestAssetRepository;
			_requestAssetsExcelExporter = requestAssetsExcelExporter;
			_assetRepository = assetRepository;
		    _requestRepository = requestRepository;		
		  }

		 public async Task<PagedResultDto<GetRequestAssetForView>> GetAll(GetAllRequestAssetsInput input)
         {
			
			var filteredRequestAssets = _requestAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetRequest.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetRequestFilter),  e => e.AssetRequest.ToLower() == input.AssetRequestFilter.ToLower().Trim());


			var query = (from o in filteredRequestAssets
                         join o1 in _assetRepository.GetAll() on o.AssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestAssetForView() {
							RequestAsset = ObjectMapper.Map<RequestAssetDto>(o),
                         	AssetAssetName = s1 == null ? "" : s1.AssetName.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestAssets = await query
                .OrderBy(input.Sorting ?? "requestAsset.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestAssetForView>(
                totalCount,
                requestAssets
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestAssets_Edit)]
		 public async Task<GetRequestAssetForEditOutput> GetRequestAssetForEdit(EntityDto input)
         {
            var requestAsset = await _requestAssetRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestAssetForEditOutput {RequestAsset = ObjectMapper.Map<CreateOrEditRequestAssetDto>(requestAsset)};

		    if (output.RequestAsset.AssetId != null)
            {
                var asset = await _assetRepository.FirstOrDefaultAsync((int)output.RequestAsset.AssetId);
                output.AssetAssetName = asset.AssetName.ToString();
            }

		    if (output.RequestAsset.RequestId != null)
            {
                var request = await _requestRepository.FirstOrDefaultAsync((int)output.RequestAsset.RequestId);
                output.RequestTenantId = request.TenantId.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestAssetDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestAssets_Create)]
		 private async Task Create(CreateOrEditRequestAssetDto input)
         {
            var requestAsset = ObjectMapper.Map<RequestAsset>(input);

			
			if (AbpSession.TenantId != null)
			{
				requestAsset.TenantId = (int?) AbpSession.TenantId;
			}

            requestAsset.RejectedBy = "0";
            await _requestAssetRepository.InsertAsync(requestAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestAssets_Edit)]
		 private async Task Update(CreateOrEditRequestAssetDto input)
         {
            var requestAsset = await _requestAssetRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestAssets_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _requestAssetRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRequestAssetsToExcel(GetAllRequestAssetsForExcelInput input)
         {
			
			var filteredRequestAssets = _requestAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AssetRequest.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetRequestFilter),  e => e.AssetRequest.ToLower() == input.AssetRequestFilter.ToLower().Trim());


			var query = (from o in filteredRequestAssets
                         join o1 in _assetRepository.GetAll() on o.AssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestAssetForView() { 
							RequestAsset = ObjectMapper.Map<RequestAssetDto>(o),
                         	AssetAssetName = s1 == null ? "" : s1.AssetName.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim());


            var requestAssetListDtos = await query.ToListAsync();

            return _requestAssetsExcelExporter.ExportToFile(requestAssetListDtos);
         }

		[AbpAuthorize(AppPermissions.Pages_RequestAssets)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString()
				});
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestAssets)]
         public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestLookupTableDto>();
			foreach(var request in requestList){
				lookupTableDtoList.Add(new RequestLookupTableDto
				{
					Id = request.Id,
					DisplayName = request.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

    }
}