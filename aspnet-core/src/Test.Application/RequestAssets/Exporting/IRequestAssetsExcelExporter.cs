using System.Collections.Generic;
using Test.RequestAssets.Dtos;
using Test.Dto;

namespace Test.RequestAssets.Exporting
{
    public interface IRequestAssetsExcelExporter
    {
        FileDto ExportToFile(List<GetRequestAssetForView> requestAssets);
    }
}