using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RequestAssets.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RequestAssets.Exporting
{
    public class RequestAssetsExcelExporter : EpPlusExcelExporterBase, IRequestAssetsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RequestAssetsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRequestAssetForView> requestAssets)
        {
            return CreateExcelPackage(
                "RequestAssets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestAssets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AssetRequest"),
                        (L("Asset")) + L("AssetName"),
                        (L("Request")) + L("TenantId")
                        );

                    AddObjects(
                        sheet, 2, requestAssets,
                        _ => _.RequestAsset.AssetRequest,
                        _ => _.AssetAssetName,
                        _ => _.RequestTenantId
                        );

					

                });
        }
    }
}
