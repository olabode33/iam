using Test.AssetTypes;
using Test.RequestTypes;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestTypesAssetTypes.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.RequestTypesAssetTypes
{
	[AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes)]
    public class RequestTypesAssetTypesAppService : TestAppServiceBase, IRequestTypesAssetTypesAppService
    {
		 private readonly IRepository<RequestTypesAssetType> _requestTypesAssetTypeRepository;
		 private readonly IRepository<AssetType,int> _assetTypeRepository;
		 private readonly IRepository<RequestType,int> _requestTypeRepository;
		 

		  public RequestTypesAssetTypesAppService(IRepository<RequestTypesAssetType> requestTypesAssetTypeRepository , IRepository<AssetType, int> assetTypeRepository, IRepository<RequestType, int> requestTypeRepository) 
		  {
			_requestTypesAssetTypeRepository = requestTypesAssetTypeRepository;
			_assetTypeRepository = assetTypeRepository;
		_requestTypeRepository = requestTypeRepository;
		
		  }

		 public async Task<PagedResultDto<GetRequestTypesAssetTypeForView>> GetAll(GetAllRequestTypesAssetTypesInput input)
         {
			
			var filteredRequestTypesAssetTypes = _requestTypesAssetTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredRequestTypesAssetTypes
                         join o1 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestTypesAssetTypeForView() {
							RequestTypesAssetType = ObjectMapper.Map<RequestTypesAssetTypeDto>(o),
                         	AssetTypeAssetTypeName = s1 == null ? "" : s1.AssetTypeName.ToString(),
                         	RequestTypeTenantId = s2 == null ? "" : s2.TenantId.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTypeTenantIdFilter), e => e.RequestTypeTenantId.ToLower() == input.RequestTypeTenantIdFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestTypesAssetTypes = await query
                .OrderBy(input.Sorting ?? "requestTypesAssetType.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestTypesAssetTypeForView>(
                totalCount,
                requestTypesAssetTypes
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes_Edit)]
		 public async Task<GetRequestTypesAssetTypeForEditOutput> GetRequestTypesAssetTypeForEdit(EntityDto input)
         {
            var requestTypesAssetType = await _requestTypesAssetTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestTypesAssetTypeForEditOutput {RequestTypesAssetType = ObjectMapper.Map<CreateOrEditRequestTypesAssetTypeDto>(requestTypesAssetType)};

		    if (output.RequestTypesAssetType.AssetTypeId != null)
            {
                var assetType = await _assetTypeRepository.FirstOrDefaultAsync((int)output.RequestTypesAssetType.AssetTypeId);
                output.AssetTypeAssetTypeName = assetType.AssetTypeName.ToString();
            }

		    if (output.RequestTypesAssetType.RequestTypeId != null)
            {
                var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)output.RequestTypesAssetType.RequestTypeId);
                output.RequestTypeTenantId = requestType.TenantId.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestTypesAssetTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes_Create)]
		 private async Task Create(CreateOrEditRequestTypesAssetTypeDto input)
         {
            var requestTypesAssetType = ObjectMapper.Map<RequestTypesAssetType>(input);

			
			if (AbpSession.TenantId != null)
			{
				requestTypesAssetType.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _requestTypesAssetTypeRepository.InsertAsync(requestTypesAssetType);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes_Edit)]
		 private async Task Update(CreateOrEditRequestTypesAssetTypeDto input)
         {
            var requestTypesAssetType = await _requestTypesAssetTypeRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestTypesAssetType);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _requestTypesAssetTypeRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes)]
         public async Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetTypeName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetTypeLookupTableDto>();
			foreach(var assetType in assetTypeList){
				lookupTableDtoList.Add(new AssetTypeLookupTableDto
				{
					Id = assetType.Id,
					DisplayName = assetType.AssetTypeName.ToString()
				});
			}

            return new PagedResultDto<AssetTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestTypesAssetTypes)]
         public async Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestTypeLookupTableDto>();
			foreach(var requestType in requestTypeList){
				lookupTableDtoList.Add(new RequestTypeLookupTableDto
				{
					Id = requestType.Id,
					DisplayName = requestType.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}