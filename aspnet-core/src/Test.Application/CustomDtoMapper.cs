using Test.ConflictAssets.Dtos;
using Test.ConflictAssets;
using Test.TreatRequests.Dtos;
using Test.TreatRequests;
using Test.ViewApprovedRequests.Dtos;
using Test.ViewApprovedRequests;
using Test.Conflicts.Dtos;
using Test.Conflicts;
using Test.JobFunctionsAssetAssets.Dtos;
using Test.JobFunctionsAssetAssets;
using Test.RequestRejections.Dtos;
using Test.RequestRejections;
using Test.ApproveRequests.Dtos;
using Test.ApproveRequests;
using Test.RequestTypesAssetTypes.Dtos;
using Test.RequestTypesAssetTypes;
using Test.AssetTypeOtherProperties.Dtos;
using Test.AssetTypeOtherProperties;
using Test.JobFunctionAssetBranches.Dtos;
using Test.JobFunctionAssetBranches;
using Test.UserSupervisors.Dtos;
using Test.UserSupervisors;
using Test.UserPriviledges.Dtos;
using Test.UserPriviledges;
using Test.RequestAssets.Dtos;
using Test.RequestAssets;
using Test.RequestEvents.Dtos;
using Test.RequestEvents;
using Test.RequestApprovals.Dtos;
using Test.RequestApprovals;
using Test.Requests.Dtos;
using Test.Requests;
using Test.AssetProperties.Dtos;
using Test.AssetProperties;
using Test.JobFunctionAssets.Dtos;
using Test.JobFunctionAssets;
using Test.JobFunctions.Dtos;
using Test.JobFunctions;
using Test.Assets.Dtos;
using Test.Assets;
using Test.RequestTypes.Dtos;
using Test.RequestTypes;
using Test.AssetTypes.Dtos;
using Test.AssetTypes;
using Test.Applications.Dtos;
using Test.Applications;
using Test.Branches.Dtos;
using Test.Branches;
using Test.Departments.Dtos;
using Test.Departments;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using AutoMapper;
using Test.Auditing.Dto;
using Test.Authorization.Accounts.Dto;
using Test.Authorization.Permissions.Dto;
using Test.Authorization.Roles;
using Test.Authorization.Roles.Dto;
using Test.Authorization.Users;
using Test.Authorization.Users.Dto;
using Test.Authorization.Users.Profile.Dto;
using Test.Chat;
using Test.Chat.Dto;
using Test.Editions;
using Test.Editions.Dto;
using Test.Friendships;
using Test.Friendships.Cache;
using Test.Friendships.Dto;
using Test.Localization.Dto;
using Test.MultiTenancy;
using Test.MultiTenancy.Dto;
using Test.MultiTenancy.HostDashboard.Dto;
using Test.MultiTenancy.Payments;
using Test.MultiTenancy.Payments.Dto;
using Test.Notifications.Dto;
using Test.Organizations.Dto;
using Test.Sessions.Dto;
using Test.IDMShared.Dtos;

namespace Test
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
           configuration.CreateMap<CreateOrEditConflictAssetDto, ConflictAsset>();
           configuration.CreateMap<ConflictAsset, ConflictAssetDto>();
           configuration.CreateMap<CreateOrEditTreatRequestDto, TreatRequest>();
           configuration.CreateMap<TreatRequest, TreatRequestDto>();
           configuration.CreateMap<CreateOrEditViewApprovedRequestDto, ViewApprovedRequest>();
           configuration.CreateMap<ViewApprovedRequest, ViewApprovedRequestDto>();
           configuration.CreateMap<CreateOrEditConflictDto, Conflict>();
           configuration.CreateMap<Conflict, ConflictDto>();
           configuration.CreateMap<CreateOrEditJobFunctionsAssetAssetDto, JobFunctionsAssetAsset>();
           configuration.CreateMap<JobFunctionsAssetAsset, JobFunctionsAssetAssetDto>();
           configuration.CreateMap<CreateOrEditRequestRejectionDto, RequestRejection>();
           configuration.CreateMap<RequestRejection, RequestRejectionDto>();
           configuration.CreateMap<RequestRejectionDto, RequestRejection>().ForMember(dto => dto.Request, opt => opt.Ignore());
           configuration.CreateMap<CreateOrEditApproveRequestDto, ApproveRequest>();
           configuration.CreateMap<ApproveRequest, ApproveRequestDto>();
           configuration.CreateMap<CreateOrEditRequestTypesAssetTypeDto, RequestTypesAssetType>();
           configuration.CreateMap<RequestTypesAssetType, RequestTypesAssetTypeDto>();
           configuration.CreateMap<CreateOrEditAssetTypeOtherPropertyDto, AssetTypeOtherProperty>();
           configuration.CreateMap<AssetTypeOtherProperty, AssetTypeOtherPropertyDto>();
           configuration.CreateMap<CreateOrEditJobFunctionAssetBrancheDto, JobFunctionAssetBranche>();
           configuration.CreateMap<JobFunctionAssetBranche, JobFunctionAssetBrancheDto>();
           configuration.CreateMap<CreateOrEditJobFunctionAssetBranchDto, JobFunctionAssetBranch>();
           configuration.CreateMap<JobFunctionAssetBranch, JobFunctionAssetBranchDto>();
           configuration.CreateMap<CreateOrEditUserSupervisorDto, UserSupervisor>();
           configuration.CreateMap<UserSupervisor, UserSupervisorDto>();
           configuration.CreateMap<CreateOrEditUserPriviledgeDto, UserPriviledge>();
           configuration.CreateMap<UserPriviledge, UserPriviledgeDto>();
           configuration.CreateMap<CreateOrEditRequestAssetDto, RequestAsset>();
           configuration.CreateMap<RequestAsset, RequestAssetDto>();
           configuration.CreateMap<CreateOrEditRequestEventDto, RequestEvent>();
           configuration.CreateMap<RequestEvent, RequestEventDto>();
           configuration.CreateMap<CreateOrEditRequestApprovalDto, RequestApproval>();
           configuration.CreateMap<RequestApproval, RequestApprovalDto>();
           
           configuration.CreateMap<CreateOrEditRequestDto, Request>();
           configuration.CreateMap<Request, CreateOrEditRequestDto>().ForMember(dto => dto.AssetId, options => options.Ignore());
            configuration.CreateMap<Conflict, CreateOrEditConflictDto>().ForMember(dto=>dto.Assets, options=>options.Ignore());
           configuration.CreateMap<Request, RequestDto>();
           configuration.CreateMap<CreateOrEditAssetPropertyDto, AssetProperty>();
           configuration.CreateMap<AssetProperty, AssetPropertyDto>();
           configuration.CreateMap<CreateOrEditJobFunctionAssetDto, JobFunctionAsset>();
           configuration.CreateMap<JobFunctionAsset, JobFunctionAssetDto>();
           configuration.CreateMap<CreateOrEditJobFunctionDto, JobFunction>();
           configuration.CreateMap<JobFunction, JobFunctionDto>();
           configuration.CreateMap<CreateOrEditAssetDto, Asset>();
           configuration.CreateMap<Asset, CreateOrEditAssetDto>().ForMember(dto=>dto.AssetProps, opt => opt.Ignore());
           configuration.CreateMap<Asset, AssetDto>();
           configuration.CreateMap<CreateOrEditRequestTypeDto, RequestType>();
           configuration.CreateMap<RequestType, RequestTypeDto>();
           configuration.CreateMap<CreateOrEditAssetTypeDto, AssetType>();
           configuration.CreateMap<AssetType,CreateOrEditAssetTypeDto>().ForMember(dto=>dto.AssetProperties, options=>options.Ignore());
           configuration.CreateMap<AssetType, AssetTypeDto>();
           configuration.CreateMap<CreateOrEditApplicationDto, Application>();
           configuration.CreateMap<Application, ApplicationDto>();
           configuration.CreateMap<CreateOrEditBranchDto, Branch>();
           configuration.CreateMap<Branch, BranchDto>();
           configuration.CreateMap<CreateOrEditDepartmentDto, Department>();
           configuration.CreateMap<Department, DepartmentDto>();
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>(); 

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();


            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */

        }
    }
}