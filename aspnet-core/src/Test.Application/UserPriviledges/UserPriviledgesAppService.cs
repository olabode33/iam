using Test.Requests;
using Test.Authorization.Users;
using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.UserPriviledges.Exporting;
using Test.UserPriviledges.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.RequestTypes;

namespace Test.UserPriviledges
{
    [AbpAuthorize(AppPermissions.Pages_UserPriviledges)]
    public class UserPriviledgesAppService : TestAppServiceBase, IUserPriviledgesAppService
    {
        private readonly IRepository<UserPriviledge> _userPriviledgeRepository;
        private readonly IUserPriviledgesExcelExporter _userPriviledgesExcelExporter;
        private readonly IRepository<Request, int> _requestRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Asset, int> _assetRepository;
        private readonly IRepository<RequestType, int> _requestTypeRepository;


        public UserPriviledgesAppService(IRepository<UserPriviledge> userPriviledgeRepository, IUserPriviledgesExcelExporter userPriviledgesExcelExporter, 
            IRepository<Request, int> requestRepository, IRepository<User, long> userRepository, IRepository<Asset, int> assetRepository,
            IRepository<RequestType, int> requestTypeRepository)
        {
            _userPriviledgeRepository = userPriviledgeRepository;
            _userPriviledgesExcelExporter = userPriviledgesExcelExporter;
            _requestRepository = requestRepository;
            _userRepository = userRepository;
            _assetRepository = assetRepository;
            _requestTypeRepository = requestTypeRepository;
        }

        public async Task<PagedResultDto<UserPriviledgesFilteredDto>> GetSelectedAll(SearchUserPriviledgeForView input)
        {
            var _userList = new List<int>();
            var filteredUserPriviledges = _userPriviledgeRepository.GetAll()
                    .WhereIf(input.UserID != null, e => input.UserID.Contains((int)e.UserId.Value))
                    .WhereIf(input.AssetID != null, e => input.AssetID.Contains(e.AssetId.Value));
                    //.WhereIf(!String.IsNullOrEmpty(input.MaxStartDate.ToString()) && !String.IsNullOrEmpty(input.MinStartDate.ToString()), e => e.StartDate <= input.MaxStartDate && e.StartDate >= input.MinStartDate);

            var query = (from o in filteredUserPriviledges
                         join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         join o2 in _requestTypeRepository.GetAll() on s1.RequestTypeId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         join o3 in _userRepository.GetAll() on o.UserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o4 in _assetRepository.GetAll() on o.AssetId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()
                         select new UserPriviledgesFilteredDto()
                         {
                             Id = (int)o.Id,
                             UserId = s3 == null ? 0 : s3.Id,
                             UserName = s3 == null ? "" : s3.Name.ToString() + " " + s3.Surname.ToString(),
                             AssetId = s4 == null ? 0 : s4.Id,
                             AssetName = s4 == null ? "" : s4.AssetName,
                             RequestId = s1 == null ? 0 : s1.Id,
                             RequestDesc = s1 == null ? "" : s1.RequestDescription,
                             RequestTypeId = s2 == null ? 0 : s2.Id,
                             RequestCode = s2 == null ? "" : s2.RequestCode,
                             ApplicationUsername = o == null ? "" : o.ApplicationUsername,
                             StartDate = o == null ? Convert.ToDateTime("01-01-1900") : o.StartDate,
                             EndDate = o == null ? Convert.ToDateTime("01-01-1900") : o.EndDate
                         }
                        ).WhereIf(input.RequestType != null, e => input.RequestType.Contains(e.RequestTypeId));

            var userPriviledgesFilteredDto = query.ToList();
            var totalcount = await query.CountAsync();

            return new PagedResultDto<UserPriviledgesFilteredDto>(
                totalcount,
                userPriviledgesFilteredDto
            );
        }


        public async Task<PagedResultDto<GetUserPriviledgeForView>> GetAll(GetAllUserPriviledgesInput input)
        {
            var filteredUserPriviledges = _userPriviledgeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ApplicationUsername.Contains(input.Filter))
                        .WhereIf(input.StatusFilter > -1, e => Convert.ToInt32(e.Status) == input.StatusFilter)
                        .WhereIf(input.MinStartDateFilter != null, e => e.StartDate >= input.MinStartDateFilter)
                        .WhereIf(input.MaxStartDateFilter != null, e => e.StartDate <= input.MaxStartDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationUsernameFilter), e => e.ApplicationUsername.ToLower() == input.ApplicationUsernameFilter.ToLower().Trim())
                        .WhereIf(input.MinEndDateFilter != null, e => e.EndDate >= input.MinEndDateFilter)
                        .WhereIf(input.MaxEndDateFilter != null, e => e.EndDate <= input.MaxEndDateFilter);


            var query = (from o in filteredUserPriviledges
                         join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _assetRepository.GetAll() on o.AssetId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new GetUserPriviledgeForView()
                         {
                             UserPriviledge = ObjectMapper.Map<UserPriviledgeDto>(o),
                             RequestRequestCode = s1 == null ? "" : s1.TenantId.ToString(),
                             UserName = s2 == null ? "" : s2.Name.ToString(),
                             AssetAssetName = s3 == null ? "" : s3.AssetName.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequestRequestCodeFilter), e => e.RequestRequestCode.ToLower() == input.RequestRequestCodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var userPriviledges = await query
                .OrderBy(input.Sorting ?? "userPriviledge.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetUserPriviledgeForView>(
                totalCount,
                userPriviledges
            );
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges_Edit)]
        public async Task<GetUserPriviledgeForEditOutput> GetUserPriviledgeForEdit(EntityDto input)
        {
            var userPriviledge = await _userPriviledgeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetUserPriviledgeForEditOutput { UserPriviledge = ObjectMapper.Map<CreateOrEditUserPriviledgeDto>(userPriviledge) };

            if (output.UserPriviledge.RequestId != null)
            {
                var request = await _requestRepository.FirstOrDefaultAsync((int)output.UserPriviledge.RequestId);
                output.RequestRequestCode = request.TenantId.ToString();
            }

            if (output.UserPriviledge.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.UserPriviledge.UserId);
                output.UserName = user.Name.ToString();
            }

            if (output.UserPriviledge.AssetId != null)
            {
                var asset = await _assetRepository.FirstOrDefaultAsync((int)output.UserPriviledge.AssetId);
                output.AssetAssetName = asset.AssetName.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditUserPriviledgeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges_Create)]
        private async Task Create(CreateOrEditUserPriviledgeDto input)
        {
            var userPriviledge = ObjectMapper.Map<UserPriviledge>(input);


            if (AbpSession.TenantId != null)
            {
                userPriviledge.TenantId = (int?)AbpSession.TenantId;
            }


            await _userPriviledgeRepository.InsertAsync(userPriviledge);
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges_Edit)]
        private async Task Update(CreateOrEditUserPriviledgeDto input)
        {
            var userPriviledge = await _userPriviledgeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, userPriviledge);
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _userPriviledgeRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetUserPriviledgesToExcel(GetAllUserPriviledgesForExcelInput input)
        {

            var filteredUserPriviledges = _userPriviledgeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ApplicationUsername.Contains(input.Filter))
                        .WhereIf(input.StatusFilter > -1, e => Convert.ToInt32(e.Status) == input.StatusFilter)
                        .WhereIf(input.MinStartDateFilter != null, e => e.StartDate >= input.MinStartDateFilter)
                        .WhereIf(input.MaxStartDateFilter != null, e => e.StartDate <= input.MaxStartDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ApplicationUsernameFilter), e => e.ApplicationUsername.ToLower() == input.ApplicationUsernameFilter.ToLower().Trim())
                        .WhereIf(input.MinEndDateFilter != null, e => e.EndDate >= input.MinEndDateFilter)
                        .WhereIf(input.MaxEndDateFilter != null, e => e.EndDate <= input.MaxEndDateFilter);


            var query = (from o in filteredUserPriviledges
                         join o1 in _requestRepository.GetAll() on o.RequestId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _assetRepository.GetAll() on o.AssetId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new GetUserPriviledgeForView()
                         {
                             UserPriviledge = ObjectMapper.Map<UserPriviledgeDto>(o),
                             RequestRequestCode = s1 == null ? "" : s1.TenantId.ToString(),
                             UserName = s2 == null ? "" : s2.Name.ToString(),
                             AssetAssetName = s3 == null ? "" : s3.AssetName.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequestRequestCodeFilter), e => e.RequestRequestCode.ToLower() == input.RequestRequestCodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());


            var userPriviledgeListDtos = await query.ToListAsync();

            return _userPriviledgesExcelExporter.ExportToFile(userPriviledgeListDtos);
        }



        [AbpAuthorize(AppPermissions.Pages_UserPriviledges)]
        public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _requestRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.TenantId.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<RequestLookupTableDto>();
            foreach (var request in requestList)
            {
                lookupTableDtoList.Add(new RequestLookupTableDto
                {
                    Id = request.Id,
                    DisplayName = request.RequestDescription.ToString()
                });
            }

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges)]
        public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _userRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<UserLookupTableDto>();
            foreach (var user in userList)
            {
                lookupTableDtoList.Add(new UserLookupTableDto
                {
                    Id = user.Id,
                    DisplayName = user.Name.ToString()
                });
            }

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_UserPriviledges)]
        public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _assetRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.AssetName.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<AssetLookupTableDto>();
            foreach (var asset in assetList)
            {
                lookupTableDtoList.Add(new AssetLookupTableDto
                {
                    Id = asset.Id,
                    DisplayName = asset.AssetName.ToString()
                });
            }

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}