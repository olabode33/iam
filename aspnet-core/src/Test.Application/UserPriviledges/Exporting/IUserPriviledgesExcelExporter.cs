using System.Collections.Generic;
using Test.UserPriviledges.Dtos;
using Test.Dto;

namespace Test.UserPriviledges.Exporting
{
    public interface IUserPriviledgesExcelExporter
    {
        FileDto ExportToFile(List<GetUserPriviledgeForView> userPriviledges);
    }
}