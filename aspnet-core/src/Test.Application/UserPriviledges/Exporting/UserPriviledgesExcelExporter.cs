using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.UserPriviledges.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.UserPriviledges.Exporting
{
    public class UserPriviledgesExcelExporter : EpPlusExcelExporterBase, IUserPriviledgesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public UserPriviledgesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetUserPriviledgeForView> userPriviledges)
        {
            return CreateExcelPackage(
                "UserPriviledges.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("UserPriviledges"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Status"),
                        L("StartDate"),
                        L("ApplicationUsername"),
                        L("EndDate"),
                        (L("Request")) + L("RequestCode"),
                        (L("User")) + L("Name"),
                        (L("Asset")) + L("AssetName")
                        );

                    AddObjects(
                        sheet, 2, userPriviledges,
                        _ => _.UserPriviledge.Status,
                        _ => _timeZoneConverter.Convert(_.UserPriviledge.StartDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.UserPriviledge.ApplicationUsername,
                        _ => _timeZoneConverter.Convert(_.UserPriviledge.EndDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.RequestRequestCode,
                        _ => _.UserName,
                        _ => _.AssetAssetName
                        );

					var startDateColumn = sheet.Column(2);
                    startDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					startDateColumn.AutoFit();
					var endDateColumn = sheet.Column(4);
                    endDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					endDateColumn.AutoFit();
					

                });
        }
    }
}
