using System.Collections.Generic;
using Test.TreatRequests.Dtos;
using Test.Dto;

namespace Test.TreatRequests.Exporting
{
    public interface ITreatRequestsExcelExporter
    {
        FileDto ExportToFile(List<GetTreatRequestForView> treatRequests);
    }
}