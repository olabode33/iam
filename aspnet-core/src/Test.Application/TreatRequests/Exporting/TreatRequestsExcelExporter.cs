using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.TreatRequests.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.TreatRequests.Exporting
{
    public class TreatRequestsExcelExporter : EpPlusExcelExporterBase, ITreatRequestsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TreatRequestsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTreatRequestForView> treatRequests)
        {
            return CreateExcelPackage(
                "TreatRequests.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TreatRequests"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("RequestName")
                        );

                    AddObjects(
                        sheet, 2, treatRequests,
                        _ => _.TreatRequest.RequestName
                        );

					

                });
        }
    }
}
