
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.TreatRequests.Exporting;
using Test.TreatRequests.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Requests;
using Test.Authorization.Users;
using Test.Assets;
using Test.RequestAssets;
using Test.Requests.Dtos;
using Test.RequestTypes;
using Test.Assets.Dtos;
using Test.AssetTypes;
using Test.AssetTypes.Dtos;
using Test.RequestTypes.Dtos;
using Abp.UI;
using Test.UserPriviledges;
using Test.IDMShared.Dtos;

namespace Test.TreatRequests
{
    [AbpAuthorize(AppPermissions.Pages_TreatRequests)]
    public class TreatRequestsAppService : TestAppServiceBase, ITreatRequestsAppService
    {
        private readonly IRepository<TreatRequest> _treatRequestRepository;
        private readonly ITreatRequestsExcelExporter _treatRequestsExcelExporter;
        private readonly IRepository<Request> _requestRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Asset, int> _assetRepository;
        private readonly IRepository<RequestType> _requestTypeRepository;
        private readonly IRepository<RequestAsset> _requestAssetsRepository;
        private readonly IRepository<AssetType> _assetTypeRepository;
        private readonly IRepository<UserPriviledge> _userPriviledgeRepository;


        public TreatRequestsAppService(IRepository<TreatRequest> treatRequestRepository, ITreatRequestsExcelExporter treatRequestsExcelExporter,
            IRepository<Request> requestRepository, IRepository<User, long> userRepository, IRepository<Asset, int> assetRepository,
            IRepository<RequestAsset> requestAssetsRepository, IRepository<RequestType> requestTypeRepository, IRepository<AssetType> assetTypeRepository,
            IRepository<UserPriviledge> userPriviledgeRepository)
        {
            _treatRequestRepository = treatRequestRepository;
            _treatRequestsExcelExporter = treatRequestsExcelExporter;
            _requestRepository = requestRepository;
            _userRepository = userRepository; _userPriviledgeRepository = userPriviledgeRepository;
            _assetRepository = assetRepository;
            _requestAssetsRepository = requestAssetsRepository;
            _requestTypeRepository = requestTypeRepository; _assetTypeRepository = assetTypeRepository;
        }

        public async Task<PagedResultDto<TreatRequestAssetDto>> GetAll()
        {
            List<GetRequestForViewTreated> GetRequestTreated = new List<GetRequestForViewTreated>();
            List<TreatRequestAssetDto> Ass = new List<TreatRequestAssetDto>();
            List<RequestDto> Req = new List<RequestDto>();
            List<RequestAsset> RequestAssets = _requestAssetsRepository.GetAll().Where(x => x.RequestAssetApproved == "A").Where(e=>e.LastApprovedDate <= DateTime.Now).ToList();
            for (int x = 0; x < RequestAssets.Count; x++)
            {
                var assetDetails = await _assetRepository.FirstOrDefaultAsync(xt => xt.Id == RequestAssets[x].AssetId);
                if (assetDetails != null)
                {
                    var RequestDetails = await _requestRepository.FirstOrDefaultAsync(xt => xt.Id == RequestAssets[x].RequestId);
                    if (RequestDetails != null)
                    {
                        var AssetTypeDetail = await _assetTypeRepository.FirstOrDefaultAsync(e => e.Id == assetDetails.AssetTypeId);
                        if (AssetTypeDetail != null)
                        {
                            var RequestType = await _requestTypeRepository.FirstOrDefaultAsync(e => e.Id == RequestDetails.RequestTypeId);
                            List<RequestAsset> RequestAssetsData = _requestAssetsRepository.GetAllList(xt => xt.RequestId == RequestAssets[x].RequestId).Where(r => r.AssetId == assetDetails.Id).ToList();
                            bool ret = false;
                            if (RequestAssets[x].RequestAssetApproved == "A")
                            {
                                if (RequestDetails.RequestApprovalStatus && RequestDetails.RequestStatus == "A")
                                {
                                    ret = true;
                                }
                            }
                            if (ret)
                            {
                                Ass.Add(new TreatRequestAssetDto
                                {
                                    AssetName = assetDetails.AssetName.ToString(),
                                    AssetCode = assetDetails.AssetCode.ToString(),
                                    AssetDescription = assetDetails.AssetDescription.ToString(),
                                    Requests = ObjectMapper.Map<RequestDto>(RequestDetails),
                                    Id = RequestAssets[x].Id,
                                    RequestAssetStatus = RequestAssetsData[0].RequestAssetApproved,
                                    RejectedBy = RequestAssetsData[0].RejectedBy,
                                    AssetType = ObjectMapper.Map<AssetTypeDto>(AssetTypeDetail),
                                    RequestOwner = ReturnFullName(RequestDetails.RequestOwnerId.ToString()),
                                    RType = ObjectMapper.Map<RequestTypeDto>(RequestType),
                                    RequestAssetTreated = RequestAssetsData[0].RequestAssetTreated,
                                    RequestAssetTreatedBy = ReturnFullName(RequestAssetsData[0].RequestAssetTreatedBy),
                                    RequestAssetTreatedDate = RequestAssetsData[0].RequestAssetTreatedDate,
                                    ApprovedBy = ReturnFullName((RequestAssetsData[0].LastApprovedBy)),
                                    ApprovedDate = RequestAssetsData[0].LastApprovedDate
                                });
                            }
                        }
                    }
                }
            }
            var query = Ass.AsQueryable();
            var data = query.OrderBy("ApprovedDate desc").ToList();

            return new PagedResultDto<TreatRequestAssetDto>(
                Ass.Count,
                data
            );
        }

        private bool ValidDouble(string y)
        {
            double r;
            bool isDouble = Double.TryParse(y, out r);
            return isDouble;
        }

        private string ReturnFullName(string id)
        {
            string name = "";
            if(ValidDouble(id))
            {
                var UserDetails = _userRepository.FirstOrDefault(e => e.Id == Convert.ToInt32(id));
                if (!String.IsNullOrEmpty(UserDetails.FullName.ToString())) {
                    name = UserDetails.FullName.ToString();
                }
            }
            return name;
        }

        public async Task RequestTreated(int RequestAssetID)
        {
            var RequestAss = await _requestAssetsRepository.FirstOrDefaultAsync(RequestAssetID);
            if (RequestAss.Id > 0)
            {
                var RequestDetails = await _requestRepository.FirstOrDefaultAsync(Convert.ToInt32(RequestAss.RequestId));
                if (RequestDetails.Id > 0)
                {
                    RequestAss.RequestAssetTreated = "A";
                    RequestAss.RequestAssetTreatedBy = AbpSession.UserId.ToString();
                    RequestAss.RequestAssetTreatedDate = DateTime.Now;

                    List<UserPriviledge> UserPri = _userPriviledgeRepository.GetAll().Where(x => x.UserId == RequestDetails.RequestOwnerId).Where(t => t.AssetId == RequestAss.AssetId).ToList();
                    var UserPrivilegeDetail = await _userPriviledgeRepository.FirstOrDefaultAsync(x => x.UserId == RequestDetails.RequestOwnerId && x.AssetId == RequestAss.AssetId);                    
                    if (UserPrivilegeDetail == null)
                    {

                        UserPriviledge User_Priviledge = new UserPriviledge();
                        User_Priviledge.ApplicationUsername = "olusegun.adeniyi";
                        User_Priviledge.StartDate = RequestDetails.InitiationTime;
                        User_Priviledge.EndDate = RequestDetails.ExpiryDate;
                        User_Priviledge.UserId = RequestDetails.RequestOwnerId;
                        User_Priviledge.AssetId = RequestAss.AssetId;
                        User_Priviledge.RequestId = RequestAss.RequestId;
                        User_Priviledge.TenantId = AbpSession.TenantId;
                        User_Priviledge.CreatorUserId = AbpSession.UserId;
                        User_Priviledge.Status = true;

                        if (RequestDetails.InitiatingRequestID > 0)
                        {
                            var InitiatingRequestDetails = await _userPriviledgeRepository.FirstOrDefaultAsync(x => x.UserId == RequestDetails.RequestOwnerId && x.AssetId == RequestAss.AssetId && x.RequestId == RequestDetails.InitiatingRequestID);
                            if (InitiatingRequestDetails != null)
                            {
                                UserPrivilegeDetail.Status = false;
                                UserPrivilegeDetail.EndDate = DateTime.Now.AddMinutes(-1);
                                await _userPriviledgeRepository.UpdateAsync(UserPrivilegeDetail);
                            }
                        }
                        await _userPriviledgeRepository.InsertAsync(User_Priviledge);
                    }
                    else
                    {
                        var InitiatingRequestDetails = await _userPriviledgeRepository.FirstOrDefaultAsync(x => x.UserId == RequestDetails.RequestOwnerId && x.AssetId == RequestAss.AssetId && x.RequestId == RequestDetails.InitiatingRequestID);
                        if (InitiatingRequestDetails != null)
                        {
                            UserPrivilegeDetail.Status = false;
                            UserPrivilegeDetail.EndDate = DateTime.Now.AddMinutes(-1);
                            await _userPriviledgeRepository.UpdateAsync(UserPrivilegeDetail);
                        }
                    }
                    await _requestAssetsRepository.UpdateAsync(RequestAss);
                }
                else
                {
                    throw new UserFriendlyException("Unable to find Request Detail.");
                }
            }
            else
            {
                throw new UserFriendlyException("Unable to find Request.");
            }
        }

        public async Task RequestRejected(int RequestAssetID)
        {
            var RequestAss = await _requestAssetsRepository.FirstOrDefaultAsync(RequestAssetID);
            if (RequestAss.Id > 0)
            {
                RequestAss.RequestAssetTreated = "R";
                RequestAss.RequestAssetTreatedBy = AbpSession.UserId.ToString();
                RequestAss.RequestAssetTreatedDate = DateTime.Now;
                await _requestAssetsRepository.UpdateAsync(RequestAss);
            }
            else
            {
                throw new UserFriendlyException("Unable to find Request.");
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TreatRequests_Edit)]
        public async Task<GetTreatRequestForEditOutput> GetTreatRequestForEdit(EntityDto input)
        {
            var treatRequest = await _treatRequestRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetTreatRequestForEditOutput { TreatRequest = ObjectMapper.Map<CreateOrEditTreatRequestDto>(treatRequest) };
            return output;
        }

        private async Task CreateOrEdit(CreateOrEditTreatRequestDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TreatRequests_Create)]
        private async Task Create(CreateOrEditTreatRequestDto input)
        {
            var treatRequest = ObjectMapper.Map<TreatRequest>(input);


            if (AbpSession.TenantId != null)
            {
                treatRequest.TenantId = (int?)AbpSession.TenantId;
            }


            await _treatRequestRepository.InsertAsync(treatRequest);
        }

        [AbpAuthorize(AppPermissions.Pages_TreatRequests_Edit)]
        private async Task Update(CreateOrEditTreatRequestDto input)
        {
            var treatRequest = await _treatRequestRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, treatRequest);
        }

        [AbpAuthorize(AppPermissions.Pages_TreatRequests_Delete)]
        private async Task Delete(EntityDto input)
        {
            await _treatRequestRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetTreatRequestsToExcel(GetAllTreatRequestsForExcelInput input)
        {

            var filteredTreatRequests = _treatRequestRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.RequestName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequestNameFilter), e => e.RequestName.ToLower() == input.RequestNameFilter.ToLower().Trim());


            var query = (from o in filteredTreatRequests
                         select new GetTreatRequestForView()
                         {
                             TreatRequest = ObjectMapper.Map<TreatRequestDto>(o)
                         });


            var treatRequestListDtos = await query.ToListAsync();

            return _treatRequestsExcelExporter.ExportToFile(treatRequestListDtos);
        }


    }
}