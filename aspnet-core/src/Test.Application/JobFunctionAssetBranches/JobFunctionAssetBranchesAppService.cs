using Test.JobFunctionAssets;
using Test.Branches;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.JobFunctionAssetBranches.Exporting;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssetBranches
{
	[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches)]
    public class JobFunctionAssetBranchesAppService : TestAppServiceBase, IJobFunctionAssetBranchesAppService
    {
		 private readonly IRepository<JobFunctionAssetBranche> _jobFunctionAssetBrancheRepository;
		 private readonly IJobFunctionAssetBranchesExcelExporter _jobFunctionAssetBranchesExcelExporter;
		 private readonly IRepository<JobFunctionAsset,int> _jobFunctionAssetRepository;
		 private readonly IRepository<Branch,int> _branchRepository;
		 

		  public JobFunctionAssetBranchesAppService(IRepository<JobFunctionAssetBranche> jobFunctionAssetBrancheRepository, IJobFunctionAssetBranchesExcelExporter jobFunctionAssetBranchesExcelExporter , IRepository<JobFunctionAsset, int> jobFunctionAssetRepository, IRepository<Branch, int> branchRepository) 
		  {
			_jobFunctionAssetBrancheRepository = jobFunctionAssetBrancheRepository;
			_jobFunctionAssetBranchesExcelExporter = jobFunctionAssetBranchesExcelExporter;
			_jobFunctionAssetRepository = jobFunctionAssetRepository;
		_branchRepository = branchRepository;
		
		  }

		 public async Task<PagedResultDto<GetJobFunctionAssetBrancheForView>> GetAll(GetAllJobFunctionAssetBranchesInput input)
         {
			
			var filteredJobFunctionAssetBranches = _jobFunctionAssetBrancheRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionAssetBranches
                         join o1 in _jobFunctionAssetRepository.GetAll() on o.JobFunctionAssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionAssetBrancheForView() {
							JobFunctionAssetBranche = ObjectMapper.Map<JobFunctionAssetBrancheDto>(o),
                         	JobFunctionAssetJobFunctionId = s1 == null ? "" : s1.JobFunctionId.ToString(),
                         	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionAssetJobFunctionIdFilter), e => e.JobFunctionAssetJobFunctionId.ToLower() == input.JobFunctionAssetJobFunctionIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var jobFunctionAssetBranches = await query
                .OrderBy(input.Sorting ?? "jobFunctionAssetBranche.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetJobFunctionAssetBrancheForView>(
                totalCount,
                jobFunctionAssetBranches
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Edit)]
		 public async Task<GetJobFunctionAssetBrancheForEditOutput> GetJobFunctionAssetBrancheForEdit(EntityDto input)
         {
            var jobFunctionAssetBranche = await _jobFunctionAssetBrancheRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetJobFunctionAssetBrancheForEditOutput {JobFunctionAssetBranche = ObjectMapper.Map<CreateOrEditJobFunctionAssetBrancheDto>(jobFunctionAssetBranche)};

		    if (output.JobFunctionAssetBranche.JobFunctionAssetId != null)
            {
                var jobFunctionAsset = await _jobFunctionAssetRepository.FirstOrDefaultAsync((int)output.JobFunctionAssetBranche.JobFunctionAssetId);
                output.JobFunctionAssetJobFunctionId = jobFunctionAsset.JobFunctionId.ToString();
            }

		    if (output.JobFunctionAssetBranche.BranchId != null)
            {
                var branch = await _branchRepository.FirstOrDefaultAsync((int)output.JobFunctionAssetBranche.BranchId);
                output.BranchBranchName = branch.BranchName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditJobFunctionAssetBrancheDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Create)]
		 private async Task Create(CreateOrEditJobFunctionAssetBrancheDto input)
         {
            var jobFunctionAssetBranche = ObjectMapper.Map<JobFunctionAssetBranche>(input);

			
			if (AbpSession.TenantId != null)
			{
				jobFunctionAssetBranche.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _jobFunctionAssetBrancheRepository.InsertAsync(jobFunctionAssetBranche);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Edit)]
		 private async Task Update(CreateOrEditJobFunctionAssetBrancheDto input)
         {
            var jobFunctionAssetBranche = await _jobFunctionAssetBrancheRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, jobFunctionAssetBranche);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _jobFunctionAssetBrancheRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetJobFunctionAssetBranchesToExcel(GetAllJobFunctionAssetBranchesForExcelInput input)
         {
			
			var filteredJobFunctionAssetBranches = _jobFunctionAssetBrancheRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionAssetBranches
                         join o1 in _jobFunctionAssetRepository.GetAll() on o.JobFunctionAssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionAssetBrancheForView() { 
							JobFunctionAssetBranche = ObjectMapper.Map<JobFunctionAssetBrancheDto>(o),
                         	JobFunctionAssetJobFunctionId = s1 == null ? "" : s1.JobFunctionId.ToString(),
                         	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionAssetJobFunctionIdFilter), e => e.JobFunctionAssetJobFunctionId.ToLower() == input.JobFunctionAssetJobFunctionIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim());


            var jobFunctionAssetBrancheListDtos = await query.ToListAsync();

            return _jobFunctionAssetBranchesExcelExporter.ExportToFile(jobFunctionAssetBrancheListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches)]
         public async Task<PagedResultDto<JobFunctionAssetLookupTableDto>> GetAllJobFunctionAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _jobFunctionAssetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.JobFunctionId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var jobFunctionAssetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<JobFunctionAssetLookupTableDto>();
			foreach(var jobFunctionAsset in jobFunctionAssetList){
				lookupTableDtoList.Add(new JobFunctionAssetLookupTableDto
				{
					Id = jobFunctionAsset.Id,
					DisplayName = jobFunctionAsset.JobFunctionId.ToString()
				});
			}

            return new PagedResultDto<JobFunctionAssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranches)]
         public async Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _branchRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.BranchName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var branchList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<BranchLookupTableDto>();
			foreach(var branch in branchList){
				lookupTableDtoList.Add(new BranchLookupTableDto
				{
					Id = branch.Id,
					DisplayName = branch.BranchName.ToString()
				});
			}

            return new PagedResultDto<BranchLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}