using Test.JobFunctionAssets;
using Test.Branches;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.JobFunctionAssetBranches.Exporting;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssetBranches
{
	[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs)]
    public class JobFunctionAssetBranchsAppService : TestAppServiceBase, IJobFunctionAssetBranchsAppService
    {
		 private readonly IRepository<JobFunctionAssetBranch> _jobFunctionAssetBranchRepository;
		 private readonly IJobFunctionAssetBranchsExcelExporter _jobFunctionAssetBranchsExcelExporter;
		 private readonly IRepository<JobFunctionAsset,int> _jobFunctionAssetRepository;
		 private readonly IRepository<Branch,int> _branchRepository;
		 

		  public JobFunctionAssetBranchsAppService(IRepository<JobFunctionAssetBranch> jobFunctionAssetBranchRepository, IJobFunctionAssetBranchsExcelExporter jobFunctionAssetBranchsExcelExporter , IRepository<JobFunctionAsset, int> jobFunctionAssetRepository, IRepository<Branch, int> branchRepository) 
		  {
			_jobFunctionAssetBranchRepository = jobFunctionAssetBranchRepository;
			_jobFunctionAssetBranchsExcelExporter = jobFunctionAssetBranchsExcelExporter;
			_jobFunctionAssetRepository = jobFunctionAssetRepository;
		_branchRepository = branchRepository;
		
		  }

		 public async Task<PagedResultDto<GetJobFunctionAssetBranchForView>> GetAll(GetAllJobFunctionAssetBranchsInput input)
         {
			
			var filteredJobFunctionAssetBranchs = _jobFunctionAssetBranchRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionAssetBranchs
                         join o1 in _jobFunctionAssetRepository.GetAll() on o.JobFunctionAssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionAssetBranchForView() {
							JobFunctionAssetBranch = ObjectMapper.Map<JobFunctionAssetBranchDto>(o),
                         	JobFunctionAssetJobFunctionId = s1 == null ? "" : s1.JobFunctionId.ToString(),
                         	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionAssetJobFunctionIdFilter), e => e.JobFunctionAssetJobFunctionId.ToLower() == input.JobFunctionAssetJobFunctionIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var jobFunctionAssetBranchs = await query
                .OrderBy(input.Sorting ?? "jobFunctionAssetBranch.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetJobFunctionAssetBranchForView>(
                totalCount,
                jobFunctionAssetBranchs
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Edit)]
		 public async Task<GetJobFunctionAssetBranchForEditOutput> GetJobFunctionAssetBranchForEdit(EntityDto input)
         {
            var jobFunctionAssetBranch = await _jobFunctionAssetBranchRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetJobFunctionAssetBranchForEditOutput {JobFunctionAssetBranch = ObjectMapper.Map<CreateOrEditJobFunctionAssetBranchDto>(jobFunctionAssetBranch)};

		    if (output.JobFunctionAssetBranch.JobFunctionAssetId != null)
            {
                var jobFunctionAsset = await _jobFunctionAssetRepository.FirstOrDefaultAsync((int)output.JobFunctionAssetBranch.JobFunctionAssetId);
                output.JobFunctionAssetJobFunctionId = jobFunctionAsset.JobFunctionId.ToString();
            }

		    if (output.JobFunctionAssetBranch.BranchId != null)
            {
                var branch = await _branchRepository.FirstOrDefaultAsync((int)output.JobFunctionAssetBranch.BranchId);
                output.BranchBranchName = branch.BranchName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditJobFunctionAssetBranchDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Create)]
		 private async Task Create(CreateOrEditJobFunctionAssetBranchDto input)
         {
            var jobFunctionAssetBranch = ObjectMapper.Map<JobFunctionAssetBranch>(input);

			
			if (AbpSession.TenantId != null)
			{
				jobFunctionAssetBranch.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _jobFunctionAssetBranchRepository.InsertAsync(jobFunctionAssetBranch);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Edit)]
		 private async Task Update(CreateOrEditJobFunctionAssetBranchDto input)
         {
            var jobFunctionAssetBranch = await _jobFunctionAssetBranchRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, jobFunctionAssetBranch);
         }

		 [AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _jobFunctionAssetBranchRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetJobFunctionAssetBranchsToExcel(GetAllJobFunctionAssetBranchsForExcelInput input)
         {
			
			var filteredJobFunctionAssetBranchs = _jobFunctionAssetBranchRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionAssetBranchs
                         join o1 in _jobFunctionAssetRepository.GetAll() on o.JobFunctionAssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _branchRepository.GetAll() on o.BranchId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionAssetBranchForView() { 
							JobFunctionAssetBranch = ObjectMapper.Map<JobFunctionAssetBranchDto>(o),
                         	JobFunctionAssetJobFunctionId = s1 == null ? "" : s1.JobFunctionId.ToString(),
                         	BranchBranchName = s2 == null ? "" : s2.BranchName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionAssetJobFunctionIdFilter), e => e.JobFunctionAssetJobFunctionId.ToLower() == input.JobFunctionAssetJobFunctionIdFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.BranchBranchNameFilter), e => e.BranchBranchName.ToLower() == input.BranchBranchNameFilter.ToLower().Trim());


            var jobFunctionAssetBranchListDtos = await query.ToListAsync();

            return _jobFunctionAssetBranchsExcelExporter.ExportToFile(jobFunctionAssetBranchListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs)]
         public async Task<PagedResultDto<JobFunctionAssetLookupTableDto>> GetAllJobFunctionAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _jobFunctionAssetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.JobFunctionId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var jobFunctionAssetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<JobFunctionAssetLookupTableDto>();
			foreach(var jobFunctionAsset in jobFunctionAssetList){
				lookupTableDtoList.Add(new JobFunctionAssetLookupTableDto
				{
					Id = jobFunctionAsset.Id,
					DisplayName = jobFunctionAsset.JobFunctionId.ToString()
				});
			}

            return new PagedResultDto<JobFunctionAssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Administration_JobFunctionAssetBranchs)]
         public async Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _branchRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.BranchName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var branchList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<BranchLookupTableDto>();
			foreach(var branch in branchList){
				lookupTableDtoList.Add(new BranchLookupTableDto
				{
					Id = branch.Id,
					DisplayName = branch.BranchName.ToString()
				});
			}

            return new PagedResultDto<BranchLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}