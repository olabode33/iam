using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.JobFunctionAssetBranches.Exporting
{
    public class JobFunctionAssetBranchesExcelExporter : EpPlusExcelExporterBase, IJobFunctionAssetBranchesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobFunctionAssetBranchesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobFunctionAssetBrancheForView> jobFunctionAssetBranches)
        {
            return CreateExcelPackage(
                "JobFunctionAssetBranches.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JobFunctionAssetBranches"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("JobFunctionAsset")) + L("JobFunctionId"),
                        (L("Branch")) + L("BranchName")
                        );

                    AddObjects(
                        sheet, 2, jobFunctionAssetBranches,
                        _ => _.JobFunctionAssetJobFunctionId,
                        _ => _.BranchBranchName
                        );

					

                });
        }
    }
}
