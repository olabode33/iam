using System.Collections.Generic;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;

namespace Test.JobFunctionAssetBranches.Exporting
{
    public interface IJobFunctionAssetBranchesExcelExporter
    {
        FileDto ExportToFile(List<GetJobFunctionAssetBrancheForView> jobFunctionAssetBranches);
    }
}