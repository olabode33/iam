using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.JobFunctionAssetBranches.Exporting
{
    public class JobFunctionAssetBranchsExcelExporter : EpPlusExcelExporterBase, IJobFunctionAssetBranchsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobFunctionAssetBranchsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobFunctionAssetBranchForView> jobFunctionAssetBranchs)
        {
            return CreateExcelPackage(
                "JobFunctionAssetBranchs.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JobFunctionAssetBranchs"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("JobFunctionAsset")) + L("JobFunctionId"),
                        (L("Branch")) + L("BranchName")
                        );

                    AddObjects(
                        sheet, 2, jobFunctionAssetBranchs,
                        _ => _.JobFunctionAssetJobFunctionId,
                        _ => _.BranchBranchName
                        );

					

                });
        }
    }
}
