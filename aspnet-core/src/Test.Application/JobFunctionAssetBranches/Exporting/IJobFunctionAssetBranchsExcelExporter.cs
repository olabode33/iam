using System.Collections.Generic;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;

namespace Test.JobFunctionAssetBranches.Exporting
{
    public interface IJobFunctionAssetBranchsExcelExporter
    {
        FileDto ExportToFile(List<GetJobFunctionAssetBranchForView> jobFunctionAssetBranchs);
    }
}