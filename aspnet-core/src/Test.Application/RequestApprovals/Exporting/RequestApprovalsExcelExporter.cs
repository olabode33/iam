using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RequestApprovals.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RequestApprovals.Exporting
{
    public class RequestApprovalsExcelExporter : EpPlusExcelExporterBase, IRequestApprovalsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RequestApprovalsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRequestApprovalForView> requestApprovals)
        {
            return CreateExcelPackage(
                "RequestApprovals.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestApprovals"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Status"),
                        (L("User")) + L("Name"),
                        (L("Request")) + L("TenantId")
                        );

                    AddObjects(
                        sheet, 2, requestApprovals,
                        _ => _.RequestApproval.Status,
                        _ => _.UserName,
                        _ => _.RequestTenantId
                        );

					

                });
        }
    }
}
