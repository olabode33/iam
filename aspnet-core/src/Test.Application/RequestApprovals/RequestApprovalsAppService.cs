using Test.Authorization.Users;
using Test.Requests;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RequestApprovals.Exporting;
using Test.RequestApprovals.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.RequestApprovals
{
	[AbpAuthorize(AppPermissions.Pages_RequestApprovals)]
    public class RequestApprovalsAppService : TestAppServiceBase, IRequestApprovalsAppService
    {
		 private readonly IRepository<RequestApproval> _requestApprovalRepository;
		 private readonly IRequestApprovalsExcelExporter _requestApprovalsExcelExporter;
		 private readonly IRepository<User,long> _userRepository;
		 private readonly IRepository<Request,int> _requestRepository;
		 

		  public RequestApprovalsAppService(IRepository<RequestApproval> requestApprovalRepository, IRequestApprovalsExcelExporter requestApprovalsExcelExporter , IRepository<User, long> userRepository, IRepository<Request, int> requestRepository) 
		  {
			_requestApprovalRepository = requestApprovalRepository;
			_requestApprovalsExcelExporter = requestApprovalsExcelExporter;
			_userRepository = userRepository;
		_requestRepository = requestRepository;
		
		  }

		 public async Task<PagedResultDto<GetRequestApprovalForView>> GetAll(GetAllRequestApprovalsInput input)
         {
			
			var filteredRequestApprovals = _requestApprovalRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false )
						.WhereIf(input.StatusFilter > -1,  e => Convert.ToInt32(e.Status) == input.StatusFilter );


			var query = (from o in filteredRequestApprovals
                         join o1 in _userRepository.GetAll() on o.UserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestApprovalForView() {
							RequestApproval = ObjectMapper.Map<RequestApprovalDto>(o),
                         	UserName = s1 == null ? "" : s1.Name.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var requestApprovals = await query
                .OrderBy(input.Sorting ?? "requestApproval.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestApprovalForView>(
                totalCount,
                requestApprovals
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RequestApprovals_Edit)]
		 public async Task<GetRequestApprovalForEditOutput> GetRequestApprovalForEdit(EntityDto input)
         {
            var requestApproval = await _requestApprovalRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRequestApprovalForEditOutput {RequestApproval = ObjectMapper.Map<CreateOrEditRequestApprovalDto>(requestApproval)};

		    if (output.RequestApproval.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.RequestApproval.UserId);
                output.UserName = user.Name.ToString();
            }

		    if (output.RequestApproval.RequestId != null)
            {
                var request = await _requestRepository.FirstOrDefaultAsync((int)output.RequestApproval.RequestId);
                output.RequestTenantId = request.TenantId.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestApprovalDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestApprovals_Create)]
		 private async Task Create(CreateOrEditRequestApprovalDto input)
         {
            var requestApproval = ObjectMapper.Map<RequestApproval>(input);

			
			if (AbpSession.TenantId != null)
			{
				requestApproval.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _requestApprovalRepository.InsertAsync(requestApproval);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestApprovals_Edit)]
		 private async Task Update(CreateOrEditRequestApprovalDto input)
         {
            var requestApproval = await _requestApprovalRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, requestApproval);
         }

		 [AbpAuthorize(AppPermissions.Pages_RequestApprovals_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _requestApprovalRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRequestApprovalsToExcel(GetAllRequestApprovalsForExcelInput input)
         {
			
			var filteredRequestApprovals = _requestApprovalRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false )
						.WhereIf(input.StatusFilter > -1,  e => Convert.ToInt32(e.Status) == input.StatusFilter );


			var query = (from o in filteredRequestApprovals
                         join o1 in _userRepository.GetAll() on o.UserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _requestRepository.GetAll() on o.RequestId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRequestApprovalForView() { 
							RequestApproval = ObjectMapper.Map<RequestApprovalDto>(o),
                         	UserName = s1 == null ? "" : s1.Name.ToString(),
                         	RequestTenantId = s2 == null ? "" : s2.TenantId.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestTenantIdFilter), e => e.RequestTenantId.ToLower() == input.RequestTenantIdFilter.ToLower().Trim());


            var requestApprovalListDtos = await query.ToListAsync();

            return _requestApprovalsExcelExporter.ExportToFile(requestApprovalListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_RequestApprovals)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RequestApprovals)]
         public async Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.TenantId.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestLookupTableDto>();
			foreach(var request in requestList){
				lookupTableDtoList.Add(new RequestLookupTableDto
				{
					Id = request.Id,
					DisplayName = request.TenantId.ToString()
				});
			}

            return new PagedResultDto<RequestLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}