using Test.Conflicts;
using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.ConflictAssets.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.ConflictAssets
{
	[AbpAuthorize(AppPermissions.Pages_ConflictAssets)]
    public class ConflictAssetsAppService : TestAppServiceBase, IConflictAssetsAppService
    {
		 private readonly IRepository<ConflictAsset> _conflictAssetRepository;
		 private readonly IRepository<Conflict,int> _conflictRepository;
		 private readonly IRepository<Asset,int> _assetRepository;
		 

		  public ConflictAssetsAppService(IRepository<ConflictAsset> conflictAssetRepository , IRepository<Conflict, int> conflictRepository, IRepository<Asset, int> assetRepository) 
		  {
			_conflictAssetRepository = conflictAssetRepository;
			_conflictRepository = conflictRepository;
		_assetRepository = assetRepository;
		
		  }

		 public async Task<PagedResultDto<GetConflictAssetForView>> GetAll(GetAllConflictAssetsInput input)
         {
			
			var filteredConflictAssets = _conflictAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredConflictAssets
                         join o1 in _conflictRepository.GetAll() on o.ConflictId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _assetRepository.GetAll() on o.AssetId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetConflictAssetForView() {
							ConflictAsset = ObjectMapper.Map<ConflictAssetDto>(o),
                         	ConflictConflictName = s1 == null ? "" : s1.ConflictName.ToString(),
                         	AssetAssetName = s2 == null ? "" : s2.AssetName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.ConflictConflictNameFilter), e => e.ConflictConflictName.ToLower() == input.ConflictConflictNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var conflictAssets = await query
                .OrderBy(input.Sorting ?? "conflictAsset.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetConflictAssetForView>(
                totalCount,
                conflictAssets
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_ConflictAssets_Edit)]
		 public async Task<GetConflictAssetForEditOutput> GetConflictAssetForEdit(EntityDto input)
         {
            var conflictAsset = await _conflictAssetRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetConflictAssetForEditOutput {ConflictAsset = ObjectMapper.Map<CreateOrEditConflictAssetDto>(conflictAsset)};

		    if (output.ConflictAsset.ConflictId != null)
            {
                var conflict = await _conflictRepository.FirstOrDefaultAsync((int)output.ConflictAsset.ConflictId);
                output.ConflictConflictName = conflict.ConflictName.ToString();
            }

		    if (output.ConflictAsset.AssetId != null)
            {
                var asset = await _assetRepository.FirstOrDefaultAsync((int)output.ConflictAsset.AssetId);
                output.AssetAssetName = asset.AssetName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditConflictAssetDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_ConflictAssets_Create)]
		 private async Task Create(CreateOrEditConflictAssetDto input)
         {
            var conflictAsset = ObjectMapper.Map<ConflictAsset>(input);

			
			if (AbpSession.TenantId != null)
			{
				conflictAsset.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _conflictAssetRepository.InsertAsync(conflictAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_ConflictAssets_Edit)]
		 private async Task Update(CreateOrEditConflictAssetDto input)
         {
            var conflictAsset = await _conflictAssetRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, conflictAsset);
         }

		 [AbpAuthorize(AppPermissions.Pages_ConflictAssets_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _conflictAssetRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_ConflictAssets)]
         public async Task<PagedResultDto<ConflictLookupTableDto>> GetAllConflictForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _conflictRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.ConflictName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var conflictList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<ConflictLookupTableDto>();
			foreach(var conflict in conflictList){
				lookupTableDtoList.Add(new ConflictLookupTableDto
				{
					Id = conflict.Id,
					DisplayName = conflict.ConflictName.ToString()
				});
			}

            return new PagedResultDto<ConflictLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_ConflictAssets)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString()
				});
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}