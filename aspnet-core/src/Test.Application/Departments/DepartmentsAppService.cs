
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Departments.Exporting;
using Test.Departments.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Test.Departments
{
	[AbpAuthorize(AppPermissions.Pages_Departments)]
    public class DepartmentsAppService : TestAppServiceBase, IDepartmentsAppService
    {
		 private readonly IRepository<Department> _departmentRepository;
		 private readonly IDepartmentsExcelExporter _departmentsExcelExporter;
		 

		  public DepartmentsAppService(IRepository<Department> departmentRepository, IDepartmentsExcelExporter departmentsExcelExporter ) 
		  {
			_departmentRepository = departmentRepository;
			_departmentsExcelExporter = departmentsExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetDepartmentForView>> GetAll(GetAllDepartmentsInput input)
         {
			
			var filteredDepartments = _departmentRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.DeptCode.Contains(input.Filter) || e.DeptName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.DeptCodeFilter),  e => e.DeptCode.ToLower() == input.DeptCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DeptNameFilter),  e => e.DeptName.ToLower() == input.DeptNameFilter.ToLower().Trim());


			var query = (from o in filteredDepartments
                         select new GetDepartmentForView() {
							Department = ObjectMapper.Map<DepartmentDto>(o)
						});

            var totalCount = await query.CountAsync();

            var departments = await query
                .OrderBy(input.Sorting ?? "department.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetDepartmentForView>(
                totalCount,
                departments
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Departments_Edit)]
		 public async Task<GetDepartmentForEditOutput> GetDepartmentForEdit(EntityDto input)
         {
            var department = await _departmentRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetDepartmentForEditOutput {Department = ObjectMapper.Map<CreateOrEditDepartmentDto>(department)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditDepartmentDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Departments_Create)]
		 private async Task Create(CreateOrEditDepartmentDto input)
         {
            var department = ObjectMapper.Map<Department>(input);

			
			if (AbpSession.TenantId != null)
			{
				department.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _departmentRepository.InsertAsync(department);
         }

		 [AbpAuthorize(AppPermissions.Pages_Departments_Edit)]
		 private async Task Update(CreateOrEditDepartmentDto input)
         {
            var department = await _departmentRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, department);
         }

		 [AbpAuthorize(AppPermissions.Pages_Departments_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _departmentRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetDepartmentsToExcel(GetAllDepartmentsForExcelInput input)
         {
			
			var filteredDepartments = _departmentRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.DeptCode.Contains(input.Filter) || e.DeptName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.DeptCodeFilter),  e => e.DeptCode.ToLower() == input.DeptCodeFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DeptNameFilter),  e => e.DeptName.ToLower() == input.DeptNameFilter.ToLower().Trim());


			var query = (from o in filteredDepartments
                         select new GetDepartmentForView() { 
							Department = ObjectMapper.Map<DepartmentDto>(o)
						 });


            var departmentListDtos = await query.ToListAsync();

            return _departmentsExcelExporter.ExportToFile(departmentListDtos);
         }


    }
}