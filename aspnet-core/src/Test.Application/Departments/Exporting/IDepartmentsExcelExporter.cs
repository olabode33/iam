using System.Collections.Generic;
using Test.Departments.Dtos;
using Test.Dto;

namespace Test.Departments.Exporting
{
    public interface IDepartmentsExcelExporter
    {
        FileDto ExportToFile(List<GetDepartmentForView> departments);
    }
}