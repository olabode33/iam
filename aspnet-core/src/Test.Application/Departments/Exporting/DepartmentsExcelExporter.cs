using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Departments.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Departments.Exporting
{
    public class DepartmentsExcelExporter : EpPlusExcelExporterBase, IDepartmentsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DepartmentsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDepartmentForView> departments)
        {
            return CreateExcelPackage(
                "Departments.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Departments"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("DeptCode"),
                        L("DeptName")
                        );

                    AddObjects(
                        sheet, 2, departments,
                        _ => _.Department.DeptCode,
                        _ => _.Department.DeptName
                        );

					

                });
        }
    }
}
