using Test.AssetTypes;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.AssetTypeOtherProperties.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.AssetTypeOtherProperties
{
	[AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties)]
    public class AssetTypeOtherPropertiesAppService : TestAppServiceBase, IAssetTypeOtherPropertiesAppService
    {
		 private readonly IRepository<AssetTypeOtherProperty> _assetTypeOtherPropertyRepository;
		 private readonly IRepository<AssetType,int> _assetTypeRepository;
		 

		  public AssetTypeOtherPropertiesAppService(IRepository<AssetTypeOtherProperty> assetTypeOtherPropertyRepository , IRepository<AssetType, int> assetTypeRepository) 
		  {
			_assetTypeOtherPropertyRepository = assetTypeOtherPropertyRepository;
			_assetTypeRepository = assetTypeRepository;
		
		  }

		 public async Task<PagedResultDto<GetAssetTypeOtherPropertyForView>> GetAll(GetAllAssetTypeOtherPropertiesInput input)
         {
			
			var filteredAssetTypeOtherProperties = _assetTypeOtherPropertyRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.PropertyName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PropertyNameFilter),  e => e.PropertyName.ToLower() == input.PropertyNameFilter.ToLower().Trim())
                        .WhereIf(!(input.AssetTypeId == 0), e => e.AssetTypeId == input.AssetTypeId);


			var query = (from o in filteredAssetTypeOtherProperties
                         join o1 in _assetTypeRepository.GetAll() on o.AssetTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetAssetTypeOtherPropertyForView() {
							AssetTypeOtherProperty = ObjectMapper.Map<AssetTypeOtherPropertyDto>(o),
                         	AssetTypeAssetTypeName = s1 == null ? "" : s1.AssetTypeName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetTypeAssetTypeNameFilter), e => e.AssetTypeAssetTypeName.ToLower() == input.AssetTypeAssetTypeNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var assetTypeOtherProperties = await query
                .OrderBy(input.Sorting ?? "assetTypeOtherProperty.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetAssetTypeOtherPropertyForView>(
                totalCount,
                assetTypeOtherProperties
            );
         }

        
        [AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties_Edit)]
		 public async Task<GetAssetTypeOtherPropertyForEditOutput> GetAssetTypeOtherPropertyForEdit(EntityDto input)
         {
            var assetTypeOtherProperty = await _assetTypeOtherPropertyRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetAssetTypeOtherPropertyForEditOutput {AssetTypeOtherProperty = ObjectMapper.Map<CreateOrEditAssetTypeOtherPropertyDto>(assetTypeOtherProperty)};

		    if (output.AssetTypeOtherProperty.AssetTypeId != null)
            {
                var assetType = await _assetTypeRepository.FirstOrDefaultAsync((int)output.AssetTypeOtherProperty.AssetTypeId);
                output.AssetTypeAssetTypeName = assetType.AssetTypeName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditAssetTypeOtherPropertyDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties_Create)]
		 private async Task Create(CreateOrEditAssetTypeOtherPropertyDto input)
         {
            var assetTypeOtherProperty = ObjectMapper.Map<AssetTypeOtherProperty>(input);

			
			if (AbpSession.TenantId != null)
			{
				assetTypeOtherProperty.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _assetTypeOtherPropertyRepository.InsertAsync(assetTypeOtherProperty);
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties_Edit)]
		 private async Task Update(CreateOrEditAssetTypeOtherPropertyDto input)
         {
            var assetTypeOtherProperty = await _assetTypeOtherPropertyRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, assetTypeOtherProperty);
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _assetTypeOtherPropertyRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_AssetTypeOtherProperties)]
         public async Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetTypeName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetTypeLookupTableDto>();
			foreach(var assetType in assetTypeList){
				lookupTableDtoList.Add(new AssetTypeLookupTableDto
				{
					Id = assetType.Id,
					DisplayName = assetType.AssetTypeName.ToString()
				});
			}

            return new PagedResultDto<AssetTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}