using Test.RequestTypes;
using Test.Authorization.Users;
using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Requests.Exporting;
using Test.Requests.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.RequestAssets;
using Test.UserSupervisors;
using Test.ApproveRequests;
using Test.RequestTypesAssetTypes;

namespace Test.Requests
{
    [AbpAuthorize(AppPermissions.Pages_Requests)]
    public class RequestsAppService : TestAppServiceBase, IRequestsAppService
    {
		 private readonly IRepository<Request> _requestRepository;
		 private readonly IRequestsExcelExporter _requestsExcelExporter;
		 private readonly IRepository<RequestType,int> _requestTypeRepository;
		 private readonly IRepository<User,long> _userRepository;
		 private readonly IRepository<Asset,int> _assetRepository;
         private readonly IRepository<RequestAsset> _requestAssetsRepository;
         private readonly IRepository<UserSupervisor> _userSupervisorRepository;
         private readonly IRepository<ApproveRequest> _approveRequestRepository;
         private readonly IRepository<RequestTypesAssetType> _requestTypesAssetTypeRepository;

        public RequestsAppService(
            IRepository<Request> requestRepository, 
            IRequestsExcelExporter requestsExcelExporter, 
            IRepository<RequestType, int> requestTypeRepository, 
            IRepository<User, long> userRepository, 
            IRepository<Asset, int> assetRepository, 
            IRepository<RequestAsset> requestAssetsRepository,
            IRepository<UserSupervisor> userSupervisorRepository, 
            IRepository<ApproveRequest> approveRequestRepository, 
            IRepository<RequestTypesAssetType> requestTypesAssetTypeRepository
            ) 
		  {
			_requestRepository = requestRepository;
			_requestsExcelExporter = requestsExcelExporter;
			_requestTypeRepository = requestTypeRepository;
		    _userRepository = userRepository;
		    _assetRepository = assetRepository;
            _requestAssetsRepository = requestAssetsRepository;
            _userSupervisorRepository = userSupervisorRepository;
            _approveRequestRepository = approveRequestRepository;
            _requestTypesAssetTypeRepository = requestTypesAssetTypeRepository;
          }

		 public async Task<PagedResultDto<GetRequestForView>> GetAll(GetAllRequestsInput input)
         {
			
			var filteredRequests = _requestRepository.GetAll().Where(x=>x.RequestOwnerId == AbpSession.UserId || x.CreatorUserId == AbpSession.UserId)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RequestDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestDescriptionFilter),  e => e.RequestDescription.ToLower() == input.RequestDescriptionFilter.ToLower().Trim())
						.WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
						.WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter);


            var query = (from o in filteredRequests
                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()


                         select new GetRequestForView()
                         {
                             Request = ObjectMapper.Map<RequestDto>(o),
                             RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                             UserName = s2 == null ? "" : s2.FullName.ToString()
                             
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequestTypeRequestCodeFilter), e => e.RequestTypeRequestCode.ToLower() == input.RequestTypeRequestCodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
                        .Where(x=> x.Request.InitiationTime < DateTime.Now);

            
            var totalCount = await query.CountAsync();

            var requests = await query
                .OrderBy(input.Sorting ?? "request.id desc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRequestForView>(
                totalCount,
                requests
            );
         }
		 

        public async Task<PagedResultDto<GetRequestForView>> GetAllForDashboard()
        {
            var result = _requestRepository.GetAll();

            var query = (from o in result

                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetRequestForView()
                         {
                             Request = ObjectMapper.Map<RequestDto>(o),
                             RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                             UserName = s2 == null ? "" : s2.FullName.ToString()
                         });

            var totalCount = await query.CountAsync();

            var requests =  query.ToList();

            return new PagedResultDto<GetRequestForView>(
                totalCount,
                requests
            );
        }

         // This method is not functional as is, work will be done to understand and refactor it
         [AbpAuthorize(AppPermissions.Pages_Requests_Edit)]
		 public async Task<GetRequestForEditOutput> GetRequestForEdit(EntityDto input)
         {
            var request = await _requestRepository.FirstOrDefaultAsync(input.Id);           
		    var output = new GetRequestForEditOutput {Request = ObjectMapper.Map<CreateOrEditRequestDto>(request)};
		    if (output.Request.RequestTypeId != null)
            {
                var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)output.Request.RequestTypeId);
                output.RequestTypeRequestCode = requestType.RequestCode.ToString();
            }
		    if (output.Request.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Request.UserId);
                output.UserName = user.Name.ToString();
            }

            var assets =  await  _requestAssetsRepository.GetAllIncluding(x => x.Asset).Where(x => x.RequestId == input.Id).ToListAsync();
            List<Asset> assetList = new List<Asset>();
            foreach (var asset in assets)
            {
                assetList.Add(asset.Asset);
            }
            output.Assets = ObjectMapper.Map<List<AssetDto>>(assetList).ToArray();

            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRequestDto input)
        {   
            if (input.Id == null){
				await Create(input);
			}
			else
            {
                var RequestAssetDel = await _requestAssetsRepository.GetAllListAsync(x => x.RequestId == input.Id);
                if (input.Id > 0 && RequestAssetDel.Count > 0)
                {
                    foreach (var r in RequestAssetDel)
                    {
                        await _requestAssetsRepository.DeleteAsync(r);
                    }
                }
                await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Requests_Create)]
		 private async Task Create(CreateOrEditRequestDto input)
         {
            try
            {
                var request = ObjectMapper.Map<Request>(input);

                request.RequestOwnerId = input.UserId;
                request.InitiationTime = DateTime.Now.AddMinutes(-1); // @ improve this field is redundant, because model has creation datetime

                if (AbpSession.TenantId != null)
                {
                    request.TenantId = (int?)AbpSession.TenantId;
                }

                var requestId =  await _requestRepository.InsertAndGetIdAsync(request);

                var RequestTypeObj = _requestTypeRepository.FirstOrDefault((int)input.RequestTypeId);

                /* The following declaration deals with approval requirement of new request */
                // If requestType has approval requirement
                if (RequestTypeObj.ApprovalNo > 0)
                {
                    // Create entry for every asset that user requested, this is necessary 
                    // because multiple assets can be attached to a single request
                    // These are the actual properties that the user is requesting assess to
                    foreach (var assetId in input.AssetId)
                    {
                        await _requestAssetsRepository.InsertAsync(new RequestAsset()
                        {
                            AssetId = Convert.ToInt32(assetId),
                            TenantId = AbpSession.TenantId,
                            RequestId = Convert.ToInt32(requestId),
                            RequestAssetApproved = "P",
                            RequestAssetTreated = "P",
                            CreatorUserId = input.UserId
                        });
                    }

                    int CurrentUser = (int)input.UserId;

                    // Create entry for every asset that user requested, that requires approval
                    for (int x = 1; x <= RequestTypeObj.ApprovalNo; x++)
                    {
                        var supervisor = _userSupervisorRepository.FirstOrDefault(xt => xt.UserId == CurrentUser);
                        
                        ApproveRequest a = new ApproveRequest();
                        if (CurrentUser.Equals((int)input.UserId))
                        {
                            a.PriorApprovalStatus = true;
                            a.PriorApprovalDate = DateTime.Now;
                        }
                        else
                        {
                            a.PriorApprovalStatus = false;
                        }
                        a.TenantId = AbpSession.TenantId;
                        a.CreatorUserId = input.UserId;
                        a.PriorApprover = CurrentUser;
                        a.RequestId = requestId;
                        a.ApproverID = supervisor.SupervisorUserId;
                        a.ApprovalStatus = false;

                        await _approveRequestRepository.InsertAsync(a);
                        CurrentUser = (int)supervisor.SupervisorUserId;
                    }
                }
                else // requestType does not require approval i.e. approvalNo = 0
                {
                    foreach (var AssetID in input.AssetId)
                    {
                        await _requestAssetsRepository.InsertAsync(new RequestAsset()
                        {
                            AssetId = Convert.ToInt32(AssetID),
                            TenantId = AbpSession.TenantId,
                            RequestId = Convert.ToInt32(requestId),
                            CreatorUserId = input.UserId,
                            RequestAssetApproved = "A",
                            RequestAssetTreated = "P"
                        });
                    }

                    // Update request because approval is not required
                    var requestApproved = _requestRepository.FirstOrDefault(requestId);
                    requestApproved.RequestApprovetime = DateTime.Now;
                    requestApproved.RequestApprovalStatus = true;
                    requestApproved.RequestStatus = "A";
                    await _requestRepository.UpdateAsync(requestApproved);
                }

                // If requesttype has expiry date, it usually has a requestype that is triggered when the original one expires
                // this is called a reverserequesttype
                // the declaration below, creates the reversalrequesttype record in the request table
                // this functionality is not working wet
                if (!String.IsNullOrEmpty(RequestTypeObj.ReverseRequestType.ToString()) && !String.IsNullOrEmpty(input.ExpiryDate.ToString()))
                {
                    var data  = new CreateOrEditRequestDto(){
                        RequestDescription = input.RequestDescription,
                        ExpiryDate = Convert.ToDateTime("0001-01-01"),
                        RequestTypeId = RequestTypeObj.ReverseRequestType,
                        UserId = input.UserId,
                        InitiationTime = input.ExpiryDate,
                        Id = null
                    };
                    var ReversalRequest = ObjectMapper.Map<Request>(data);
                    ReversalRequest.CreatorUserId = input.UserId;
                    ReversalRequest.TenantId = AbpSession.TenantId;
                    ReversalRequest.RequestOwnerId = input.UserId;
                    ReversalRequest.RequestStatus = "A";
                    ReversalRequest.InitiatingRequestID = requestId;
                    List<AssetLookupTableDto> RequestTypeID_Assets = GetAssets((int)requestId);
                    if (true)
                    {
                        var ReversalID = await _requestRepository.InsertAndGetIdAsync(ReversalRequest);
                        CurrentUnitOfWork.SaveChanges();

                        foreach (var AssetID in input.AssetId)
                        {
                            var req = new RequestAsset
                            {
                                CreatorUserId = input.UserId,
                                AssetId = Convert.ToInt32(AssetID),
                                TenantId = AbpSession.TenantId,
                                RequestId = Convert.ToInt32(ReversalID),
                                RequestAssetApproved = "A",
                                LastApprovedBy = AbpSession.UserId.ToString(),
                                LastApprovedDate = Convert.ToDateTime(input.ExpiryDate),
                                RequestAssetTreated = "P"
                            };
                            await _requestAssetsRepository.InsertAsync(req);
                        }

                        var ReversalRequestD = _requestRepository.FirstOrDefault(ReversalID);
                        ReversalRequestD.RequestApprovetime = DateTime.Now;
                        ReversalRequestD.RequestApprovalStatus = true;
                        ReversalRequestD.RequestStatus = "A";
                        await _requestRepository.UpdateAsync(ReversalRequestD);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("An error occured please review the form you are sending");
            }
         }

		 [AbpAuthorize(AppPermissions.Pages_Requests_Edit)]
		 private async Task Update(CreateOrEditRequestDto input)
         {
            var request = await _requestRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, request);
            foreach (var AssetID in input.AssetId)
            {
                RequestAsset req = new RequestAsset();
                req.CreatorUserId = AbpSession.UserId;
                req.AssetId = Convert.ToInt32(AssetID);
                req.TenantId = AbpSession.TenantId;
                req.RequestId = Convert.ToInt32(input.Id);
                req.RequestAssetApproved = "P";
                req.RequestAssetTreated = "P";
                await _requestAssetsRepository.InsertAsync(req);
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_Requests_Delete)]
         public async Task Delete(EntityDto input)
         {
            //Delete Request Approval Requests
            var approveRequestDel = await _approveRequestRepository.GetAllListAsync(x => x.RequestId == input.Id);
            if (approveRequestDel.Count > 0)
            {
                foreach (var t in approveRequestDel)
                {
                    await _approveRequestRepository.DeleteAsync(t);
                }
            }
            //Delete Request Assets
            var RequestAssetDel = await _requestAssetsRepository.GetAllListAsync(x => x.RequestId == input.Id);
            if (RequestAssetDel.Count > 0)
            {
                foreach (var r in RequestAssetDel)
                {
                    await _requestAssetsRepository.DeleteAsync(r);
                }
            }            
            await _requestRepository.DeleteAsync(input.Id);
        } 

		public async Task<FileDto> GetRequestsToExcel(GetAllRequestsForExcelInput input)
         {
			
			var filteredRequests = _requestRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RequestDescription.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequestDescriptionFilter),  e => e.RequestDescription.ToLower() == input.RequestDescriptionFilter.ToLower().Trim())
						.WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
						.WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter);


            var query = (from o in filteredRequests
                         join o1 in _requestTypeRepository.GetAll() on o.RequestTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()


                         select new GetRequestForView()
                         {
                             Request = ObjectMapper.Map<RequestDto>(o),
                             RequestTypeRequestCode = s1 == null ? "" : s1.RequestCode.ToString(),
                             UserName = s2 == null ? "" : s2.Name.ToString(),
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequestTypeRequestCodeFilter), e => e.RequestTypeRequestCode.ToLower() == input.RequestTypeRequestCodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());


            var requestListDtos = await query.ToListAsync();

            return _requestsExcelExporter.ExportToFile(requestListDtos);
         }

		[AbpAuthorize(AppPermissions.Pages_Requests)]
         public async Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _requestTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.RequestCode.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var requestTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RequestTypeLookupTableDto>();
			foreach(var requestType in requestTypeList){
				lookupTableDtoList.Add(new RequestTypeLookupTableDto
				{
					Id = requestType.Id,
					DisplayName = requestType.RequestCode.ToString()
				});
			}

            return new PagedResultDto<RequestTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Requests)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_Requests)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString(),
                    AssetDescription = asset.AssetDescription.ToString()
                });
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

        public async Task<GetRequestForEditOutput> GetSingleRequest(EntityDto<int> input)
        {
            var request = await _requestRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetRequestForEditOutput { Request = ObjectMapper.Map<CreateOrEditRequestDto>(request) };
            if (output.Request.RequestTypeId != null)
            {
                var requestType = await _requestTypeRepository.FirstOrDefaultAsync((int)output.Request.RequestTypeId);
                output.RequestTypeRequestCode = requestType.RequestCode.ToString();
            }
            if (output.Request.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Request.UserId);
                output.UserName = user.Name.ToString();
            }
            var assets = await _requestAssetsRepository.GetAllIncluding(x => x.Asset).Where(x => x.RequestId == input.Id).ToListAsync();
            List<AssetDto> assetList = new List<AssetDto>();
            for (int x = 0; x < assets.Count; x++)
            {
                string Fullname = "";
                int UserID = 0;
                if (!String.IsNullOrEmpty(assets[x].RejectedBy))
                {
                    UserID = Convert.ToInt32(assets[x].RejectedBy);
                }
                List<Asset> assetDetails = _assetRepository.GetAllList().Where(xu => xu.Id == assets[x].AssetId).ToList();
                List<User> UserDetail = _userRepository.GetAllList().Where(r => r.Id == UserID).ToList();
                if (UserDetail.Count > 0)
                {
                    Fullname = UserDetail[0].FullName.ToString();
                }
                assetList.Add(new AssetDto
                {
                    Id = assets[x].Id,
                    AssetName = assetDetails[0].AssetName.ToString(),
                    AssetCode = assetDetails[0].AssetCode.ToString(),
                    AssetDescription = assetDetails[0].AssetDescription.ToString(),
                    RejectedBy = Fullname,
                    RequestAssetStatus = Convert.ToString(assets[x].RequestAssetApproved)
                });
            }
            output.Assets = assetList.ToArray();
            
            return output;
        }

        public async Task<PagedResultDto<AssetLookupTableDto>> GetSingleAssetForLookupTable(int requestTypeId, int? branchId)
        {
            List<AssetLookupTableDto> AssetLookUpTable = new List<AssetLookupTableDto>();
            List<RequestTypesAssetType> RequestAssets = new List<RequestTypesAssetType>();
            List<Asset> Assets = new List<Asset>();

            
            RequestAssets = await _requestTypesAssetTypeRepository.GetAllListAsync(x => x.RequestTypeId == requestTypeId);
           
            if (branchId == null)
            {
                if (RequestAssets.Count > 0)
                {
                    for (int x = 0; x < RequestAssets.Count; x++)
                    {
                        int AssetTypeID = (int)RequestAssets[x].AssetTypeId;
                        List<Asset> assetsList = await _assetRepository.GetAllListAsync(xt => xt.AssetTypeId == AssetTypeID);
                        if (assetsList.Count > 0)
                        {
                            for (int w = 0; w < assetsList.Count; w++)
                            {
                                string AssetDesc = "";
                                if (!String.IsNullOrEmpty(assetsList[w].AssetDescription.ToString()))
                                {
                                    AssetDesc = assetsList[w].AssetDescription.ToString();
                                }
                                AssetLookUpTable.Add(new AssetLookupTableDto
                                {
                                    Id = assetsList[w].Id,
                                    DisplayName = assetsList[w].AssetName.ToString(),
                                    AssetDescription = AssetDesc
                                });
                            }
                        }
                    }
                }
            }
            else
            {
                if (RequestAssets.Count > 0)
                {
                    for (int x = 0; x < RequestAssets.Count; x++)
                    {
                        int AssetTypeID = (int)RequestAssets[x].AssetTypeId;
                        List<Asset> assetsList =  _assetRepository.GetAll()
                                                    .Where(xt => xt.AssetTypeId == AssetTypeID)
                                                    .Where(t => t.BranchId == branchId)
                                                    .ToList();
                        if (assetsList.Count > 0)
                        {
                            for (int w = 0; w < assetsList.Count; w++)
                            {
                                string AssetDesc = "";
                                if (!String.IsNullOrEmpty(assetsList[w].AssetDescription.ToString()))
                                {
                                    AssetDesc = assetsList[w].AssetDescription.ToString();
                                }
                                AssetLookUpTable.Add(new AssetLookupTableDto
                                {
                                    Id = assetsList[w].Id,
                                    DisplayName = assetsList[w].AssetName.ToString(),
                                    AssetDescription = AssetDesc
                                });
                            }
                        }
                    }
                }
            }

            return new PagedResultDto<AssetLookupTableDto>(
                AssetLookUpTable.Count,
                AssetLookUpTable
            );
        }

        private List<AssetLookupTableDto> GetAssets(int RequestId)
        {
            List<AssetLookupTableDto> AssetLookUpTable = new List<AssetLookupTableDto>();

            List<RequestAsset> RequestAssets = _requestAssetsRepository.GetAllList(x=>x.RequestId == RequestId);

            if (RequestAssets.Count > 0)
            {
                for (int x = 0; x < RequestAssets.Count; x++)
                {
                    int AssetID = (int)RequestAssets[x].AssetId;
                    List<Asset> assetTypeDetails = _assetRepository.GetAll().Where(xe => xe.Id == AssetID).ToList();
                    AssetLookUpTable.Add(new AssetLookupTableDto
                    {
                        Id = assetTypeDetails[0].Id,
                        DisplayName = assetTypeDetails[0].AssetName.ToString(),
                        AssetDescription = assetTypeDetails[0].AssetDescription.ToString()
                    });
                }
            }
            return AssetLookUpTable;
        }
    }
}