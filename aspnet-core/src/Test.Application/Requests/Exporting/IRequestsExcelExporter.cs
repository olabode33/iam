using System.Collections.Generic;
using Test.Requests.Dtos;
using Test.Dto;

namespace Test.Requests.Exporting
{
    public interface IRequestsExcelExporter
    {
        FileDto ExportToFile(List<GetRequestForView> requests);
    }
}