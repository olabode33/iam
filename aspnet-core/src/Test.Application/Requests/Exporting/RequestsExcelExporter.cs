using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Requests.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Requests.Exporting
{
    public class RequestsExcelExporter : EpPlusExcelExporterBase, IRequestsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RequestsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRequestForView> requests)
        {
            return CreateExcelPackage(
                "Requests.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Requests"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("RequestDescription"),
                        L("ExpiryDate"),
                        (L("RequestType")) + L("RequestCode"),
                        (L("User")) + L("Name"),
                        (L("Asset")) + L("AssetName")
                        );

                    AddObjects(
                        sheet, 2, requests,
                        _ => _.Request.RequestDescription,
                        _ => _timeZoneConverter.Convert(_.Request.ExpiryDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.RequestTypeRequestCode,
                        _ => _.UserName,
                        _ => _.AssetAssetName
                        );

					var expiryDateColumn = sheet.Column(2);
                    expiryDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					expiryDateColumn.AutoFit();
					

                });
        }
    }
}
