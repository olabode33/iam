using Test.Assets;
using Test.JobFunctions;
using Test.Branches;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.JobFunctionAssets.Exporting;
using Test.JobFunctionAssets.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;
using Test.JobFunctionsAssetAssets;
using Test.JobFunctionAssetBranches;

namespace Test.JobFunctionAssets
{
	[AbpAuthorize(AppPermissions.Pages_JobFunctionAssets)]
    public class JobFunctionAssetsAppService : TestAppServiceBase, IJobFunctionAssetsAppService
    {
		 private readonly IRepository<JobFunctionAsset> _jobFunctionAssetRepository;
		 private readonly IJobFunctionAssetsExcelExporter _jobFunctionAssetsExcelExporter;
		 private readonly IRepository<Asset,int> _assetRepository;
		 private readonly IRepository<JobFunction,int> _jobFunctionRepository;
		 private readonly IRepository<Branch,int> _branchRepository;
        private readonly IRepository<JobFunctionsAssetAsset> _jobFunctionsAssetAssetRepository;
        private readonly IRepository<JobFunctionAssetBranche> _jobFunctionAssetBrancheRepository;


        public JobFunctionAssetsAppService(IRepository<JobFunctionAsset> jobFunctionAssetRepository, IJobFunctionAssetsExcelExporter jobFunctionAssetsExcelExporter , IRepository<Asset, int> assetRepository, IRepository<JobFunction, int> jobFunctionRepository,
            IRepository<Branch, int> branchRepository, IRepository<JobFunctionsAssetAsset> jobFunctionsAssetAssetRepository, IRepository<JobFunctionAssetBranche> jobFunctionAssetBrancheRepository) 
		  {
			_jobFunctionAssetRepository = jobFunctionAssetRepository;
			_jobFunctionAssetsExcelExporter = jobFunctionAssetsExcelExporter;
			_assetRepository = assetRepository;
		    _branchRepository = branchRepository;
            _jobFunctionRepository = jobFunctionRepository;
            _jobFunctionAssetBrancheRepository = jobFunctionAssetBrancheRepository;
            _jobFunctionsAssetAssetRepository = jobFunctionsAssetAssetRepository;
          }

		 public async Task<PagedResultDto<GetJobFunctionAssetForView>> GetAll(GetAllJobFunctionAssetsInput input)
         {
			
			var filteredJobFunctionAssets = _jobFunctionAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


            var query = (from o in filteredJobFunctionAssets

                    join o2 in _jobFunctionRepository.GetAll() on o.JobFunctionId equals o2.Id into j2
                    from s2 in j2.DefaultIfEmpty()
                    
                    select new GetJobFunctionAssetForView()
                    {
                        JobFunctionAsset = ObjectMapper.Map<JobFunctionAssetDto>(o),
                        JobFunctionJobName = s2 == null ? "" : s2.JobName.ToString(),
                        SelectedBranchType = o.SelectedbranchType
                    })
                .WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionJobNameFilter), e => e.JobFunctionJobName.ToLower() == input.JobFunctionJobNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var jobFunctionAssets = await query
                .OrderBy(input.Sorting ?? "jobFunctionAsset.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetJobFunctionAssetForView>(
                totalCount,
                jobFunctionAssets
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_JobFunctionAssets_Edit)]
		 public async Task<GetJobFunctionAssetForEditOutput> GetJobFunctionAssetForEdit(EntityDto input)
         {
            var jobFunctionAsset = await _jobFunctionAssetRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetJobFunctionAssetForEditOutput {JobFunctionAsset = ObjectMapper.Map<CreateOrEditJobFunctionAssetDto>(jobFunctionAsset)};	
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditJobFunctionAssetDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
                var deleteAssets = await _jobFunctionsAssetAssetRepository.GetAllListAsync(x => x.JobFunctionAssetId == input.Id);
                var deleteBranches = await _jobFunctionAssetBrancheRepository.GetAllListAsync(x => x.JobFunctionAssetId == input.Id);
                if (input.Id > 0)
                {
                    if (deleteAssets.Count > 0) { foreach (var r in deleteAssets) {  await _jobFunctionsAssetAssetRepository.DeleteAsync(r); } }
                    if (deleteBranches.Count > 0) { foreach (var r in deleteBranches) { await _jobFunctionAssetBrancheRepository.DeleteAsync(r); } }
                }
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionAssets_Create)]
		 private async Task Create(CreateOrEditJobFunctionAssetDto input)
         {
            var jobFunctionAsset = ObjectMapper.Map<JobFunctionAsset>(input);
			if (AbpSession.TenantId != null)
			{
				jobFunctionAsset.TenantId = (int?) AbpSession.TenantId;
			}
            jobFunctionAsset.SelectedbranchType = input.JBAssetBranches;
            var JBAssetID = await _jobFunctionAssetRepository.InsertAndGetIdAsync(jobFunctionAsset);
            CurrentUnitOfWork.SaveChanges();

            if ((int)JBAssetID > 0)
            {
                foreach(var AssetID in input.AssetId)
                {
                    var AssetReq = new JobFunctionsAssetAsset
                    {
                        TenantId = AbpSession.TenantId,
                        CreatorUserId = AbpSession.UserId,
                        JobFunctionAssetId = (int)JBAssetID,
                        AssetId = (int)AssetID.Id
                    };
                    await _jobFunctionsAssetAssetRepository.InsertAsync(AssetReq);
                }
                if((int)JBAssetID > 0)
                {
                    if (input.JBAssetBranches.Equals("A"))
                    {
                        List<Branch> BranchData = new List<Branch>();
                        BranchData = await _branchRepository.GetAllListAsync();
                        foreach (var branchID in BranchData)
                        {
                            var BranchReq = new JobFunctionAssetBranche
                            {
                                TenantId = AbpSession.TenantId,
                                CreatorUserId = AbpSession.UserId,
                                JobFunctionAssetId = (int)JBAssetID,
                                BranchId = branchID.Id,
                                JobFunctionAssetNumber = input.JobFunctionId
                            };
                            await _jobFunctionAssetBrancheRepository.InsertAsync(BranchReq);
                        }
                    }
                    else if (input.JBAssetBranches.Equals("S"))
                    {
                        foreach (var branchID in input.BranchId)
                        {
                            var BranchReq = new JobFunctionAssetBranche
                            {
                                TenantId = AbpSession.TenantId,
                                CreatorUserId = AbpSession.UserId,
                                JobFunctionAssetId = (int)JBAssetID,
                                BranchId = branchID.Id,
                                JobFunctionAssetNumber = input.JobFunctionId
                            };
                            await _jobFunctionAssetBrancheRepository.InsertAsync(BranchReq);
                        }
                    }
                }
            }
         }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionAssets_Edit)]
		 private async Task Update(CreateOrEditJobFunctionAssetDto input)
         {
            var jobFunctionAsset = await _jobFunctionAssetRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, jobFunctionAsset);

            
            foreach (var AssetID in input.AssetId)
            {
                if ((int)AssetID.Id > 0)
                {
                    var AssetReq = new JobFunctionsAssetAsset
                    {
                        TenantId = AbpSession.TenantId,
                        CreatorUserId = AbpSession.UserId,
                        JobFunctionAssetId = (int)input.Id,
                        AssetId = (int)AssetID.Id
                    };
                    await _jobFunctionsAssetAssetRepository.InsertAsync(AssetReq);
                }
            }

            if ((int)input.Id > 0)
            {
                if (input.JBAssetBranches.Equals("A"))
                {
                    List<Branch> BranchData = new List<Branch>();
                    BranchData = await _branchRepository.GetAllListAsync();
                    foreach (var branchID in BranchData)
                    {
                        var BranchReq = new JobFunctionAssetBranche
                        {
                            TenantId = AbpSession.TenantId,
                            CreatorUserId = AbpSession.UserId,
                            JobFunctionAssetId = (int)input.Id,
                            BranchId = branchID.Id,
                            JobFunctionAssetNumber = input.JobFunctionId
                        };
                        await _jobFunctionAssetBrancheRepository.InsertAsync(BranchReq);
                    }
                }
                else if (input.JBAssetBranches.Equals("S"))
                {
                    foreach (var branchID in input.BranchId)
                    {
                        var BranchReq = new JobFunctionAssetBranche
                        {
                            TenantId = AbpSession.TenantId,
                            CreatorUserId = AbpSession.UserId,
                            JobFunctionAssetId = (int)input.Id,
                            BranchId = branchID.Id,
                            JobFunctionAssetNumber = input.JobFunctionId
                        };
                        await _jobFunctionAssetBrancheRepository.InsertAsync(BranchReq);
                    }
                }
            }
        }

		 [AbpAuthorize(AppPermissions.Pages_JobFunctionAssets_Delete)]
         public async Task Delete(EntityDto input)
         {
            var deleteAssets = await _jobFunctionsAssetAssetRepository.GetAllListAsync(x => x.JobFunctionAssetId == input.Id);
            var deleteBranches = await _jobFunctionAssetBrancheRepository.GetAllListAsync(x => x.JobFunctionAssetId == input.Id);
            if (input.Id > 0)
            {
                if (deleteAssets.Count > 0) { foreach (var r in deleteAssets) { await _jobFunctionsAssetAssetRepository.DeleteAsync(r); } }
                if (deleteBranches.Count > 0) { foreach (var r in deleteBranches) { await _jobFunctionAssetBrancheRepository.DeleteAsync(r); } }
            }
            await _jobFunctionAssetRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetJobFunctionAssetsToExcel(GetAllJobFunctionAssetsForExcelInput input)
         {
			
			var filteredJobFunctionAssets = _jobFunctionAssetRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredJobFunctionAssets
                         
                         join o2 in _jobFunctionRepository.GetAll() on o.JobFunctionId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetJobFunctionAssetForView() { 
							JobFunctionAsset = ObjectMapper.Map<JobFunctionAssetDto>(o),
                         	JobFunctionJobName = s2 == null ? "" : s2.JobName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobFunctionJobNameFilter), e => e.JobFunctionJobName.ToLower() == input.JobFunctionJobNameFilter.ToLower().Trim());


            var jobFunctionAssetListDtos = await query.ToListAsync();

            return _jobFunctionAssetsExcelExporter.ExportToFile(jobFunctionAssetListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_JobFunctionAssets)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString(),
                    AssetDescription = asset.AssetDescription.ToString()
                });
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_JobFunctionAssets)]
         public async Task<PagedResultDto<JobFunctionLookupTableDto>> GetAllJobFunctionForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _jobFunctionRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.JobName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var jobFunctionList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<JobFunctionLookupTableDto>();
			foreach(var jobFunction in jobFunctionList){
				lookupTableDtoList.Add(new JobFunctionLookupTableDto
				{
					Id = jobFunction.Id,
					DisplayName = jobFunction.JobName.ToString()
				});
			}

            return new PagedResultDto<JobFunctionLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_JobFunctionAssets)]
         public async Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _branchRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.BranchName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var branchList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<BranchLookupTableDto>();
			foreach(var branch in branchList){
				lookupTableDtoList.Add(new BranchLookupTableDto
				{
					Id = branch.Id,
					DisplayName = branch.BranchName.ToString(),
                    BranchCode = branch.BranchCode.ToString()
                });
			}

            return new PagedResultDto<BranchLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }


        public async Task<PagedResultDto<GetJobFunctionAssetForViewEvery>> GetSelected(int ID)
        {
            List<JobFunctionAsset> AllJobfuctionAssets = null;
            List<GetJobFunctionAssetForViewEvery> AllJB = new List<GetJobFunctionAssetForViewEvery>();
            if (ID <= 0)
            {
                AllJobfuctionAssets = await _jobFunctionAssetRepository.GetAllListAsync();
            }
            else {
                AllJobfuctionAssets = await _jobFunctionAssetRepository.GetAllListAsync(x=>x.Id == ID);
            }
            if (AllJobfuctionAssets.Count > 0)
            {
                for (int x = 0; x < AllJobfuctionAssets.Count; x++)
                {
                    var JobFunctionName = await _jobFunctionRepository.FirstOrDefaultAsync(Convert.ToInt32(AllJobfuctionAssets[x].JobFunctionId));
                    List<JobFunctionAssetBranchesList> Br_ = Getbranches(Convert.ToInt32(AllJobfuctionAssets[x].Id));
                    List<JobFunctionAssetAssetsList> As_ = GetAssets(Convert.ToInt32(AllJobfuctionAssets[x].Id));
                    AllJB.Add(new GetJobFunctionAssetForViewEvery
                    {
                        ID = Convert.ToInt32(AllJobfuctionAssets[x].Id),
                        JobFunction = JobFunctionName.JobName,
                        JobFunctionID = Convert.ToInt32(AllJobfuctionAssets[x].JobFunctionId),
                        SelectedBranchType = Convert.ToString(AllJobfuctionAssets[x].SelectedbranchType),
                        JBAssetBranches = Br_,
                        JBAsset = As_,
                        JBAssetsNum = As_.Count,
                        JBBranchesNum = Br_.Count
                    });
                }
            }
            return new PagedResultDto<GetJobFunctionAssetForViewEvery>(AllJB.Count, AllJB);
        }

        private List<JobFunctionAssetBranchesList> Getbranches(int ID)
        {
            List<JobFunctionAssetBranchesList> data = new List<JobFunctionAssetBranchesList>();
            var JobAssets =  _jobFunctionAssetBrancheRepository.GetAllList(x=>x.JobFunctionAssetId == ID);
            for (int x = 0; x < JobAssets.Count; x++)
            {
                var branchname = _branchRepository.FirstOrDefault(Convert.ToInt32(JobAssets[x].BranchId));
                data.Add(new JobFunctionAssetBranchesList
                {
                    BranchID = Convert.ToInt32(branchname.Id),
                    BranchName = branchname.BranchName,
                    BranchCode = branchname.BranchCode
                });
            }
            return data;
        }

        private List<JobFunctionAssetAssetsList> GetAssets(int ID)
        {
            List<JobFunctionAssetAssetsList> data = new List<JobFunctionAssetAssetsList>();
            var JobAssets = _jobFunctionsAssetAssetRepository.GetAllList(x => x.JobFunctionAssetId == ID);
            for (int x = 0; x < JobAssets.Count; x++)
            {
                var AssetDetails = _assetRepository.FirstOrDefault(Convert.ToInt32(JobAssets[x].AssetId));
                data.Add(new JobFunctionAssetAssetsList
                {
                    AssetDescription = AssetDetails.AssetName,
                    AssetID = Convert.ToInt32(JobAssets[x].AssetId),
                    AssetName = AssetDetails.AssetName
                });
            }
            return data;
        }


        public async Task<GetJobFunctionAssetForViewEveryView> GetSingleSelected(int ID)
        {
            GetJobFunctionAssetForViewEveryView output = new GetJobFunctionAssetForViewEveryView();
            var AllJobfuctionAssets = await _jobFunctionAssetRepository.GetAllListAsync(x => x.Id == ID);

            output.JobFunctionID = Convert.ToInt32(AllJobfuctionAssets[0].JobFunctionId);
            var JobFunctionName = await _jobFunctionRepository.FirstOrDefaultAsync(Convert.ToInt32(AllJobfuctionAssets[0].JobFunctionId));
            output.JobFunction = JobFunctionName.JobName;
            output.SelectedBranchType = AllJobfuctionAssets[0].SelectedbranchType.ToString();

            List<JobFunctionAssetBranchesList> Br_ = Getbranches(ID);
            List<JobFunctionAssetAssetsList> As_ = GetAssets(ID);

            output.ID = ID;
            output.JBAssetsNum = As_.Count;
            output.JBBranchesNum = Br_.Count;


            output.JBAsset = As_.ToArray();
            output.JBAssetBranches = Br_.ToArray();

            return output;
        }

        public async Task<int> CheckIfJobCreated(int ID)
        {
            int returnID = 0;
            var jobDetails = await _jobFunctionAssetRepository.FirstOrDefaultAsync(x => x.JobFunctionId == ID);
            if (jobDetails != null)
            {
                returnID = jobDetails.Id;
            }
            return returnID;
        }

    }
}