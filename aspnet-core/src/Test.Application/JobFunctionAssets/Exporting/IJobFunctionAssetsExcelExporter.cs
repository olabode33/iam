using System.Collections.Generic;
using Test.JobFunctionAssets.Dtos;
using Test.Dto;

namespace Test.JobFunctionAssets.Exporting
{
    public interface IJobFunctionAssetsExcelExporter
    {
        FileDto ExportToFile(List<GetJobFunctionAssetForView> jobFunctionAssets);
    }
}