using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.JobFunctionAssets.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.JobFunctionAssets.Exporting
{
    public class JobFunctionAssetsExcelExporter : EpPlusExcelExporterBase, IJobFunctionAssetsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobFunctionAssetsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobFunctionAssetForView> jobFunctionAssets)
        {
            return CreateExcelPackage(
                "JobFunctionAssets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JobFunctionAssets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("Asset")) + L("AssetName"),
                        (L("JobFunction")) + L("JobName"),
                        (L("Branch")) + L("BranchName")
                        );

                    AddObjects(
                        sheet, 2, jobFunctionAssets,
                        _ => _.AssetAssetName,
                        _ => _.JobFunctionJobName,
                        _ => _.BranchBranchName
                        );

					

                });
        }
    }
}
