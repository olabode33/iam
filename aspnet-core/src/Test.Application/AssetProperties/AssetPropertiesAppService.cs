using Test.Assets;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.AssetProperties.Exporting;
using Test.AssetProperties.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.IDMShared.Dtos;

namespace Test.AssetProperties
{
	[AbpAuthorize(AppPermissions.Pages_AssetProperties)]
    public class AssetPropertiesAppService : TestAppServiceBase, IAssetPropertiesAppService
    {
		 private readonly IRepository<AssetProperty> _assetPropertyRepository;
		 private readonly IAssetPropertiesExcelExporter _assetPropertiesExcelExporter;
		 private readonly IRepository<Asset,int> _assetRepository;
		 

		  public AssetPropertiesAppService(IRepository<AssetProperty> assetPropertyRepository, IAssetPropertiesExcelExporter assetPropertiesExcelExporter , IRepository<Asset, int> assetRepository) 
		  {
			_assetPropertyRepository = assetPropertyRepository;
			_assetPropertiesExcelExporter = assetPropertiesExcelExporter;
			_assetRepository = assetRepository;
		
		  }

		 public async Task<PagedResultDto<GetAssetPropertyForView>> GetAll(GetAllAssetPropertiesInput input)
         {
			
			var filteredAssetProperties = _assetPropertyRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.PropertyValueFilter),  e => e.PropertyValue.ToLower() == input.PropertyValueFilter.ToLower().Trim());


			var query = (from o in filteredAssetProperties
                         join o1 in _assetRepository.GetAll() on o.AssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetAssetPropertyForView() {
							AssetProperty = ObjectMapper.Map<AssetPropertyDto>(o),
                         	AssetAssetName = s1 == null ? "" : s1.AssetName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var assetProperties = await query
                .OrderBy(input.Sorting ?? "assetProperty.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetAssetPropertyForView>(
                totalCount,
                assetProperties
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_AssetProperties_Edit)]
		 public async Task<GetAssetPropertyForEditOutput> GetAssetPropertyForEdit(EntityDto input)
         {
            var assetProperty = await _assetPropertyRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetAssetPropertyForEditOutput {AssetProperty = ObjectMapper.Map<CreateOrEditAssetPropertyDto>(assetProperty)};

		    if (output.AssetProperty.AssetId != null)
            {
                var asset = await _assetRepository.FirstOrDefaultAsync((int)output.AssetProperty.AssetId);
                output.AssetAssetName = asset.AssetName.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditAssetPropertyDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetProperties_Create)]
		 private async Task Create(CreateOrEditAssetPropertyDto input)
         {
            var assetProperty = ObjectMapper.Map<AssetProperty>(input);

			
			if (AbpSession.TenantId != null)
			{
				assetProperty.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _assetPropertyRepository.InsertAsync(assetProperty);
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetProperties_Edit)]
		 private async Task Update(CreateOrEditAssetPropertyDto input)
         {
            var assetProperty = await _assetPropertyRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, assetProperty);
         }

		 [AbpAuthorize(AppPermissions.Pages_AssetProperties_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _assetPropertyRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetAssetPropertiesToExcel(GetAllAssetPropertiesForExcelInput input)
         {
			
			var filteredAssetProperties = _assetPropertyRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.PropertyValueFilter),  e => e.PropertyValue.ToLower() == input.PropertyValueFilter.ToLower().Trim());


			var query = (from o in filteredAssetProperties
                         join o1 in _assetRepository.GetAll() on o.AssetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetAssetPropertyForView() { 
							AssetProperty = ObjectMapper.Map<AssetPropertyDto>(o),
                         	AssetAssetName = s1 == null ? "" : s1.AssetName.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.AssetAssetNameFilter), e => e.AssetAssetName.ToLower() == input.AssetAssetNameFilter.ToLower().Trim());


            var assetPropertyListDtos = await query.ToListAsync();

            return _assetPropertiesExcelExporter.ExportToFile(assetPropertyListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_AssetProperties)]
         public async Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _assetRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AssetName.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var assetList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AssetLookupTableDto>();
			foreach(var asset in assetList){
				lookupTableDtoList.Add(new AssetLookupTableDto
				{
					Id = asset.Id,
					DisplayName = asset.AssetName.ToString()
				});
			}

            return new PagedResultDto<AssetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}