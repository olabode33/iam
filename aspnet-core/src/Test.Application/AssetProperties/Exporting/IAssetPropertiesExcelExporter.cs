using System.Collections.Generic;
using Test.AssetProperties.Dtos;
using Test.Dto;

namespace Test.AssetProperties.Exporting
{
    public interface IAssetPropertiesExcelExporter
    {
        FileDto ExportToFile(List<GetAssetPropertyForView> assetProperties);
    }
}