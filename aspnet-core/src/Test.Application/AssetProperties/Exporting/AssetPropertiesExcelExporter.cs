using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.AssetProperties.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.AssetProperties.Exporting
{
    public class AssetPropertiesExcelExporter : EpPlusExcelExporterBase, IAssetPropertiesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public AssetPropertiesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAssetPropertyForView> assetProperties)
        {
            return CreateExcelPackage(
                "AssetProperties.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("AssetProperties"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("PropertyOfAsset"),
                        L("PropertyValue"),
                        (L("Asset")) + L("AssetName")
                        );

                    AddObjects(
                        sheet, 2, assetProperties,
                        _ => _.AssetProperty.PropertyOfAsset,
                        _ => _.AssetProperty.PropertyValue,
                        _ => _.AssetAssetName
                        );

					

                });
        }
    }
}
