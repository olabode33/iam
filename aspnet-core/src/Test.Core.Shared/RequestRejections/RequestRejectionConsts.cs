namespace Test.RequestRejections
{
    public class RequestRejectionConsts
    {

		public const int MinRejectionMessageLength = 1;
		public const int MaxRejectionMessageLength = 8000;
						
    }
}