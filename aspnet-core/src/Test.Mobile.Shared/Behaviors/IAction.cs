﻿using Xamarin.Forms.Internals;

namespace Test.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}