using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.UserSupervisors.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.UserSupervisors
{
    public interface IUserSupervisorsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetUserSupervisorForView>> GetAll(GetAllUserSupervisorsInput input);

		Task<GetUserSupervisorForEditOutput> GetUserSupervisorForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditUserSupervisorDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetUserSupervisorsToExcel(GetAllUserSupervisorsForExcelInput input);

		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}