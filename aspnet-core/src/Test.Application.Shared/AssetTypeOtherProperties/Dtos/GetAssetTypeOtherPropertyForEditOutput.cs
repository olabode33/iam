using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.AssetTypeOtherProperties.Dtos
{
    public class GetAssetTypeOtherPropertyForEditOutput
    {
		public CreateOrEditAssetTypeOtherPropertyDto AssetTypeOtherProperty { get; set; }

		public string AssetTypeAssetTypeName { get; set;}


    }
}