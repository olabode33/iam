
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.AssetTypeOtherProperties.Dtos
{
    public class CreateOrEditAssetTypeOtherPropertyDto : EntityDto<int?>
    {
		public string PropertyName { get; set; }		
		public int? AssetTypeId { get; set; }
    }
}