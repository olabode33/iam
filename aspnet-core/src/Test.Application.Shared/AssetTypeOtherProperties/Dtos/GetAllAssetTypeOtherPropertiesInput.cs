using Abp.Application.Services.Dto;
using System;

namespace Test.AssetTypeOtherProperties.Dtos
{
    public class GetAllAssetTypeOtherPropertiesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string PropertyNameFilter { get; set; }


		 public string AssetTypeAssetTypeNameFilter { get; set; }

        public int AssetTypeId { get; set; }

		 
    }
}