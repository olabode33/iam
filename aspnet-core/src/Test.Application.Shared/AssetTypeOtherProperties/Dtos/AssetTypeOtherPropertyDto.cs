
using System;
using Abp.Application.Services.Dto;

namespace Test.AssetTypeOtherProperties.Dtos
{
    public class AssetTypeOtherPropertyDto : EntityDto
    {
		public string PropertyName { get; set; }
		public int? AssetTypeId { get; set; }		 
    }
}