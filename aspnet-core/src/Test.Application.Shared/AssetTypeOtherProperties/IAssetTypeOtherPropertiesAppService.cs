using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.AssetTypeOtherProperties.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.AssetTypeOtherProperties
{
    public interface IAssetTypeOtherPropertiesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetAssetTypeOtherPropertyForView>> GetAll(GetAllAssetTypeOtherPropertiesInput input);

		Task<GetAssetTypeOtherPropertyForEditOutput> GetAssetTypeOtherPropertyForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditAssetTypeOtherPropertyDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input);
		
    }
}