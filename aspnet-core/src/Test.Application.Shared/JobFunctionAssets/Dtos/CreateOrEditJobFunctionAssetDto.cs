
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.Assets.Dtos;
using Test.Branches.Dtos;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssets.Dtos
{
    public class CreateOrEditJobFunctionAssetDto : EntityDto<int?>
    {
        public AssetLookupTableDto[] AssetId { get; set; }		 
		public int JobFunctionId { get; set; }
		public BranchLookupTableDto[] BranchId { get; set; }	
        public string JBAssetBranches { get; set; }
    }
}