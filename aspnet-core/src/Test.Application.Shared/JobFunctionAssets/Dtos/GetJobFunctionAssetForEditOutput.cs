using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.Assets.Dtos;
using Test.Branches.Dtos;
using Test.JobFunctions.Dtos;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssets.Dtos
{
    public class GetJobFunctionAssetForEditOutput
    {
		public CreateOrEditJobFunctionAssetDto JobFunctionAsset { get; set; }

        public AssetDto[] AssetId { get; set; }
        public JobFunctionDto JobFunctionId { get; set; }
        public BranchDto[] BranchId { get; set; }


    }
}