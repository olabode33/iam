using System.Collections.Generic;
using Test.JobFunctionAssetBranches.Dtos;

namespace Test.JobFunctionAssets.Dtos
{
    public class GetJobFunctionAssetForView
    {
		public JobFunctionAssetDto JobFunctionAsset { get; set; }

        public JobFunctionAssetBranchDto JBAssetBranches { get; set; }

		public string AssetAssetName { get; set;}

		public string JobFunctionJobName { get; set;}

		public string BranchBranchName { get; set;}

        public string SelectedBranchType { get; set; }

    }


    public class GetJobFunctionAssetForViewEvery
    {
        public List<JobFunctionAssetAssetsList> JBAsset { get; set; }
        public List<JobFunctionAssetBranchesList> JBAssetBranches { get; set; }
        public string JobFunction { get; set; }
        public int JobFunctionID { get; set; }
        public string SelectedBranchType { get; set; }
        public int ID { get; set; }
        public int JBBranchesNum { get; set; }
        public int JBAssetsNum { get; set; }
    }

    public class JobFunctionAssetBranchesList
    {
        public int BranchID { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
    }

    public class JobFunctionAssetAssetsList
    {
        public int AssetID { get; set; }
        public string AssetName { get; set; }
        public string AssetDescription { get; set; }
    }



    public class GetJobFunctionAssetForViewEveryView
    {
        public JobFunctionAssetAssetsList[] JBAsset { get; set; }
        public JobFunctionAssetBranchesList[] JBAssetBranches { get; set; }
        public string JobFunction { get; set; }
        public int JobFunctionID { get; set; }
        public string SelectedBranchType { get; set; }
        public int ID { get; set; }
        public int JBBranchesNum { get; set; }
        public int JBAssetsNum { get; set; }
    }


}