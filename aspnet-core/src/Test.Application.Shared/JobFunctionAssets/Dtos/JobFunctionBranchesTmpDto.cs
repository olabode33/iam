﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.JobFunctionAssets.Dtos
{
    public class JobFunctionBranchesTmpDto
    {
        public int BranchId { get; set; }

        public string BranchName { get; set; }
    }
}
