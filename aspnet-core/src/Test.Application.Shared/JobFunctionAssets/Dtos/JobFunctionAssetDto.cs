
using System;
using Abp.Application.Services.Dto;

namespace Test.JobFunctionAssets.Dtos
{
    public class JobFunctionAssetDto : EntityDto
    {
        public int? AssetId { get; set; }
        public int? JobFunctionId { get; set; }
        public int? BranchId { get; set; }		 
    }
}