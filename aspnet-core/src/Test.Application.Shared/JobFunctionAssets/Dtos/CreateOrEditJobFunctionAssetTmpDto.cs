﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssets.Dtos
{
    public class CreateOrEditJobFunctionAssetTmpDto : EntityDto<int?>
    {

        //public JobFunctionAssetBranche Branch { get; set; }
        public int? AssetId { get; set; }
        public int? JobFunctionId { get; set; }

        //public BranchLookupTableDto[] SelectedBranches { get; set; }
    }
}
