using Abp.Application.Services.Dto;
using System;

namespace Test.JobFunctionAssets.Dtos
{
    public class GetAllJobFunctionAssetsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string AssetAssetNameFilter { get; set; }

		 		 public string JobFunctionJobNameFilter { get; set; }

		 		 public string BranchBranchNameFilter { get; set; }

		 
    }
}