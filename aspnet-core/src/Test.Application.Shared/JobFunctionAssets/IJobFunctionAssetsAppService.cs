using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.JobFunctionAssets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssets
{
    public interface IJobFunctionAssetsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobFunctionAssetForView>> GetAll(GetAllJobFunctionAssetsInput input);

		Task<GetJobFunctionAssetForEditOutput> GetJobFunctionAssetForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobFunctionAssetDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobFunctionAssetsToExcel(GetAllJobFunctionAssetsForExcelInput input);
        		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<JobFunctionLookupTableDto>> GetAllJobFunctionForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input);
		
    }
}