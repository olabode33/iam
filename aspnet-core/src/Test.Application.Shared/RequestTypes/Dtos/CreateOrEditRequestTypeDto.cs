
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.RequestTypes.Dtos
{
    public class CreateOrEditRequestTypeDto : EntityDto<int?>
    {

		public string RequestCode { get; set; }
		public string RequestDescription { get; set; }
		public string RequestOtherDetails { get; set; }
		public int ApprovalNo { get; set; }
		public bool RequestExpiry { get; set; }
		public int? AssetTypeId { get; set; }
        public int? ReverseRequestType { get; set; }
        public AssetTypeLookupTableDto[] AssetTypeID { get; set; }
    }
}