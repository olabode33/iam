
using System;
using Abp.Application.Services.Dto;

namespace Test.RequestTypes.Dtos
{
    public class RequestTypeDto : EntityDto
    {
		public string RequestCode { get; set; }

		public string RequestDescription { get; set; }

		public string RequestOtherDetails { get; set; }

		public int ApprovalNo { get; set; }

		public bool RequestExpiry { get; set; }

        public int ReverseRequestType { get; set; }

         public int? AssetTypeId { get; set; }

		 
    }
}