using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.RequestTypes.Dtos
{
    public class GetRequestTypeForEditOutput
    {
		public CreateOrEditRequestTypeDto RequestType { get; set; }
		public string AssetTypeAssetTypeName { get; set; }
        public string ReverseRequestTypeName { get; set; }
        public AssetTypeLookupTableDto[] AssetTypeID { get; set; }
    }
}