using Abp.Application.Services.Dto;
using System;

namespace Test.RequestTypes.Dtos
{
    public class GetAllRequestTypesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string RequestCodeFilter { get; set; }

		public string RequestDescriptionFilter { get; set; }

		public string RequestOtherDetailsFilter { get; set; }

		public int? MaxApprovalNoFilter { get; set; }
		public int? MinApprovalNoFilter { get; set; }


		 public string AssetTypeAssetTypeNameFilter { get; set; }

		 
    }
}