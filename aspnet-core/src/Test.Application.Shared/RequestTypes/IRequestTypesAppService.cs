using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestTypes.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.RequestTypes
{
    public interface IRequestTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestTypeForView>> GetAll(GetAllRequestTypesInput input);
		Task<GetRequestTypeForEditOutput> GetRequestTypeForEdit(EntityDto input);
		Task CreateOrEdit(CreateOrEditRequestTypeDto input);
		Task Delete(EntityDto input);
		Task<FileDto> GetRequestTypesToExcel(GetAllRequestTypesForExcelInput input);
		Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input);		
    }
}