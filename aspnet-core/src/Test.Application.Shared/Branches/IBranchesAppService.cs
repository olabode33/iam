using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Branches.Dtos;
using Test.Dto;

namespace Test.Branches
{
    public interface IBranchesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetBranchForView>> GetAll(GetAllBranchesInput input);

		Task<GetBranchForEditOutput> GetBranchForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditBranchDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetBranchesToExcel(GetAllBranchesForExcelInput input);
        Task<GetBranchForView> GetBranch(EntityDto input);


    }
}