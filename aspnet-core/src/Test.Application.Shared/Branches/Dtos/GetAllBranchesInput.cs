using Abp.Application.Services.Dto;
using System;

namespace Test.Branches.Dtos
{
    public class GetAllBranchesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string BranchNameFilter { get; set; }

		public string BranchCodeFilter { get; set; }



    }
}