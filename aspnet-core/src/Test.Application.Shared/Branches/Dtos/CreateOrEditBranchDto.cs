
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Branches.Dtos
{
    public class CreateOrEditBranchDto : EntityDto<int?>
    {

		public string BranchName { get; set; }
		
		
		public string BranchCode { get; set; }
		
		

    }
}