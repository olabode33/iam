
using System;
using Abp.Application.Services.Dto;

namespace Test.Branches.Dtos
{
    public class BranchDto : FullAuditedEntityDto
    {
		public string BranchName { get; set; }

		public string BranchCode { get; set; }



    }
}