using Abp.Application.Services.Dto;

namespace Test.Branches.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}