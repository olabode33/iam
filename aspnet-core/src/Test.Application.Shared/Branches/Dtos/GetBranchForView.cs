namespace Test.Branches.Dtos
{
    public class GetBranchForView
    {
		public BranchDto Branch { get; set; }

        public string Creator { get; set; }

        public string Modifier { get; set; }
    }
}