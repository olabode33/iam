using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.JobFunctionsAssetAssets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionsAssetAssets
{
    public interface IJobFunctionsAssetAssetsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobFunctionsAssetAssetForView>> GetAll(GetAllJobFunctionsAssetAssetsInput input);

		Task<GetJobFunctionsAssetAssetForEditOutput> GetJobFunctionsAssetAssetForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobFunctionsAssetAssetDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<JobFunctionLookupTableDto>> GetAllJobFunctionForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);

        

    }
}