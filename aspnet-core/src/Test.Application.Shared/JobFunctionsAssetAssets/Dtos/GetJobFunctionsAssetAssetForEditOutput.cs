namespace Test.JobFunctionsAssetAssets.Dtos
{
    public class GetJobFunctionsAssetAssetForEditOutput
    {
		public CreateOrEditJobFunctionsAssetAssetDto JobFunctionsAssetAsset { get; set; }

		public string JobFunctionJobName { get; set;}

		public string AssetAssetName { get; set;}
    }
}