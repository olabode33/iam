namespace Test.JobFunctionsAssetAssets.Dtos
{
    public class GetJobFunctionsAssetAssetForView
    {
		public JobFunctionsAssetAssetDto JobFunctionsAssetAsset { get; set; }

		public string JobFunctionJobName { get; set;}

		public string AssetAssetName { get; set;}


    }
}