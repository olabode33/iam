using Abp.Application.Services.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionsAssetAssets.Dtos
{
    public class CreateOrEditJobFunctionsAssetAssetDto : EntityDto<int?>
    {

		 public int? JobFunctionId { get; set; }
		 public int? AssetId { get; set; }
		 public string selectedBranches { get; set; }
         public GetJobFunctionForView JobFunction { get; set; }
    }
}