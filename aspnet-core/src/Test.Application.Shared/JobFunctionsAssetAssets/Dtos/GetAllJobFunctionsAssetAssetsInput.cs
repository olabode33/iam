using Abp.Application.Services.Dto;
using System;

namespace Test.JobFunctionsAssetAssets.Dtos
{
    public class GetAllJobFunctionsAssetAssetsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string JobFunctionJobNameFilter { get; set; }

		 		 public string AssetAssetNameFilter { get; set; }

		 
    }
}