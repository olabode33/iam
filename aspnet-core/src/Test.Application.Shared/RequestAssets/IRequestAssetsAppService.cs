using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestAssets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;
using Test.Assets.Dtos;

namespace Test.RequestAssets
{
    public interface IRequestAssetsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestAssetForView>> GetAll(GetAllRequestAssetsInput input);
		Task<GetRequestAssetForEditOutput> GetRequestAssetForEdit(EntityDto input);
		Task CreateOrEdit(CreateOrEditRequestAssetDto input);
		Task Delete(EntityDto input);
		Task<FileDto> GetRequestAssetsToExcel(GetAllRequestAssetsForExcelInput input);		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
    }
}