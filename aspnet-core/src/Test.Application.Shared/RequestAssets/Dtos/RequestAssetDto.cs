
using System;
using Abp.Application.Services.Dto;

namespace Test.RequestAssets.Dtos
{
    public class RequestAssetDto : EntityDto
    {
		public string AssetRequest { get; set; }
		public int? AssetId { get; set; }
		public int? RequestId { get; set; }		 
    }
}