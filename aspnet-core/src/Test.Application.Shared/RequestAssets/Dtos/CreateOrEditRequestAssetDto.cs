
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestAssets.Dtos
{
    public class CreateOrEditRequestAssetDto : EntityDto<int?>
    {
		public string AssetRequest { get; set; }
		public int? AssetId { get; set; }		 
		public int? RequestId { get; set; }
    }
}