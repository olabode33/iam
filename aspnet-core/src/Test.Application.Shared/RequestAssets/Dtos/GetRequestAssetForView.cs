namespace Test.RequestAssets.Dtos
{
    public class GetRequestAssetForView
    {
		public RequestAssetDto RequestAsset { get; set; }

		public string AssetAssetName { get; set;}

		public string RequestTenantId { get; set;}


    }
}