using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestAssets.Dtos
{
    public class GetRequestAssetForEditOutput
    {
		public CreateOrEditRequestAssetDto RequestAsset { get; set; }

		public string AssetAssetName { get; set;}

		public string RequestTenantId { get; set;}


    }
}