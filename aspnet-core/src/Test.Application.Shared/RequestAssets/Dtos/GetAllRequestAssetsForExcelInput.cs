using Abp.Application.Services.Dto;
using System;

namespace Test.RequestAssets.Dtos
{
    public class GetAllRequestAssetsForExcelInput
    {
		public string Filter { get; set; }

		public string AssetRequestFilter { get; set; }


		 public string AssetAssetNameFilter { get; set; }

		 		 public string RequestTenantIdFilter { get; set; }

		 
    }
}