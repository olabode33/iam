using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.ConflictAssets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.ConflictAssets
{
    public interface IConflictAssetsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetConflictAssetForView>> GetAll(GetAllConflictAssetsInput input);

		Task<GetConflictAssetForEditOutput> GetConflictAssetForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditConflictAssetDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<ConflictLookupTableDto>> GetAllConflictForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);
		
    }
}