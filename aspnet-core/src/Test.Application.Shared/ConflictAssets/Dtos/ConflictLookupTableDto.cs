using Abp.Application.Services.Dto;

namespace Test.ConflictAssets.Dtos
{
    public class ConflictLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}