
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.ConflictAssets.Dtos
{
    public class CreateOrEditConflictAssetDto : EntityDto<int?>
    {

		 public int? ConflictId { get; set; }
		 
		 		 public int? AssetId { get; set; }
		 
		 
    }
}