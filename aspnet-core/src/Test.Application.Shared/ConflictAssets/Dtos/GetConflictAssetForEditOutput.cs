using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.ConflictAssets.Dtos
{
    public class GetConflictAssetForEditOutput
    {
		public CreateOrEditConflictAssetDto ConflictAsset { get; set; }

		public string ConflictConflictName { get; set;}

		public string AssetAssetName { get; set;}


    }
}