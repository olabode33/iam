using Abp.Application.Services.Dto;
using System;

namespace Test.ConflictAssets.Dtos
{
    public class GetAllConflictAssetsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string ConflictConflictNameFilter { get; set; }

		 		 public string AssetAssetNameFilter { get; set; }

		 
    }
}