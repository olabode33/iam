using Abp.Application.Services.Dto;

namespace Test.JobFunctions.Dtos
{
    public class DepartmentLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}