using Test.IDMShared.Dtos;

namespace Test.JobFunctions.Dtos
{
    public class GetJobFunctionForEditOutput
    {
		public CreateOrEditJobFunctionDto JobFunction { get; set; }

		public string DepartmentDeptName { get; set;}


    }
}