using Abp.Application.Services.Dto;
using System;

namespace Test.JobFunctions.Dtos
{
    public class GetAllJobFunctionsForExcelInput
    {
		public string Filter { get; set; }

		public string JobCodeFilter { get; set; }

		public string JobNameFilter { get; set; }


		 public string DepartmentDeptNameFilter { get; set; }

		 
    }
}