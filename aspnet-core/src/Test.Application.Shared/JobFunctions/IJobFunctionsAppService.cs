using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.JobFunctions.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctions
{
    public interface IJobFunctionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobFunctionForView>> GetAll(GetAllJobFunctionsInput input);

		Task<GetJobFunctionForEditOutput> GetJobFunctionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobFunctionDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobFunctionsToExcel(GetAllJobFunctionsForExcelInput input);

		
		Task<PagedResultDto<DepartmentLookupTableDto>> GetAllDepartmentForLookupTable(GetAllForLookupTableInput input);

        Task<GetJobFunctionForView> GetJobFuntionAndRelatedAssets(int jobFunctionId);

    }
}