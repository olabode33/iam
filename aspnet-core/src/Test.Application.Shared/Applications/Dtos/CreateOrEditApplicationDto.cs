using Test;

using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Applications.Dtos
{
    public class CreateOrEditApplicationDto : EntityDto<int?>
    {

		public string ApplicationName { get; set; }
		
		
		public string ApplicationDesc { get; set; }
		
		
		public string URL { get; set; }
		
		
		public string ApplicationUsername { get; set; }
		
		
		public string ApplicationPassword { get; set; }
		
		
		public ApplicationImportance Importance { get; set; }
		
		

    }
}