using Abp.Application.Services.Dto;
using System;

namespace Test.Applications.Dtos
{
    public class GetAllApplicationsForExcelInput
    {
		public string Filter { get; set; }

		public string ApplicationNameFilter { get; set; }

		public string ApplicationDescFilter { get; set; }

		public string URLFilter { get; set; }

		public int ImportanceFilter { get; set; }



    }
}