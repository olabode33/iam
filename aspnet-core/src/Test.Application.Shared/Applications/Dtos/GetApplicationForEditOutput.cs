using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Applications.Dtos
{
    public class GetApplicationForEditOutput
    {
		public CreateOrEditApplicationDto Application { get; set; }


    }
}