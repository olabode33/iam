using Test;

using System;
using Abp.Application.Services.Dto;

namespace Test.Applications.Dtos
{
    public class ApplicationDto : EntityDto
    {
		public string ApplicationName { get; set; }

		public string ApplicationDesc { get; set; }

		public string URL { get; set; }

		public ApplicationImportance Importance { get; set; }



    }
}