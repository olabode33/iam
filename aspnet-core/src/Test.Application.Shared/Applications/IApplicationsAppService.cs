using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Applications.Dtos;
using Test.Dto;

namespace Test.Applications
{
    public interface IApplicationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetApplicationForView>> GetAll(GetAllApplicationsInput input);

		Task<GetApplicationForEditOutput> GetApplicationForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditApplicationDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetApplicationsToExcel(GetAllApplicationsForExcelInput input);

		
    }
}