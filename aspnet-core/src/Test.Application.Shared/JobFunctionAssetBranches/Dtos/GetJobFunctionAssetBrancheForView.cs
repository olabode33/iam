namespace Test.JobFunctionAssetBranches.Dtos
{
    public class GetJobFunctionAssetBrancheForView
    {
		public JobFunctionAssetBrancheDto JobFunctionAssetBranche { get; set; }

		public string JobFunctionAssetJobFunctionId { get; set;}

		public string BranchBranchName { get; set;}


    }
}