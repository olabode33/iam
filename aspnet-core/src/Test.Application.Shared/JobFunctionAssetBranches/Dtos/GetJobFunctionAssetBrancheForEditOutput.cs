using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.JobFunctionAssetBranches.Dtos
{
    public class GetJobFunctionAssetBrancheForEditOutput
    {
		public CreateOrEditJobFunctionAssetBrancheDto JobFunctionAssetBranche { get; set; }

		public string JobFunctionAssetJobFunctionId { get; set;}

		public string BranchBranchName { get; set;}


    }
}