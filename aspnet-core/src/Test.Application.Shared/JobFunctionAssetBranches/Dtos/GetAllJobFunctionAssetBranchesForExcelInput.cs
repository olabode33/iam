using Abp.Application.Services.Dto;
using System;

namespace Test.JobFunctionAssetBranches.Dtos
{
    public class GetAllJobFunctionAssetBranchesForExcelInput
    {
		public string Filter { get; set; }


		 public string JobFunctionAssetJobFunctionIdFilter { get; set; }

		 		 public string BranchBranchNameFilter { get; set; }

		 
    }
}