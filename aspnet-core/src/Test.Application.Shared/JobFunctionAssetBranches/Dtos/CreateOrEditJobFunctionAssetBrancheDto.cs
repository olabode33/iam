
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.JobFunctionAssetBranches.Dtos
{
    public class CreateOrEditJobFunctionAssetBrancheDto : EntityDto<int?>
    {

		 public int JobFunctionAssetId { get; set; }
		 
		 		 public int? BranchId { get; set; }
		 
		 
    }
}