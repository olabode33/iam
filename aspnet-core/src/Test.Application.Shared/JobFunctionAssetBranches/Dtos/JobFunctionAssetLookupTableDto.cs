using Abp.Application.Services.Dto;

namespace Test.JobFunctionAssetBranches.Dtos
{
    public class JobFunctionAssetLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}