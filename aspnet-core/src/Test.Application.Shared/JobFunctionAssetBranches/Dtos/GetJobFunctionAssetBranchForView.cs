namespace Test.JobFunctionAssetBranches.Dtos
{
    public class GetJobFunctionAssetBranchForView
    {
		public JobFunctionAssetBranchDto JobFunctionAssetBranch { get; set; }

		public string JobFunctionAssetJobFunctionId { get; set;}

		public string BranchBranchName { get; set;}


    }
}