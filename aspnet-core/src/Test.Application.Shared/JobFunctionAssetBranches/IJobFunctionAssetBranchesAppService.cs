using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssetBranches
{
    public interface IJobFunctionAssetBranchesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobFunctionAssetBrancheForView>> GetAll(GetAllJobFunctionAssetBranchesInput input);

		Task<GetJobFunctionAssetBrancheForEditOutput> GetJobFunctionAssetBrancheForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobFunctionAssetBrancheDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobFunctionAssetBranchesToExcel(GetAllJobFunctionAssetBranchesForExcelInput input);

		
		Task<PagedResultDto<JobFunctionAssetLookupTableDto>> GetAllJobFunctionAssetForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input);
		
    }
}