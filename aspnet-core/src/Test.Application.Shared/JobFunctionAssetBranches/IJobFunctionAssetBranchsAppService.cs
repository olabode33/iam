using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.JobFunctionAssetBranches.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.JobFunctionAssetBranches
{
    public interface IJobFunctionAssetBranchsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobFunctionAssetBranchForView>> GetAll(GetAllJobFunctionAssetBranchsInput input);

		Task<GetJobFunctionAssetBranchForEditOutput> GetJobFunctionAssetBranchForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobFunctionAssetBranchDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobFunctionAssetBranchsToExcel(GetAllJobFunctionAssetBranchsForExcelInput input);

		
		Task<PagedResultDto<JobFunctionAssetLookupTableDto>> GetAllJobFunctionAssetForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input);
		
    }
}