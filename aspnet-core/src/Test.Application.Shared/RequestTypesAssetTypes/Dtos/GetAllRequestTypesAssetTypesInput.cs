using Abp.Application.Services.Dto;
using System;

namespace Test.RequestTypesAssetTypes.Dtos
{
    public class GetAllRequestTypesAssetTypesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string AssetTypeAssetTypeNameFilter { get; set; }

		 		 public string RequestTypeTenantIdFilter { get; set; }

		 
    }
}