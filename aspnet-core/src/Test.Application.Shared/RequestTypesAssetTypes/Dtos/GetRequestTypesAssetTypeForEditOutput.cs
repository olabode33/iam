using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestTypesAssetTypes.Dtos
{
    public class GetRequestTypesAssetTypeForEditOutput
    {
		public CreateOrEditRequestTypesAssetTypeDto RequestTypesAssetType { get; set; }

		public string AssetTypeAssetTypeName { get; set;}

		public string RequestTypeTenantId { get; set;}


    }
}