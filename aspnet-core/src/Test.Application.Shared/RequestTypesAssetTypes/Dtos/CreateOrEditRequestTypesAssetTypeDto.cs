
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestTypesAssetTypes.Dtos
{
    public class CreateOrEditRequestTypesAssetTypeDto : EntityDto<int?>
    {

		 public int? AssetTypeId { get; set; }
		 
		 		 public int? RequestTypeId { get; set; }
		 
		 
    }
}