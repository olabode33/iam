namespace Test.RequestTypesAssetTypes.Dtos
{
    public class GetRequestTypesAssetTypeForView
    {
		public RequestTypesAssetTypeDto RequestTypesAssetType { get; set; }

		public string AssetTypeAssetTypeName { get; set;}

		public string RequestTypeTenantId { get; set;}


    }
}