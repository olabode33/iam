using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestTypesAssetTypes.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.RequestTypesAssetTypes
{
    public interface IRequestTypesAssetTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestTypesAssetTypeForView>> GetAll(GetAllRequestTypesAssetTypesInput input);

		Task<GetRequestTypesAssetTypeForEditOutput> GetRequestTypesAssetTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRequestTypesAssetTypeDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input);
		
    }
}