
using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.ApproveRequests.Dtos;
using Test.AssetTypes.Dtos;
using Test.Requests.Dtos;
using Test.RequestTypes.Dtos;

namespace Test.Assets.Dtos
{
    public class RequestAssetsDto : EntityDto
    {
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public string AssetDescription { get; set; }
        public bool AssetConflictChecker { get; set; }
        public List<string> AssetConflictNames { get; set; }
    }
}