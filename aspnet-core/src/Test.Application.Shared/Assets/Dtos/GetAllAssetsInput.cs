using Abp.Application.Services.Dto;
using System;

namespace Test.Assets.Dtos
{
    public class GetAllAssetsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string AssetNameFilter { get; set; }

		public string AssetCodeFilter { get; set; }

		public string AssetDescriptionFilter { get; set; }


		 public string ApplicationApplicationNameFilter { get; set; }

		 		 public string BranchBranchNameFilter { get; set; }

		 		 public string AssetTypeAssetTypeNameFilter { get; set; }

		 
    }
}