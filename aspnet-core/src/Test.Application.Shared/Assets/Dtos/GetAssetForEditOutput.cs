using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.Assets.Dtos
{
    public class GetAssetForEditOutput
    {
		public CreateOrEditAssetDto Asset { get; set; }

		public string ApplicationApplicationName { get; set;}

		public string BranchBranchName { get; set;}

		public string AssetTypeAssetTypeName { get; set;}

        public AssetProps[] AssetProps { get; set; }

    }
}