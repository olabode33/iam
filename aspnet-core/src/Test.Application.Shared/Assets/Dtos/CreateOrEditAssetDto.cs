
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.Assets.Dtos
{
    public class CreateOrEditAssetDto : EntityDto<int?>
    {
        public string AssetName { get; set; }				
        public string AssetCode { get; set; }				
        public string AssetDescription { get; set; }				
        public int? ApplicationId { get; set; }		 
        public int? BranchId { get; set; }		 
        public int? AssetTypeId { get; set; }
        public AssetProps[] AssetProps { get; set; }
    }
}