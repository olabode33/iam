using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.Assets
{
    public interface IAssetsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetAssetForView>> GetAll(GetAllAssetsInput input);

		Task<GetAssetForEditOutput> GetAssetForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditAssetDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetAssetsToExcel(GetAllAssetsForExcelInput input);

		
		Task<PagedResultDto<ApplicationLookupTableDto>> GetAllApplicationForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<BranchLookupTableDto>> GetAllBranchForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<AssetTypeLookupTableDto>> GetAllAssetTypeForLookupTable(GetAllForLookupTableInput input);
		
    }
}