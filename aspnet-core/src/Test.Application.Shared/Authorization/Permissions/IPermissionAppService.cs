﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Authorization.Permissions.Dto;

namespace Test.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
