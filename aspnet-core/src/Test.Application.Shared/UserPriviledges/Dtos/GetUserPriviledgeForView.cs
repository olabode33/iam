namespace Test.UserPriviledges.Dtos
{
    public class GetUserPriviledgeForView
    {
		public UserPriviledgeDto UserPriviledge { get; set; }
		public string RequestRequestCode { get; set;}
		public string UserName { get; set;}
		public string AssetAssetName { get; set;}
    }
}