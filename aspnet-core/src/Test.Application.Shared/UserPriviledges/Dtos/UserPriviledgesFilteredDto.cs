﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.UserPriviledges.Dtos
{
    public class UserPriviledgesFilteredDto
    {
        public int Id { get; set; }
        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public int RequestId { get; set; }
        public string RequestDesc { get; set; }
        public int RequestTypeId { get; set; }
        public string RequestCode { get; set; }
        public string ApplicationUsername { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
