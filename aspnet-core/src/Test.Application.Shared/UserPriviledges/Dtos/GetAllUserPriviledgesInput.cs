using Abp.Application.Services.Dto;
using System;

namespace Test.UserPriviledges.Dtos
{
    public class GetAllUserPriviledgesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public int StatusFilter { get; set; }

		public DateTime? MaxStartDateFilter { get; set; }
		public DateTime? MinStartDateFilter { get; set; }

		public string ApplicationUsernameFilter { get; set; }

		public DateTime? MaxEndDateFilter { get; set; }
		public DateTime? MinEndDateFilter { get; set; }


		 public string RequestRequestCodeFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 		 public string AssetAssetNameFilter { get; set; }

		 
    }
}