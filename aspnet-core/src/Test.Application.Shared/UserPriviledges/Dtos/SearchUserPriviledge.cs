using System;
using System.Collections.Generic;

namespace Test.UserPriviledges.Dtos
{
    public class SearchUserPriviledgeForView
    {
        public int[] UserID { get; set; }
        public int[] RequestType { get; set; }
        public int[] AssetID { get; set; }
        public DateTime MinStartDate { get; set; }
        public DateTime MaxStartDate { get; set; }
    }
}