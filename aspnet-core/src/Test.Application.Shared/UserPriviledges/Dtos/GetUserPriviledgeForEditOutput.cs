using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.UserPriviledges.Dtos
{
    public class GetUserPriviledgeForEditOutput
    {
		public CreateOrEditUserPriviledgeDto UserPriviledge { get; set; }

		public string RequestRequestCode { get; set;}

		public string UserName { get; set;}

		public string AssetAssetName { get; set;}


    }
}