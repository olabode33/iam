
using System;
using Abp.Application.Services.Dto;

namespace Test.UserPriviledges.Dtos
{
    public class UserPriviledgeDto : EntityDto
    {
        public bool Status { get; set; }
        public DateTime StartDate { get; set; }
        public string ApplicationUsername { get; set; }
        public DateTime EndDate { get; set; }
        public int? RequestId { get; set; }
        public long? UserId { get; set; }
        public int? AssetId { get; set; }
    }
}