using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.UserPriviledges.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.UserPriviledges
{
    public interface IUserPriviledgesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetUserPriviledgeForView>> GetAll(GetAllUserPriviledgesInput input);

		Task<GetUserPriviledgeForEditOutput> GetUserPriviledgeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditUserPriviledgeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetUserPriviledgesToExcel(GetAllUserPriviledgesForExcelInput input);

		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);
		
    }
}