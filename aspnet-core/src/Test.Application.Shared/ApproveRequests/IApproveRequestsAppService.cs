using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.ApproveRequests.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.ApproveRequests
{
    public interface IApproveRequestsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetApproveRequestForView>> GetAll(GetAllApproveRequestsInput input);

		Task<GetApproveRequestForEditOutput> GetApproveRequestForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditApproveRequestDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetApproveRequestsToExcel(GetAllApproveRequestsForExcelInput input);

		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}