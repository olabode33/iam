using System;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;
using Test.Requests.Dtos;

namespace Test.ApproveRequests.Dtos
{
    public class RejectRequestDto : EntityDto
    {
        public string RejectionMessage { get; set; }
        public int? RequestId { get; set; }
        public long? UserId { get; set; }
        public int[] AssetId { get; set; }
    }
}