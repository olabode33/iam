
using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;
using Test.IDMShared.Dtos;
using Test.Requests.Dtos;

namespace Test.ApproveRequests.Dtos
{
    public class ApproveRequestDto : EntityDto
    {
        public int RequestId { get; set; }
        public int[] Assets { get; set; }
    }
}