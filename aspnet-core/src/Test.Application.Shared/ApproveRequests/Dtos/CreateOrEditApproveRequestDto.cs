
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.ApproveRequests.Dtos
{
    public class CreateOrEditApproveRequestDto : EntityDto<int?>
    {

		public bool ApprovalStatus { get; set; }
		
		
		public bool PriorApprovalStatus { get; set; }
		
		
		 public int? RequestId { get; set; }
		 
		 		 public long ApproverID { get; set; }
		 
		 		 public long PriorApprover { get; set; }
		 
		 
    }
}