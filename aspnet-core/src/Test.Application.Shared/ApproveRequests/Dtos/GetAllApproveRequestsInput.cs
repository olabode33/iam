using Abp.Application.Services.Dto;
using System;

namespace Test.ApproveRequests.Dtos
{
    public class GetAllApproveRequestsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string RequestTenantIdFilter { get; set; }
        public string UserNameFilter { get; set; }
        public string UserName2Filter { get; set; }

		 
    }
}