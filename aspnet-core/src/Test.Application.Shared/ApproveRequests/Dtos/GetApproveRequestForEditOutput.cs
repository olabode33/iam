using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.ApproveRequests.Dtos
{
    public class GetApproveRequestForEditOutput
    {
		public CreateOrEditApproveRequestDto ApproveRequest { get; set; }

		public string RequestTenantId { get; set;}

		public string UserName { get; set;}

		public string UserName2 { get; set;}


    }
}