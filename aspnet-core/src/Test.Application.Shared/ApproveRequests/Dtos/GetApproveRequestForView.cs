using System;
using Test.IDMShared.Dtos;
using Test.Requests.Dtos;
using Test.RequestTypes.Dtos;

namespace Test.ApproveRequests.Dtos
{
    public class GetApproveRequestForView
    {
		public string RequestTenantId { get; set; }
		public string ApproverIDName { get; set; }
		public string PriorApprover { get; set; }
        public string RequestTypeName { get; set; }
        public ApproveRequestDto ApproveRequest { get; set; }
        public RequestDto Req { get; set; }
        public RequestTypeDto reqType { get; set; }
        public string RequesterName { get; set; }
        public string RequestStatus { get; set; }
    }
}