
using System;
using Abp.Application.Services.Dto;

namespace Test.RequestRejections.Dtos
{
    public class RequestRejectionDto : EntityDto
    {
        public string RejectionMessage { get; set; }
        public int? RequestId { get; set; }
        public long? UserId { get; set; }
    }
}