
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestRejections.Dtos
{
    public class CreateOrEditRequestRejectionDto : EntityDto<int?>
    {
		[StringLength(RequestRejectionConsts.MaxRejectionMessageLength, MinimumLength = RequestRejectionConsts.MinRejectionMessageLength)]
		public string RejectionMessage { get; set; }
		public int? RequestId { get; set; }
		public long? UserId { get; set; }
    }
}