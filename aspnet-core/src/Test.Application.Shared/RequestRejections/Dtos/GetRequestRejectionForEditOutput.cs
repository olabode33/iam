using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestRejections.Dtos
{
    public class GetRequestRejectionForEditOutput
    {
		public CreateOrEditRequestRejectionDto RequestRejection { get; set; }

		public string RequestTenantId { get; set;}

		public string UserName { get; set;}


    }
}