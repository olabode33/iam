using Abp.Application.Services.Dto;
using System;

namespace Test.RequestRejections.Dtos
{
    public class GetAllRequestRejectionsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string RequestTenantIdFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 
    }
}