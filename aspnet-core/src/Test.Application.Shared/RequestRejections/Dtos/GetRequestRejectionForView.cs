namespace Test.RequestRejections.Dtos
{
    public class GetRequestRejectionForView
    {
		public RequestRejectionDto RequestRejection { get; set; }

		public string RequestTenantId { get; set;}

		public string UserName { get; set;}


    }
}