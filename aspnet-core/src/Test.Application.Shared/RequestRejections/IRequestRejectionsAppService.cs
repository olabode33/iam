using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestRejections.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.RequestRejections
{
    public interface IRequestRejectionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestRejectionForView>> GetAll(GetAllRequestRejectionsInput input);

		Task<GetRequestRejectionForEditOutput> GetRequestRejectionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRequestRejectionDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}