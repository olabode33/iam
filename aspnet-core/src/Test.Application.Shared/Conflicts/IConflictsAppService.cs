using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Conflicts.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.Conflicts
{
    public interface IConflictsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetConflictForView>> GetAll(GetAllConflictsInput input);
		Task<GetConflictForEditOutput> GetConflictForEdit(EntityDto input);
		Task CreateOrEdit(CreateOrEditConflictDto input);
		Task Delete(EntityDto input);
		Task<FileDto> GetConflictsToExcel(GetAllConflictsForExcelInput input);
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);		
    }
}