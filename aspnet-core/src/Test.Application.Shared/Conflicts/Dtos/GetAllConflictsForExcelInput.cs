using Abp.Application.Services.Dto;
using System;

namespace Test.Conflicts.Dtos
{
    public class GetAllConflictsForExcelInput
    {
		public string Filter { get; set; }

		public string ConflictNameFilter { get; set; }

		public string ConflictDescriptionFilter { get; set; }
        		 
    }
}