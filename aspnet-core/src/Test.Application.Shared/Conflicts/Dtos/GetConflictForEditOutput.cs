using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.Conflicts.Dtos
{
    public class GetConflictForEditOutput
    {
		public CreateOrEditConflictDto Conflict { get; set; }
		public string AssetAssetName { get; set;}
        public AssetLookupTableDto[] Assets { get; set; }
    }
}