
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.Conflicts.Dtos
{
    public class CreateOrEditConflictDto : EntityDto<int?>
    {
		public string ConflictName { get; set; }
		public string ConflictDescription { get; set; }		
		public string ConflictRisk { get; set; }
		public int[] Assets { get; set; }  		 
    }
}