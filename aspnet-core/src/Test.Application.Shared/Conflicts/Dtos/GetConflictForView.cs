using Test.IDMShared.Dtos;

namespace Test.Conflicts.Dtos
{
    public class GetConflictForView
    {
		public ConflictDto Conflict { get; set; }
        public AssetLookupTableDto[] Assets { get; set; }
    }
}