
using System;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;
using Test.ConflictAssets;
using Test.ConflictAssets.Dtos;

namespace Test.Conflicts.Dtos
{
    public class ConflictDto : EntityDto
    {
		public string ConflictName { get; set; }
		public string ConflictDescription { get; set; }
        public string ConflictRisk { get; set; }
        public int[] ConflictsAssetsID { get; set; }

    }
}