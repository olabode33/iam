
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestEvents.Dtos
{
    public class CreateOrEditRequestEventDto : EntityDto<int?>
    {

		public string Event { get; set; }
		
		
		public string EventDescription { get; set; }
		
		
		public DateTime EventDate { get; set; }
		
		
		 public int? RequestTypeId { get; set; }
		 
		 		 public int? RequestId { get; set; }
		 
		 		 public long? UserId { get; set; }
		 
		 
    }
}