using Abp.Application.Services.Dto;
using System;

namespace Test.RequestEvents.Dtos
{
    public class GetAllRequestEventsForExcelInput
    {
		public string Filter { get; set; }

		public string EventFilter { get; set; }

		public string EventDescriptionFilter { get; set; }

		public DateTime? MaxEventDateFilter { get; set; }
		public DateTime? MinEventDateFilter { get; set; }


		 public string RequestTypeRequestCodeFilter { get; set; }

		 		 public string RequestTenantIdFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 
    }
}