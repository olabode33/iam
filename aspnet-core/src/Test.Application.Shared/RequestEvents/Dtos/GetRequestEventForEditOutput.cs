using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestEvents.Dtos
{
    public class GetRequestEventForEditOutput
    {
		public CreateOrEditRequestEventDto RequestEvent { get; set; }

		public string RequestTypeRequestCode { get; set;}

		public string RequestTenantId { get; set;}

		public string UserName { get; set;}


    }
}