
using System;
using Abp.Application.Services.Dto;

namespace Test.RequestEvents.Dtos
{
    public class RequestEventDto : EntityDto
    {
		public string Event { get; set; }

		public string EventDescription { get; set; }

		public DateTime EventDate { get; set; }


		 public int? RequestTypeId { get; set; }

		 		 public int? RequestId { get; set; }

		 		 public long? UserId { get; set; }

		 
    }
}