namespace Test.RequestEvents.Dtos
{
    public class GetRequestEventForView
    {
		public RequestEventDto RequestEvent { get; set; }

		public string RequestTypeRequestCode { get; set;}

		public string RequestTenantId { get; set;}

		public string UserName { get; set;}


    }
}