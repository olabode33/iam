using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestEvents.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.RequestEvents
{
    public interface IRequestEventsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestEventForView>> GetAll(GetAllRequestEventsInput input);

		Task<GetRequestEventForEditOutput> GetRequestEventForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRequestEventDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRequestEventsToExcel(GetAllRequestEventsForExcelInput input);

		
		Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}