using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class AssetLookupTableDto
    {
		public int Id { get; set; }
		public string DisplayName { get; set; }
        public string AssetDescription { get; set; }
    }
}