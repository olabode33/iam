
using System;
using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class JobFunctionDto : EntityDto
    {
		public string JobCode { get; set; }

		public string JobName { get; set; }


		 public int? DepartmentId { get; set; }

		 
    }
}