
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.IDMShared.Dtos
{
    public class CreateOrEditJobFunctionDto : EntityDto<int?>
    {

		public string JobCode { get; set; }
		
		
		public string JobName { get; set; }
		
		
		 public int? DepartmentId { get; set; }
		 
		 
    }
}