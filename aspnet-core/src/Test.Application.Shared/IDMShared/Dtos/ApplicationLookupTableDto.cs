using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class ApplicationLookupTableDto
    {
		public int Id { get; set; }
		public string DisplayName { get; set; }
        public string ApplicationDesc { get; set; }
    }
}