﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.IDMShared.Dtos
{
    public class AssetProps
    {
        public int id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }
}
