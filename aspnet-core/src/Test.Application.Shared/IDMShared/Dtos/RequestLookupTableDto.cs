using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class RequestLookupTableDto
    {
		public int Id { get; set; }
		public string DisplayName { get; set; }
    }
}