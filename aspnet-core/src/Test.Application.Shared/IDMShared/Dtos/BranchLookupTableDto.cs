using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class BranchLookupTableDto
    {
		public int Id { get; set; }
        public string BranchCode { get; set; }
        public string DisplayName { get; set; }
    }
}