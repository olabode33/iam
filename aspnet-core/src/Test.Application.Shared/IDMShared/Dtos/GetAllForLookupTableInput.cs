using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}