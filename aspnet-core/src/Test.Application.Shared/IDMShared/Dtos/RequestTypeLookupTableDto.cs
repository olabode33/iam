using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class RequestTypeLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}