
using System;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;

namespace Test.IDMShared.Dtos
{
    public class RequestDto : EntityDto
    {
		public string RequestDescription { get; set; }
		public DateTime ExpiryDate { get; set; }
        public DateTime InitiationTime { get; set; }
        public int? RequestTypeId { get; set; }
		public long? UserId { get; set; }
		public int? AssetId { get; set; }
        public string RequestStatus { get; set; }
        public long? RequestOwnerId { get; set; }
        public string RequestOwnerName { get; set; }
    }
}