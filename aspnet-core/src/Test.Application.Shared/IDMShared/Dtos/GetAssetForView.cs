using Test.IDMShared.Dtos;

namespace Test.IDMShared.Dtos
{
    public class GetAssetForView
    {
		public AssetDto Asset { get; set; }

		public string ApplicationApplicationName { get; set;}

		public string BranchBranchName { get; set;}

		public string AssetTypeAssetTypeName { get; set;}


    }
}