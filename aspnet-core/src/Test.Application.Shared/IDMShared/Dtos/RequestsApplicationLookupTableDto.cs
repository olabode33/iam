using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class RequestsApplicationLookupTableDto
    {
		public int Id { get; set; }
		public string DisplayName { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationDesc { get; set; }
    }
}