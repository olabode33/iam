
using System;
using Abp.Application.Services.Dto;
using Test.ApproveRequests.Dtos;
using Test.AssetTypes.Dtos;
using Test.Requests.Dtos;
using Test.RequestTypes.Dtos;

namespace Test.IDMShared.Dtos
{
    public class AssetDto : EntityDto
    {
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public string AssetDescription { get; set; }
        public int? ApplicationId { get; set; }
        public int? BranchId { get; set; }
        public int? AssetTypeId { get; set; }
        public string RejectedBy { get; set; }
        public string RequestAssetStatus { get; set; }
        public string RequestOwner { get; set; }
        public string RequestAssetTreated { get; set; }
        public DateTime RequestAssetTreatedDate { get; set; }
        public string RequestAssetTreatedBy { get; set; }
        public RequestTypeDto RType { get; set; }
        public AssetProps[] AssetProp { get; set; }
        public RequestDto Requests { get; set; }
        public AssetTypeDto AssetType { get; set; }
        public string ApplicationName { get; set; }
    }
}