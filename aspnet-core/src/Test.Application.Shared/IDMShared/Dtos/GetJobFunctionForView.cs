using System.Collections.Generic;
using Test.Assets.Dtos;
using Test.JobFunctions.Dtos;

namespace Test.IDMShared.Dtos
{
    public class GetJobFunctionForView
    {
		public JobFunctionDto JobFunction { get; set; }
		public string DepartmentDeptName { get; set;}
        public string JFAssetCount { get; set; }
        public string JFBranchCount { get; set; }

        public List<GetAssetForView> AssetForView { get; set; }
        public AssetDto[] AssetDto { get; set; }
    }
}