using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class UserLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}