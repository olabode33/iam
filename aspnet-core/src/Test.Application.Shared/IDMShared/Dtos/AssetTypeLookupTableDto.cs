using Abp.Application.Services.Dto;

namespace Test.IDMShared.Dtos
{
    public class AssetTypeLookupTableDto
    {
		public int Id { get; set; }
		public string DisplayName { get; set; }
        public string AssetTypeDescription { get; set; }
    }
}