using Abp.Application.Services.Dto;
using System;

namespace Test.AssetTypes.Dtos
{
    public class GetAllAssetTypesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string AssetTypeNameFilter { get; set; }

		public string AssetTypeDescFilter { get; set; }

		public int PermissionFilter { get; set; }


    }
}