
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.AssetTypeOtherProperties.Dtos;
using System.Collections.Generic;

namespace Test.AssetTypes.Dtos
{
    public class CreateOrEditAssetTypeDto : EntityDto<int?>
    {
		public string AssetTypeName { get; set; }
		public string AssetTypeDesc { get; set; }
		public string AssetTypeOtherDetails { get; set; }
		public bool Permission { get; set; }
		public bool AttachBranch { get; set; }
		public bool AttachApplication { get; set; }
        public string[] AssetProperties { get; set; }
    }
}