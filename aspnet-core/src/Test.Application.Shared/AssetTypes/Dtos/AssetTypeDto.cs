
using System;
using Abp.Application.Services.Dto;

namespace Test.AssetTypes.Dtos
{
    public class AssetTypeDto : EntityDto
    {
		public string AssetTypeName { get; set; }

		public string AssetTypeDesc { get; set; }

		public bool Permission { get; set; }

		public bool AttachBranch { get; set; }

		public bool AttachApplication { get; set; }



    }
}