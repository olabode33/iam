using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.AssetTypeOtherProperties.Dtos;
using System.Collections.Generic;

namespace Test.AssetTypes.Dtos
{
    public class GetAssetTypeForEditOutput
    {
		public CreateOrEditAssetTypeDto AssetType { get; set; }
        public List<AssetTypeOtherPropertyDto> AssetProperties { get; set; }
    }
}