using Abp.Application.Services.Dto;
using System;

namespace Test.AssetTypes.Dtos
{
    public class GetAllAssetTypesForExcelInput
    {
		public string Filter { get; set; }

		public string AssetTypeNameFilter { get; set; }

		public string AssetTypeDescFilter { get; set; }

		public int PermissionFilter { get; set; }

		public int AttachBranchFilter { get; set; }

		public int AttachApplicationFilter { get; set; }



    }
}