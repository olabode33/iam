using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.AssetTypes.Dtos;
using Test.Dto;

namespace Test.AssetTypes
{
    public interface IAssetTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetAssetTypeForView>> GetAll(GetAllAssetTypesInput input);

		Task<GetAssetTypeForEditOutput> GetAssetTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditAssetTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetAssetTypesToExcel(GetAllAssetTypesForExcelInput input);

		
    }
}