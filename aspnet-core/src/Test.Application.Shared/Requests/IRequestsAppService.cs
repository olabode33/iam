using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Requests.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.Requests
{
    public interface IRequestsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestForView>> GetAll(GetAllRequestsInput input);

        Task<PagedResultDto<GetRequestForView>> GetAllForDashboard();
		Task<GetRequestForEditOutput> GetRequestForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRequestDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRequestsToExcel(GetAllRequestsForExcelInput input);

		
		Task<PagedResultDto<RequestTypeLookupTableDto>> GetAllRequestTypeForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);
    }
}