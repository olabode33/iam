
using System;
using Abp.Application.Services.Dto;

namespace Test.Requests.Dtos
{
    public class RequestApprovalsDto : EntityDto
    {
		public int RequestID { get; set; }
        public int RequestedBy { get; set; }
        public int PriorApprover { get; set; }
        public int Approver { get; set; }
    }
}