using Test.IDMShared.Dtos;

namespace Test.Requests.Dtos
{
    public class GetRequestForEditOutput
    {
		public CreateOrEditRequestDto Request { get; set; }
		public string RequestTypeRequestCode { get; set;}
		public string UserName { get; set;}
		public string AssetAssetName { get; set;}
        public AssetDto[] Assets { get; set; }
    }
}