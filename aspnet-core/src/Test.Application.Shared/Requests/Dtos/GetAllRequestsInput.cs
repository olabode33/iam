using Abp.Application.Services.Dto;
using System;

namespace Test.Requests.Dtos
{
    public class GetAllRequestsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
		public string RequestDescriptionFilter { get; set; }
		public DateTime? MaxExpiryDateFilter { get; set; }
		public DateTime? MinExpiryDateFilter { get; set; }
        public string RequestTypeRequestCodeFilter { get; set; }
		public string UserNameFilter { get; set; }
        public string AssetAssetNameFilter { get; set; }
    }
}