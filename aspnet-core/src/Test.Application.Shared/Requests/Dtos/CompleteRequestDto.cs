
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;
using Test.Assets.Dtos;
using Test.RequestTypes.Dtos;
using Test.AssetTypes.Dtos;
using Test.JobFunctions.Dtos;
using Test.Departments.Dtos;

namespace Test.Requests.Dtos
{
    public class CompleteRequestDto : EntityDto<int?>
    {
        public AssetDto[] assets { get; set; }
        public string RequestOwner { get; set; }
        public DateTime RequestDate { get; set; }
        public RequestTypeDto[] RType { get; set; }
        public RequestDto[] Requests { get; set; }
        public AssetTypeDto[] AssetType { get; set; }
        public JobFunctionDto[] Job { get; set; }
        public DepartmentDto[] Dept { get; set; }
    }
}