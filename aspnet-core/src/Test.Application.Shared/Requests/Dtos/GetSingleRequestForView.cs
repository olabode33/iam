using Test.IDMShared.Dtos;
using Test.RequestAssets.Dtos;

namespace Test.Requests.Dtos
{
    public class GetSingleRequestForView
    {
		public RequestDto Request { get; set; }
		public string RequestTypeRequestCode { get; set;}
		public string UserName { get; set;}
		public string AssetAssetName { get; set;}
        public RequestAssetDto[] AssetID { get; set; }
    }
}