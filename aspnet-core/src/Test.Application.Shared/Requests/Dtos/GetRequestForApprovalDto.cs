using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.Assets.Dtos;
using Test.Conflicts.Dtos;
using System.Collections.Generic;
using Test.RequestTypes.Dtos;
using Test.IDMShared.Dtos;

namespace Test.Requests.Dtos
{
    public class GetRequestForApprovalDto
    {
		public CreateOrEditRequestDto Request { get; set; }

		public string RequestTypeRequestCode { get; set;}

		public string UserName { get; set;}

		public string AssetAssetName { get; set;}

        public RequestDto RequestDto { get; set; }

        public RequestTypeDto RequestType { get; set; }

        public List<RequestAssetsDto> Assets { get; set; }

        public List<ConflictDto> Conflicts { get; set; }

    }
}