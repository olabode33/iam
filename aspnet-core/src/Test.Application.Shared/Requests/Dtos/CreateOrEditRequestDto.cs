
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.IDMShared.Dtos;

namespace Test.Requests.Dtos
{
    public class CreateOrEditRequestDto : EntityDto<int?>
    {

        public string RequestDescription { get; set; }
        public DateTime InitiationTime { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int? RequestTypeId { get; set; }
        public long? UserId { get; set; }
        public int[] AssetId { get; set; }
        public long? RequestOwnerId { get; set; }
        public string RequestStatus { get; set; }

        public CreateOrEditRequestDto() {
            RequestStatus = "P";
        }

    }
}