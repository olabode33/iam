using Test.Assets.Dtos;
using Test.TreatRequests.Dtos;
using Test.IDMShared.Dtos;

namespace Test.Requests.Dtos
{
    public class GetRequestForViewTreated
    {
        public TreatRequestAssetDto[] Assets { get; set; }
        public RequestDto[] Request { get; set; }
    }
}