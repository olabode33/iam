using Abp.Application.Services.Dto;
using System;

namespace Test.RequestApprovals.Dtos
{
    public class GetAllRequestApprovalsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public int StatusFilter { get; set; }


		 public string UserNameFilter { get; set; }

		 		 public string RequestTenantIdFilter { get; set; }

		 
    }
}