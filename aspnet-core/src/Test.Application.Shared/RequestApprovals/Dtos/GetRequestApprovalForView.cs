namespace Test.RequestApprovals.Dtos
{
    public class GetRequestApprovalForView
    {
		public RequestApprovalDto RequestApproval { get; set; }

		public string UserName { get; set;}

		public string RequestTenantId { get; set;}


    }
}