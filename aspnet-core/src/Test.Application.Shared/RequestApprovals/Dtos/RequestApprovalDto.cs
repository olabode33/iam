
using System;
using Abp.Application.Services.Dto;

namespace Test.RequestApprovals.Dtos
{
    public class RequestApprovalDto : EntityDto
    {
		public bool Status { get; set; }


		 public long? UserId { get; set; }

		 		 public int? RequestId { get; set; }

		 
    }
}