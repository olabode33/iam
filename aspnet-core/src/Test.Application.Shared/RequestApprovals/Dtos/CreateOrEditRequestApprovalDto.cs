
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RequestApprovals.Dtos
{
    public class CreateOrEditRequestApprovalDto : EntityDto<int?>
    {

		public bool Status { get; set; }
		
		
		 public long? UserId { get; set; }
		 
		 		 public int? RequestId { get; set; }
		 
		 
    }
}