using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RequestApprovals.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.RequestApprovals
{
    public interface IRequestApprovalsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestApprovalForView>> GetAll(GetAllRequestApprovalsInput input);

		Task<GetRequestApprovalForEditOutput> GetRequestApprovalForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRequestApprovalDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRequestApprovalsToExcel(GetAllRequestApprovalsForExcelInput input);

		
		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<RequestLookupTableDto>> GetAllRequestForLookupTable(GetAllForLookupTableInput input);
		
    }
}