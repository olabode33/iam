using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.ViewApprovedRequests.Dtos;
using Test.Dto;
using Test.Requests.Dtos;

namespace Test.ViewApprovedRequests
{
    public interface IViewApprovedRequestsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRequestForView>> GetAll(GetAllViewApprovedRequestsInput input);

		Task<GetRequestForEditOutput> GetViewApprovedRequestForEdit(EntityDto input);
        
    }
}