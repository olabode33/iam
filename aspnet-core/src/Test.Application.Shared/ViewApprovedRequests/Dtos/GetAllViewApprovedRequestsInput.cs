using Abp.Application.Services.Dto;
using System;

namespace Test.ViewApprovedRequests.Dtos
{
    public class GetAllViewApprovedRequestsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }



    }
}