
using System;
using Abp.Application.Services.Dto;

namespace Test.AssetProperties.Dtos
{
    public class AssetPropertyDto : EntityDto
    {
		public string PropertyOfAsset { get; set; }

		public string PropertyValue { get; set; }


		 public int? AssetId { get; set; }

		 
    }
}