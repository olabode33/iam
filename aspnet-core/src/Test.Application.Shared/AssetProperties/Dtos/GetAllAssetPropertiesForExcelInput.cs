using Abp.Application.Services.Dto;
using System;

namespace Test.AssetProperties.Dtos
{
    public class GetAllAssetPropertiesForExcelInput
    {
		public string Filter { get; set; }

		public string PropertyOfAssetFilter { get; set; }

		public string PropertyValueFilter { get; set; }


		 public string AssetAssetNameFilter { get; set; }

		 
    }
}