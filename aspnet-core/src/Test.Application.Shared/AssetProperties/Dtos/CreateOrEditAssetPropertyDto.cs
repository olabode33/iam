
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.AssetProperties.Dtos
{
    public class CreateOrEditAssetPropertyDto : EntityDto<int?>
    {

		public string PropertyOfAsset { get; set; }
		
		
		public string PropertyValue { get; set; }
		
		
		 public int? AssetId { get; set; }
		 
		 
    }
}