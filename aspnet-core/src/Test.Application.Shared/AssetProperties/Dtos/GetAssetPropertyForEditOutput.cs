using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.AssetProperties.Dtos
{
    public class GetAssetPropertyForEditOutput
    {
		public CreateOrEditAssetPropertyDto AssetProperty { get; set; }

		public string AssetAssetName { get; set;}


    }
}