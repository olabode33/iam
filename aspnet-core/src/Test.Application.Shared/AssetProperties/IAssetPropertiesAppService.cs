using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.AssetProperties.Dtos;
using Test.Dto;
using Test.IDMShared.Dtos;

namespace Test.AssetProperties
{
    public interface IAssetPropertiesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetAssetPropertyForView>> GetAll(GetAllAssetPropertiesInput input);

		Task<GetAssetPropertyForEditOutput> GetAssetPropertyForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditAssetPropertyDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetAssetPropertiesToExcel(GetAllAssetPropertiesForExcelInput input);

		
		Task<PagedResultDto<AssetLookupTableDto>> GetAllAssetForLookupTable(GetAllForLookupTableInput input);
		
    }
}