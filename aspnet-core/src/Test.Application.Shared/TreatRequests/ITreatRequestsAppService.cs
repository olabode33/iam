using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.TreatRequests.Dtos;
using Test.Dto;
using Test.Requests.Dtos;
using Test.Assets.Dtos;

namespace Test.TreatRequests
{
    public interface ITreatRequestsAppService : IApplicationService 
    {
        Task<PagedResultDto<TreatRequestAssetDto>> GetAll();
		Task<GetTreatRequestForEditOutput> GetTreatRequestForEdit(EntityDto input);
		//Task CreateOrEdit(CreateOrEditTreatRequestDto input);
		//Task Delete(EntityDto input);
		Task<FileDto> GetTreatRequestsToExcel(GetAllTreatRequestsForExcelInput input);		
    }
}