using Abp.Application.Services.Dto;
using System;

namespace Test.TreatRequests.Dtos
{
    public class GetAllTreatRequestsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string RequestNameFilter { get; set; }



    }
}