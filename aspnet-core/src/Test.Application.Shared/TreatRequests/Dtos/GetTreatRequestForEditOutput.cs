using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.TreatRequests.Dtos
{
    public class GetTreatRequestForEditOutput
    {
		public CreateOrEditTreatRequestDto TreatRequest { get; set; }


    }
}