
using System;
using Abp.Application.Services.Dto;
using Test.Assets.Dtos;
using Test.AssetTypes.Dtos;
using Test.Departments.Dtos;
using Test.IDMShared.Dtos;
using Test.JobFunctions.Dtos;
using Test.Requests.Dtos;
using Test.RequestTypes.Dtos;

namespace Test.TreatRequests.Dtos
{
    public class TreatRequestAssetDto : EntityDto
    {
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public string AssetDescription { get; set; }
        public int? ApplicationId { get; set; }
        public int? BranchId { get; set; }
        public int? AssetTypeId { get; set; }
        public string RejectedBy { get; set; }
        public string RequestAssetStatus { get; set; }
        public string RequestOwner { get; set; }
        public string RequestAssetTreated { get; set; }
        public DateTime RequestAssetTreatedDate { get; set; }
        public string RequestAssetTreatedBy { get; set; }
        public RequestTypeDto RType { get; set; }
        public RequestDto Requests { get; set; }
        public AssetTypeDto AssetType { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public JobFunctionDto Job { get; set; }
        public DepartmentDto Dept { get; set; }
    }
}