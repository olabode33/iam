using Abp.Application.Services.Dto;
using System;

namespace Test.Departments.Dtos
{
    public class GetAllDepartmentsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string DeptCodeFilter { get; set; }

		public string DeptNameFilter { get; set; }



    }
}