
using System;
using Abp.Application.Services.Dto;

namespace Test.Departments.Dtos
{
    public class DepartmentDto : EntityDto
    {
		public string DeptCode { get; set; }
		public string DeptName { get; set; }
    }
}