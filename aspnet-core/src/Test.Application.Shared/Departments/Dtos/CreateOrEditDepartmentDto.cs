
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Departments.Dtos
{
    public class CreateOrEditDepartmentDto : EntityDto<int?>
    {
		public string DeptCode { get; set; }
		public string DeptName { get; set; }
    }
}