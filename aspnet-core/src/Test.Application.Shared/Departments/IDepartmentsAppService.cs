using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Departments.Dtos;
using Test.Dto;

namespace Test.Departments
{
    public interface IDepartmentsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetDepartmentForView>> GetAll(GetAllDepartmentsInput input);
		Task<GetDepartmentForEditOutput> GetDepartmentForEdit(EntityDto input);
		Task CreateOrEdit(CreateOrEditDepartmentDto input);
		Task Delete(EntityDto input);
		Task<FileDto> GetDepartmentsToExcel(GetAllDepartmentsForExcelInput input);		
    }
}