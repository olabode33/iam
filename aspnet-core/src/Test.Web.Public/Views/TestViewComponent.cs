﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Test.Web.Public.Views
{
    public abstract class TestViewComponent : AbpViewComponent
    {
        protected TestViewComponent()
        {
            LocalizationSourceName = TestConsts.LocalizationSourceName;
        }
    }
}