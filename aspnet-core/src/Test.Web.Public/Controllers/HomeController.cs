using Microsoft.AspNetCore.Mvc;
using Test.Web.Controllers;

namespace Test.Web.Public.Controllers
{
    public class HomeController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}